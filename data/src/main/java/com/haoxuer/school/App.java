package com.haoxuer.school;

import com.haoxuer.imake.ChainMake;
import com.haoxuer.imake.template.hibernate.TemplateHibernateDir;
import com.haoxuer.imake.templates.adminlte.TemplateAdminLTE;
import com.haoxuer.school.data.entity.Config;
import com.haoxuer.school.data.entity.CourseCatalog;
import com.haoxuer.school.actions.admin.Controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {

    System.out.println(Controllers.class.getResource("/").getPath());
    ChainMake make = new ChainMake(TemplateAdminLTE.class, TemplateHibernateDir.class);
    make.setAction(Controllers.class.getPackage().getName());
    File view = new File("E:\\codes\\maven\\school\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
    make.setView(view);

    List<Class<?>> cs = new ArrayList<Class<?>>();
    cs.add(CourseCatalog.class);

    make.setDao(false);
    make.setService(false);
    make.setView(true);
    make.setAction(false);
    make.setRest(false);
    make.setApi(false);
    make.makes(cs);
  }


}
