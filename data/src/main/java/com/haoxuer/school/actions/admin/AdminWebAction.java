package com.haoxuer.school.actions.admin;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.school.data.entity.Config;
import com.haoxuer.school.data.service.ConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "admin")
public class AdminWebAction {


  @RequestMapping(value = "index", method = RequestMethod.GET)
  public String index(HttpServletRequest request,
                      HttpServletResponse response, Model model) {
    return "admin/home";
  }

  @Autowired
  private ConfigService configService;

  @RequiresPermissions("system_config")
  @RequestMapping(value = "config", method = RequestMethod.GET)
  public String config(Model model) {
    model.addAttribute("model",configService.config());
    return "admin/system/config";
  }


  @ResponseBody
  @RequiresPermissions("system_config")
  @RequestMapping(value = "update_config", method = RequestMethod.POST)
  public ResponseObject updateConfig(@RequestBody Config config) {
    ResponseObject result=new ResponseObject();
    configService.update(config);
    return result;
  }
}
