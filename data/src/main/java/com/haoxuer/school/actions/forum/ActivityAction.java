package com.haoxuer.school.actions.forum;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.discover.data.core.Pagination;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.haoxuer.school.data.entity.City;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Town;
import com.haoxuer.school.data.entity.forum.Activity;
import com.haoxuer.school.data.entity.forum.ActivityComment;
import com.haoxuer.school.data.entity.forum.ActivityMember;
import com.haoxuer.school.data.entity.forum.ActivitySchedule;
import com.haoxuer.school.data.service.ActivityService;
import com.haoxuer.school.data.service.CityService;
import com.haoxuer.school.data.service.TownService;
import com.haoxuer.school.web.utils.FrontUtils;

@Controller
public class ActivityAction extends BaseAction {

  private static final Log logger = LogFactory.getLog(ActivityAction.class);

  private static final Calendar calendar = Calendar.getInstance();

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

  @Autowired
  private ActivityService activityService;
  @Autowired
  private CityService cityService;
  @Autowired
  private TownService townService;

  @RequestMapping(value = "forum/initialActivity", method = RequestMethod.GET)
  public String initialActivity(
      @RequestParam(value = "columnid", required = true, defaultValue = "2") int columnid,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    logger.info("开始进入活动初始页面...");
    //get all city values, default it is Shannxi province
    List<City> cityList = cityService.findByProvince(11);
    //get all activity type
    Map<String, Object> params = new HashMap<String, Object>();
    List<String> orders = new ArrayList<String>();
    orders.add("id");
    Pagination page = activityService.getPageOfActivities(params, orders, curpage, pagesize);
    model.addAttribute("page", page);
    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("cityList", cityList);
    return "forum/activity";
  }

  /**
   * 进入创建活动准备
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/addPreActivity", method = RequestMethod.GET)
  public String addPreActivity(HttpServletRequest request, HttpServletResponse response, Model model) {
    logger.info("进入添加活动...");
    //get all activity type
    List<City> cityList = cityService.findByProvince(11);
    model.addAttribute("cityList", cityList);
    return "forum/add_pre_activity";
  }

  /**
   * 创建活动（保存信息）
   *
   * @param request
   * @param response
   * @param model
   * @return
   */

  @RequestMapping(value = "forum/addActivityInfo", method = RequestMethod.POST)
  public String addActivity(HttpServletRequest request, HttpServletResponse response, Model model) {
    logger.info("添加活动...");
    String category = request.getParameter("category");
    String subject = request.getParameter("subject");
    String scheduleType = request.getParameter("scheduleType");
    String activityStartDate = request.getParameter("activityStartDate");
    String activityEndDate = request.getParameter("activityEndDate");
    String activityStartTime = request.getParameter("activityStartTime");
    String activityEndTime = request.getParameter("activityEndTime");
    String addressCity = request.getParameter("addressCity");
    String addressTown = request.getParameter("addressTown");
    String addressDetail = request.getParameter("addressDetail");
    String content = request.getParameter("content");
    int free = request.getParameter("free") == null ? 0 : Integer.parseInt(request.getParameter("free"));
    String cost = request.getParameter("cost");
    model.addAttribute("category", category);
    model.addAttribute("subject", subject);
    model.addAttribute("scheduleType", scheduleType);
    model.addAttribute("activityStartDate", activityStartDate);
    model.addAttribute("activityEndDate", activityEndDate);
    model.addAttribute("addressCity", addressCity);
    model.addAttribute("addressTown", addressTown);
    model.addAttribute("addressDetail", addressDetail);
    model.addAttribute("content", content);
    model.addAttribute("free", free);
    model.addAttribute("cost", cost);
//		BigDecimal decimal = new BigDecimal(cost);
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    City city = new City();
    city.setId(Integer.parseInt(addressCity));
    Town town = new Town();
    town.setCity(city);
    town.setId(Integer.parseInt(addressTown));
    Activity activity = new Activity();
    activity.setAddress(addressDetail);
    activity.setContent(content);
    activity.setFree(free);
//		activity.setCost(decimal);
    activity.setScheduleType(Integer.parseInt(scheduleType));

    try {
      if ("0".equals(scheduleType)) {
        activity.setFromDate(new Timestamp(sdf.parse(activityStartDate).getTime()));
        activity.setToDate(new Timestamp(sdf.parse(activityStartDate).getTime()));
      } else {
        activity.setFromDate(new Timestamp(sdf.parse(activityStartDate).getTime()));
        activity.setToDate(new Timestamp(sdf.parse(activityEndDate).getTime()));
        List<ActivitySchedule> scheduleList = getActivityListByDate(activity, activityStartDate, activityEndDate, activityStartTime, activityEndTime);
        activity.setSchedules(scheduleList);
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    activity.setLineofbus("204");
    activity.setMember(member);
    activity.setStatus(0);
    activity.setSubject(subject);
    //暂时设置为默认的图片地址
    activity.setImgPath("http://localhost:8080/mendao/i/laoshi/u603.png");
//		activity.setSummary("活动概要");
    activity.setTown(town);
//		model.addAttribute("activity", activity);
    Activity act = activityService.save(activity);
    //保存活动的日期
    try {
      if ("0".equals(scheduleType)) {
        ActivitySchedule schedule = new ActivitySchedule();
        schedule.setActivity(activity);
        schedule.setStartTime(FrontUtils.parseTimestamp(sdf.format(calendar.getTime()) + " " + activityStartTime + ":00"));
        schedule.setEndTime(FrontUtils.parseTimestamp(sdf.format(calendar.getTime()) + " " + activityEndTime + ":00"));
        activityService.save(schedule);
      } else {
        List<ActivitySchedule> scheduleList = getActivityListByDate(activity, activityStartDate, activityEndDate, activityStartTime, activityEndTime);
        for (int i = 0; i < scheduleList.size(); i++) {
          ActivitySchedule schedule = new ActivitySchedule();
          schedule.setActivity(activity);
          schedule.setStartTime(FrontUtils.parseTimestamp(sdf.format(calendar.getTime()) + " " + activityStartTime + ":00"));
          schedule.setEndTime(FrontUtils.parseTimestamp(sdf.format(calendar.getTime()) + " " + activityEndTime + ":00"));
          System.out.println(FrontUtils.parseTimestamp(sdf.format(calendar.getTime()) + " " + activityEndTime + ":00"));
          activityService.save(schedule);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    model.addAttribute("activity", act);
    return "forum/upload_activity_image";
  }

  /**
   * 上传海报
   *
   * @param request
   * @param response
   * @param model
   * @return
   * @throws IllegalStateException
   */
  @RequestMapping(value = "forum/upload_image", method = RequestMethod.GET)
  public String uploadImage(HttpServletRequest request, HttpServletResponse response, Model model, Activity activity) throws IllegalStateException {
    activityService.update(activity);
    return initialActivity(2, 1, 10, request, response, model);
  }

  /**
   * 活动详情页面展示
   *
   * @param request
   * @param response
   * @param model
   * @return
   * @author guyuwei
   */
  @RequestMapping(value = "forum/activitydetail", method = RequestMethod.GET)
  public String activityDetail(
      @RequestParam(value = "id", required = true) int id,
      HttpServletRequest request, HttpServletResponse response, Model model) {
    Activity activity = activityService.findActivityById(id);
    model.addAttribute("model", activity);
    return "forum/activity/detailinfo";
  }

  /**
   * 活动点赞
   */
  @RequestMapping(value = "forum/upActivityClick", method = RequestMethod.GET)
  public String upActivityClick(
      HttpServletRequest request, HttpServletResponse response, Model model) {
    String id = request.getParameter("id");
    Activity act = activityService.findActivityById(Integer.parseInt(id));
    act.setFollowCount(act.getFollowCount() + 1);
    act = activityService.update(act);
    String msg = "";
    msg = act.getFollowCount() + "";
    model.addAttribute("msg", msg);
    return "common/text";
  }

  /**
   * 删除活动评论
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/deleteActivityComment", method = RequestMethod.GET)
  public String deleteActivityComment(HttpServletRequest request, HttpServletResponse response, Model model) {
    String id = request.getParameter("id");
    String activityId = request.getParameter("activityId");
    activityService.deleteCommentById(Integer.parseInt(id));
    return activityDetail(Integer.parseInt(activityId), request, response, model);
  }

  /**
   * 活动评论点赞
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/upActivityComment", method = RequestMethod.GET)
  public String upActivityComment(HttpServletRequest request, HttpServletResponse response, Model model) {
    String id = request.getParameter("id");
    ActivityComment comment = activityService.findCommentById(Integer.parseInt(id));
    comment.setUps(comment.getUps() + 1);
    activityService.update(comment);
    String msg = comment.getUps().toString();
    model.addAttribute("msg", msg);
    return "common/text";
  }

  /**
   * 获取活动的所有评论
   *
   * @param id
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/activity/comments", method = RequestMethod.GET)
  public String activityComments(
      @RequestParam(value = "id", required = true) int id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("activity.id =", id);

    List<String> orders = new ArrayList<String>();
    orders.add("id");

    Pagination page = activityService.getPageOfComments(params, orders, curpage, pagesize);
    model.addAttribute("page", page);
    model.addAttribute("commlist", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);

    return "forum/activity/comments";
  }

  /**
   * 发表评论
   *
   * @param activity
   * @param request
   * @param response
   * @param model
   * @return
   * @author guyuwei
   */
  @RequestMapping(value = "forum/pubcomment", method = RequestMethod.GET)
  public String pubComment(Activity activity, String content,
                           HttpServletRequest request, HttpServletResponse response, Model model) {
    logger.info("发表评论");
    Activity act = activityService.findActivityById(activity.getId());
    act.setCommentCount(act.getCommentCount() + 1);
    activityService.update(act);
    Timestamp time = new Timestamp(System.currentTimeMillis());
    ActivityComment comment = new ActivityComment();
    comment.setPubdate(time);
    comment.setContent(content);
    comment.setActivity(act);
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    comment.setMember(member);
    activityService.save(comment);
    return activityDetail(activity.getId(), request, response, model);     //转发到小组详情展示
  }

  /**
   * 评论回复
   *
   * @param activity
   * @param comment
   * @param request
   * @param response
   * @param model
   * @return
   * @author guyuwei
   */
  @RequestMapping(value = "forum/replaycomment", method = RequestMethod.GET)
  public String replayCommnet(Activity activity, ActivityComment comment, String content,
                              HttpServletRequest request, HttpServletResponse response, Model model) {
    logger.info("评论回复");
    ActivityComment lordcomment = activityService.findCommentById(comment.getId());
    String replaycontent = content + "//<a class='userlink' href='http://www.91mydoor.com//memberfront/index.htm?id=" + lordcomment.getMember().getId() + "'>@" + lordcomment.getMember().getName() + "</a>" + lordcomment.getContent();
    ActivityComment acomment = new ActivityComment();
    Timestamp time = new Timestamp(System.currentTimeMillis());
    acomment.setPubdate(time);
    acomment.setActivity(activity);
    acomment.setActivityComment(lordcomment);
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    acomment.setMember(member);
    acomment.setContent(replaycontent);
    activityService.save(acomment);
    return null;
  }

  /**
   * 按活动类型查询活动列表
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   * @author guyuwei
   */
  @RequestMapping(value = "forum/activitydetailBycategrory", method = RequestMethod.GET)
  public String findActivityByCategoyId(
                                        @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                                        @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                                        HttpServletRequest request,
                                        HttpServletResponse response, Model model) {
    logger.info("按活动类型查询");
    Map<String, Object> map = new HashMap<String, Object>();
    List<City> cityList = cityService.findByProvince(11);
    List<String> orders = new ArrayList<String>();
    orders.add("id");
    Pagination page = activityService.getPageOfActivities(map, orders, 0, 100);
    model.addAttribute("page", page);
    model.addAttribute("list", page.getList());
    model.addAttribute("cityList", cityList);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    return "forum/activity";
  }

  /**
   * 按活动地点查询活动列表
   *
   * @param town
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   * @author guyuwei
   */
  @RequestMapping(value = "forum/showactivityByaddr", method = RequestMethod.GET)
  public String findActivityByTwon(Town town,
                                   @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                                   @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                                   HttpServletRequest request,
                                   HttpServletResponse response, Model model) {
    logger.info("按活动地区来查询活动列表");
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("town.name = ", town.getName());
    List<City> cityList = cityService.findByProvince(11);
    List<String> orders = new ArrayList<String>();
    orders.add("id");
    Pagination page = activityService.getPageOfActivities(map, orders, 0, 100);
    model.addAttribute("page", page);
    model.addAttribute("list", page.getList());
    model.addAttribute("cityList", cityList);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    return "forum/activity";
  }

  /**
   * 查找今天的活动
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/findtoday", method = RequestMethod.GET)
  public String findActivityToday(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    logger.info("查找今天的活动");
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    Date curDate = new Date(System.currentTimeMillis());//获取当前时间
    String str = formatter.format(curDate);
    Map<String, Object> params = new HashMap<String, Object>();
    List<String> orders = new ArrayList<String>();
    orders.add("id");
    List<City> cityList = cityService.findByProvince(11);
    Pagination page = activityService.getPageOfActivities(params, orders, 0, 1000);
    List<Activity> list = (List<Activity>) page.getList();
    List<Activity> todaylist = new ArrayList<Activity>();
    for (Activity activity : list) {
      if (activity.getFromDate().toString().substring(0, 10).equals(str)) {
        todaylist.add(activity);
      }
    }

    model.addAttribute("page", page);
    model.addAttribute("list", todaylist);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("cityList", cityList);
    return "forum/activity";

  }

  /**
   * 查询明天的活动
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/findnextday", method = RequestMethod.GET)
  public String findActivityNextday(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    logger.info("时间查询活动");
    Calendar c = new GregorianCalendar();
    c.setTime(new Date());
    c.add(Calendar.DAY_OF_MONTH, 1);
    Date date = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    String str = formatter.format(date);
    Map<String, Object> params = new HashMap<String, Object>();
    List<String> orders = new ArrayList<String>();
    orders.add("id");
    List<City> cityList = cityService.findByProvince(11);
    Pagination page = activityService.getPageOfActivities(params, orders, 1, 1000);
    List<Activity> list = (List<Activity>) page.getList();
    List<Activity> todaylist = new ArrayList<Activity>();

    for (Activity activity : list) {
      String dbtime = activity.getFromDate().toString().substring(0, 10);
      if (dbtime.equals(str)) {
        todaylist.add(activity);
      }
    }

    model.addAttribute("page", page);
    model.addAttribute("list", todaylist);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("cityList", cityList);
    return "forum/activity";
  }

  /**
   * 查看最近一周的活动
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/findActivityByWeek", method = RequestMethod.GET)
  public String findActivityNextWeek(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    logger.info("插最近一周的活动");
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    Date curDate = new Date(System.currentTimeMillis());//获取当前时间
    long now = curDate.getTime();
    Calendar c = new GregorianCalendar();
    c.setTime(curDate);
    c.add(Calendar.DATE, 7);
    long week = c.getTime().getTime();
    Map<String, Object> params = new HashMap<String, Object>();
    List<String> orders = new ArrayList<String>();
    orders.add("id");
    List<City> cityList = cityService.findByProvince(11);
    Pagination page = activityService.getPageOfActivities(params, orders, 0, 1000);
    List<Activity> list = (List<Activity>) page.getList();
    List<Activity> todaylist = new ArrayList<Activity>();
    for (Activity activity : list) {
      long dbtime = activity.getFromDate().getTime();
      if (dbtime >= now && dbtime <= week) {
        todaylist.add(activity);
      }
    }

    model.addAttribute("page", page);
    model.addAttribute("list", todaylist);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("cityList", cityList);
    return "forum/activity";
  }

  /**
   * 查找时间列表
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/findactivitybytime", method = RequestMethod.GET)
  public String findActivityWeekend(String activityStartDate,
                                    @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                                    @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                                    HttpServletRequest request,
                                    HttpServletResponse response, Model model) {
    logger.info("查找时间列表");
    Map<String, Object> map = new HashMap<String, Object>();
    try {
      map.put("fromDate =", new Timestamp(sdf.parse(activityStartDate).getTime()));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    List<String> orders = new ArrayList<String>();
    orders.add("id");
    List<City> cityList = cityService.findByProvince(11);
    Pagination page = activityService.getPageOfActivities(map, orders, 0, 6);
    model.addAttribute("page", page);
    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("cityList", cityList);
    return null;
  }

  /**
   * 加入活动
   *
   * @param activity
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/joinActivity", method = RequestMethod.GET)
  public String joinActivity(Activity activity,
                             HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    logger.info("加入活动");
    HttpSession session = request.getSession();
    Member member = (Member) session.getAttribute("member");
    ActivityMember amember = new ActivityMember();
    amember.setMembers(member);
    amember.setActivity(activity);
    amember.setRoleid(0);// .setActivityRole(dictionnaryId);
    activityService.save(amember);
    return null;

  }

  /**
   * 对活动感兴趣
   *
   * @param activity
   * @return
   */
  @RequestMapping(value = "forum/careActivity", method = RequestMethod.GET)
  public String careActivity(Activity activity, HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    Activity nactivity = activityService.findActivityById(activity.getId());
    int followCount = nactivity.getFollowCount() + 1;
    nactivity.setFollowCount(followCount);
    activityService.update(nactivity);
    return null;          //
  }

  /**
   * 查找区县
   *
   * @param city
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/findTown", method = RequestMethod.GET)

  public String findTown(City city, HttpServletRequest request,
                         HttpServletResponse response, Model model) {
    logger.info("查找区县");
    System.out.println("-----------");
    List<Town> townList = townService.findByCity(city.getId());
    Gson gson = new Gson();
    model.addAttribute("msg", gson.toJson(townList));
    return "{'success', 'msg'}";
  }

  private static List<ActivitySchedule> getActivityListByDate(Activity activity, String startDate, String endDate, String startTime, String endTime) {
    List<ActivitySchedule> scheduleList = new ArrayList<ActivitySchedule>();
    try {
      Date sDate = sdf.parse(startDate);
      Date eDate = sdf.parse(endDate);
      long diff = eDate.getTime() - sDate.getTime();
      calendar.setTime(sdf.parse(startDate));
      int days = (int) (diff / (1000 * 3600 * 24)) + 1;
      for (int i = 0; i < days; i++) {
        ActivitySchedule as = new ActivitySchedule();
        as.setStartTime(FrontUtils.parseTimestamp(sdf.format(calendar.getTime()) + " " + startTime + ":00"));
        as.setEndTime(FrontUtils.parseTimestamp(sdf.format(calendar.getTime()) + " " + endTime + ":00"));

        as.setActivity(activity);
        scheduleList.add(as);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
      }
    } catch (ParseException e1) {
      e1.printStackTrace();
    }
    return scheduleList;
  }

}
