package com.haoxuer.school.actions.forum;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import com.haoxuer.discover.web.controller.front.BaseController;
import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.school.data.entity.forum.ForumPost;
import com.haoxuer.school.data.entity.forum.ForumPostComment;
import com.haoxuer.school.data.service.ForumGroupService;
import com.haoxuer.school.data.service.ForumPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Controller
public class ForumAction extends BaseController {

  @Autowired
  ForumPostService forumPostService;

  @Autowired
  ForumGroupService forumGroupService;
  // private int pagesize = 10;

  /**
   * 社区版块页面
   *
   * @param columnid 访问的版块id
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/index", method = RequestMethod.GET)
  public String Index(
      @RequestParam(value = "columnid", required = true, defaultValue = "2") int columnid,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("forumColumn.id =", columnid);
    Pagination page = forumPostService.getForumPostPage(params, null,
        curpage, pagesize);
    model.addAttribute("columnid", columnid);
    model.addAttribute("page", page);
    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);

    // 获取当前版块下的讨论组列表（仅取前7个，现在是在正文右边“推荐的讨论组”）
    //Member member = super.getmember(request);

//		page = forumGroupService.getRecommendPage(
//				null == member ? 0 : member.getId(), 1, 7);
//
//		model.addAttribute("grouplist", page.getList());
    model.addAttribute("showBtn", page.getTotalCount() > page.getPageSize()); // true:显示更多按钮；false:不显示

    return getView("forum/post/list");
  }

  /**
   * 讨论组发帖页面
   *
   * @param postid   帖子id
   * @param groupid  所在小组id
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/post", method = RequestMethod.GET)
  public String post(
      @RequestParam(value = "id", required = true, defaultValue = "0") int postid,
      @RequestParam(value = "groupid", required = true, defaultValue = "1") int groupid,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    ForumPost post;
    if (postid > 0) {
      post = forumPostService.findById(postid);
    } else {
      post = new ForumPost();
      // 设置当前讨论组
      ForumGroup group = forumGroupService.findById(groupid);
      post.setForumGroup(group);
//			Member member = super.getmember(request);
//			if (forumGroupService.isMemberOf(member.getId(), groupid)) {
//				// 设置当前版块
//				post.setForumColumn(group.getForumColumn());
//			} else {
//				model.addAttribute("errormsg", "您不是改组成员，请先加入改组才能发帖！");
//			}
    }
    model.addAttribute("model", post);
    return "forum/post/post_add";
  }

  /**
   * 添加帖子
   *
   * @param post
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/post_add", method = RequestMethod.POST)
  public String addPost(ForumPost post, HttpServletRequest request,
                        HttpServletResponse response, Model model) {

//		Member smember = super.getmember(request);
//		if (smember != null) {
//			post.setMember(smember);
//			post.setVerified(1);
//			// post.setIs_recommend(0);
//			post.setCreateDate(new Timestamp(System.currentTimeMillis()));
//			post.setPubdate(new Timestamp(System.currentTimeMillis()));
//
//			// 获取帖子中的第一个图片作为此贴的标识图
//			post.setImgpath(ContentUtils.getFirstImg(post.getContent(),
//					"http://www.91mydoor.com/"));
//
//			ForumGroup group = forumGroupService.findById(post
//					.getForumGroup().getId());
//
//			post.setForumColumn(group.getForumColumn());
//			model.addAttribute("gid", group.getId());
//
//			forumPostService.save(post);
//			model.addAttribute("pid", post.getId());
//
//			return "forum/post/post_success";
//		} else {
//			return "login";
//		}
    return "login";
  }

  /**
   * 显示帖子详情
   *
   * @param postid   帖子id
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/view_post", method = RequestMethod.GET)
  public String postDetail(
      @RequestParam(value = "id", required = true, defaultValue = "0") int postid,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    boolean show = false;
    ForumPost post = forumPostService.findById(postid);
    if (post != null) {
//			Member member = super.getmember(request);
//			if (member == null || member.getId() != post.getMember().getId()) {
//				show = true;
//				post = forumPostService.viewForumPost(postid);
//			}

      // // 获取评论信息
      Map<String, Object> params = new HashMap<String, Object>();
      // params.put("forumPost.id =", post.getId());
      // List<String> orders = new ArrayList<String>();
      // orders.add("pubdate desc");
      // Pagination page = forumPostService.getCommentPage(params, orders,
      // curpage, pagesize);
      // model.addAttribute("page", page);
      // model.addAttribute("list", page.getList());
      // model.addAttribute("curpage", curpage);
      // model.addAttribute("pagesize", pagesize);

      // 讨论组相关信息
      model.addAttribute("groupInfo", post.getForumGroup());

      // 当前用户角色相关信息
//			if (member != null) {
//				params.clear();
//				params.put("group.id =", post.getForumGroup().getId());
//				params.put("member.id =", member.getId());
//				List<ForumGroupMember> memberList = forumGroupService
//						.findGroupMember(params, null);
//				if (memberList.size() > 0) {
//					model.addAttribute("userrole", memberList.get(0).getRole());
//				}
//			}
    }

    model.addAttribute("show", show);
    model.addAttribute("model", post);
    return "forum/post/post_detail";
  }

  @RequestMapping(value = "forum/postcomments", method = RequestMethod.GET)
  public String postComments(
      @RequestParam(value = "id", required = true, defaultValue = "0") int postid,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    boolean show = false;
    ForumPost post = forumPostService.findById(postid);
    if (post != null) {
      // 获取评论信息
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("forumPost.id =", post.getId());
      List<String> orders = new ArrayList<String>();
      orders.add("pubdate desc");
      Pagination page = forumPostService.getCommentPage(params, orders,
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);
    }

    model.addAttribute("show", show);
    model.addAttribute("model", post);
    return "forum/post/post_commets";
  }

  @RequestMapping(value = "forum/setTop", method = RequestMethod.GET)
  public String setTop(HttpServletRequest request, HttpServletResponse response, Model model) {
    String id = request.getParameter("id");
    String msg = "";
    try {
      forumPostService.setTop(Integer.parseInt(id));
      msg = "置顶成功";
    } catch (Exception e) {
      msg = "置顶出错";
    }
    model.addAttribute("msg", msg);
    return "common/text";
  }


  /**
   * 帖子点赞
   *
   * @param postid
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/post/up")
  public String upPost(
      @RequestParam(value = "id", required = true, defaultValue = "0") int postid,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    ForumPost post = forumPostService.upForumPost(postid);
    if (post != null) {
      model.addAttribute("msg", post.getUps());
    } else {
      model.addAttribute("msg", "点赞失败");
    }
    return "common/text";
  }

  /**
   * @param postid
   * @param request
   * @param response
   * @param model
   * @param comment
   * @return
   */
  @RequestMapping(value = "user/forum/addcomment", method = RequestMethod.POST)
  public String commentPost(
      @RequestParam(value = "id", required = true, defaultValue = "0") int postid,
      HttpServletRequest request, HttpServletResponse response,
      Model model, ForumPostComment comment) {

    if (comment != null) {
      UserInfo member = UserUtil.getCurrentUser();
      if (member != null) {
        // ForumPost post = forumPostService.upForumPost(postid);
        // comment.setForumPost(post);
        //comment.setMember(member);
        // comment.setUps(0);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));
        ForumPostComment result = forumPostService.saveComment(comment);
        // model.addAttribute("item", result);
        model.addAttribute("msg", "success");
        return "common/text";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }
  }

  /**
   * 查询列表
   *
   * @param keyword  查询关键字
   * @param type     查询的类型（0为小组；1为话题）
   * @param sort     话题排序方式
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   * @author 付建辉
   */
  @RequestMapping(value = "forum/search", method = RequestMethod.GET)
  public String search(
      @RequestParam(value = "keyword", required = true, defaultValue = "") String keyword,
      @RequestParam(value = "type", required = true, defaultValue = "0") int type,
      @RequestParam(value = "sort", required = true, defaultValue = "0") int sort,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    Map<String, Object> params = new HashMap<String, Object>();
    List<String> orders = new ArrayList<String>();

    Pagination page = null;
    if (type == 0) {
      params.put("title like ", keyword);
      if (sort == 0) {
        orders.add("title");
      } else {
        orders.add("pubdate");
      }

      page = forumPostService.getForumPostPage(params, orders, curpage,
          pagesize);
    } else {

      params.put("name like ", keyword);
      orders.add("name");
      page = forumGroupService.getGroupPage(params, orders, curpage,
          pagesize);
    }
    model.addAttribute("type", type + "");
    model.addAttribute("page", page);
    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    return "forum/result";
  }

  @RequestMapping(value = "admin/forum/column_index", method = RequestMethod.GET)
  public String index(HttpServletRequest request,
                      HttpServletResponse response, Model model) {
    return "forum/forumcolumn/index";
  }

  @RequestMapping(value = "admin/forum/column_list", method = RequestMethod.GET)
  public String addForumColumn(
      @RequestParam(value = "columnid", required = true, defaultValue = "1") int columnid,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {

    // Map<String, Object> params = new HashMap<String, Object>();

    List<ForumColumn> list = forumPostService.getForumColumnChild(columnid);

    model.addAttribute("list", list);
    model.addAttribute("rootColumn", columnid);

    ForumColumn forumColumn = forumPostService
        .findForumColumnById(columnid);
    model.addAttribute("forumcolumn", forumColumn);

    return "forum/forumcolumn/list";
  }

  @RequestMapping(value = "admin/forum/column_add", method = RequestMethod.POST)
  public String addForumColumn(ForumColumn forumColumn,
                               HttpServletRequest request,
                               HttpServletResponse response, Model model) {

    forumPostService.saveForumColumn(forumColumn);

    return "redirect:/admin/forum/column_list.do";
  }

  @RequestMapping(value = "a", method = RequestMethod.GET)
  public String test() {
    System.out.println("111");
    return "WEB-INF/index";
  }

}
