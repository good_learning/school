package com.haoxuer.school.actions.forum;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.discover.data.core.Pagination;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.commons.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.school.data.entity.forum.ForumGroupMember;
import com.haoxuer.school.data.entity.forum.ForumPost;
import com.haoxuer.school.data.service.ForumGroupService;
import com.haoxuer.school.data.service.ForumPostService;

/**
 * 小组功能
 *
 * @author Alfredo.Diego
 * <p>
 * 2015年3月21日
 */
@Controller
public class GroupAction extends BaseAction {

  private Log log = LogFactory.getLog(GroupAction.class);
  /**
   * 初始创建小组时的成员数量
   */

  private final int START_MEMBER_COUNT = 1;
  /**
   * 小组成员
   */
  private final int ROLE_MEMBER = 100;
  /**
   * 小组组长
   */
  private final int ROLE_LEADER = 0;

  /**
   * 小组管理员
   */
  private final int ROLE_MANAGER = 1;

  @Autowired
  ForumGroupService forumGroupService;

  @Autowired
  ForumPostService forumPostService;

  /**
   * 开始创建小组首页
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/group/index", method = RequestMethod.GET)
  public String groupindex(HttpServletRequest request,
                           HttpServletResponse response, Model model) {
    log.info("开始创建小组");
    List<ForumColumn> forumlist = forumGroupService.findforumcolumnByParentid(1);
    log.info("小组类型：" + forumlist);
    model.addAttribute("groupTypes", forumlist);
    return "forum/group/groupcreate";
  }


  /**
   * 保存小组信息(创建小组)
   *
   * @author 谷玉伟 2015.4.3
   */
  @RequestMapping(value = "forum/group/pregroupsave", method = RequestMethod.POST)
  public String preliminaryGroupSave(ForumGroup group, HttpServletRequest request,
                                     HttpServletResponse response, Model model) {
    log.info("保存小组");
    log.info("保存数据" + group);
    Timestamp time = new Timestamp(System.currentTimeMillis());
    group.setCreateDate(time);
    group.setMemberCount(START_MEMBER_COUNT);
    //保存小组信息
    ForumGroup savegroup = forumGroupService.save(group);

    ForumGroupMember gmember = new ForumGroupMember();
    gmember.setGroup(savegroup);
    gmember.setRole(ROLE_LEADER);
    gmember.setMember(getmember(request));
    //保存小组成员信息
    forumGroupService.save(gmember);
    model.addAttribute("groupid", savegroup.getId());
    model.addAttribute("operFlag", 1);
    return "forum/group/groupimage";
  }

  @RequestMapping(value = "forum/group/saveGroupImageUrl", method = RequestMethod.GET)
  public String saveGroupUrl(HttpServletRequest request, HttpServletResponse response, Model model) {
    String groupid = request.getParameter("groupid");
    String imageurl = request.getParameter("imagePath");
    String operFlag = request.getParameter("operFlag");
    ForumGroup group = new ForumGroup();
    group.setId(Integer.parseInt(groupid));
    group.setTmpImgPath(imageurl);
    forumGroupService.update(group);
    model.addAttribute("groupid", groupid);
    if ("1".equals(operFlag)) {
      return "forum/group/groupsuccess";
    } else {
      return groupDetail(Integer.parseInt(groupid), 1, 10, request, response, model);
    }
  }


  /**
   * 修改小组的信息入口
   *
   * @author 谷玉伟 2015.4.3
   */
  @RequestMapping(value = "forum/group/gogroupupdate", method = RequestMethod.GET)
  public String gogroupUpdate(HttpServletRequest request,
                              HttpServletResponse response, Model model, Integer groupid) {
    log.info("enter update group page...");
    ForumGroup forumGroup = forumGroupService.findById(groupid);
    model.addAttribute("groupinfo", forumGroup);
    return "forum/group/groupupdate";
  }


  /**
   * 修改小组信息
   *
   * @author 谷玉伟
   */
  @RequestMapping(value = "forum/group/groupupdate", method = RequestMethod.POST)
  public String groupUpdate(HttpServletRequest request, ForumGroup group,
                            HttpServletResponse response, Model model) {
    log.info("update group...");
    forumGroupService.update(group);
    model.addAttribute("operFlag", 2);
    model.addAttribute("groupid", group.getId());
    return "forum/group/groupimage";
  }


  /**
   * 快速搜索成员
   *
   * @author 谷玉伟
   */
  @RequestMapping(value = "forum/group/serachmenber", method = RequestMethod.GET)
  public String serachMenber(String param, String groupid, HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    boolean flag = false;
    try {
      String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
      Pattern regex = Pattern.compile(check);
      Matcher matcher = regex.matcher(param);
      flag = matcher.matches();
    } catch (Exception e) {
      flag = false;
    }
    if (flag) {
      //email格式
      model.addAttribute("pramname", param);
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("member.email =", param);
      map.put("group.id =", Integer.parseInt(groupid));
      map.put("role =", ROLE_MEMBER);
      List<ForumGroupMember> list = forumGroupService.findGroupMember(map, null);
      model.addAttribute("memberlist", list);
      //小组组长
      List<ForumGroupMember> leader = forumGroupService.findByrole(Integer.valueOf(groupid), ROLE_LEADER);
      //小组管理员
      List<ForumGroupMember> manager = forumGroupService.findByrole(Integer.valueOf(groupid), ROLE_MANAGER);
      model.addAttribute("managerlist", manager);
      if (null != leader) {
        model.addAttribute("leader", leader.get(0));
      }
      //获取当前用户的角色
      HttpSession session = request.getSession();
      Member member = (Member) session.getAttribute("member");
      if (member == null) {
        model.addAttribute("role", 2);
      } else {
        Map<String, Object> mapr = new HashMap<String, Object>();
        map.put("member.id =", member.getId());
        map.put("group.id =", Integer.parseInt(groupid));
        List<ForumGroupMember> listr = forumGroupService.findRoleByGMid(mapr);
        ForumGroupMember rolemember = listr.get(0);
        model.addAttribute("role", rolemember.getRole());
      }

    } else {
      //username
      model.addAttribute("pramname", param);
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("member.name=", param);
      map.put("group.id=", Integer.parseInt(groupid));
      map.put("role =", ROLE_MEMBER);
      List<ForumGroupMember> list = forumGroupService.findGroupMember(map, null);
      model.addAttribute("memberlistr", list);
      //小组组长
      List<ForumGroupMember> leader = forumGroupService.findByrole(Integer.valueOf(groupid), ROLE_LEADER);
      //小组管理员
      List<ForumGroupMember> manager = forumGroupService.findByrole(Integer.valueOf(groupid), ROLE_MANAGER);
      model.addAttribute("managerlist", manager);
      if (null != leader) {
        model.addAttribute("leader", leader.get(0));
      }
      //获取当前用户的角色
      HttpSession session = request.getSession();
      Member member = (Member) session.getAttribute("member");
      if (member == null) {
        model.addAttribute("role", 2);
      } else {
        Map<String, Object> mapr = new HashMap<String, Object>();
        map.put("member.id =", member.getId());
        map.put("group.id =", Integer.parseInt(groupid));
        List<ForumGroupMember> listr = forumGroupService.findRoleByGMid(mapr);
        ForumGroupMember rolemember = listr.get(0);
        model.addAttribute("role", rolemember.getRole());
      }
    }
    return "forum/group/groupmanage";
  }


  /**
   * 小组详情页面
   *
   * @param groupid
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/group/groupdetail", method = RequestMethod.GET)
  public String groupDetail(@RequestParam(value = "groupid", required = true) int groupid,
                            @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                            @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                            HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("start enter groupdetail page...");
    String type = request.getParameter("type");
    //小组信息
    // if(null!=groupid){
    ForumGroup group = forumGroupService.findById(groupid);
    //小组成员信息
    List<ForumGroupMember> memberlist = forumGroupService.findGroupMember(group);
    model.addAttribute("group", group);
    model.addAttribute("memberlist", memberlist);
    //组长信息
    for (ForumGroupMember grmember : memberlist) {
      int role = grmember.getRole();
      if (role == ROLE_LEADER) {
        model.addAttribute("groupleader", grmember);
      }
    }
    //当前用户信息
    Member member = getmember(request);
    if (null != member) {
      //增加是否是小组成员标志true：是 ；false：否
      model.addAttribute("isgroupmber", forumGroupService.isMemberOf(member.getId(), groupid));
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("member.id =", member.getId());
      map.put("group.id =", groupid);
      List<ForumGroupMember> list = forumGroupService.findRoleByGMid(map);
      if (list != null && !list.isEmpty()) {
        ForumGroupMember rolemember = list.get(0);
        model.addAttribute("role", rolemember.getRole());
      }
    } else {
      model.addAttribute("isgroupmber", false);
    }
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("forumGroup.id =", Integer.valueOf(groupid));
    List<String> sortlist = new ArrayList<String>();
    sortlist.add("sortSeq,id desc");
    if (type != null && "1".equals(type)) {
      sortlist.removeAll(sortlist);
      sortlist.add("id desc");
    }
    if (type != null && "2".equals(type)) {
      sortlist.removeAll(sortlist);
      sortlist.add("comments desc");
    }
    Pagination page = forumPostService.getForumPostPage(params, sortlist, curpage, pagesize);
    model.addAttribute("page", page);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", page.getPageSize());
    model.addAttribute("list", page.getList());

    return "forum/group/groupdetail";
  }


  /**
   * 加入小组
   *
   * @param groupid
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/group/joinGroup", method = RequestMethod.GET)
  public String joinGroup(String groupid, HttpServletRequest request,
                          HttpServletResponse response, Model model) {
    log.info("joinGroup");
    String msg = "加入成功";
    // response.setContentType("text/plain; charset=utf-8");
    if (CommonUtils.isNotEmpty(groupid)) {
      ForumGroup group = forumGroupService.findById(Integer.valueOf(groupid));
      int memberCount = group.getMemberCount() + 1;
      group.setMemberCount(memberCount);
      ForumGroupMember gm = new ForumGroupMember();
      gm.setMember(getmember(request));
      gm.setGroup(group);
      gm.setRole(ROLE_MEMBER);
      boolean upflag = forumGroupService.joinMember(gm);
      if (upflag) {
        msg = "加入成功";
      } else {
        msg = "已经加入小组";
      }
      // ForumGroup forumGroup =
      // forumGroupService.findById(Integer.parseInt(groupid));
      // msg += "|"+forumGroup.getMemberCount();
    } else {
      msg = "请选择小组";
    }

    model.addAttribute("msg", msg);
    // response.getWriter().write(msg);
    return "common/text";
  }

  /**
   * 小组话题
   *
   * @param groupid
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/group/grouppost", method = RequestMethod.GET)
  public String groupPost(String groupid, String sort,
                          @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                          @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                          HttpServletRequest request,
                          HttpServletResponse response, Model model) {

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("forumGroup.id =", Integer.valueOf(groupid));
    List<String> sortlist = new ArrayList<String>();
    if ("comments".equals(sort)) {
      sortlist.add(sort + " desc");
    }

    Pagination page = forumPostService.getForumPostPage(params, sortlist,
        curpage, pagesize);

    model.addAttribute("page", page);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", page.getPageSize());
    model.addAttribute("list", page.getList());

    return "forum/group/groupposts";

  }

  /**
   * 小组成员管理
   *
   * @param groupid
   * @param sort
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/group/groupmanage", method = RequestMethod.GET)
  public String groupmanager(String groupid,
                             @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                             @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                             HttpServletRequest request,
                             HttpServletResponse response, Model model) {

    //小组组长
    List<ForumGroupMember> leader = forumGroupService.findByrole(Integer.valueOf(groupid), ROLE_LEADER);
    //小组管理员
    List<ForumGroupMember> manager = forumGroupService.findByrole(Integer.valueOf(groupid), ROLE_MANAGER);
    model.addAttribute("managerlist", manager);
    if (null != leader) {
      model.addAttribute("leader", leader.get(0));
    }
    //小组成员分页
    groupMember(groupid, model, curpage, pagesize);
    //获取当前用户的角色
    HttpSession session = request.getSession();
    Member member = (Member) session.getAttribute("member");
    if (member == null) {
      model.addAttribute("role", 2);
    } else {
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("member.id =", member.getId());
      map.put("group.id =", Integer.parseInt(groupid));
      List<ForumGroupMember> list = forumGroupService.findRoleByGMid(map);
      ForumGroupMember rolemember = list.get(0);
      model.addAttribute("role", rolemember.getRole());
    }

    return "forum/group/groupmanage";

  }

  /**
   * 小组成员
   *
   * @param groupid
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/group/groupMember", method = RequestMethod.GET)
  public String groupMember(String groupid,
                            @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                            @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                            HttpServletRequest request,
                            HttpServletResponse response, Model model) {
    //小组成员分页
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("role =", ROLE_MEMBER);
    params.put("group.id =", Integer.valueOf(groupid));
    Pagination page = forumGroupService.getGroupMemberPage(params, null,
        curpage, pagesize);
    model.addAttribute("memberlist", page.getList());
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("page", page);
    model.addAttribute("curpage", curpage);
    return "forum/group/memberlist";

  }

  private void groupMember(String groupid, Model model, int curpage, int pagesize) {
    //小组成员分页
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("role =", ROLE_MEMBER);
    params.put("group.id =", Integer.valueOf(groupid));
    Pagination page = forumGroupService.getGroupMemberPage(params, null,
        curpage, pagesize);
    model.addAttribute("memberlist", page.getList());
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("page", page);
    model.addAttribute("curpage", curpage);
  }


  /**
   * 设置为小组成员
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/group/setGroupMemberRole", method = RequestMethod.GET)
  public String setMemberRole(String memberId, String groupId, String id, String roleId,
                              HttpServletRequest request,
                              HttpServletResponse response, Model model) {
    ForumGroupMember gmember = new ForumGroupMember();
    ForumGroup group = new ForumGroup();
    group.setId(Integer.valueOf(groupId));
    Member member = new Member();
    member.setId(Long.valueOf(memberId));
    gmember.setId(Integer.valueOf(id));
    gmember.setMember(member);
    gmember.setGroup(group);
    gmember.setRole(Integer.parseInt(roleId));
    forumGroupService.updateRole(gmember);
    return "common/text";
  }

  /**
   * 将小组成员移除小组
   *
   * @param memberid
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/group/tomove", method = RequestMethod.GET)
  public String move(String memberid, String groupid,
                     HttpServletRequest request,
                     HttpServletResponse response, Model model) {
    if (null != memberid && null != groupid) {
      forumGroupService.deleteGroupMember(Integer.valueOf(groupid), Integer.valueOf(memberid));
    }

    return "forward:/forum/group/groupmanage.htm";
  }

  /**
   * 获取推荐的讨论组列表
   *
   * @param request
   * @param model
   * @return
   */
  @RequestMapping(value = "forum/group/recommendgroups", method = RequestMethod.GET)
  public String getRecommendGroups(HttpServletRequest request, Model model) {
    Member member = super.getmember(request);

    Map<String, Object> params = new HashMap<String, Object>();

    Pagination page = forumGroupService.getRecommendPage(null == member ? 0
        : member.getId(), 1, 7);

    model.addAttribute("grouplist", page.getList());
    model.addAttribute("showBtn", page.getTotalCount() > page.getPageSize()); // true:显示更多按钮；false:不显示

    return "forum/post/recommend_groups";
  }

  private List<Map<String, Object>> buildPostlist(List<?> postlist) {
    List<Map<String, Object>> newpost = new ArrayList<Map<String, Object>>();
    for (Object obj : postlist) {
      if (obj instanceof ForumPost) {
        ForumPost post = (ForumPost) obj;
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("postid", String.valueOf(post.getId()));
        map.put("title", post.getTitle());
        if (null != post.getViews()) {
          map.put("views", post.getViews().toString());
        }
        if (null != post.getMember()) {
          map.put("member", post.getMember().getName());
        }
        if (null != post.getCreateDate()) {
          SimpleDateFormat dtFormat1 = new SimpleDateFormat("yyyy-MM-dd  HH:mm");
          String date = dtFormat1.format(post.getCreateDate());
          map.put("createdate", date);
        }

        map.put("comments", post.getComments());
        newpost.add(map);
      }

    }
    return newpost;
  }


  /**
   * 判断是否是小组的成员
   *
   * @param mber
   * @param grmList 所有小组成员集合
   * @return
   */
  private boolean isGroupMember(Member mber, List<ForumGroupMember> grmlist) {
    boolean isGroupMber = false;
    if (null != grmlist && null != mber) {
      for (ForumGroupMember gmber : grmlist) {
        if (mber.getId() == gmber.getMember().getId()) {
          isGroupMber = true;
          break;
        }
      }
    }

    return isGroupMber;
  }


}
