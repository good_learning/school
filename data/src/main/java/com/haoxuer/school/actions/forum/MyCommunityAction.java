package com.haoxuer.school.actions.forum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.discover.data.core.Pagination;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.ActivityService;
import com.haoxuer.school.data.service.ForumGroupService;
import com.haoxuer.school.data.service.ForumPostService;

/**
 * 我的社区
 *
 * @author Alredo.Diego
 */
@Controller
public class MyCommunityAction extends BaseAction {

  @Autowired
  ForumGroupService forumGroupService;
  @Autowired
  ForumPostService forumPostService;
  @Autowired
  ActivityService activityService;

  private Log log = LogFactory.getLog(MyCommunityAction.class);
  /**
   * 创建的小组
   */
  private final String ROLE_TYPE_OWN = "own";
  /**
   * 加入的小组
   */
  private final String ROLE_TYPE_JOIN = "join";

  /**
   * 社区首页
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/community/index", method = RequestMethod.GET)
  public String communityindex(
      @RequestParam(value = "md", required = true, defaultValue = "1") int module,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    model.addAttribute("md", module);
    return "forum/community/index";
  }

  /**
   * 我创建的小组
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/community/myowngroup", method = RequestMethod.GET)
  public String createGroup(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    Member currentMember = getMember(request);
    Pagination page = forumGroupService.findGroupByRoletype(
        1, ROLE_TYPE_OWN, null, curpage, pagesize);
    model.addAttribute("list", page.getList());

    model.addAttribute("curpage", curpage);
    model.addAttribute("page", page);
    model.addAttribute("searchtype", "21");
    return "forum/community/group_items";
  }

  /**
   * 我加入的小组
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/community/myjoinedgroup", method = RequestMethod.GET)
  public String joinedGroup(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    Member currentMember = getMember(request);
    Pagination page = forumGroupService.findGroupByRoletype(
        1, ROLE_TYPE_JOIN, null, curpage, pagesize);
    model.addAttribute("list", page.getList());

    model.addAttribute("curpage", curpage);
    model.addAttribute("page", page);
    model.addAttribute("searchtype", 1);
    return "forum/community/group_items";
  }

  /**
   * 发布的话题
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/community/myposts", method = RequestMethod.GET)
  public String myPosts(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    Member member = getMember(request);
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("member.id =", member.getId());
    List<String> orders = new ArrayList<String>();
    orders.add("id desc");
    Pagination page = forumPostService.getForumPostPage(params, orders,
        curpage, pagesize);

    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("page", page);
    model.addAttribute("searchtype", "01");
    return "forum/community/post_items";
  }

  /**
   * 我参与的话题
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/community/myparticipation", method = RequestMethod.GET)
  public String myParticipation(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    Member currentMember = getMember(request);
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("member.id =", currentMember.getId());
    List<String> orders = new ArrayList<String>();
    orders.add("id desc");
    Pagination page = forumPostService.getMyParticipation(params, orders,
        curpage, pagesize);
    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("page", page);
    model.addAttribute("searchtype", "02");
    return "forum/community/post_items";
  }

  /**
   * 我发布的活动
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/community/myactivities", method = RequestMethod.GET)
  public String myActivities(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Member member = getMember(request);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("member.id =", member.getId());

    Pagination page = activityService.getPageOfActivities(params, null,
        curpage, pagesize);
    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("page", page);
    model.addAttribute("searchtype", "11");

    return "forum/community/activity_items";
  }

  /**
   * 我参与的活动
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "user/forum/community/myjoined", method = RequestMethod.GET)
  public String joinedActivity(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request,
      HttpServletResponse response, Model model) {
    Member member = getMember(request);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("members.id =", member.getId());
    Pagination page = activityService.getMyJoinedActivities(params, null,
        curpage, pagesize);
    model.addAttribute("list", page.getList());

    model.addAttribute("curpage", curpage);
    model.addAttribute("page", page);
    model.addAttribute("searchtype", "12");
    return "forum/community/activity_items";
  }

  @RequestMapping(value = "user/forum/community/mycommented", method = RequestMethod.GET)
  public String commentedActivity(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    Member member = getMember(request);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("member.id =", member.getId());
    Pagination page = activityService.getMycommentedActivities(params,
        null,
        curpage, pagesize);
    model.addAttribute("list", page.getList());

    model.addAttribute("curpage", curpage);
    model.addAttribute("page", page);
    model.addAttribute("searchtype", "12");
    return "forum/community/activity_items";
  }

  /**
   * 获取当前登陆用户
   *
   * @param request
   * @return
   */
  private Member getMember(HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    return member;
  }
}
