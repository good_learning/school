package com.haoxuer.school.actions.front;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.BankRecord;
import com.haoxuer.school.data.service.BankRecordService;


@Controller
@RequestMapping(value = "app")
public class AppController extends BaseAction {

  @Autowired
  BankRecordService bankRecordService;

  @RequestMapping(value = "timeout", method = RequestMethod.GET)
  public String view(
      @RequestParam(value = "size", required = true, defaultValue = "50") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    List<BankRecord> article = bankRecordService.backdata(pagesize);
    int result = 0;
    if (article != null) {
      result = article.size();
    }
    model.addAttribute("msg", "" + result);
    return "common/text";
  }

}
