package com.haoxuer.school.actions.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.ArticleService;
import com.haoxuer.school.data.service.ArticlecommentService;
import com.haoxuer.school.data.service.ArticletypeService;
import com.haoxuer.school.data.service.FavArtilceService;

@Controller
@RequestMapping(value = "article")
public class ArtcileWebAction extends BaseAction {

  @Autowired
  ArticleService articleService;

  @Autowired
  ArticletypeService articletypeService;

  @Autowired
  FavArtilceService favArtilceService;

  @Autowired
  ArticlecommentService articlecommentService;

  @RequestMapping(value = "view", method = RequestMethod.GET)
  public String view(
      @RequestParam(value = "id", required = true) int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Article article = articleService.findById(id);
    Pagination rs = articlecommentService.pageByArticleId(id, sorttype, pagesize,
        curpage);
    if (rs != null) {
      model.addAttribute("list", rs.getList());
      model.addAttribute("page", rs);
    }
    model.addAttribute("articleitem", article);
    model.addAttribute("curpage", curpage);
    model.addAttribute("id", id);
    model.addAttribute("sorttype", sorttype);
    model.addAttribute("pagesize", pagesize);
    return "article/view";
  }

  @RequestMapping(value = "articles", method = RequestMethod.GET)
  public String articles(
      @RequestParam(value = "id", required = true, defaultValue = "5") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "30") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Pagination rs = articleService.pageByPid(id, sorttype, pagesize,
        curpage);
    if (rs != null) {
      model.addAttribute("list", rs.getList());
      model.addAttribute("page", rs);
    }

    model.addAttribute("curpage", curpage);
    model.addAttribute("id", id);
    model.addAttribute("sorttype", sorttype);
    model.addAttribute("pagesize", pagesize);

    Articletype curtype = articletypeService.findById(id);
    model.addAttribute("curtype", curtype);
    model.addAttribute("navinfo", 1);

    return "article/articles";
  }

  /**
   * @param id
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "up", method = RequestMethod.GET)
  public String up(@RequestParam(value = "id", required = true) int id,
                   HttpServletRequest request, HttpServletResponse response,
                   Model model) {
    try {
      Integer num = articleService.up(id);
      model.addAttribute("msg", num);
    } catch (Exception e) {
      model.addAttribute("msg", "点赞失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "fav", method = RequestMethod.GET)
  public String fav(@RequestParam(value = "id", required = true) int id,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {
    try {
      Member smember = getmember(request);
      if (smember != null) {
        int type = favArtilceService.fav(smember.getId(), id);
        if (type == 1) {
          model.addAttribute("msg", "收藏成功");
        } else {
          model.addAttribute("msg", "你已收藏了");
        }

      } else {
        model.addAttribute("msg", "你还没登陆");
      }
    } catch (Exception e) {
      model.addAttribute("msg", "收藏失败");
    }

    return "common/text";
  }
}
