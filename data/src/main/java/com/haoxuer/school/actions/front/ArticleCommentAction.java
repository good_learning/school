package com.haoxuer.school.actions.front;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.entity.Articlecomment;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.ArticlecommentService;

@Controller
@RequestMapping(value = "articlecomment")
public class ArticleCommentAction extends BaseAction {

  @Autowired
  ArticlecommentService articlecommentService;

  @RequestMapping(value = "add", method = RequestMethod.POST)
  public String add(Articlecomment comment, int articleid,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {

    if (comment != null) {
      HttpSession session = getSession(request);
      Member member = (Member) session.getAttribute("member");
      if (member != null) {
        Article article = new Article();
        article.setId(articleid);
        comment.setArticle(article);
        comment.setMember(member);
        comment.setUps(0);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));
        Articlecomment result = articlecommentService.save(comment);
        model.addAttribute("item", result);
        return "articlecomment/add";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }

  }

  @RequestMapping(value = "addcomment", method = RequestMethod.POST)
  public String addComment(Articlecomment comment,
                           HttpServletRequest request, HttpServletResponse response,
                           Model model) {

    if (comment != null) {
      HttpSession session = getSession(request);
      Member member = (Member) session.getAttribute("member");
      if (member != null) {
        comment.setMember(member);
        comment.setUps(0);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));

        Articlecomment result = articlecommentService.save(comment);
        result = articlecommentService.findById(result.getId());
        model.addAttribute("item", result);

        return "articlecomment/add";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }

  }

  @RequestMapping(value = "up", method = RequestMethod.GET)
  public String up(@RequestParam(value = "id", required = true) int id,
                   HttpServletRequest request, HttpServletResponse response,
                   Model model) {
    try {
      articlecommentService.up(id);
      model.addAttribute("msg", "支持成功");
    } catch (Exception e) {
      model.addAttribute("msg", "支持失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "ups", method = RequestMethod.GET)
  public String ups(@RequestParam(value = "id", required = true) int id,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {
    try {
      Integer ups = articlecommentService.up(id);
      model.addAttribute("msg", ups);
    } catch (Exception e) {
      model.addAttribute("msg", "支持失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "view", method = RequestMethod.GET)
  public String pageajaxlist(
      @RequestParam(value = "id", required = true) int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Pagination rs = articlecommentService.pageByArticleId(id, sorttype,
        pagesize, curpage);
    if (rs != null) {
      model.addAttribute("list", rs.getList());
      model.addAttribute("page", rs);
    }
    model.addAttribute("curpage", curpage);
    model.addAttribute("id", id);
    model.addAttribute("sorttype", sorttype);
    model.addAttribute("pagesize", pagesize);
    return "articlecomment/pageajaxlist";
  }

  @RequestMapping(value = "list", method = RequestMethod.GET)
  public String list(int commentid, HttpServletRequest request,
                     HttpServletResponse response, Model model) {

    // Pagination page= articlecommentService.pageByCommentId(commentid, 2,
    // 1, 10);
    // model.addAttribute("comments", page.getList());
    // model.addAttribute("commentid", commentid);

    return "coursecomment/list";
  }

}
