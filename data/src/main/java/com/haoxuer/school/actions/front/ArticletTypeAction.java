package com.haoxuer.school.actions.front;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.school.data.service.ArticleService;
import com.haoxuer.school.data.service.ArticletypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping(value = "article")
public class ArticletTypeAction {

  @Autowired
  ArticletypeService articletypeService;

  @Autowired
  ArticleService articleService;

  Logger logger = LoggerFactory.getLogger("log");
  int aid = 0;

  @RequestMapping(value = "findbypid", method = RequestMethod.GET)
  public @ResponseBody
  List<Articletype> findbypid(
      @RequestParam(value = "id", required = true, defaultValue = "1") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    List<Articletype> rs = articletypeService.findByParentId(id);
    return rs;
  }

  @RequestMapping(value = "findbypidjson", method = RequestMethod.GET)
  public String findbypidjson(
      @RequestParam(value = "id", required = true, defaultValue = "1") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    List<Articletype> rs = articletypeService.findByParentId(id);

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      String json = objectMapper.writeValueAsString(rs);
      model.addAttribute("msg", json);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return "common/text";
  }

  @RequestMapping(value = "allarticletype", method = RequestMethod.GET)
  public String findByParent(
      @RequestParam(value = "id", required = true, defaultValue = "5") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    List<Articletype> rs = articletypeService.findByParentId(id);
    model.addAttribute("articletype", rs);
    Articletype curtype = articletypeService.findById(id);
    model.addAttribute("curtype", curtype);
    return "article/articletype";
  }

  @RequestMapping(value = "xuezhidao", method = RequestMethod.GET)
  public String xuezhidao(
      @RequestParam(value = "id", required = true, defaultValue = "2") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    List<Articletype> rs = articletypeService.findByParentId(id);
    model.addAttribute("articletype", rs);
    Articletype curtype = articletypeService.findById(id);
    model.addAttribute("curtype", curtype);
    return "article/xuezhidao";
  }

  @RequestMapping(value = "shizhidao", method = RequestMethod.GET)
  public String shizhidao(
      @RequestParam(value = "id", required = true, defaultValue = "10") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    List<Articletype> rs = articletypeService.findByParentId(id);
    model.addAttribute("articletype", rs);
    Articletype curtype = articletypeService.findById(id);
    model.addAttribute("curtype", curtype);
    return "article/shizhidao";
  }

  @RequestMapping(value = "jiazhidao", method = RequestMethod.GET)
  public String jiazhidao(
      @RequestParam(value = "id", required = true, defaultValue = "3") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    List<Articletype> rs = articletypeService.findByParentId(id);
    model.addAttribute("articletype", rs);
    Articletype curtype = articletypeService.findById(id);
    model.addAttribute("curtype", curtype);
    return "article/jiazhidao";
  }

  @RequestMapping(value = "hangyedao", method = RequestMethod.GET)
  public String hangyedao(
      @RequestParam(value = "id", required = true, defaultValue = "4") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    List<Articletype> rs = articletypeService.findByParentId(id);
    model.addAttribute("articletype", rs);
    Articletype curtype = articletypeService.findById(id);
    model.addAttribute("curtype", curtype);
    return "article/hangyedao";
  }

  @RequestMapping(value = "pagelist", method = RequestMethod.GET)
  public String pagelist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Pagination rs = articleService.pageByPid(id, sorttype, pagesize,
        curpage);
    if (rs != null) {
      model.addAttribute("list", rs.getList());

    }
    return "article/pagelist";
  }

  private HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      logger.info("会话不存在");
      session = request.getSession(true);
    }
    return session;
  }

}
