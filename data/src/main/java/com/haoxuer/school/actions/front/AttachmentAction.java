package com.haoxuer.school.actions.front;

import java.sql.Timestamp;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Attachment;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.AttachmentService;

@Controller
@RequestMapping(value = "attachment")
public class AttachmentAction extends BaseAction {

  @Autowired
  AttachmentService attachmentService;

  @RequestMapping(value = "save", method = RequestMethod.POST)
  public String save(Attachment attachment, HttpServletRequest request,
                     HttpServletResponse response, Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      String origName = attachment.getFileurl();
      if (origName != null) {
        String ext = FilenameUtils.getExtension(origName).toLowerCase(
            Locale.ENGLISH);
        attachment.setFiletype(ext);
      }
      attachment.setAdddate(new Timestamp(System.currentTimeMillis()));
      attachment.setMember(member);
      Attachment back = attachmentService.save(attachment);
      model.addAttribute("item", back);
      model.addAttribute("msg", "保存数据成功");

      return "common/text";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "update", method = RequestMethod.POST)
  public String update(Attachment attachment, HttpServletRequest request,
                       HttpServletResponse response, Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Attachment back = attachmentService.update(attachment);
      model.addAttribute("item", back);
      model.addAttribute("msg", "更新数据成功");

      return "common/text";
    } else {
      return login;

    }

  }

  @RequestMapping("/pagelist")
  public String pagelist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    Pagination pagination = attachmentService.pageByUserId(id, curpage,
        pagesize);
    model.addAttribute("curpage", pagination.getPageNo());
    model.addAttribute("list", pagination.getList());

    return "/attachment/pagelist";

  }

  @RequestMapping("/list")
  public String list(@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                     @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
                     HttpServletRequest request, ModelMap model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination pagination = attachmentService.pageByUserId(
//          member.getId(), curpage, pagesize);
//      model.addAttribute("curpage", pagination.getPageNo());
//      model.addAttribute("list", pagination.getList());

      return "/attachment/pagelist";

    } else {
      return login;

    }

  }


  @RequestMapping("/listbytype")
  public String listbytype(@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                           @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
                           HttpServletRequest request, ModelMap model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination pagination = attachmentService.pagetypeByUserId(
//          member.getId(), 3, curpage, pagesize);
//      model.addAttribute("curpage", pagination.getPageNo());
//      model.addAttribute("list", pagination.getList());

      return "/attachment/listbytype";

    } else {
      return login;

    }

  }
}
