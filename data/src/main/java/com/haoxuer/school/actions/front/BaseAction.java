package com.haoxuer.school.actions.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.haoxuer.school.data.entity.Member;

public class BaseAction {

  Logger logger = LoggerFactory.getLogger("log");

  public HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      logger.info("会话不存在");
      session = request.getSession(true);
    }
    return session;
  }

  protected Member getmember(HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member smember = (Member) session.getAttribute("member");
    return smember;
  }

  protected String login = "redirect:/login.htm";
}
