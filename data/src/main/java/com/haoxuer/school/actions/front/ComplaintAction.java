package com.haoxuer.school.actions.front;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.haoxuer.school.data.entity.Complaint;
import com.haoxuer.school.data.service.ComplaintService;

@Controller
@RequestMapping(value = "complaint")
public class ComplaintAction extends BaseAction {

  @Autowired
  ComplaintService complaintService;

  @RequestMapping(value = "precomplaint", method = RequestMethod.GET)
  public String precomplaint(HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    return "complaint/precomplaint";
  }

  @RequestMapping(value = "addcomplaint", method = RequestMethod.POST)
  public String addcomplaint(Complaint complaint, String vcode, HttpServletRequest request,
                             HttpServletResponse response, Model model) {

    HttpSession session = request.getSession(false);
    String captchaToken = session == null ? "" : session
        .getAttribute("captchaToken") + "";

    logger.info("vcode:" + vcode + "     captchaToken:" + captchaToken);
    if (captchaToken.equals(vcode)) {
      complaint.setAdddate(new Timestamp(System.currentTimeMillis()));
      complaintService.save(complaint);
      return "complaint/addcomplaint";

    } else {
      model.addAttribute("info", "验证码不正确");
      return "complaint/precomplaint";

    }
  }
}
