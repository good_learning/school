package com.haoxuer.school.actions.front;

import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import com.haoxuer.discover.web.controller.front.BaseController;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.service.CourseService;
import com.haoxuer.school.data.service.TradingrecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "user")
public class CourseAction extends BaseController {

  @Autowired
  CourseService courseService;


  @Autowired
  TradingrecordService tradingrecordService;


  @RequestMapping(value = "preaddcourses", method = RequestMethod.GET)
  public String preaddcourses(HttpServletRequest request,
                              HttpServletResponse response, Model model) {

    //todo catalog
    //model.addAttribute("coursetypes", coursetypeService.findTop());


    return "addcourses";

  }


  @RequestMapping(value = "allrecord", method = RequestMethod.GET)
  public String allRecord(HttpServletRequest request,
                          HttpServletResponse response, Model model) {
    UserInfo userInfo = UserUtil.getCurrentUser();

    Long uid = userInfo.getId();
    if (uid != 0) {
      List<Tradingrecord> rs = tradingrecordService.findByUid(uid);
      model.addAttribute("tradingrecord", rs);
    }
    return "tradingrecords";

  }


  @RequestMapping(value = "addcourses", method = RequestMethod.POST)
  public String addcourses(Course course, HttpServletRequest request,
                           HttpServletResponse response, Model model) {


    Course course1 = null;
    try {
      course1 = courseService.add(course);
      //todo cataalog
      //model.addAttribute("coursetypes", coursetypeService.findTop());

    } catch (Exception e) {
      e.printStackTrace();
    }
    if (course1 != null) {
      return "addcourses";
    } else {
      return "addcourses";
    }

  }

  @RequestMapping(value = "deletecourses", method = RequestMethod.POST)
  public String deleteCourses(Course course1, HttpServletRequest request,
                              HttpServletResponse response, Model model) {
    return "courses";

  }


  @RequestMapping(value = "findtradingrecord", method = RequestMethod.POST)
  public String findTradingRecord(Course course1,
                                  HttpServletRequest request, HttpServletResponse response,
                                  Model model) {
    return "tradingrecords";
  }

  @RequestMapping(value = "findRecordByType", method = RequestMethod.POST)
  public String findRecordByType(Course course1,
                                 HttpServletRequest request, HttpServletResponse response,
                                 Model model) {
    return "typerecords";
  }


}
