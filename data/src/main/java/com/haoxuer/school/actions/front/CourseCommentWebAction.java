package com.haoxuer.school.actions.front;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.CourseComment;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.CourseCommentService;
import com.haoxuer.school.data.service.CourseService;

@Controller
@RequestMapping(value = "coursecomment")
public class CourseCommentWebAction implements Serializable {
  @Autowired
  CourseService courseService;
  @Autowired
  CourseCommentService commentService;

  @RequestMapping(value = "add", method = RequestMethod.POST)
  public String add(CourseComment comment, int courseid,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {

    if (comment != null) {
      HttpSession session = getSession(request);
      Member member = (Member) session.getAttribute("member");
      if (member != null) {
        Course course = new Course();
        course.setId(courseid);
        comment.setCourse(course);
        comment.setMember(member);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));
        CourseComment result = commentService.save(comment);
        model.addAttribute("item", result);
        return "coursecomment/add";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }

  }

  @RequestMapping(value = "goodbyuser", method = RequestMethod.GET)
  public String goodbyuser(
      @RequestParam(value = "id", required = true) int id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    if (id != 0) {
      List<Course> rs = courseService.goodByUser(id, 3);
      model.addAttribute("good", rs);
      return "course/goodbyuser";
    } else {
      return "course/goodbyuser";

    }
  }

  @RequestMapping(value = "addcomment", method = RequestMethod.POST)
  public String addComment(CourseComment comment, int courseid,
                           long commentid, HttpServletRequest request,
                           HttpServletResponse response, Model model) {

    if (comment != null) {
      HttpSession session = getSession(request);
      Member member = (Member) session.getAttribute("member");
      if (member != null) {
        Course course = new Course();
        course.setId(courseid);
        comment.setCourse(course);
        comment.setMember(member);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));
        CourseComment c = new CourseComment();
        c.setId(commentid);
        comment.setCourseComment(c);
        CourseComment result = commentService.save(comment);
        model.addAttribute("item", result);
        return "coursecomment/addcomment";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }

  }

  @RequestMapping(value = "list", method = RequestMethod.GET)
  public String list(int commentid, HttpServletRequest request,
                     HttpServletResponse response, Model model) {

    Pagination page = commentService.pageByCommentId(commentid, 2, 1, 10);
    model.addAttribute("comments", page.getList());
    model.addAttribute("commentid", commentid);

    return "coursecomment/list";
  }

  @RequestMapping(value = "pagelist", method = RequestMethod.GET)
  public String pagelist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "type", required = true, defaultValue = "0") int type,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    model.addAttribute("type", type);

    Pagination page = commentService.pageForCourseByType(id, type, curpage,
        pagesize);
    List list = page.getList();
    if (list != null && list.size() > 0) {
      model.addAttribute("list", list);
    }
    model.addAttribute("page", page);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("id", id);

    return "coursecomment/pagelist";
  }

  @RequestMapping(value = "pagelistformember", method = RequestMethod.GET)
  public String pagelistformember(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "type", required = true, defaultValue = "0") int type,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    model.addAttribute("type", type);

    Pagination page = commentService.pageForMemberByType(id, type, curpage,
        pagesize);
    List list = page.getList();
    if (list != null && list.size() > 0) {
      model.addAttribute("list", list);
    }
    model.addAttribute("page", page);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("id", id);

    return "coursecomment/pagelistformember";
  }

  private HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      session = request.getSession(true);
    }
    return session;
  }

}
