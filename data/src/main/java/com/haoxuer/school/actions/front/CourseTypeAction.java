package com.haoxuer.school.actions.front;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haoxuer.school.data.entity.CourseCatalog;


@Controller
@RequestMapping(value = "coursetype")
public class CourseTypeAction {


  @RequestMapping(value = "findbypid", method = RequestMethod.GET)
  public @ResponseBody
  List<CourseCatalog> findbypid(
      @RequestParam(value = "id", required = true, defaultValue = "1") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    List<CourseCatalog> rs = null;// coursetypeService.findChild(id);
    return rs;
  }

  @RequestMapping(value = "findbypidjson", method = RequestMethod.GET)
  public String findbypidjson(
      @RequestParam(value = "id", required = true, defaultValue = "1") Integer id,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    //todo catalog
    List<CourseCatalog> rs = null;//coursetypeService.findChild(id);
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      String json = objectMapper.writeValueAsString(rs);
      model.addAttribute("msg", json);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return "common/text";
  }
}
