package com.haoxuer.school.actions.front;

import com.haoxuer.discover.area.data.service.AreaService;
import com.haoxuer.discover.web.controller.front.BaseController;
import com.haoxuer.school.data.entity.*;
import com.haoxuer.school.data.oto.CourseTypeInfos;
import com.haoxuer.school.data.service.*;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "course")
public class CourseWebAction extends BaseController {

  @Autowired
  CourseService courseService;

  @Autowired
  FavcourseService favcourseService;

  @Autowired
  MemberService memberService;

  @Autowired
  CourseCommentService commentService;

  @Autowired
  BrowsingHistoryService browsingHistoryService;

  @Autowired
  CourseSubscribeService courseSubscribeService;

  @Autowired
  CourseCountService courseCountService;


  @Autowired
  PhoneRecordService phoneRecordService;

  @Autowired
  TownService townService;

  @Autowired
  TradingrecordService tradingrecordService;

  @Autowired
  CourseCatalogService courseCatalogService;

  @Autowired
  private AreaService areaService;

  /**
   * 课程查看列表功能
   *
   * @param id
   * @param sorttype
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "index", method = RequestMethod.GET)
  public String index(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "townid", required = true, defaultValue = "0") int townid,
      @RequestParam(value = "typeid", required = true, defaultValue = "0") int typeid,
      @RequestParam(value = "coursestate", required = true, defaultValue = "0") int coursestate,
      @RequestParam(value = "sorttype", required = true, defaultValue = "0") int sorttype,
      @RequestParam(value = "keyword", required = true, defaultValue = "") String keyword,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    model.addAttribute("sorttype", sorttype);
    Pagination page = courseService.search(id, townid, typeid, coursestate,
        sorttype, keyword, curpage, pagesize);
    // Pagination page = courseService.pageByType(id, sorttype, curpage,
    // pagesize);
    if (page.getList() != null) {
      model.addAttribute("list", page.getList());
    }
    model.addAttribute("page", page);

    model.addAttribute("navinfo", 2);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("id", id);

    //todo catalog
    CourseCatalog type = null;//coursetypeService.findById(id);
    model.addAttribute("coursetype", type);
    //todo catalog
    CourseTypeInfos infos = courseCatalogService.find(id);
    model.addAttribute("level1", infos.getLevel1());
    model.addAttribute("typelist1", infos.getList1());
    model.addAttribute("level2", infos.getLevel2());
    model.addAttribute("typelist2", infos.getList2());
    model.addAttribute("level3", infos.getLevel3());
    model.addAttribute("typelist3", infos.getList3());
    model.addAttribute("level4", infos.getLevel4());
    model.addAttribute("typelist4", infos.getList4());
    model.addAttribute("towns", areaService.child(345));
    model.addAttribute("townid", townid);
    model.addAttribute("coursestate", coursestate);
    model.addAttribute("typeid", typeid);

    // 1101

    return getView("course/index");
  }

  @RequestMapping(value = "ajaxlist", method = RequestMethod.GET)
  public String ajaxlist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    if (sorttype == 1) {
      model.addAttribute("sorttype", sorttype);
    }

    Pagination page = courseService.pageByType(id, sorttype, curpage,
        pagesize);
    List list = page.getList();
    if (list != null && list.size() > 0) {
      model.addAttribute("list", list);
    }
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("id", id);

    return "course/ajaxlist";
  }

  @RequestMapping(value = "view", method = RequestMethod.GET)
  public String view(@RequestParam(value = "id", required = true) int id,
                     HttpServletRequest request, HttpServletResponse response,
                     Model model) {

    Course course = courseService.view(id);
    model.addAttribute("course", course);
    Member member = memberService.findById(course.getMember().getId());
    model.addAttribute("teacher", member);
    Pagination page = commentService.pageByType(course.getId(), 1, 1, 10);
    model.addAttribute("comments", page.getList());

    model.addAttribute("showActions", "show"); // 默认显示购买课程的button

    HttpSession session = getSession(request);
    Member curmember = (Member) session.getAttribute("member");
    if (curmember != null) {
//      browsingHistoryService.saveRecord(curmember.getId(), id);
//
//      // 老师、机构不能购买课程
//      if (curmember.getMembertype().getId() > 1)
//        model.addAttribute("showActions", "hide");
    }

    CourseCount bean = new CourseCount();
    bean.setCourse(course);
    Calendar calendar = Calendar.getInstance();
    bean.setYearcount(calendar.get(Calendar.YEAR));
    bean.setWeekcount(calendar.get(Calendar.WEEK_OF_YEAR));
    courseCountService.save(bean);

    return "course/view";
  }

  @RequestMapping(value = "presubscribe", method = RequestMethod.GET)
  public String presubscribe(int courseid, int state,
                             HttpServletRequest request, HttpServletResponse response,
                             Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("courseid", courseid);
      model.addAttribute("state", state);
      model.addAttribute("now", new Date());
      model.addAttribute("course", courseService.findById(courseid));
      return "course/presubscribe";
    } else {
      model.addAttribute("msg", "你还没有登陆");
      return "redirect:/login.htm";

    }

  }

  /**
   * 立即报名 或者 预约试听
   *
   * @param subscribe
   * @param courseid
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "subscribe", method = RequestMethod.POST)
  public String subscribe(CourseSubscribe subscribe, int courseid,
                          HttpServletRequest request, HttpServletResponse response,
                          Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Course course = new Course();
      course.setId(courseid);
      subscribe.setCourse(course);
      subscribe.setMember(member);
      subscribe.setState(2);
      subscribe.setAdddate(new Timestamp(System.currentTimeMillis()));
      CourseSubscribe courseSubscribe = courseSubscribeService
          .order(subscribe);
      model.addAttribute("item", courseSubscribe);

      return "course/subscribeok";

    } else {
      model.addAttribute("msg", "你还没有登陆");
      return "redirect:/login.htm";

    }

  }

  @Autowired
  CouponService couponService;

  /**
   * 立即报名
   *
   * @param courseid
   * @param state
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "preenroll", method = RequestMethod.GET)
  public String preenroll(int courseid, int state,
                          HttpServletRequest request, HttpServletResponse response,
                          Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("courseid", courseid);
      model.addAttribute("state", state);
      model.addAttribute("course", courseService.findById(courseid));
//      Pagination page = couponService.pageByUserNotUser(member.getId(),
//          1, 10);
//      model.addAttribute("list", page.getList());
      return "course/preenroll";
    } else {
      model.addAttribute("msg", "你还没有登陆");
      return "redirect:/login.htm";

    }
  }

  /**
   * 立即报名
   *
   * @param courseid
   * @param state
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "enroll", method = RequestMethod.POST)
  public String enroll(CourseSubscribe subscribe, int courseid, int state,
                       HttpServletRequest request, HttpServletResponse response,
                       Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Course course = new Course();
      course.setId(courseid);
      subscribe.setCourse(course);
      subscribe.setMember(member);
      subscribe.setAdddate(new Timestamp(System.currentTimeMillis()));
      CourseSubscribe courseSubscribe = courseSubscribeService
          .order(subscribe);
      model.addAttribute("item", courseSubscribe);
      model.addAttribute("id", courseSubscribe.getTradingrecord().getId());

      return "redirect:/member/prepay2.htm";

    } else {
      model.addAttribute("msg", "你还没有登陆");
      return "redirect:/login.htm";

    }
  }

  @RequestMapping(value = "phone", method = RequestMethod.GET)
  public String phone(@RequestParam(value = "id", required = false) int id,
                      String phonenum, HttpServletRequest request,
                      HttpServletResponse response, Model model) {

    if (phonenum != null) {
      PhoneRecord bean = new PhoneRecord();
      bean.setPhonenum(phonenum);
      bean.setName("来之网页");
      Course course = new Course();
      course.setId(id);
      bean.setCourse(course);
      bean.setAdddate(new Timestamp(System.currentTimeMillis()));
      phoneRecordService.save(bean);
      model.addAttribute("msg", "我们已经收到你的电话号码，稍后我们会联系你");

    } else {
      model.addAttribute("msg", "电话号码不正确，系统没有接收到你的电话号码");
    }

    return "common/text";
  }

  /**
   * @param id
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "up", method = RequestMethod.GET)
  public String up(@RequestParam(value = "id", required = true) int id,
                   HttpServletRequest request, HttpServletResponse response,
                   Model model) {

    Course course = courseService.up(id);
    model.addAttribute("msg", course.getUps());
    return "common/text";
  }

  @RequestMapping(value = "fav", method = RequestMethod.GET)
  public String fav(int id, HttpServletRequest request,
                    HttpServletResponse response, Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      int result = favcourseService.fav(member.getId(), id);
      if (result > 0) {
        model.addAttribute("msg", "你已经收藏过了");

      } else {
        model.addAttribute("msg", "收藏成功");
      }
    } else {
      model.addAttribute("msg", "你还没有登陆");
    }

    return "common/text";
  }

  @RequestMapping(value = "favlist", method = RequestMethod.GET)
  public String favlist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "3") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
//    Pagination page = favcourseService.pageByUserId(id, curpage, pagesize);
//    if (page.getList() != null) {
//      model.addAttribute("list", page.getList());
//    }
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    return "course/favlist";
  }

  private HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      session = request.getSession(true);
    }
    return session;
  }

}
