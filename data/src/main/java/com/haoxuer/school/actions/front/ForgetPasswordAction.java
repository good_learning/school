package com.haoxuer.school.actions.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.haoxuer.school.data.service.MemberService;

@Controller
public class ForgetPasswordAction {

  @Autowired
  MemberService memberService;

  @RequestMapping(value = "findpasswordbypassword", method = {
      RequestMethod.POST, RequestMethod.GET})
  public String findpasswordbypassword(String email,
                                       HttpServletRequest request, HttpServletResponse response,
                                       Model model) {
    int result =3;
    if (result == 1) {
      model.addAttribute("msg", "密码已经成功发到你的邮箱，请注意查收");
    }
    if (result == -1) {
      model.addAttribute("msg", "重置密码失败，邮箱格式不正确！");
    }
    if (result == -2) {
      model.addAttribute("msg", "重置密码失败，系统没有找到该帐号！");
    }
    return "common/text";
  }

  @RequestMapping(value = "findpasswordbyphone", method = {
      RequestMethod.POST, RequestMethod.GET})
  public String findpasswordbyphone(String phonenum,
                                    HttpServletRequest request, HttpServletResponse response,
                                    Model model) {
    int result = 3;
    if (result == 1) {
      model.addAttribute("msg", "密码已经成功发到你的手机，请注意查收");
    }
    if (result == -1) {
      model.addAttribute("msg", "重置密码失败，手机号码不正确！");
    }
    if (result == -2) {
      model.addAttribute("msg", "重置密码失败，系统没有找到该帐号！");
    }
    if (result == 0) {
      model.addAttribute("msg", "重置密码失败，系统没有找到该帐号！");
    }
    return "common/text";
  }
}
