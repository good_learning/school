package com.haoxuer.school.actions.front;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.service.FriendService;

@Controller
@RequestMapping(value = "friend")
public class FriendAction extends BaseAction {

  @Autowired
  private FriendService friendService;

  @RequestMapping(value = "list", method = RequestMethod.GET)
  public String list(
      int userid,
      int type,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "12") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    Pagination page = friendService.pageByType(userid, type, curpage,
        pagesize);
    List<?> datas = page.getList();
    model.addAttribute("list", datas);
    return "friend/list";
  }

  @RequestMapping(value = "frontlist", method = RequestMethod.GET)
  public String frontlist(
      int userid,
      int type,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "12") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    Pagination page = friendService.pageByType(userid, type, curpage,
        pagesize);
    List<?> datas = page.getList();
    model.addAttribute("list", datas);
    return "friend/frontlist";
  }


  @RequestMapping(value = "userlist", method = RequestMethod.GET)
  public String userlist(
      int userid,
      int type,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "3") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    Pagination page = friendService.pageByType(userid, type, curpage,
        pagesize);
    List<?> datas = page.getList();
    model.addAttribute("list", datas);
    return "friend/userlist";
  }


  @RequestMapping(value = "schoollist", method = RequestMethod.GET)
  public String schoollist(
      int userid,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "3") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    Pagination page = friendService.pageByType(userid, 7, curpage,
        pagesize);
    List<?> datas = page.getList();
    model.addAttribute("list", datas);
    return "friend/schoollist";
  }
}
