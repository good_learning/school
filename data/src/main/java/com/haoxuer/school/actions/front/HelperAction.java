package com.haoxuer.school.actions.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.haoxuer.discover.web.controller.front.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "helper")
public class HelperAction extends BaseController {

  @RequestMapping(value = "helpstudent_three", method = RequestMethod.GET)
  public String helpstudent_three(HttpServletRequest request,
                                  HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_three");

  }

  @RequestMapping(value = "helpstudent_two", method = RequestMethod.GET)
  public String helpstudent_two(HttpServletRequest request,
                                HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_two");

  }

  @RequestMapping(value = "helpstudent", method = RequestMethod.GET)
  public String helpstudent(HttpServletRequest request,
                            HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent");

  }

  @RequestMapping(value = "helpstudent_right", method = RequestMethod.GET)
  public String helpstudent_right(HttpServletRequest request,
                                  HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right");

  }

  @RequestMapping(value = "helpstudent_right2", method = RequestMethod.GET)
  public String helpstudent_right2(HttpServletRequest request,
                                   HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right2");

  }

  @RequestMapping(value = "helpstudent_right3", method = RequestMethod.GET)
  public String helpstudent_right3(HttpServletRequest request,
                                   HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right3");

  }

  @RequestMapping(value = "helpstudent_right4", method = RequestMethod.GET)
  public String helpstudent_right4(HttpServletRequest request,
                                   HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right4");

  }

  @RequestMapping(value = "helpstudent_right5", method = RequestMethod.GET)
  public String helpstudent_right5(HttpServletRequest request,
                                   HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right5");

  }

  @RequestMapping(value = "helpstudent_right6", method = RequestMethod.GET)
  public String helpstudent_right6(HttpServletRequest request,
                                   HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right6");

  }

  @RequestMapping(value = "helpstudent_right_two", method = RequestMethod.GET)
  public String helpstudent_right_two(HttpServletRequest request,
                                      HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two");

  }

  @RequestMapping(value = "helpstudent_right_two2", method = RequestMethod.GET)
  public String helpstudent_right_two2(HttpServletRequest request,
                                       HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two2");

  }

  @RequestMapping(value = "helpstudent_right_two3", method = RequestMethod.GET)
  public String helpstudent_right_two3(HttpServletRequest request,
                                       HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two3");

  }

  @RequestMapping(value = "helpstudent_right_two4", method = RequestMethod.GET)
  public String helpstudent_right_two4(HttpServletRequest request,
                                       HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two4");

  }

  @RequestMapping(value = "helpstudent_right_two5", method = RequestMethod.GET)
  public String helpstudent_right_two5(HttpServletRequest request,
                                       HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two5");

  }

  @RequestMapping(value = "helpstudent_right_two6", method = RequestMethod.GET)
  public String helpstudent_right_two6(HttpServletRequest request,
                                       HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two6");

  }

  @RequestMapping(value = "helpstudent_right_two7", method = RequestMethod.GET)
  public String helpstudent_right_two7(HttpServletRequest request,
                                       HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two7");

  }

  @RequestMapping(value = "helpstudent_right_two8", method = RequestMethod.GET)
  public String helpstudent_right_two8(HttpServletRequest request,
                                       HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_two8");

  }

  @RequestMapping(value = "helpstudent_right_three", method = RequestMethod.GET)
  public String helpstudent_right_three(HttpServletRequest request,
                                        HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_three");

  }

  @RequestMapping(value = "helpstudent_right_three2", method = RequestMethod.GET)
  public String helpstudent_right_three2(HttpServletRequest request,
                                         HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_three2");

  }

  @RequestMapping(value = "helpstudent_right_three3", method = RequestMethod.GET)
  public String helpstudent_right_three3(HttpServletRequest request,
                                         HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_three3");

  }

  @RequestMapping(value = "helpstudent_right_three4", method = RequestMethod.GET)
  public String helpstudent_right_three4(HttpServletRequest request,
                                         HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_three4");

  }

  @RequestMapping(value = "helpstudent_right_three5", method = RequestMethod.GET)
  public String helpstudent_right_three5(HttpServletRequest request,
                                         HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_three5");

  }

  @RequestMapping(value = "helpstudent_right_three6", method = RequestMethod.GET)
  public String helpstudent_right_three6(HttpServletRequest request,
                                         HttpServletResponse response, Model model) {

    return getView("helpers/helpstudent_right_three6");

  }


  @RequestMapping(value = "aboutus", method = RequestMethod.GET)
  public String aboutus(HttpServletRequest request,
                        HttpServletResponse response, Model model) {
    return getView("aboutus/aboutus");

  }

  @RequestMapping(value = "aboutus_two", method = RequestMethod.GET)
  public String aboutus_two(HttpServletRequest request,
                            HttpServletResponse response, Model model) {
    return getView("aboutus/aboutus_two");

  }

  @RequestMapping(value = "aboutus_three", method = RequestMethod.GET)
  public String aboutus_three(HttpServletRequest request,
                              HttpServletResponse response, Model model) {
    return getView("aboutus/aboutus_three");

  }

  @RequestMapping(value = "serve", method = RequestMethod.GET)
  public String serve(HttpServletRequest request,
                      HttpServletResponse response, Model model) {
    return getView("serve/serve");

  }

  @RequestMapping(value = "serve_two", method = RequestMethod.GET)
  public String serve_two(HttpServletRequest request,
                          HttpServletResponse response, Model model) {
    return getView("serve/serve_two");

  }

  @RequestMapping(value = "serve_three", method = RequestMethod.GET)
  public String serve_three(HttpServletRequest request,
                            HttpServletResponse response, Model model) {
    return getView("serve/serve_three");

  }

  @RequestMapping(value = "serve_four", method = RequestMethod.GET)
  public String serve_four(HttpServletRequest request,
                           HttpServletResponse response, Model model) {
    return getView("serve/serve_four");

  }

  @RequestMapping(value = "news", method = RequestMethod.GET)
  public String news(HttpServletRequest request,
                     HttpServletResponse response, Model model) {
    return getView("news/news");

  }

}
