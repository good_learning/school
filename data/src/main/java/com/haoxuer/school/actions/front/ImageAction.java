package com.haoxuer.school.actions.front;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.web.utils.FileBackup;

@Controller
@RequestMapping(value = "image")
public class ImageAction extends BaseAction {

  @RequestMapping(value = "upimg", method = RequestMethod.POST)
  @ResponseBody
  public String upimg(
      @RequestParam(value = "file", required = false) MultipartFile file,
      HttpServletRequest request, ModelMap model) {

    String origName = file.getOriginalFilename();
    String ext = FilenameUtils.getExtension(origName).toLowerCase(
        Locale.ENGLISH);
    System.out.println(origName);

    return "{'success', 'true'}";
  }

  @RequestMapping(value = "upfile", method = RequestMethod.POST)
  @ResponseBody
  public Object upfile(
      @RequestParam(value = "imgFile", required = false) MultipartFile file,
      HttpServletRequest request, ModelMap model) {
    /**
     * KindEditor JSP
     *
     * 本JSP程序是演示程序，建议不要直接在实际项目中使用。 如果您确定直接使用本程序，使用之前请仔细确认相关安全设置。
     *
     */

    // 文件保存目录路径
    String savePath = request.getServletContext().getRealPath("/")
        + "attached/";

    // 文件保存目录URL
    // String saveUrl = request.getContextPath() + "/attached/";
    // 文件保存目录URL
    String saveUrl = "/attached/";
    // 定义允许上传的文件扩展名
    HashMap<String, String> extMap = new HashMap<String, String>();
    extMap.put("image", "gif,jpg,jpeg,png,bmp");
    extMap.put("flash", "swf,flv");
    extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
    extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");

    // 最大文件大小
    long maxSize = 1000000000;

    if (!ServletFileUpload.isMultipartContent(request)) {
      return getError("请选择文件。");
    }
    // 检查目录
    File uploadDir = new File(savePath);
    if (!uploadDir.exists()) {
      uploadDir.mkdirs();
    }
    if (!uploadDir.isDirectory()) {
      return getError("上传目录不存在。");
    }
    // 检查目录写权限
    if (!uploadDir.canWrite()) {
      return getError("上传目录没有写权限。");
    }

    String dirName = request.getParameter("dir");
    if (dirName == null) {
      dirName = "image";
    }
    if (!extMap.containsKey(dirName)) {
      return (getError("目录名不正确。"));
    }
    // 创建文件夹
    savePath += dirName + "/";
    saveUrl += dirName + "/";
    File saveDirFile = new File(savePath);
    if (!saveDirFile.exists()) {
      saveDirFile.mkdirs();
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String ymd = sdf.format(new Date());
    savePath += ymd + "/";
    saveUrl += ymd + "/";
    File dirFile = new File(savePath);
    if (!dirFile.exists()) {
      dirFile.mkdirs();
    }

    if (file != null) {
      String fileName = file.getOriginalFilename();
      long fileSize = file.getSize();
      // 检查文件大小
      if (file.getSize() > maxSize) {
        return getError("上传文件大小超过限制。");
      }
      // 检查扩展名
      String fileExt = FilenameUtils.getExtension(fileName).toLowerCase(
          Locale.ENGLISH);
      if (!Arrays.asList(extMap.get(dirName).split(","))
          .contains(fileExt)) {
        return getError("上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName)
            + "格式。");
      }

      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      String newFileName = df.format(new Date()) + "_"
          + new Random().nextInt(1000) + "." + fileExt;
      try {
        File uploadedFile = new File(savePath, newFileName);
        // file.g
        FileUtils.writeByteArrayToFile(uploadedFile, file.getBytes());
        // item.write(uploadedFile);
      } catch (Exception e) {
        return getError("上传文件失败。");
      }
      Upitem itemss = new Upitem();
      itemss.setError(0);
      itemss.setUrl(saveUrl + newFileName);
      return itemss;
    }
    return "{'success', 'true'}";
  }

  @RequestMapping(value = "upimgwork", method = RequestMethod.POST)
  public String upimgwork(
      @RequestParam(value = "imgFile", required = false) MultipartFile file,
      HttpServletRequest request, ModelMap model) {
    return cccc(file, request, model, "/attached/");
  }

  @RequestMapping(value = "upremoteimgwork", method = RequestMethod.POST)
  public String upremoteimgwork(
      @RequestParam(value = "imgFile", required = false) MultipartFile file,
      HttpServletRequest request, ModelMap model) {
    return cccc(file, request, model, "http://www.91mydoor.com/attached/");
  }

  @RequestMapping(value = "upmendaoimgwork", method = RequestMethod.POST)
  public String upmendaoimgwork(
      @RequestParam(value = "imgFile", required = false) MultipartFile file,
      HttpServletRequest request, ModelMap model) {
    return cccc(file, request, model, "http://www.91mydoor.com/attached/");
  }

  protected String cccc(MultipartFile file, HttpServletRequest request,
                        ModelMap model, String saveurls) {
    /**
     * KindEditor JSP
     *
     * 本JSP程序是演示程序，建议不要直接在实际项目中使用。 如果您确定直接使用本程序，使用之前请仔细确认相关安全设置。
     *
     */

    // 文件保存目录路径
    String savePath = request.getServletContext().getRealPath("/")
        + "/attached/";

    // 文件保存目录URL
    // String saveUrl = request.getContextPath() + "/attached/";
    // 文件保存目录URL
    String saveUrl = saveurls;
    // 定义允许上传的文件扩展名
    HashMap<String, String> extMap = new HashMap<String, String>();
    extMap.put("image", "gif,jpg,jpeg,png,bmp");
    extMap.put("flash", "swf,flv");
    extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
    extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");

    // 最大文件大小
    long maxSize = 1000000000;

    if (!ServletFileUpload.isMultipartContent(request)) {
      return getErrorString("请选择文件。", model);
    }
    // 检查目录
    File uploadDir = new File(savePath);
    if (!uploadDir.exists()) {
      uploadDir.mkdirs();
    }
    if (!uploadDir.isDirectory()) {
      return getErrorString("上传目录不存在。", model);
    }
    // 检查目录写权限
    if (!uploadDir.canWrite()) {
      return getErrorString("上传目录没有写权限。", model);
    }

    String dirName = request.getParameter("dir");
    if (dirName == null) {
      dirName = "image";
    }
    if (!extMap.containsKey(dirName)) {
      return (getErrorString("目录名不正确。", model));
    }
    // 创建文件夹
    savePath += dirName + "/";
    saveUrl += dirName + "/";
    File saveDirFile = new File(savePath);
    if (!saveDirFile.exists()) {
      saveDirFile.mkdirs();
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String ymd = sdf.format(new Date());
    savePath += ymd + "/";
    saveUrl += ymd + "/";
    File dirFile = new File(savePath);
    if (!dirFile.exists()) {
      dirFile.mkdirs();
    }

    if (file != null) {
      String fileName = file.getOriginalFilename();
      // 检查文件大小
      if (file.getSize() > maxSize) {
        return getErrorString("上传文件大小超过限制。", model);
      }
      // 检查扩展名
      String fileExt = FilenameUtils.getExtension(fileName).toLowerCase(
          Locale.ENGLISH);
      if (!Arrays.asList(extMap.get(dirName).split(","))
          .contains(fileExt)) {
        return getErrorString(
            "上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。",
            model);
      }

      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      String newFileName = df.format(new Date()) + "_"
          + new Random().nextInt(1000) + "." + fileExt;
      int imgWidth = 0, imgHeight = 0; // 用于上传图片时计算图片大小
      try {
        File uploadedFile = new File(savePath, newFileName);

        // file.g
        FileUtils.writeByteArrayToFile(uploadedFile, file.getBytes());
        FileBackup.BackupFile(uploadedFile);
        /*
         * 以下代码用于上传图片时计算图片大小
         */
        try {
          int[] imgWH = getImageWithAndHeight(uploadedFile);
          imgWidth = imgWH[0];
          imgHeight = imgWH[1];
        } catch (FileNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        // item.write(uploadedFile);
      } catch (Exception e) {
        return getErrorString("上传文件失败。", model);
      }
      Upitem itemss = new Upitem();
      itemss.setError(0);
      itemss.setUrl(saveUrl + newFileName);
      itemss.setWidth(String.valueOf(imgWidth)); // 用于上传图片时返回图片大小
      itemss.setHeight(String.valueOf(imgHeight));// 用于上传图片时返回图片大小
      Gson gson = new Gson();
      model.addAttribute("msg", gson.toJson(itemss));
      return "common/text";
    }
    return "{'success', 'true'}";
  }

  protected String upfile(MultipartFile file, HttpServletRequest request,
                          ModelMap model, String saveurls) {
    String result = "";
    // 文件保存目录路径
    String savePath = request.getServletContext().getRealPath("/")
        + "/attached/";

    // 文件保存目录URL
    // String saveUrl = request.getContextPath() + "/attached/";
    // 文件保存目录URL
    String saveUrl = saveurls;
    // 定义允许上传的文件扩展名
    HashMap<String, String> extMap = new HashMap<String, String>();
    extMap.put("image", "gif,jpg,jpeg,png,bmp");
    extMap.put("flash", "swf,flv");
    extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
    extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");

    // 最大文件大小
    long maxSize = 1000000000;

    if (!ServletFileUpload.isMultipartContent(request)) {
      return getErrorString("请选择文件。", model);
    }
    // 检查目录
    File uploadDir = new File(savePath);
    if (!uploadDir.exists()) {
      uploadDir.mkdirs();
    }
    if (!uploadDir.isDirectory()) {
      return getErrorString("上传目录不存在。", model);
    }
    // 检查目录写权限
    if (!uploadDir.canWrite()) {
      return getErrorString("上传目录没有写权限。", model);
    }

    String dirName = request.getParameter("dir");
    if (dirName == null) {
      dirName = "image";
    }
    if (!extMap.containsKey(dirName)) {
      return (getErrorString("目录名不正确。", model));
    }
    // 创建文件夹
    savePath += dirName + "/";
    saveUrl += dirName + "/";
    File saveDirFile = new File(savePath);
    if (!saveDirFile.exists()) {
      saveDirFile.mkdirs();
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String ymd = sdf.format(new Date());
    savePath += ymd + "/";
    saveUrl += ymd + "/";
    File dirFile = new File(savePath);
    if (!dirFile.exists()) {
      dirFile.mkdirs();
    }

    if (file != null) {
      String fileName = file.getOriginalFilename();
      // 检查文件大小
      if (file.getSize() > maxSize) {
        return getErrorString("上传文件大小超过限制。", model);
      }
      // 检查扩展名
      String fileExt = FilenameUtils.getExtension(fileName).toLowerCase(
          Locale.ENGLISH);
      if (!Arrays.asList(extMap.get(dirName).split(","))
          .contains(fileExt)) {
        return getErrorString(
            "上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。",
            model);
      }

      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      String newFileName = df.format(new Date()) + "_"
          + new Random().nextInt(1000) + "." + fileExt;
      try {
        File uploadedFile = new File(savePath, newFileName);
        // file.g
        FileUtils.writeByteArrayToFile(uploadedFile, file.getBytes());
        FileBackup.BackupFile(uploadedFile);
        result = saveUrl + newFileName;
      } catch (Exception e) {
        return getErrorString("上传文件失败。", model);
      }
      return result;
    } else {
      return result;
    }
  }

  private Upinfo getError(String message) {
    Upinfo info = new Upinfo();
    info.setError(1);
    info.setMessage(message);
    return info;
  }

  private String getErrorString(String message, ModelMap model) {
    Upinfo info = new Upinfo();
    info.setError(1);
    info.setMessage(message);
    Gson gson = new Gson();
    model.addAttribute("msg", gson.toJson(info));

    return "common/text";
  }

  public class Upinfo {

    private int error;
    private String message;

    public int getError() {
      return error;
    }

    public void setError(int error) {
      this.error = error;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }

  public static class Upitem {

    private int error;
    private String url;

    private String width;
    private String height;

    public String getWidth() {
      return width;
    }

    public void setWidth(String width) {
      this.width = width;
    }

    public String getHeight() {
      return height;
    }

    public void setHeight(String height) {
      this.height = height;
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public int getError() {
      return error;
    }

    public void setError(int error) {
      this.error = error;
    }

  }

  @RequestMapping(value = "upheadimage", method = RequestMethod.POST)
  public String uploadimgwork(
      @RequestParam(value = "imgFile", required = false) MultipartFile file,
      HttpServletRequest request, ModelMap model) {
    return uploadimage(file, request, model, 300, 300);
  }

  @RequestMapping(value = "upcourseimage", method = RequestMethod.POST)
  public String uploadcourseimgwork(
      @RequestParam(value = "imgFile", required = false) MultipartFile file,
      HttpServletRequest request, ModelMap model) {
    return uploadimage(file, request, model, 400, 300);
  }

  protected String uploadimage(MultipartFile file,
                               HttpServletRequest request, ModelMap model,
                               int maxHeight, int maxWidth) {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String ymd = sdf.format(new Date());

    // 服务器中img保存路径
    String basePath = request.getServletContext().getRealPath("/");
    String savePath = "/attached/image/" + ymd + "/";


    // 检查img目录
    File dir = new File(basePath + savePath);
    if (!dir.exists()) {
      dir.mkdirs();
    }

    // 定义允许上传的文件扩展名
    HashMap<String, String> extMap = new HashMap<String, String>();
    extMap.put("image", "gif,jpg,jpeg,png,bmp");

    // 最大文件大小
    long maxSize = 1000000000;

    if (!ServletFileUpload.isMultipartContent(request)) {
      return getErrorString("请选择文件。", model);
    }

    if (file != null) {
      String fileName = file.getOriginalFilename();
      // 检查文件大小
      if (file.getSize() > maxSize) {
        return getErrorString("上传文件大小超过限制。", model);
      }
      // 检查扩展名
      String fileExt = FilenameUtils.getExtension(fileName).toLowerCase(
          Locale.ENGLISH);
      if (!Arrays.asList(extMap.get("image").split(","))
          .contains(fileExt)) {
        return getErrorString(
            "上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get("image") + "格式。",
            model);
      }

      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      String newFileName = df.format(new Date()) + "_"
          + new Random().nextInt(1000) + "." + fileExt;
      int imgWidth = 0, imgHeight = 0; // 用于上传图片时计算图片大小
      try {

        // 获取图片长宽
        BufferedImage sourceImg = ImageIO.read(file.getInputStream());
        imgWidth = sourceImg.getWidth();
        imgHeight = sourceImg.getHeight();

        if (imgHeight > maxHeight || imgWidth > maxWidth) {
          // 临时文件保存路径. 需要裁剪
          savePath = "/attached/tempimage/" + ymd + "/";

          // 检查临时文件目录
          dir = new File(basePath + savePath);
          if (!dir.exists()) {
            dir.mkdirs();
          }
        }
        File uploadedFile = new File(basePath + savePath, newFileName);
        // file.g
        FileUtils.writeByteArrayToFile(uploadedFile, file.getBytes());

      } catch (Exception e) {
        return getErrorString("上传文件失败。", model);
      }
      Upitem itemss = new Upitem();
      itemss.setError(0);
      itemss.setUrl(savePath + newFileName);
      itemss.setWidth(String.valueOf(imgWidth)); // 用于上传图片时返回图片大小
      itemss.setHeight(String.valueOf(imgHeight));// 用于上传图片时返回图片大小
      Gson gson = new Gson();
      model.addAttribute("msg", gson.toJson(itemss));
      return "common/text";
    }
    return "{'success', 'true'}";
  }

  @RequestMapping(value = "loadimgcut", method = RequestMethod.GET)
  public String loadimgcut(
      @RequestParam(value = "imgpath", required = true) String imgpath,
      @RequestParam(value = "rwidth", required = true) String rwidth,
      @RequestParam(value = "rheight", required = true) String rheight,
      @RequestParam(value = "swidth", required = true) String swidth,
      @RequestParam(value = "sheight", required = true) String sheight,
      HttpServletRequest request, ModelMap model) {
    model.addAttribute("imgurl", imgpath);
    model.addAttribute("ratio", rwidth + ':' + rheight);
    model.addAttribute("swidth", swidth);
    model.addAttribute("sheight", sheight);
    return "common/imgcut";
    // return cccc(file, request, model,
    // "http://www.91mydoor.com/attached/");
  }

  @SuppressWarnings("unchecked")
  @RequestMapping(value = "filemanager", method = RequestMethod.GET)
  public String filemanager(
      @RequestParam(value = "filetype", required = false) String filetype,
      HttpServletRequest request, ModelMap model) {
    /*
     * String siteurl = request.getScheme() + "://"; siteurl +=
     * request.getHeader("host"); siteurl +=
     * request.getServletContext().getContextPath() + "/" + "images/";
     */

    // 根目录路径，可以指定绝对路径，比如 /var/www/attached/
    String rootPath = request.getServletContext().getRealPath("/")
        + "/attached/";
    // 根目录URL，可以指定绝对路径，比如 http://www.yoursite.com/attached/
    String rootUrl = request.getContextPath() + "/attached/";
    // 图片扩展名
    String[] fileTypes = new String[]{"gif", "jpg", "jpeg", "png", "bmp"};

    String dirName = request.getParameter("dir");
    if (dirName != null) {
      /*
       * if(!Arrays.<String>asList(new String[]{"image", "flash", "media",
       * "file"}).contains(dirName)){
       * out.println("Invalid Directory name."); return; }
       */

      int mbrType = 1;
      Member member = (Member) getSession(request).getAttribute("member");
      if (member != null) {
        //mbrType = member.getMembertype().getId();
      }
      rootPath += dirName + "/";
      rootUrl += dirName + "/";
      if (1 == mbrType) {
        rootPath += "studenticon/";
        rootUrl += "studenticon/";
      } else if (2 == mbrType) {
        rootPath += "teachericon/";
        rootUrl += "teachericon/";
      } else {
        rootPath += "schoolicon/";
        rootUrl += "schoolicon/";
      }
      File saveDirFile = new File(rootPath);
      if (!saveDirFile.exists()) {
        saveDirFile.mkdirs();
      }
    }
    // 根据path参数，设置各路径和URL
    String path = request.getParameter("path") != null ? request
        .getParameter("path") : "";
    String currentPath = rootPath + path;
    String currentUrl = rootUrl + path;
    String currentDirPath = path;
    String moveupDirPath = "";
    if (!"".equals(path)) {
      String str = currentDirPath.substring(0,
          currentDirPath.length() - 1);
      moveupDirPath = str.lastIndexOf("/") >= 0 ? str.substring(0,
          str.lastIndexOf("/") + 1) : "";
    }

    // 排序形式，name or size or type
    String order = request.getParameter("order") != null ? request
        .getParameter("order").toLowerCase() : "name";

    // 不允许使用..移动到上一级目录

    if (path.indexOf("..") >= 0) {
      // out.println("Access is not allowed.");
      // return;
    }

    // 最后一个字符不是/
    /*
     * if (!"".equals(path) && !path.endsWith("/")) {
     * out.println("Parameter is not valid."); return; }
     */
    // 目录不存在或不是目录
    File currentPathFile = new File(currentPath);
    /*
     * if(!currentPathFile.isDirectory()){
     * out.println("Directory does not exist."); return; }
     */

    // 遍历目录取的文件信息
    List<Hashtable> fileList = new ArrayList<Hashtable>();
    if (currentPathFile.listFiles() != null) {
      for (File file : currentPathFile.listFiles()) {
        Hashtable<String, Object> hash = new Hashtable<String, Object>();
        String fileName = file.getName();
        if (file.isDirectory()) {
          hash.put("is_dir", true);
          hash.put("has_file", (file.listFiles() != null));
          hash.put("filesize", 0L);
          hash.put("is_photo", false);
          hash.put("filetype", "");
        } else if (file.isFile()) {
          String fileExt = fileName.substring(
              fileName.lastIndexOf(".") + 1).toLowerCase();
          hash.put("is_dir", false);
          hash.put("has_file", false);
          hash.put("filesize", file.length());
          hash.put("is_photo", Arrays.asList(fileTypes)
              .contains(fileExt));
          hash.put("filetype", fileExt);
        }
        hash.put("filename", fileName);
        hash.put("datetime",
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file
                .lastModified()));
        fileList.add(hash);
      }
    }

    if ("size".equals(order)) {
      Collections.sort(fileList, new SizeComparator());
    } else if ("type".equals(order)) {
      Collections.sort(fileList, new TypeComparator());
    } else {
      Collections.sort(fileList, new NameComparator());
    }
    FileMangament result = new FileMangament();
    result.setMoveup_dir_path(moveupDirPath);
    result.setCurrent_dir_path(currentDirPath);
    result.setCurrent_url(currentUrl);
    result.setTotal_count(fileList.size());
    result.setFile_list(fileList);

    /*
     * response.setContentType("application/json; charset=UTF-8");
     * out.println(result.toJSONString());
     */
    Gson gson = new Gson();
    model.addAttribute("msg", gson.toJson(result));
    return "common/text";
  }

  public class FileMangament {
    private String moveup_dir_path;

    public String getMoveup_dir_path() {
      return moveup_dir_path;
    }

    public void setMoveup_dir_path(String moveup_dir_path) {
      this.moveup_dir_path = moveup_dir_path;
    }

    public String getCurrent_dir_path() {
      return current_dir_path;
    }

    public void setCurrent_dir_path(String current_dir_path) {
      this.current_dir_path = current_dir_path;
    }

    public String getCurrent_url() {
      return current_url;
    }

    public void setCurrent_url(String current_url) {
      this.current_url = current_url;
    }

    public int getTotal_count() {
      return total_count;
    }

    public void setTotal_count(int total_count) {
      this.total_count = total_count;
    }

    public List<Hashtable> getFile_list() {
      return file_list;
    }

    public void setFile_list(List<Hashtable> file_list) {
      this.file_list = file_list;
    }

    private String current_dir_path;
    private String current_url;
    private int total_count;
    private List<Hashtable> file_list;
  }

  @RequestMapping(value = "savingimgcut", method = RequestMethod.POST)
  public String saveimgcut(
      @RequestParam(value = "imgpath", required = true) String imgpath,
      @RequestParam(value = "scale", required = true, defaultValue = "1") String scale,
      @RequestParam(value = "ratio", required = true, defaultValue = "1:1") String ratio,
      @RequestParam(value = "x1", required = true) String x1,
      @RequestParam(value = "y1", required = true) String y1,
      @RequestParam(value = "x2", required = true) String x2,
      @RequestParam(value = "y2", required = true) String y2,
      HttpServletRequest request, ModelMap model) {
    String filePath = request.getServletContext().getRealPath("/");
    String fileName = imgpath;// .replaceAll("/", "\\\\");

    int px1 = Integer.parseInt(x1);
    px1 = (int) (px1 / Double.parseDouble(scale));
    int px2 = Integer.parseInt(x2);
    px2 = (int) (px2 / Double.parseDouble(scale));

    int diff = px2 - px1;

    int py1 = Integer.parseInt(y1);
    py1 = (int) (py1 / Double.parseDouble(scale));
    int py2 = py1 + diff;
    if (ratio.equals("1:1") == false) {
      py2 = (int) (Integer.parseInt(y2) / Double.parseDouble(scale));
    }
    // int py2 = Integer.parseInt(y2);
    // py2 = (int) (py2 / Double.parseDouble(scale));
    fileName = imageCut(filePath, fileName, "jpg", new int[]{px1, py1,
        px2, py2}, 1f);

    model.addAttribute("msg", fileName);
    return "common/text";
  }

  private int[] getImageWithAndHeight(File uploadedFile)
      throws IOException {
    int[] imgWH = new int[2];
    BufferedImage sourceImg = ImageIO
        .read(new FileInputStream(uploadedFile));
    imgWH[0] = sourceImg.getWidth();
    imgWH[1] = sourceImg.getHeight();
    return imgWH;
  }

  private String imageCut(String filePath, String fileName, String fileExt,
                          float scale, float quality) {

    try {
      Thumbnails.of(filePath + fileName).scale(1f).outputQuality(0.28f)
          .outputFormat(fileExt).toFile(filePath + fileName);
    } catch (FileNotFoundException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    return fileName;
  }

  private String imageCut(String filePath, String fileName, String fileExt,
                          int[] region,
                          float quality) {

    String newFileName = fileName.replaceAll("/tempimage/", "/image/");
    try {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      String ymd = sdf.format(new Date());

      Thumbnails
          .of(filePath + fileName)
          .scale(1f)
          .sourceRegion(region[0], region[1], region[2] - region[0],
              region[3] - region[1]).outputQuality(quality)
          .outputFormat(fileExt).toFile(filePath + newFileName);
    } catch (FileNotFoundException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    return newFileName;
  }

  /**
   * KindEditor JSP
   * <p>
   * 本JSP程序是演示程序，建议不要直接在实际项目中使用。 如果您确定直接使用本程序，使用之前请仔细确认相关安全设置。
   */

  /*
   * @RequestMapping(value = "filemanager", method = RequestMethod.POST)
   *
   * @ResponseBody public String filemanager(HttpServletRequest request,
   * ModelMap model) { // 根目录路径，可以指定绝对路径，比如 /var/www/attached/ String rootPath
   * = request.getServletContext().getRealPath("/") + "attached/"; //
   * 根目录URL，可以指定绝对路径，比如 http://www.yoursite.com/attached/ String rootUrl =
   * request.getContextPath() + "/attached/"; // 图片扩展名 String[] fileTypes =
   * new String[] { "gif", "jpg", "jpeg", "png", "bmp" }; String dirName =
   * request.getParameter("dir"); if (dirName != null) { if (!Arrays.<String>
   * asList( new String[] { "image", "flash", "media", "file" })
   * .contains(dirName)) { return "Invalid Directory name."; } rootPath +=
   * dirName + "/"; rootUrl += dirName + "/"; File saveDirFile = new
   * File(rootPath); if (!saveDirFile.exists()) { saveDirFile.mkdirs(); } } //
   * 根据path参数，设置各路径和URL String path = request.getParameter("path") != null ?
   * request .getParameter("path") : ""; String currentPath = rootPath + path;
   * String currentUrl = rootUrl + path; String currentDirPath = path; String
   * moveupDirPath = ""; if (!"".equals(path)) { String str =
   * currentDirPath.substring(0, currentDirPath.length() - 1); moveupDirPath =
   * str.lastIndexOf("/") >= 0 ? str.substring(0, str.lastIndexOf("/") + 1) :
   * ""; } // 排序形式，name or size or type String order =
   * request.getParameter("order") != null ? request
   * .getParameter("order").toLowerCase() : "name"; // 不允许使用..移动到上一级目录 if
   * (path.indexOf("..") >= 0) { return "Access is not allowed."; } //
   * 最后一个字符不是/ if (!"".equals(path) && !path.endsWith("/")) { return
   * "Parameter is not valid."; } // 目录不存在或不是目录 File currentPathFile = new
   * File(currentPath); if (!currentPathFile.isDirectory()) { return
   * "Directory does not exist."; } // 遍历目录取的文件信息 List<Hashtable> fileList =
   * new ArrayList<Hashtable>(); if (currentPathFile.listFiles() != null) {
   * for (File file : currentPathFile.listFiles()) { Hashtable<String, Object>
   * hash = new Hashtable<String, Object>(); String fileName = file.getName();
   * if (file.isDirectory()) { hash.put("is_dir", true); hash.put("has_file",
   * (file.listFiles() != null)); hash.put("filesize", 0L);
   * hash.put("is_photo", false); hash.put("filetype", ""); } else if
   * (file.isFile()) { String fileExt = fileName.substring(
   * fileName.lastIndexOf(".") + 1).toLowerCase(); hash.put("is_dir", false);
   * hash.put("has_file", false); hash.put("filesize", file.length());
   * hash.put("is_photo", Arrays.<String> asList(fileTypes)
   * .contains(fileExt)); hash.put("filetype", fileExt); }
   * hash.put("filename", fileName); hash.put("datetime", new
   * SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file .lastModified()));
   * fileList.add(hash); } } if ("size".equals(order)) {
   * Collections.sort(fileList, new SizeComparator()); } else if
   * ("type".equals(order)) { Collections.sort(fileList, new
   * TypeComparator()); } else { Collections.sort(fileList, new
   * NameComparator()); } JSONObject result = new JSONObject(); try {
   * result.put("moveup_dir_path", moveupDirPath);
   * result.put("current_dir_path", currentDirPath); result.put("current_url",
   * currentUrl); result.put("total_count", fileList.size());
   * result.put("file_list", fileList); } catch (JSONException e) { // TODO
   * Auto-generated catch block e.printStackTrace(); }
   *
   * return result.toString();
   *
   * }
   */

  public class NameComparator implements Comparator {
    public int compare(Object a, Object b) {
      Hashtable hashA = (Hashtable) a;
      Hashtable hashB = (Hashtable) b;
      if (((Boolean) hashA.get("is_dir"))
          && !((Boolean) hashB.get("is_dir"))) {
        return -1;
      } else if (!((Boolean) hashA.get("is_dir"))
          && ((Boolean) hashB.get("is_dir"))) {
        return 1;
      } else {
        return ((String) hashA.get("filename"))
            .compareTo((String) hashB.get("filename"));
      }
    }
  }

  public class SizeComparator implements Comparator {
    public int compare(Object a, Object b) {
      Hashtable hashA = (Hashtable) a;
      Hashtable hashB = (Hashtable) b;
      if (((Boolean) hashA.get("is_dir"))
          && !((Boolean) hashB.get("is_dir"))) {
        return -1;
      } else if (!((Boolean) hashA.get("is_dir"))
          && ((Boolean) hashB.get("is_dir"))) {
        return 1;
      } else {
        if (((Long) hashA.get("filesize")) > ((Long) hashB
            .get("filesize"))) {
          return 1;
        } else if (((Long) hashA.get("filesize")) < ((Long) hashB
            .get("filesize"))) {
          return -1;
        } else {
          return 0;
        }
      }
    }
  }

  public class TypeComparator implements Comparator {
    public int compare(Object a, Object b) {
      Hashtable hashA = (Hashtable) a;
      Hashtable hashB = (Hashtable) b;
      if (((Boolean) hashA.get("is_dir"))
          && !((Boolean) hashB.get("is_dir"))) {
        return -1;
      } else if (!((Boolean) hashA.get("is_dir"))
          && ((Boolean) hashB.get("is_dir"))) {
        return 1;
      } else {
        return ((String) hashA.get("filetype"))
            .compareTo((String) hashB.get("filetype"));
      }
    }
  }
}
