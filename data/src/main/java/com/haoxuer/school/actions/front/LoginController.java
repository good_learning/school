/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
 */

package com.haoxuer.school.actions.front;

import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.entity.UserLoginLog;
import com.haoxuer.discover.user.data.enums.LoginState;
import com.haoxuer.discover.user.data.service.UserAccountService;
import com.haoxuer.discover.user.data.service.UserLoginLogService;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import com.haoxuer.discover.web.controller.front.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * 登录页
 *
 * @author langhsu
 */
@Controller
public class LoginController extends BaseController {

  @Autowired
  UserLoginLogService loginLogService;
  @Autowired
  UserAccountService userAccountService;

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String view() {
    return getView(Views.LOGIN);
  }


  /**
   * 提交登录
   *
   * @param username
   * @param password
   * @param model
   * @return
   */
  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String login(String username, String password, ModelMap model) {
    String ret = getView(Views.LOGIN);

    if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
      return ret;
    }

    UsernamePasswordToken token = new UsernamePasswordToken(username,
        password);
    token.setRememberMe(true);
    if (token == null) {
      model.put("message", "用户名或密码错误");
      return ret;
    }

    try {
      SecurityUtils.getSubject().login(token);

      ret = Views.REDIRECT_HOME;

      // 更新消息数量
      // pushBadgesCount();
    } catch (AuthenticationException e) {
      if (e instanceof UnknownAccountException) {
        model.put("message", "用户不存在");
      } else if (e instanceof LockedAccountException) {
        model.put("message", "用户被禁用");
      } else {
        model.put("message", "用户认证失败");
      }
    }

    return ret;
  }

  @RequestMapping(value = "/loginok", method = RequestMethod.GET)
  public String loginok(HttpServletRequest request) {
    UserInfo user = UserUtil.getCurrentUser();
    if (user != null) {
      UserLoginLog bean = new UserLoginLog();
      try {
        bean.setIp(getIpAddr(request));
      } catch (Exception e) {
        e.printStackTrace();
      }
      bean.setUser(user);
      bean.setState(LoginState.success);
      loginLogService.save(bean);
    }
    if (SecurityUtils.getSubject().isAuthenticated()) {
      return "redirect:/admin/index.htm";
    } else {
      return getView("login");
    }

  }


  /**
   * 退出登录
   *
   * @return
   */
  @RequestMapping("/signout")
  public String signout() {
    SecurityUtils.getSubject().logout();
    return "redirect:/login.htm";
  }

}
