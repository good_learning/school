package com.haoxuer.school.actions.front;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haoxuer.school.data.entity.City;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.CityService;
import com.haoxuer.school.data.service.MemberService;
import com.haoxuer.school.data.service.ProvinceService;
import com.haoxuer.school.data.service.TownService;

@Controller
@RequestMapping(value = "member")
public class MemberController extends BaseAction {

  private String DEFAULT_ICON_MALE = "images/male.jpg";
  private String DEFAULT_ICON_FEMALE = "images/female.jpg";

  @Autowired
  MemberService memberService;

  @Autowired
  ProvinceService provinceService;


  @Autowired
  CityService cityService;

  @Autowired
  TownService townService;

  @RequestMapping(value = "modifypasswordpre", method = RequestMethod.GET)
  public String modifypasswordpre() {
    return "user/modifypassword";
  }

  //证照、视频添加
  @RequestMapping(value = "addvideos", method = RequestMethod.GET)
  public String addvideos() {
    return "user/addvideos";
  }

  @RequestMapping(value = "modifypassword", method = RequestMethod.POST)
  public String modifypassword(String password, String oldpassword,
                               HttpServletRequest request, HttpServletResponse response,
                               Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      return "common/text";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "modifyuserinfopre", method = RequestMethod.GET)
  public String modifyuserinfopre(HttpServletRequest request,
                                  HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member temp;
    Object member = session.getAttribute("member");
    if (member != null) {
      if (member instanceof Member) {
        model.addAttribute("member", member);
        model.addAttribute("provinces", provinceService.all());
        temp = (Member) member;
//        if (temp.getCity() != null) {
//          temp.getCity().getProvince();
//          int pid = ((Member) member).getCity().getProvince().getId();
//          model.addAttribute("citys", cityService.findByProvince(pid));
//
//          model.addAttribute("towns", townService.findByCity(temp.getCity().getId()));
//
//        } else {
//          model.addAttribute("towns", townService.findByCity(1101));
//        }

      } else {
        return login;
      }
    } else {
      return login;

    }
//    if (temp.getMembertype().getId() == 1) {
//      return "user/modifystudentinfo";
//    } else if (temp.getMembertype().getId() == 2) {
//      return "user/modifyuserinfo";
//    } else if (temp.getMembertype().getId() == 3) {
//      return "user/modifyschoolinfo";
//    } else {
//      return "user/modifyuserinfo";
//    }

    return login;
  }

  @RequestMapping(value = "upheadimg")
  public String modifyUserHeadimg(int id, String imgpath,
                                  HttpServletRequest request, HttpServletResponse response,
                                  Model model) {
//    Member member = memberService.findById(id);
//    if (member != null) {
//      String oldimg = member.getHeadimg();
//      member.setHeadimg(imgpath);
//      memberService.update(member);
//      HttpSession session = getSession(request);
//      Member curUser = (Member) session.getAttribute("member");
//      curUser.setHeadimg(member.getHeadimg());
//      session.setAttribute("member", curUser);
//      try {
//        if (oldimg != null && !oldimg.equals("")
//            && !oldimg.endsWith(DEFAULT_ICON_MALE)
//            && !oldimg.endsWith(DEFAULT_ICON_FEMALE)) {
//          String path = request.getServletContext().getRealPath("/");
//          java.io.File oldfile = new java.io.File(path + oldimg);
//          if (oldfile.exists() && oldfile.isFile()) {
//            oldfile.delete();
//          }
//        }
//      } finally {
//
//      }
//    }

    model.addAttribute("msg", "OK");
    return "common/text";
  }

  @RequestMapping(value = "findbyprovince", method = RequestMethod.GET)
  public @ResponseBody
  List<City> findByProvince(HttpServletRequest request,
                            HttpServletResponse response, Model model, int pid) {
    List<City> result = cityService.findByProvince(pid);
    return result;
  }


  @RequestMapping(value = "modifyuserinfo", method = RequestMethod.POST)
  public String modifyuserinfo(Member m, String date,
                               HttpServletRequest request, HttpServletResponse response,
                               Model model) {
    HttpSession session = getSession(request);
    Object member = session.getAttribute("member");
    if (member != null) {
      if (member instanceof Member) {

        try {
          DateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日");
          Member me = (Member) member;
          try {
//						m.setBirthday(new Timestamp(dateFormat.parse(date)
//								.getTime()));
          } catch (Exception e) {
            e.printStackTrace();
          }
          m.setId(me.getId());
          //m.setUsername(me.getUsername());
          memberService.update(m);
          m = memberService.findById(m.getId());
          session.setAttribute("member", m);
          model.addAttribute("msg", "修改个人信息成功");
        } catch (Exception e) {
          e.printStackTrace();
          model.addAttribute("msg", "修改个人信息失败");
        }

        return "common/text";

      } else {
        return login;
      }
    } else {
      return login;

    }


  }

}
