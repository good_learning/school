package com.haoxuer.school.actions.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.FriendService;
import com.haoxuer.school.data.service.MemberService;
import com.haoxuer.school.data.service.MemberVisitService;

@Controller
@RequestMapping(value = "memberfront")
public class MemberFrontAction extends BaseAction {

  @Autowired
  MemberService memberService;

  @Autowired
  FriendService friendService;

  @Autowired
  MemberVisitService memberVisitService;

  @RequestMapping(value = "index", method = RequestMethod.GET)
  public String index(Long id, HttpServletRequest request,
                      HttpServletResponse response, Model model) {

    HttpSession session = getSession(request);
    Member sessionmember = (Member) session.getAttribute("member");
    if (sessionmember != null) {
     // memberVisitService.visit(id, sessionmember.getId());
    }
    Member member = memberService.findById(id);
    if (member == null) {
      return "common/404";

    } else {
      model.addAttribute("memberfront", member);

      Member smember = getmember(request);
      if (smember != null) {
//        model.addAttribute("isfollow",
//            friendService.isfollows(smember.getId(), id));
      } else {
        model.addAttribute("isfollow", 0);
      }
      return "memberfront/index";

    }

  }


  /**
   * @param id
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "up", method = RequestMethod.GET)
  public String up(@RequestParam(value = "id", required = true) int id,
                   HttpServletRequest request, HttpServletResponse response,
                   Model model) {
    try {
     // memberService.up(id);
      model.addAttribute("msg", "点赞成功");
    } catch (Exception e) {
      model.addAttribute("msg", "点赞失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "follow", method = RequestMethod.GET)
  public String follow(@RequestParam(value = "id", required = true) int id,
                       HttpServletRequest request, HttpServletResponse response,
                       Model model) {
    try {
      Member smember = getmember(request);
      if (smember != null) {
//        int type = friendService.follows(smember.getId(), id);
//        if (type == 1) {
//          model.addAttribute("msg", "关注成功");
//        } else {
//          model.addAttribute("msg", "你已关注了");
//        }

      } else {
        model.addAttribute("msg", "你还没登陆");
      }
    } catch (Exception e) {
      model.addAttribute("msg", "关注失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "fav", method = RequestMethod.GET)
  public String fav(@RequestParam(value = "id", required = true) int id,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {
    try {
      Member smember = getmember(request);
      if (smember != null) {
//        int type = friendService.save(smember.getId(), id, 5);
//        if (type == 1) {
//          model.addAttribute("msg", "收藏成功");
//        } else {
//          model.addAttribute("msg", "你已收藏了");
//        }

      } else {
        model.addAttribute("msg", "你还没登陆");
      }
    } catch (Exception e) {
      model.addAttribute("msg", "收藏失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "visitlist", method = RequestMethod.GET)
  public String visitlist(
      @RequestParam(value = "id", required = true) int id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "12") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Pagination rs = memberVisitService.findByUserId(id, curpage,
        pagesize);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("list", rs.getList());

    return "memberfront/visitlist";
  }
}
