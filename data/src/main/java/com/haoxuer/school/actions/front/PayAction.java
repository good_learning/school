package com.haoxuer.school.actions.front;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.RefundRecord;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.service.BankRecordService;
import com.haoxuer.school.data.service.RefundRecordService;
import com.haoxuer.school.data.service.TradingrecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;

@Controller
@RequestMapping(value = "member")
public class PayAction extends BaseAction {

  @Autowired
  TradingrecordService tradingrecordService;

  @Autowired
  RefundRecordService refundRecordService;

  /**
   * 立即支付
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "prepay", method = RequestMethod.GET)
  public String prepay(int id, HttpServletRequest request,
                       HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Tradingrecord item = tradingrecordService.findById(id);
      model.addAttribute("item", item);
      return "pay/prepay";
    } else {
      return login;

    }

  }


  @RequestMapping(value = "prerefund", method = RequestMethod.GET)
  public String prerefund(int id, HttpServletRequest request,
                          HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("id", id);
      return "pay/prerefund";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "refund", method = RequestMethod.POST)
  public String refund(RefundRecord record, int id,
                       HttpServletRequest request, HttpServletResponse response,
                       Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      record.setMember(member);
      record.setAddtime(new Timestamp(System.currentTimeMillis()));
      Tradingrecord tradingrecord = new Tradingrecord();
      tradingrecord.setId(id);
      record.setTradingrecord(tradingrecord);
      RefundRecord item = refundRecordService.save(record);
      model.addAttribute("item", item);
      model.addAttribute("msg", "退款成功");
      return "common/text";

    } else {
      return login;
    }

  }

  @RequestMapping(value = "payusecard", method = RequestMethod.POST)
  public String payusecard(int id, String card, HttpServletRequest request,
                           HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      int item = tradingrecordService.payusecard(id, card, member);
      model.addAttribute("item", item);
      if (item == 0) {
        model.addAttribute("msg", "支付成功");

      } else if (item == 1) {
        model.addAttribute("msg", "支付失败，充值卡无效");

      } else if (item == 2) {
        model.addAttribute("msg", "支付失败，充值卡余额不足");
      } else if (item == 3) {
        model.addAttribute("msg", "你已经支付过了");
      }
      return "common/text";

    } else {
      return login;

    }

  }

  @Autowired
  BankRecordService bankRecordService;

  @RequestMapping(value = "pay2ok", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String pay2ok(int id, HttpServletRequest request,
                       HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Tradingrecord item = tradingrecordService.findById(id);
      model.addAttribute("item", item);
      return "course/subscribepayok";
    } else {
      return login;

    }

  }
}
