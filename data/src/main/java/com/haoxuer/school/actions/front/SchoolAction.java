package com.haoxuer.school.actions.front;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.area.data.service.AreaService;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.web.controller.front.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.FavTeacherService;
import com.haoxuer.school.data.service.MemberService;
import com.haoxuer.school.data.service.MemberVisitService;
import com.haoxuer.school.data.service.ProvinceService;
import com.haoxuer.school.data.service.TownService;

@Controller
@RequestMapping(value = "school")
public class SchoolAction extends BaseController {

  @Autowired
  MemberService memberService;

  @Autowired
  TownService townService;

  @Autowired
  private AreaService areaService;

  @Autowired
  ProvinceService provinceService;


  @Autowired
  FavTeacherService favTeacherService;

  Logger logger = LoggerFactory.getLogger("log");


  @Autowired
  MemberVisitService memberVisitService;
/*	@RequestMapping(value = "allschools", method = RequestMethod.GET)
	public String findByUType1(
			@RequestParam(value = "sorttype", required = true, defaultValue = "3") int sorttype,
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "9") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model
			) {

			List<Member> result = memberService.findByType(sorttype, pagesize, curpage);
				model.addAttribute("members", result);
				
				model.addAttribute("navinfo",4);
	
				
			return "schools";

	}*/


  @RequestMapping(value = "spagelist", method = RequestMethod.GET)
  public String spagelist(
      @RequestParam(value = "sorttype", required = true, defaultValue = "3") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "9") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model
  ) {

    //List<Member> result = memberService.findByType(sorttype, pagesize, curpage);
    //model.addAttribute("member", result);
    return "school/spagelist";

  }


  /**
   * @param id
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "up", method = RequestMethod.GET)
  public String up(@RequestParam(value = "id", required = true) int id,
                   HttpServletRequest request, HttpServletResponse response,
                   Model model) {

    //Member member = memberService.up(id);
    model.addAttribute("msg", "点赞成功");
    return "common/text";
  }


  @RequestMapping(value = "fav", method = RequestMethod.GET)
  public String fav(int id, HttpServletRequest request,
                    HttpServletResponse response, Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      int result = favTeacherService.fav(member.getId(), id);
      if (result > 0) {
        model.addAttribute("msg", "你已经收藏过了");

      } else {
        model.addAttribute("msg", "收藏成功");
      }
    } else {
      model.addAttribute("msg", "你还没有登陆");
    }

    return "common/text";
  }


  @RequestMapping(value = "school", method = RequestMethod.GET)
  public String findSchoolById(int id, HttpServletRequest request,
                               HttpServletResponse response, Model model) {
    Member result = memberService.findById(1L);
    model.addAttribute("school", result);


    HttpSession session = getSession(request);
    Member sessionmember = (Member) session.getAttribute("member");
    if (sessionmember != null) {
     // memberVisitService.visit(id, sessionmember.getId());
    }
    return "xuexiao";

  }


  private HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      logger.info("会话不存在");
      session = request.getSession(true);
    }
    return session;
  }


  /**
   * 课程查看列表功能
   *
   * @param id
   * @param sorttype
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "allschools", method = RequestMethod.GET)
  public String index(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "townid", required = true, defaultValue = "0") int townid,
      @RequestParam(value = "sexs", required = true, defaultValue = "1") int sexs,
      @RequestParam(value = "typeid", required = true, defaultValue = "0") int typeid,
      @RequestParam(value = "coursestate", required = true, defaultValue = "0") int coursestate,
      @RequestParam(value = "sorttype", required = true, defaultValue = "0") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      @RequestParam(value = "keyword", required = true, defaultValue = "") String keyword,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    model.addAttribute("sorttype", sorttype);

//    Pagination page = memberService.searchSchool(id, sorttype, 3, townid,
//        keyword, pagesize, curpage);
//    if (page.getList() != null) {
//      model.addAttribute("list", page.getList());
//    }
//    model.addAttribute("page", page);

    model.addAttribute("navinfo", 4);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("id", id);

    //CourseCatalog type = coursetypeService.findById(id);
    //model.addAttribute("coursetype", type);
    //CourseTypeInfos infos = coursetypeService.find(id);
    //model.addAttribute("level1", infos.getLevel1());
    //model.addAttribute("typelist1", infos.getList1());
    //model.addAttribute("level2", infos.getLevel2());
    //model.addAttribute("typelist2", infos.getList2());
    //model.addAttribute("level3", infos.getLevel3());
    //model.addAttribute("typelist3", infos.getList3());
    //model.addAttribute("level4", infos.getLevel4());
    //model.addAttribute("typelist4", infos.getList4());
    model.addAttribute("towns", areaService.child(345));
    model.addAttribute("townid", townid);
    model.addAttribute("coursestate", coursestate);
    model.addAttribute("typeid", typeid);
    model.addAttribute("sexs", sexs);
    // 1101

    return getView("schools");
  }

}
