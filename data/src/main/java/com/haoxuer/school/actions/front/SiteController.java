package com.haoxuer.school.actions.front;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haoxuer.school.data.entity.City;
import com.haoxuer.school.data.entity.Town;
import com.haoxuer.school.data.service.CityService;
import com.haoxuer.school.data.service.TownService;

@Controller
public class SiteController {
  @Autowired
  CityService cityService;
  @Autowired
  TownService townService;

  @RequestMapping(value = "findbyprovince", method = RequestMethod.GET)
  public @ResponseBody
  List<City> findByProvince(HttpServletRequest request,
                            HttpServletResponse response, Model model, int pid) {
    List<City> result = cityService.findByProvince(pid);
    return result;
  }

  @RequestMapping(value = "findbycity", method = RequestMethod.GET)
  public @ResponseBody
  List<Town> findbycity(HttpServletRequest request,
                        HttpServletResponse response, Model model, int pid) {
    List<Town> result = townService.findByCity(pid);
    return result;
  }


  @RequestMapping(value = "usjoin", method = RequestMethod.GET)
  public String usjoin(HttpServletRequest request,
                       HttpServletResponse response, Model model) {
    return "usjoin";

  }

  @RequestMapping(value = "uslegal", method = RequestMethod.GET)
  public String uslegal(HttpServletRequest request,
                        HttpServletResponse response, Model model) {
    return "uslegal";

  }

  @RequestMapping(value = "usservice", method = RequestMethod.GET)
  public String usservice(HttpServletRequest request,
                          HttpServletResponse response, Model model) {
    return "usservice";

  }
}
