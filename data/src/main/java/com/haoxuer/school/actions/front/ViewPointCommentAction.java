package com.haoxuer.school.actions.front;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.CourseComment;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.school.data.entity.Viewpointcomment;
import com.haoxuer.school.data.service.ViewpointcommentService;

@Controller
@RequestMapping(value = "viewpointvomment")
public class ViewPointCommentAction extends BaseAction {

  @Autowired
  ViewpointcommentService viewpointcommentService;

  @RequestMapping(value = "add", method = RequestMethod.POST)
  public String add(Viewpointcomment comment, int viewpointid,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {

    if (comment != null) {
      HttpSession session = getSession(request);
      Member member = (Member) session.getAttribute("member");
      if (member != null) {
        comment.setMember(member);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));
        Viewpoint viewpoint = new Viewpoint();
        viewpoint.setId(1L);
        comment.setViewpoint(viewpoint);
        Viewpointcomment result = viewpointcommentService.save(comment);
        model.addAttribute("item", result);
        return "viewpointvomment/add";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }

  }

  @RequestMapping(value = "addcomment", method = RequestMethod.POST)
  public String addComment(Viewpointcomment comment, int viewpointid,
                           int commentid, HttpServletRequest request,
                           HttpServletResponse response, Model model) {

    if (comment != null) {
      HttpSession session = getSession(request);
      Member member = (Member) session.getAttribute("member");
      if (member != null) {
        comment.setMember(member);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));
        Viewpoint viewpoint = new Viewpoint();
        viewpoint.setId(1L);
        comment.setViewpoint(viewpoint);

        Viewpointcomment tem = new Viewpointcomment();
        tem.setId(commentid);
        comment.setViewpointcomment(tem);

        Viewpointcomment result = viewpointcommentService.save(comment);
        result = viewpointcommentService.findById(result.getId());
        model.addAttribute("item", result);
        return "viewpointvomment/add";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }

  }

  @RequestMapping(value = "addcomment1", method = RequestMethod.POST)
  public String addComment1(Viewpointcomment comment, int courseid,
                            long commentid, HttpServletRequest request,
                            HttpServletResponse response, Model model) {

    if (comment != null) {
      HttpSession session = getSession(request);
      Member member = (Member) session.getAttribute("member");
      if (member != null) {
        comment.setMember(member);
        comment.setPubdate(new Timestamp(System.currentTimeMillis()));
        CourseComment c = new CourseComment();
        c.setId(commentid);
        Viewpointcomment result = viewpointcommentService.save(comment);
        model.addAttribute("item", result);
        return "viewpointvomment/addcomment";
      } else {
        model.addAttribute("msg", "你还没有登陆");
        return "common/text";
      }
    } else {
      model.addAttribute("msg", "输入的内容有问题");
      return "common/text";
    }

  }

  @RequestMapping(value = "ups", method = RequestMethod.GET)
  public String ups(@RequestParam(value = "id", required = true) int id,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {
    try {
      Integer ups = viewpointcommentService.up(id);
      model.addAttribute("msg", ups);
    } catch (Exception e) {
      model.addAttribute("msg", "支持失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "list", method = RequestMethod.GET)
  public String list(int viewpointid, HttpServletRequest request,
                     HttpServletResponse response, Model model) {

    Pagination page = viewpointcommentService.pageByPointId(viewpointid, 1,
        10);
    model.addAttribute("list", page.getList());
    model.addAttribute("viewpointid", viewpointid);

    return "viewpointvomment/list";
  }
}
