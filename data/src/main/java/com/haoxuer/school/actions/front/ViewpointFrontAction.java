package com.haoxuer.school.actions.front;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.school.data.service.FavviewpointService;
import com.haoxuer.school.data.service.ViewpointService;
import com.haoxuer.school.data.service.ViewpointcommentService;

@Controller
@RequestMapping(value = "viewpoint")
public class ViewpointFrontAction extends BaseAction implements Serializable {

  @Autowired
  private ViewpointService viewpointService;
  @Autowired
  private FavviewpointService favviewpointService;

  @Autowired
  private ViewpointcommentService viewpointcommentService;

  @RequestMapping(value = "view", method = RequestMethod.GET)
  public String view(
      @RequestParam(value = "id", required = true) int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    Viewpoint article = viewpointService.view(id);
    Pagination rs = viewpointcommentService.pageByPointId(id, curpage,
        pagesize);
    if (rs != null) {
      model.addAttribute("list", rs.getList());
      model.addAttribute("page", rs);
    }
    model.addAttribute("articleitem", article);
    model.addAttribute("curpage", curpage);
    model.addAttribute("id", id);
    model.addAttribute("sorttype", sorttype);
    model.addAttribute("pagesize", pagesize);
    return "viewpoint/view";
  }

  @RequestMapping("/pagelist")
  public String pagelist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    Pagination pagination = viewpointService.pageByUserId(id, curpage,
        pagesize);
    model.addAttribute("pageNo", pagination.getPageNo());
    model.addAttribute("list", pagination.getList());

    return "/viewpoint/pagelist";

  }

  @RequestMapping("/list")
  public String list(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    Pagination pagination = viewpointService.pageByUserId(id, curpage,
        pagesize);
    model.addAttribute("page", pagination);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("list", pagination.getList());
    return "/viewpoint/list";

  }

  @RequestMapping("/favpagelist")
  public String favpagelist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
//    Pagination pagination = favviewpointService.pageByUserId(id, curpage,
//        pagesize);
//    model.addAttribute("pageNo", pagination.getPageNo());
//    model.addAttribute("list", pagination.getList());

    return "/viewpoint/favpagelist";

  }

  @RequestMapping(value = "up", method = RequestMethod.GET)
  public String up(@RequestParam(value = "id", required = true) int id,
                   HttpServletRequest request, HttpServletResponse response,
                   Model model) {

    try {
      Viewpoint course = viewpointService.up(id);
      model.addAttribute("msg", course.getUps());
    } catch (Exception e) {
      model.addAttribute("msg", "点赞失败");
    }

    return "common/text";
  }

  @RequestMapping(value = "fav", method = RequestMethod.GET)
  public String fav(@RequestParam(value = "id", required = true) int id,
                    HttpServletRequest request, HttpServletResponse response,
                    Model model) {
    try {
      HttpSession session = getSession(request);
      Member smember = (Member) session.getAttribute("member");
      if (smember != null) {
//        int type = favviewpointService.fav(smember.getId(), id);
//        if (type == 2) {
//          model.addAttribute("msg", "收藏成功");
//        } else {
//          model.addAttribute("msg", "你已收藏了");
//        }

      } else {
        model.addAttribute("msg", "你还没登陆");
      }
    } catch (Exception e) {
      e.printStackTrace();
      model.addAttribute("msg", "收藏失败");
    }

    return "common/text";
  }

  @RequestMapping("myviewlist")
  public String myviewlist(
      int userid,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, ModelMap model) {
//    Pagination page = favviewpointService.pageByUserId(userid, curpage,
//        pagesize);
//    model.addAttribute("page", page);
//    model.addAttribute("list", page.getList());
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    return "viewpoint/myviewlist";
  }

  @RequestMapping("/myviewpointlists")
  public String myviewpointlists(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    Pagination pagination = viewpointService.pageByUserId(id, curpage,
        pagesize);
    model.addAttribute("curpage", curpage);
    model.addAttribute("pagesize", pagesize);
    model.addAttribute("page", pagination);
    model.addAttribute("list", pagination.getList());

    return "/viewpoint/myviewpointlists";

  }
}
