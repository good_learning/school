package com.haoxuer.school.actions.front;

import com.haoxuer.discover.web.controller.front.BaseController;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.MemberType;
import com.haoxuer.school.data.service.CourseService;
import com.haoxuer.school.data.service.MemberService;
import com.haoxuer.school.data.service.MemberTypeService;
import com.haoxuer.school.data.service.PhoneRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Pattern;

@Controller
public class WebAction extends BaseController {

  @Autowired
  MemberService memberService;

  @Autowired
  MemberTypeService memberTypeService;
  Logger logger = LoggerFactory.getLogger("log");

  @Autowired
  CourseService courseService;

  @RequestMapping(value = "loginx", method = RequestMethod.GET)
  public String login(HttpServletRequest request,
                      HttpServletResponse response, Model model) {

    return "login";
  }


  @RequestMapping(value = "weibologin", method = RequestMethod.GET)
  public String weibologin(HttpServletRequest request,
                           HttpServletResponse response, Model model) {
    response.setContentType("text/html;charset=utf-8");
    try {
      response.sendRedirect("https://api.weibo.com/oauth2/authorize?client_id=1268671789&response_type=code&redirect_uri=http://www.91mydoor.com/weibologinwork.htm");
    } catch (IOException e) {
      e.printStackTrace();
    }

    return "login";
  }


  @RequestMapping(value = "register", method = RequestMethod.GET)
  public String register(HttpServletRequest request,
                         HttpServletResponse response, Model model) {

    return "register";
  }

  @RequestMapping(value = "retrievepassword", method = RequestMethod.GET)
  public String retrievepassword(HttpServletRequest request,
                                 HttpServletResponse response, Model model) {

    return "retrievepassword";
  }

  @RequestMapping(value = "index", method = RequestMethod.GET)
  public String index() {
    return getView("index");
  }


  @RequestMapping(value = "css", method = RequestMethod.GET)
  public String css() {
    return "css";
  }


  @RequestMapping(value = "findwork", method = RequestMethod.POST)
  public String findwork(Member member, HttpServletRequest request,
                         HttpServletResponse response, Model model) {
    Member members = null;
    if (members != null) {
      return "register_success";
    } else {
      return "register";
    }

  }

  @RequestMapping(value = "registerwork", method = RequestMethod.POST)
  public String registerwork(Member member, int usertype,
                             HttpServletRequest request, HttpServletResponse response,
                             Model model) {
    Member members = null;
    try {
      MemberType type = new MemberType();
      type.setId(usertype);
     // member.setMembertype(type);
      //members = memberService.register(member);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (members != null) {
      return "register_success";
    } else {
      return "register";
    }

  }

  @RequestMapping(value = "checkcode", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String checkcode(String vcode, HttpServletRequest request,
                          HttpServletResponse response, Model model) {
    HttpSession session = request.getSession(false);
    String captchaToken = session == null ? "" : session
        .getAttribute("captchaToken") + "";

    logger.info("vcode:" + vcode + "     captchaToken:" + captchaToken);

    if (captchaToken.equals(vcode)) {
      model.addAttribute("info", "{\"ok\":\"\"}");
    } else {
      model.addAttribute("info", "{\"error\":\"验证码不正确\"}");
    }
    model.addAttribute("info", "{\"ok\":\"\"}");
    return "json";
  }

  @RequestMapping(value = "checkusername", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String checkusername(String username, HttpServletRequest request,
                              HttpServletResponse response, Model model) {
    HttpSession session = request.getSession(false);
    String captchaToken = session == null ? "" : session
        .getAttribute("captchaToken") + "";
    return "json";
  }

  @RequestMapping(value = "checkphonecode", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String checkphonecode(String code, HttpServletRequest request,
                               HttpServletResponse response, Model model) {
    model.addAttribute("info", "{\"ok\":\"\"}");
    return "json";
  }

  @RequestMapping(value = "checkphonenum", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String checkphonenum(String phonenum, HttpServletRequest request,
                              HttpServletResponse response, Model model) {
    model.addAttribute("info", "{\"ok\":\"\"}");
    return "json";
  }

  @RequestMapping(value = "sendcode", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String sendcode(String phonenum, HttpServletRequest request,
                         HttpServletResponse response, Model model) {
    model.addAttribute("msg", "成功");
    return "common/text";
  }

  // 判断，返回布尔值
  public static boolean isPhoneNumber(String input) {
    String regex = "1([\\d]{10})|((\\+[0-9]{2,4})?\\(?[0-9]+\\)?-?)?[0-9]{7,8}";
    // Pattern p = Pattern.compile(regex);
    return Pattern.matches(regex, input);
  }

  public static void main(String[] args) {
    System.out.println(isPhoneNumber("029-874397"));
  }

  @RequestMapping(value = "sendtel", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String sendtel(String phonenum, Integer id,
                        HttpServletRequest request, HttpServletResponse response,
                        Model model) {
    model.addAttribute("msg", "成功");
    return "common/text";
  }

  @RequestMapping(value = "checkemail", method = {RequestMethod.POST,
      RequestMethod.GET})
  public String checkemail(String email, HttpServletRequest request,
                           HttpServletResponse response, Model model) {
    return "json";
  }

  /**
   * 定向到激活页面。
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "activate", method = RequestMethod.GET)
  public String activate(HttpServletRequest request,
                         HttpServletResponse response, Model model) {

    return "activate";
  }

  /**
   * 使用注册手机号激活账户
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "activatework", method = RequestMethod.POST)
  public String activateWork(HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    return "member/index";
  }

  @Autowired
  private PhoneRecordService phoneRecordService;
}
