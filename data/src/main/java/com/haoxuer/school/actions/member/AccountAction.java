package com.haoxuer.school.actions.member;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.CouponService;
import com.haoxuer.school.data.service.MemberService;
import com.haoxuer.school.data.service.MoneyRecordService;
import com.haoxuer.school.data.service.ScorerecordService;

@Controller
@RequestMapping(value = "member")
public class AccountAction extends BaseAction {

  @Autowired
  ScorerecordService scorerecordService;
  private int pagesize = 10;

  @Autowired
  CouponService couponService;

  @Autowired
  MoneyRecordService moneyRecordService;


  @RequestMapping(value = "chongzhi", method = RequestMethod.GET)
  public String chongzhi(HttpServletRequest request,
                         HttpServletResponse response, Model model) {
    return "member/account/chongzhi";
  }


  @RequestMapping(value = "moneylist", method = RequestMethod.GET)
  public String moneylist(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = moneyRecordService.pageByUser(member.getId(),
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/account/moneylist";
    } else {
      return login;
    }

  }

  @RequestMapping(value = "moneylistfirst", method = RequestMethod.GET)
  public String moneylistfirst(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = moneyRecordService.pageByUser(member.getId(),
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/account/moneylistfirst";
    } else {
      return login;
    }

  }

  @RequestMapping(value = "deleteformoneylist", method = RequestMethod.GET)
  public String deleteformoneylist(Integer id, HttpServletRequest request,
                                   HttpServletResponse response, Model model) {
    moneyRecordService.delete(id);
    return "redirect:/member/moneylist.htm";
  }

  @RequestMapping(value = "deleteformoneylistfirst", method = RequestMethod.GET)
  public String deleteformoneylistfirst(Integer id, HttpServletRequest request,
                                        HttpServletResponse response, Model model) {
    moneyRecordService.delete(id);
    return "redirect:/member/moneylistfirst.htm";
  }

  @RequestMapping(value = "mymoney", method = RequestMethod.GET)
  public String mymoney(HttpServletRequest request,
                        HttpServletResponse response, Model model) {

    return "member/account/account_mymoney";
  }

  @RequestMapping(value = "myscore", method = RequestMethod.GET)
  public String myscore(HttpServletRequest request,
                        HttpServletResponse response, Model model) {

    return "member/account/account_myscore";
  }

  @RequestMapping(value = "myscorelist", method = RequestMethod.GET)
  public String myscorelist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, ModelMap model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination pagination = scorerecordService.pageByUserId(member.getId(),
//          curpage, pagesize);
//      model.addAttribute("curpage", pagination.getPageNo());
//      model.addAttribute("pagesize", pagesize);
//      model.addAttribute("page", pagination);
//
//      model.addAttribute("list", pagination.getList());
    }
    return "member/account/account_myscorelist";
  }

  @RequestMapping(value = "myrecord", method = RequestMethod.GET)
  public String myrecord(HttpServletRequest request,
                         HttpServletResponse response, Model model) {

    return "member/account/account_myrecord";
  }

  @RequestMapping(value = "mycouponlist", method = RequestMethod.GET)
  public String mycouponlist(HttpServletRequest request,
                             HttpServletResponse response, Model model) {

    return "member/account/mycouponlist";
  }

  @RequestMapping(value = "couponlist", method = RequestMethod.GET)
  public String couponlist(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = couponService.pageByUser(member.getId(), curpage,
//          pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/account/couponlist";
    } else {
      return login;
    }
  }

  @RequestMapping(value = "coupondelete", method = RequestMethod.GET)
  public String coupondelete(int id, HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    couponService.deleteById(id);
    return "redirect:/member/couponlist.htm";
  }

  /**
   * 安全信息
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "mysafeinfo", method = RequestMethod.GET)
  public String mysafeinfo(HttpServletRequest request,
                           HttpServletResponse response, Model model) {

    return "member/account/account_mysafeinfo";
  }

  /**
   * 安全信息
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "mysafeinfoframe", method = RequestMethod.GET)
  public String mysafeinfoframe(HttpServletRequest request,
                                HttpServletResponse response, Model model) {

    return "member/account/account_mysafeinfoframe";
  }

  /**
   * 修改安全密码
   *
   * @param password
   * @param oldpassword
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "modifySecurityCode", method = RequestMethod.POST)
  public String modifySecurityCode(String password, String oldpassword,
                                   HttpServletRequest request, HttpServletResponse response,
                                   Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

//      String newmember = memberService.modifySecurityCode(member.getId(),
//          oldpassword, password);
//
//      model.addAttribute("msg", newmember);
//      if ("修改安全密码成功".equals(newmember)) {
//        session.setAttribute("member", memberService.findById(member.getId()));
//        return "member/account/account_mysafeinfook";
//      } else {
//        return "member/account/account_mysafeinfoframe";
//
//      }
    } else {
      return login;

    }
    return login;

  }

  @Autowired
  MemberService memberService;
}
