package com.haoxuer.school.actions.member;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Bankbind;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.BankbindService;

@Controller
@RequestMapping(value = "member")
public class BankbindAction extends BaseAction {


  @Autowired
  BankbindService bankbindService;


  @RequestMapping(value = "bank_view_add", method = RequestMethod.GET)
  public String bank_view_add(HttpServletRequest request,
                              HttpServletResponse response, Model model) {
    return "member/account/bank_view_add";
  }

  @RequestMapping(value = "bank_view_add_ok", method = RequestMethod.GET)
  public String bank_view_add_ok(HttpServletRequest request,
                                 HttpServletResponse response, Model model) {
    return "member/account/bank_view_add_ok";
  }

  @RequestMapping(value = "bank_model_add", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String bank_model_add(Bankbind bankbind, HttpServletRequest request,
                               HttpServletResponse response, Model model) {
    bankbind.setMember(getmember(request));
    bankbind.setBinddate(new Timestamp(System.currentTimeMillis()));
    bankbindService.save(bankbind);
    return "member/account/bank_model_add";
  }


  @RequestMapping(value = "bank_model_delete", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String bank_model_delete(int id, HttpServletRequest request,
                                  HttpServletResponse response, Model model) {
    bankbindService.deleteById(id);
    model.addAttribute("msg", "ok");
    return "common/text";
  }

  /**
   * 收藏的课程
   */
  @RequestMapping(value = "bank_view_list", method = RequestMethod.GET)
  public String bank_view_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      Pagination page = bankbindService.pagetByUser(member.getId(), curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);

      String ret = "member/account/bank_view_list";
      return ret;

    } else {
      return login;

    }

  }
}
