package com.haoxuer.school.actions.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.CardService;

@Controller
@RequestMapping(value = "member")
public class CardAction extends BaseAction {

  @RequestMapping(value = "card_chongzhi", method = RequestMethod.GET)
  public String card_chongzhi(HttpServletRequest request,
                              HttpServletResponse response, Model model) {
    return "member/account/card_chongzhi";
  }

  @Autowired
  CardService cardService;

  @RequestMapping(value = "card_chongzhiwork", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String card_chongzhiwork(HttpServletRequest request, String cardnum,
                                  String cardpass, float money, HttpServletResponse response,
                                  Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    String msg = "";
    if (member != null) {
      msg = cardService.chongzhi(cardnum, cardpass, money, member);
    }
    model.addAttribute("msg", msg);
    return "common/text";
  }

  @RequestMapping(value = "card_chongzhi_ok", method = RequestMethod.GET)
  public String card_chongzhi_ok(HttpServletRequest request,
                                 HttpServletResponse response, Model model) {
    return "member/account/card_chongzhi_ok";
  }
}
