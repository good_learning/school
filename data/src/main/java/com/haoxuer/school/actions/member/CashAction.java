package com.haoxuer.school.actions.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Bankbind;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.BankbindService;
import com.haoxuer.school.data.service.CashRecordService;
import com.haoxuer.school.data.service.MemberService;

@Controller
@RequestMapping(value = "member")
public class CashAction extends BaseAction {

  @RequestMapping(value = "cash_ok", method = RequestMethod.GET)
  public String cash_ok(HttpServletRequest request,
                        HttpServletResponse response, Model model) {

    return "member/account/cash_ok";

  }

  @RequestMapping(value = "cash_record_list", method = RequestMethod.GET)
  public String cash_record_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = cashRecordService.pagetByUser(member.getId(),
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);
      return "member/account/cash_record_list";

    } else {
      return login;

    }
  }

  @Autowired
  BankbindService bankbindService;

  @RequestMapping(value = "cash_add", method = RequestMethod.GET)
  public String cash_add(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = bankbindService.pagetByUser(member.getId(),
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);
      return "member/account/cash_add";

    } else {
      return login;

    }
  }

  @RequestMapping(value = "cash_model_add", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String cash_model_add(
      Integer bankid,
      Float money,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      if (bankid != null) {
        if (money != null && money > 0) {
          Bankbind bankbind = bankbindService.findById(bankid);
          model.addAttribute("bank", bankbind);
          return "member/account/cash_confirm";
        } else {
          model.addAttribute("msg", "输入的金额不正确");
        }
      } else {
        model.addAttribute("msg", "请选择提现银行卡");
      }
      Pagination page = bankbindService.pagetByUser(member.getId(),
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);
      return "member/account/cash_add";
    } else {
      return login;

    }
  }

  @Autowired
  CashRecordService cashRecordService;

  @Autowired
  MemberService memberService;

  @RequestMapping(value = "cash_model_confirm", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String cash_model_confirm(
      Integer bankid,
      Float money,
      String password,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      if (bankid != null) {
        if (money != null && money > 0) {
          String msg = cashRecordService.tixian(bankid, money,
              password, member);
          model.addAttribute("msg", msg);

          member = memberService.findById(member.getId());
          session.setAttribute("member", member);
        } else {
          model.addAttribute("msg", "输入的金额不正确");
        }
      } else {
        model.addAttribute("msg", "请选择提现银行卡");
      }
      return "common/text";
    } else {
      return login;

    }
  }
}
