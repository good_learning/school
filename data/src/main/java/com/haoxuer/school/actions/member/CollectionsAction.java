package com.haoxuer.school.actions.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.FavArtilceService;
import com.haoxuer.school.data.service.FavTeacherService;
import com.haoxuer.school.data.service.FavcourseService;
import com.haoxuer.school.data.service.FavviewpointService;

@Controller
@RequestMapping(value = "member")
public class CollectionsAction extends BaseAction {

  @Autowired
  FavcourseService favcourseService;
  @Autowired
  FavviewpointService favviewpointService;

  @Autowired
  FavArtilceService favArtilceService;


  @Autowired
  FavTeacherService favTeacherService;

  /**
   * 收藏的课程
   */
  @RequestMapping(value = "collections_favcourse_list", method = RequestMethod.GET)
  public String collections_favcourse_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      Pagination page = favcourseService.pageByUserId(member.getId(),
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);

      String ret = "member/collections/favcourse_list";
      return ret;

    } else {
      return login;

    }

  }

  /**
   * 收藏的观点
   */
  @RequestMapping(value = "collections_favviewpoint_list", method = RequestMethod.GET)
  public String collections_favviewpoint_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = favArtilceService.pageByUserId(member.getId(),
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);

      String ret = "member/collections/favviewpoint_list";
      return ret;

    } else {
      return login;

    }

  }

  /**
   * 收藏的学校
   */
  @RequestMapping(value = "collections_school_list", method = RequestMethod.GET)
  public String collections_school_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = favTeacherService.pageByUser(member.getId(), 3,
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);

      String ret = "member/collections/school_list";
      return ret;

    } else {
      return login;

    }

  }

  /**
   * 收藏的老师
   */
  @RequestMapping(value = "collections_teacher_list", method = RequestMethod.GET)
  public String collections_teacher_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = favTeacherService.pageByUser(member.getId(), 2,
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);

      String ret = "member/collections/teacher_list";
      return ret;

    } else {
      return login;

    }

  }
}
