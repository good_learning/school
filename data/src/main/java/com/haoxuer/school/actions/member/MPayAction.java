package com.haoxuer.school.actions.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "member")
public class MPayAction {

  /**
   * 充值
   *
   * @return
   */
  @RequestMapping(value = "mpay_chongzhi", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String chongzhi(HttpServletRequest request, HttpServletResponse response,
                         Model model) {
    return "member/mpay/chongzhi";

  }

  /**
   * 提现
   *
   * @return
   */
  @RequestMapping(value = "mpay_tixian", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String tixian(HttpServletRequest request, HttpServletResponse response,
                       Model model) {
    return "member/mpay/tixian";

  }
}
