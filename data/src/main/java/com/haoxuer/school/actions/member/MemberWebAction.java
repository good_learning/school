package com.haoxuer.school.actions.member;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.oto.OrderInfo;
import com.haoxuer.school.data.service.BrowsingHistoryService;
import com.haoxuer.school.data.service.CourseService;
import com.haoxuer.school.data.service.CourseSubscribeService;
import com.haoxuer.school.data.service.MemberService;


/*用户信息 1 个人  老师 学校
 * 课程管理2 个人  老师 学校
 * 授课管理3	   老师 学校
 * 师资管理4		学校
 * 分校管理5		学校
 * 我的观点6 个人   老师 学校
 * 我的收藏7 个人    老师 学校
 * 我的账户8 个人  老师 学校
 * 系统消息9 个人   老师 学校
 *
 */
@Controller
@RequestMapping(value = "member")
public class MemberWebAction extends BaseAction implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 4695893394103673171L;

  @Autowired
  CourseService courseService;
  @Autowired
  BrowsingHistoryService browsingHistoryService;

  @Autowired
  CourseSubscribeService courseSubscribeService;
  @Autowired
  MemberService memberService;

  /**
   * 我的账户
   *
   * @param password
   * @param oldpassword
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "index", method = RequestMethod.GET)
  public String index(String password, String oldpassword,
                      HttpServletRequest request, HttpServletResponse response,
                      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 8);

      member = memberService.findById(member.getId());
      session.setAttribute("member", member);

      return "member/index";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "userinfo", method = RequestMethod.GET)
  public String userinfo(String password, String oldpassword,
                         HttpServletRequest request, HttpServletResponse response,
                         Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 1);
      String ret = "member/userinfo";
      return ret;

    } else {
      return login;

    }

  }

  @RequestMapping(value = "viewpoints", method = RequestMethod.GET)
  public String viewpoints(String password, String oldpassword,
                           HttpServletRequest request, HttpServletResponse response,
                           Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      model.addAttribute("menu", 6);
      return "member/viewpoints";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolnetworks", method = RequestMethod.GET)
  public String schoolnetworks(String password, String oldpassword,
                               HttpServletRequest request, HttpServletResponse response,
                               Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      model.addAttribute("menu", 5);
      return "member/schoolnetworks";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "systeminfos", method = RequestMethod.GET)
  public String systeminfos(String password, String oldpassword,
                            HttpServletRequest request, HttpServletResponse response,
                            Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      model.addAttribute("menu", 9);
      return "member/systeminfos";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "courses", method = RequestMethod.GET)
  public String courses(String password, String oldpassword,
                        HttpServletRequest request, HttpServletResponse response,
                        Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 2);
      return "member/courses";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist", method = RequestMethod.GET)
  public String teachcourselist(HttpServletRequest request, HttpServletResponse response,
                                Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 2);
      return "member/teachcourselist";

    } else {
      return login;

    }
  }

  @RequestMapping(value = "collections", method = RequestMethod.GET)
  public String collections(String password, String oldpassword,
                            HttpServletRequest request, HttpServletResponse response,
                            Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 7);
      return "member/collections";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachers", method = RequestMethod.GET)
  public String teachers(String password, String oldpassword,
                         HttpServletRequest request, HttpServletResponse response,
                         Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 4);
      return "member/teachers";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "course_history", method = RequestMethod.GET)
  public String course_history(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = browsingHistoryService.getPage(member.getId(),
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("list", page.getList());
//      }

      return "member/course_history";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "course_order", method = RequestMethod.GET)
  public String course_order(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

//      Pagination page = courseSubscribeService.pageByUserId(
//          member.getId(), curpage, pagesize);
//      model.addAttribute("list", page.getList());

      return "member/course_order";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "course_recommend", method = RequestMethod.GET)
  public String course_recommend(String password, String oldpassword,
                                 HttpServletRequest request, HttpServletResponse response,
                                 Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      return "member/course_recommend";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "course_transaction", method = RequestMethod.GET)
  public String course_transaction(String password, String oldpassword,
                                   HttpServletRequest request, HttpServletResponse response,
                                   Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      return "member/course_transaction";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "course_transaction_type", method = RequestMethod.GET)
  public String course_transaction_type(
      int typeid,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.pageByTransactionType(typeid,
//          member.getId(), curpage, pagesize);
//      List<?> datas = page.getList();
//      if (datas != null && datas.size() > 0) {
//        List<OrderInfo> infos = new ArrayList<OrderInfo>();
//        for (Object orderInfo : datas) {
//
//          OrderInfo info = new OrderInfo();
//          Object[] object = (Object[]) orderInfo;
//          Course c = (Course) object[0];
//          Tradingrecord r = (Tradingrecord) object[1];
//          info.setCourse(c);
//          info.setRecord(r);
//          infos.add(info);
//        }
//        model.addAttribute("list", infos);


      return "member/course_transaction_type";
    } else {
      return login;
    }

  }


  @RequestMapping(value = "schoolteaching", method = RequestMethod.GET)
  public String schoolteaching(String password, String oldpassword,
                               HttpServletRequest request, HttpServletResponse response,
                               Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 3);
      return "member/schoolteaching";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "teacherteaching", method = RequestMethod.GET)
  public String teacherteaching(String password, String oldpassword,
                                HttpServletRequest request, HttpServletResponse response,
                                Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 3);
      return "member/teacherteaching";

    } else {
      return login;

    }

  }
}
