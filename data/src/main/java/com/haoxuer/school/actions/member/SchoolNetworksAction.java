package com.haoxuer.school.actions.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Schoolnetwork;
import com.haoxuer.school.data.entity.Town;
import com.haoxuer.school.data.service.SchoolnetworkService;

@Controller
@RequestMapping(value = "member")
public class SchoolNetworksAction extends BaseAction {

  @Autowired
  SchoolnetworkService schoolnetworkService;

  @RequestMapping(value = "schoolnetworks_view_add", method = RequestMethod.GET)
  public String schoolnetworks_view_add(String password, String oldpassword,
                                        HttpServletRequest request, HttpServletResponse response,
                                        Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("menu", 1);
      String ret = "member/schoolnetworks/add";
      return ret;

    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolnetworks_view_list", method = RequestMethod.GET)
  public String schoolnetworks_view_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = schoolnetworkService.getPage(member.getId(),
//          curpage, pagesize);
//      model.addAttribute("page", page);
//      model.addAttribute("list", page.getList());
//      model.addAttribute("curpage", curpage);
//      model.addAttribute("pagesize", pagesize);

      String ret = "member/schoolnetworks/list";
      return ret;

    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolnetworks_model_add", method = RequestMethod.POST)
  public String schoolnetworks_model_add(Schoolnetwork schoolnetwork,
                                         Integer townid, HttpServletRequest request,
                                         HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      // schoolnetwork
      if (townid != null) {
        Town town = new Town();
        town.setId(townid);
        schoolnetwork.setTown(town);
      }
      schoolnetwork.setMember(member);
      schoolnetworkService.save(schoolnetwork);
      model.addAttribute("msg", "添加分校成功");
      return "redirect:/member/schoolnetworks_pubcourseok.htm";

    } else {
      return login;

    }

  }

  @RequestMapping("schoolnetworks_pubcourseok")
  public String pubcourseok(ModelMap model, HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      return "member/schoolnetworks/pubcourseok";
    } else {
      return login;

    }
  }

  @RequestMapping(value = "schoolnetworks_model_update", method = RequestMethod.POST)
  public String schoolnetworks_model_update(Schoolnetwork schoolnetwork,
                                            Integer townid, HttpServletRequest request,
                                            HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      // schoolnetwork
      if (townid != null) {
        Town town = new Town();
        town.setId(townid);
        schoolnetwork.setTown(town);
      }
      schoolnetwork.setMember(member);
      schoolnetworkService.update(schoolnetwork);
      model.addAttribute("msg", "更新分校成功");
      return "common/text";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolnetworks_view_update", method = RequestMethod.GET)
  public String schoolnetworks_view_update(int id,
                                           HttpServletRequest request, HttpServletResponse response,
                                           Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("item", schoolnetworkService.findById(id));
      String ret = "member/schoolnetworks/update";
      return ret;

    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolnetworks_model_delete", method = RequestMethod.GET)
  public String schoolnetworks_model_delete(
      int id,
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      schoolnetworkService.deleteById(id);
//      Pagination page = schoolnetworkService.getPage(member.getId(),
//          curpage, pagesize);
//      model.addAttribute("page", page);
//      model.addAttribute("list", page.getList());
//      model.addAttribute("curpage", curpage);
//      model.addAttribute("pagesize", pagesize);
      String ret = "member/schoolnetworks/list";
      return ret;

    } else {
      return login;

    }

  }
}
