package com.haoxuer.school.actions.member;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.school.data.entity.CourseLessonTeacher;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.CourseLessonStudentService;
import com.haoxuer.school.data.service.CourseLessonTeacherService;
import com.haoxuer.school.data.service.CourseService;
import com.haoxuer.school.data.service.SchoolteacherService;

/**
 * 学校授课管理
 *
 * @author 年高
 */
@Controller
@RequestMapping(value = "member")
public class SchoolTeaching extends BaseAction {
  @Autowired
  CourseService courseService;

  @Autowired
  SchoolteacherService schoolteacherService;

  @Autowired
  CourseLessonStudentService courseLessonStudentService;

  @Autowired
  CourseLessonTeacherService courseLessonTeacherService;

  @RequestMapping(value = "schoolteaching_teaching", method = RequestMethod.GET)
  public String teaching(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.teachingsForStar(member.getId(), 0,
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      Pagination pageteacher = schoolteacherService.findSchoolTeacherList(member.getId(), 1, 100);
      if (pageteacher != null) {
        model.addAttribute("teacherlist", pageteacher.getList());

      }
      //teacherlist
      return "member/schoolteaching/teaching";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolteaching_teachingend", method = RequestMethod.GET)
  public String teachingend(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.teachings(member.getId(), 2,
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }

      return "member/schoolteaching/teachingend";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolteaching_teachingstart", method = RequestMethod.GET)
  public String teachingstart(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.teachings(member.getId(), 1,
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/schoolteaching/teachingstart";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolteaching_courselessonteachers", method = RequestMethod.GET)
  public String courselessonteachers(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "1000") int pagesize,
      int courseid, HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseLessonTeacherService.findBySchoolId(
//          member.getId(), courseid, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/schoolteaching/courselessonteachers";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolteaching_courselessonstudents", method = RequestMethod.GET)
  public String courselessonstudents(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "1000") int pagesize,
      int courseid, int perid, HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = courseLessonStudentService.findByPeriod(
          perid, courseid, curpage, pagesize);
      if (page != null) {
        model.addAttribute("page", page);
        List<?> datas = page.getList();
        model.addAttribute("list", datas);
        model.addAttribute("curpage", curpage);
        model.addAttribute("pagesize", pagesize);
      }
      return "member/schoolteaching/courselessonstudents";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "schoolteaching_setteacher", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String setteacher(int teacherid, Model model, int courseid) {
    Course course = courseService.setTeacher(courseid, teacherid);
    if (course != null) {
      model.addAttribute("msg", "设置老师成功");
    }
    return "common/text";
  }

  /**
   * 老师提醒学生确认
   *
   * @return
   */
  @RequestMapping(value = "schoolteaching_reminder", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String reminder(long id, Model model) {
    CourseLessonStudent course = courseLessonStudentService.reminder(id);
    if (course != null) {
      model.addAttribute("msg", "提醒成功");
    }
    return "common/text";
  }

  /**
   * 老师提醒学生确认
   *
   * @return
   */
  @RequestMapping(value = "schoolteaching_invitecomments", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String invitecomments(long id, Model model) {
    CourseLessonStudent course = courseLessonStudentService.invitecomments(id);
    if (course != null) {
      model.addAttribute("msg", "邀请成功");
    }
    return "common/text";
  }


  @RequestMapping(value = "schoolteaching_start", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String start(Model model, int courseid) {
    Course course = courseService.startCourse(courseid);
    if (course != null) {
      model.addAttribute("msg", "开始课程成功");
    }
    return "common/text";
  }

  @RequestMapping(value = "schoolteaching_finshed", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String finshed(Model model, long id, HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      CourseLessonTeacher course = courseLessonTeacherService.finshed(
//          member.getId(), id);
//      if (course != null) {
//        model.addAttribute("msg", "ok");
//      }
      return "common/text";
    } else {
      return login;

    }

  }

}
