package com.haoxuer.school.actions.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.SystemInfoService;

@Controller
@RequestMapping(value = "member")
public class SystemInfoAction extends BaseAction {

  @Autowired
  SystemInfoService systemInfoService;

  @RequestMapping(value = "systeminfos_list", method = RequestMethod.GET)
  public String systeminfos_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = systemInfoService.pageByUser(member.getId(), 0,
//          curpage, pagesize);
//      model.addAttribute("page", page);
//      model.addAttribute("list", page.getList());
//      model.addAttribute("curpage", curpage);
//      model.addAttribute("pagesize", pagesize);

      String ret = "member/systeminfos/list";
      return ret;

    } else {
      return login;

    }

  }

  @RequestMapping(value = "systeminfos_detelelist", method = RequestMethod.GET)
  public String systeminfos_detelelist(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = systemInfoService.pageByUser(member.getId(), 1,
//          curpage, pagesize);
//      model.addAttribute("page", page);
//      model.addAttribute("list", page.getList());
//      model.addAttribute("curpage", curpage);
//      model.addAttribute("pagesize", pagesize);

      String ret = "member/systeminfos/detelelist";
      return ret;

    } else {
      return login;
    }

  }

  @RequestMapping(value = "systeminfos_delete", method = RequestMethod.GET)
  public String systeminfos_delete(long id,
                                   @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                                   @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                                   HttpServletRequest request, HttpServletResponse response,
                                   Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      systemInfoService.deleteById(id);
      String ret = "redirect:/member/systeminfos_detelelist.htm";
      return ret;

    } else {
      return login;
    }

  }

  @RequestMapping(value = "systeminfos_recovery", method = RequestMethod.GET)
  public String systeminfos_recovery(long id,
                                     @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                                     @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                                     HttpServletRequest request, HttpServletResponse response,
                                     Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//			Pagination page = systemInfoService.pageByUser(member.getId(),2,
//					curpage, pagesize);
//			model.addAttribute("page", page);
//			model.addAttribute("list", page.getList());
//			model.addAttribute("curpage", curpage);
//			model.addAttribute("pagesize", pagesize);
      systemInfoService.recovery(id);
      String ret = "redirect:/member/systeminfos_list.htm";
      return ret;

    } else {
      return login;
    }

  }

  @RequestMapping(value = "systeminfos_read", method = RequestMethod.GET)
  public String systeminfos_read(long id,
                                 @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                                 @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                                 HttpServletRequest request, HttpServletResponse response,
                                 Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//			Pagination page = systemInfoService.pageByUser(member.getId(),2,
//					curpage, pagesize);
//			model.addAttribute("page", page);
//			model.addAttribute("list", page.getList());
//			model.addAttribute("curpage", curpage);
//			model.addAttribute("pagesize", pagesize);
      systemInfoService.read(id);
      String ret = "redirect:/member/systeminfos_list.htm";
      return ret;

    } else {
      return login;
    }

  }
}
