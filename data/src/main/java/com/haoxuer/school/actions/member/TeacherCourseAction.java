package com.haoxuer.school.actions.member;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.*;
import com.haoxuer.school.data.service.*;
import com.haoxuer.school.web.webbinding.CustomTimestampEditor;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
@RequestMapping(value = "member")
public class TeacherCourseAction extends BaseAction {

  @Autowired
  CourseService courseService;


  @Autowired
  CourseSubscribeService courseSubscribeService;

  @Autowired
  TradingrecordService tradingrecordService;

  @Autowired
  CourseImgService courseImgService;

  @Autowired
  SchoolnetworkService schoolnetworkService;

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    dateFormat.setLenient(false);

    SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    datetimeFormat.setLenient(false);

    binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(
        dateFormat, true));
    binder.registerCustomEditor(java.sql.Timestamp.class,
        new CustomTimestampEditor(datetimeFormat, true));
  }

  @RequestMapping(value = "teachcourselist_view_course_refunds", method = RequestMethod.GET)
  public String view_course_refunds(int id, HttpServletRequest request,
                                    HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      TradingrecordRefund item = tradingrecordRefundService
          .findByTradId(id);
      model.addAttribute("item", item);
      return "member/teachcourselist/view_course_refunds";
    } else {
      return login;
    }
  }

  @RequestMapping(value = "teachcourselist_course_refunds", method = RequestMethod.POST)
  public String course_refunds(int id, HttpServletRequest request,
                               HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      TradingrecordRefund item = tradingrecordRefundService.refund(id);
      return "redirect:/member/teachcourselist_transactions_refunds.htm";
    } else {
      return login;
    }
  }

  @Autowired
  private TradingrecordRefundService tradingrecordRefundService;

  @RequestMapping(value = "teachcourselist_recyclerscoursesubscribes", method = RequestMethod.GET)
  public String recyclerscoursesubscribes(
      @RequestParam(value = "type", required = true, defaultValue = "2") int type,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseSubscribeService.pageByUserForRecyclers(
//          member.getId(), type, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", type);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/recyclers_coursesubscribe";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist_coursesubscribes", method = RequestMethod.GET)
  public String coursesubscribes(
      @RequestParam(value = "type", required = true, defaultValue = "2") int type,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseSubscribeService.pageByUserForTeacherGood(
//          member.getId(), type, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", type);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/coursesubscribes";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist_handlestate", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String handlestate(long id, String msg, HttpServletRequest request,
                            Model model) {

    courseSubscribeService.handle(id, msg);

    return "redirect:/member/teachcourselist_coursesubscribes.htm";

  }

  @RequestMapping(value = "teachcourselist_coursesubscribes_delete", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String coursesubscribesDelete(long id, String msg, HttpServletRequest request,
                                       Model model) {

    courseSubscribeService.delete(id);

    return "redirect:/member/teachcourselist_coursesubscribes.htm";

  }

  @RequestMapping(value = "teachcourselist_coursesubscribes_remove", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String coursesubscribesRemove(long id, String msg, HttpServletRequest request,
                                       Model model) {

    courseSubscribeService.remove(id);

    return "redirect:/member/teachcourselist_recyclerscoursesubscribes.htm";

  }

  @RequestMapping(value = "teachcourselist_mycourselist", method = RequestMethod.GET)
  public String mycourselist(
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.pageByUser(member.getId(), 1,
//          sorttype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/mycourselist";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist_mycourselistref", method = RequestMethod.GET)
  public String mycourselistref(
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.pageByUser(member.getId(), 1,
//          sorttype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/mycourselistref";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist_showmycourselist", method = RequestMethod.GET)
  public String showmycourselist(
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "5") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.pageByUser(member.getId(), 1,
//          sorttype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/showmycourselist";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist_delete", method = RequestMethod.GET)
  public String delete(
      int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      Course c = courseService.findById(id);
      c.setStatetype(100);
      c.setDeletedate(new Timestamp(System.currentTimeMillis()));
      courseService.update(c);
      // courseService.
//      Pagination page = courseService.pageByUser(member.getId(), 1,
//          sorttype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/mycourselist";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist_stop", method = RequestMethod.GET)
  public String stop(
      int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      Course c = courseService.findById(id);
      c.setStopstate(1);
      courseService.update(c);
      // courseService.
//      Pagination page = courseService.pageByUser(member.getId(), 1,
//          sorttype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/mycourselist";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachcourselist_recyclers", method = RequestMethod.GET)
  public String recyclers(
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.pageByUser(member.getId(), 100,
//          sorttype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/recyclers";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "mycourseajaxlist", method = RequestMethod.GET)
  public String mycourseajaxlist(
      @RequestParam(value = "id", required = true, defaultValue = "1") int id,
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      Model model) {

    Pagination page = courseService.pageByUser(id, 1, sorttype, curpage,
        pagesize);

    if (page != null) {
      List datas = page.getList();
      if (datas != null && datas.size() > 0) {
        model.addAttribute("list", datas);
      }
      model.addAttribute("sorttype", sorttype);
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);
      model.addAttribute("id", id);

    }

    return "user/mycourseajaxlist";
  }

  @RequestMapping(value = "prepubcourse", method = RequestMethod.GET)
  public String prepubcourse(HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    setPubAddressList(model, request);
    //List<CourseCatalog> types = coursetypeService.findTop();
    //model.addAttribute("types", types);
    return "member/teachcourselist/prepubcourse";
  }

  @RequestMapping(value = "teachcourselist_transactions", method = RequestMethod.GET)
  public String transactions(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = tradingrecordService.pageByUserGood(member.getId(),
//          1, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", 1);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/transactions";
    } else {
      return login;

    }
  }

  @RequestMapping(value = "teachcourselist_transactions_payments", method = RequestMethod.GET)
  public String transactions_payments(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = tradingrecordService.pageByUserGood(member.getId(),
//          2, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", 2);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/transactions_payments";
    } else {
      return login;

    }
  }

  @RequestMapping(value = "teachcourselist_transactions_refunds", method = RequestMethod.GET)
  public String transactions_refunds(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = tradingrecordRefundService.pageByForTeacher(member.getId(),
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", 3);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teachcourselist/transactions_refunds";
    } else {
      return login;

    }
  }

  @RequestMapping(value = "preupdatecourse", method = RequestMethod.GET)
  public String preupdatecourse(int id, HttpServletRequest request,
                                HttpServletResponse response, Model model) {
    setPubAddressList(model, request);
    //List<CourseCatalog> types = coursetypeService.findTop();
    //model.addAttribute("types", types);
    model.addAttribute("course", courseService.findById(id));
    model.addAttribute("imgs", courseImgService.pageByCourse(id, 1, 100)
        .getList());
    return "member/teachcourselist/preupdatecourse";
  }

  @RequestMapping(value = "prerefcourse", method = RequestMethod.GET)
  public String prerefcourse(int id, HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    setPubAddressList(model, request);
    //List<CourseCatalog> types = coursetypeService.findTop();
    //model.addAttribute("types", types);
    Course course = courseService.findById(id);
    if (course.getCourse() != null) {
      course = course.getCourse();
    }
    model.addAttribute("course", course);
    model.addAttribute("imgs", courseImgService.pageByCourse(id, 1, 100)
        .getList());
    return "member/teachcourselist/prerefcourse";
  }

  @RequestMapping(value = "courserecord", method = RequestMethod.GET)
  public String courserecord() {
    return "user/courserecord";
  }

  @RequestMapping(value = "pubcourse", method = RequestMethod.POST)
  public String pubcourse(Course course, String[] imgurls, Integer pid,
                          HttpServletRequest request, HttpServletResponse response,
                          Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      // course.setBegintime(FrontUtils.parseTimestamp(begintime));
      // course.setDuedate(FrontUtils.parseTimestamp(duedate));
      if (pid != null) {

        CourseCatalog courseCatalog = new CourseCatalog();
        courseCatalog.setId(pid);
        course.setCourseCatalog(courseCatalog);
      }
      course.setJointimes(0);
      course.setJointimes(0);
      course.setStatetype(1);
      course.setMember(member);
      course.setAdddate(new Timestamp(System.currentTimeMillis()));
      courseService.add(course, imgurls);
      model.addAttribute("msg", "添加课程成功");
      return "member/teachcourselist/pubcourseok";
    } else {
      return login;

    }
  }

  @RequestMapping(value = "pubcourseok", method = RequestMethod.GET)
  public String pubcourseok() {
    return "member/teachcourselist/pubcourseok";
  }

  @RequestMapping(value = "updatecourse", method = RequestMethod.POST)
  public String updatecourse(Course course, String[] imgurls,
                             String startime, Integer pid, HttpServletRequest request,
                             HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      //course.setBegintime(FrontUtils.parseTimestamp(startime));
      if (pid != null) {
        CourseCatalog courseCatalog = new CourseCatalog();
        courseCatalog.setId(pid);
        course.setCourseCatalog(courseCatalog);
      }
      course.setStatetype(1);
      course.setMember(member);
      course.setAdddate(new Timestamp(System.currentTimeMillis()));
      courseService.update(course, imgurls);
      model.addAttribute("msg", "更新课程成功");
      return "common/text";
    } else {
      return login;

    }
  }


  @Autowired
  CourseTeacherCommentService commentService;


  @RequestMapping(value = "teachercourse_precomment", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String precomment(Model model, int lession, int courseid,
                           HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      CourseTeacherComment course = commentService.findCourse(courseid, lession);
      model.addAttribute("lessionid", lession);
      model.addAttribute("courseid", courseid);
      if (course != null) {
        model.addAttribute("item", course);
        return "member/teachcourselist/updatecomment";
      } else {
        return "member/teachcourselist/precomment";

      }
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachercourse_savecomment", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String savecomment(Model model, CourseTeacherComment comment,
                            HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      comment.setMember(member);
      comment.setUps(0);
      comment.setPubdate(new Timestamp(System.currentTimeMillis()));
      CourseTeacherComment course = commentService.comment(comment);
      if (course != null) {
        model.addAttribute("msg", "ok");
      } else {
        model.addAttribute("msg", "nook");
      }
      return "common/text";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teachercourse_updatecomment", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String updatecomment(Model model, CourseTeacherComment comment,
                              HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      CourseTeacherComment course = commentService.update(comment);
      if (course != null) {
        model.addAttribute("msg", "ok");
      } else {
        model.addAttribute("msg", "nook");
      }
      return "common/text";
    } else {
      return login;

    }

  }

  @Autowired
  CourseLessonStudentService courseLessonStudentService;

  @RequestMapping(value = "teachercourse_changestates", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String changestates(Model model, String info, long id,
                             HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      CourseLessonStudent course = courseLessonStudentService.updateInfo(id, info);
      if (course != null) {
        model.addAttribute("msg", "ok");
      } else {
        model.addAttribute("msg", "nook");
      }
      return "common/text";
    } else {
      return login;

    }

  }

  private void setPubAddressList(Model model, HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
//    if (member != null) {
//      if (member.getMembertype().getId() == 2) {
//        model.addAttribute("defaddress", member.getTeacheraddress());
//      } else {
//        model.addAttribute("defaddress", member.getSchooladdress());
//      }
//      model.addAttribute("addresses", schoolnetworkService.getPage(member.getId(), 0, 0).getList());
//    }
  }
}
