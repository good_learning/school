package com.haoxuer.school.actions.member;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.CourseService;
import com.haoxuer.school.data.service.SchoolteacherService;

/**
 * 老师授课管理
 *
 * @author 年高
 */

@Controller
@RequestMapping(value = "member")
public class TeacherTeaching extends BaseAction {

  @Autowired
  CourseService courseService;

  @Autowired
  SchoolteacherService schoolteacherService;

  @RequestMapping(value = "teacherteaching_teaching", method = RequestMethod.GET)
  public String teaching(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.teachingsForStar(member.getId(), 0,
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teacherteaching/teaching";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teacherteaching_start", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String start(Model model, int courseid) {
    Course course = courseService.startCourseForTeacher(courseid);
    if (course != null) {
      model.addAttribute("msg", "开始课程成功");
    }
    return "common/text";
  }

  @RequestMapping(value = "teacherteaching_teachingend", method = RequestMethod.GET)
  public String teachingend(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.teachingsForTeacher(member.getId(), 2,
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }

      return "member/teacherteaching/teachingend";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "teacherteaching_teachingstart", method = RequestMethod.GET)
  public String teachingstart(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseService.teachingsForTeacher(member.getId(), 1,
//          curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/teacherteaching/teachingstart";
    } else {
      return login;

    }

  }

}
