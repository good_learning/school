package com.haoxuer.school.actions.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.SchoolteacherService;


/**
 * 师资管理
 *
 * @author 年高
 */

@RequestMapping(value = "member")
@Controller
public class TeachersAction extends BaseAction {

  @Autowired
  SchoolteacherService schoolteacherService;

  /**
   * 入住申请
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "teachers_list", method = RequestMethod.GET)
  public String teachers_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = schoolteacherService.findBySchoolAll(member.getId(),
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);

      String ret = "member/teachers/list";
      return ret;

    } else {
      return login;
    }

  }

  /**
   * 我的老师
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "teachers_mylist", method = RequestMethod.GET)
  public String teachers_mylist(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, HttpServletResponse response,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = schoolteacherService.findByType(member.getId(), 1,
          curpage, pagesize);
      model.addAttribute("page", page);
      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);

      String ret = "member/teachers/mylist";
      return ret;

    } else {
      return login;
    }

  }


  @RequestMapping(value = "teachers_agree", method = RequestMethod.GET)
  public String teachers_agree(int id,
                               @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
                               @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
                               HttpServletRequest request, HttpServletResponse response,
                               Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      schoolteacherService.agree(id);
      return "redirect:/member/teachers_list.htm";

    } else {
      return login;
    }

  }

  @RequestMapping(value = "teachers_cancel", method = RequestMethod.GET)
  public String teachers_cancel(int id,
                                HttpServletRequest request, HttpServletResponse response,
                                Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      schoolteacherService.cancel(id);
      return "redirect:/member/teachers_list.htm";

    } else {
      return login;
    }

  }

  @RequestMapping(value = "teachers_reject", method = RequestMethod.GET)
  public String teachers_reject(int id,
                                HttpServletRequest request, HttpServletResponse response,
                                Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      schoolteacherService.reject(id);
      return "redirect:/member/teachers_list.htm";

    } else {
      return login;
    }

  }

  @RequestMapping(value = "teachers_agreegood", method = RequestMethod.GET)
  public String teachers_agreegood(int id,
                                   HttpServletRequest request, HttpServletResponse response,
                                   Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      schoolteacherService.agreegood(id);
      return "redirect:/member/teachers_mylist.htm";

    } else {
      return login;
    }

  }

  @RequestMapping(value = "teachers_cancelgood", method = RequestMethod.GET)
  public String teachers_cancelgood(int id,
                                    HttpServletRequest request, HttpServletResponse response,
                                    Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      schoolteacherService.cancelgood(id);
      return "redirect:/member/teachers_mylist.htm";

    } else {
      return login;
    }

  }

  @RequestMapping(value = "teachers_delete", method = RequestMethod.GET)
  public String teachers_delete(int id,
                                HttpServletRequest request, HttpServletResponse response,
                                Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      schoolteacherService.deleteById(id);
      return "redirect:/member/teachers_mylist.htm";

    } else {
      return login;
    }

  }
}
