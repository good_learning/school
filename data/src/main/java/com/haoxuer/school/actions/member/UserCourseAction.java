package com.haoxuer.school.actions.member;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.CourseComment;
import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.entity.TradingrecordRefund;
import com.haoxuer.school.data.service.CourseCommentService;
import com.haoxuer.school.data.service.CourseLessonStudentService;
import com.haoxuer.school.data.service.CourseSubscribeService;
import com.haoxuer.school.data.service.TradingrecordRefundService;
import com.haoxuer.school.data.service.TradingrecordService;

@Controller
@RequestMapping(value = "member")
public class UserCourseAction extends BaseAction {

  @Autowired
  TradingrecordService tradingrecordService;

  @Autowired
  CourseLessonStudentService courseLessonStudentService;

  @Autowired
  CourseCommentService commentService;

  @Autowired
  CourseSubscribeService courseSubscribeService;

  /**
   * 我的课程》未付款的课程
   *
   * @param request
   * @param response
   * @param curpage
   * @param pagesize
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_transactions", method = RequestMethod.GET)
  public String transactions(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = tradingrecordService.pageByBuyUser(
//          member.getId(), 1, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", 1);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/usercourse/transactions";
    } else {
      return login;

    }
  }

  /**
   * 我的课程》已付款的课程
   *
   * @param request
   * @param response
   * @param curpage
   * @param pagesize
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_transactions_payments", method = RequestMethod.GET)
  public String transactions_payments(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = tradingrecordService.pageByBuyUser2(
//          member.getId(), 2, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", 2);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/usercourse/transactions_payments";
    } else {
      return login;

    }
  }

  /**
   * 我的课程》已付款的课程
   *
   * @param request
   * @param response
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_transactions_delete", method = RequestMethod.GET)
  public String transactions_delete(
      HttpServletRequest request,
      HttpServletResponse response,
      int id,
      @RequestParam(value = "type", required = true, defaultValue = "10") int type,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      tradingrecordService.delete(id);
      if (type == 1) {
        return "redirect:/member/usercourse_transactions.htm";
      } else if (type == 2) {
        return "redirect:/member/usercourse_transactions_payments.htm";

      } else if (type == 3) {
        return "redirect:/member/usercourse_transactions_refunds.htm";

      } else {
        return "redirect:/member/usercourse_transactions.htm";
      }
    } else {
      return login;

    }
  }

  /**
   * 我的课程》》退款中
   *
   * @param request
   * @param response
   * @param curpage
   * @param pagesize
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_transactions_refunds", method = RequestMethod.GET)
  public String transactions_refunds(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = tradingrecordService.pageByBuyUserInfo(
//          member.getId(), 3, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", 3);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/usercourse/transactions_refunds";
    } else {
      return login;

    }
  }

  @RequestMapping(value = "pre_course_refunds", method = RequestMethod.GET)
  public String pre_course_refunds(int id, HttpServletRequest request,
                                   HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Tradingrecord item = tradingrecordService.findById(id);
      model.addAttribute("item", item);
      return "member/usercourse/pre_course_refunds";
    } else {
      return login;

    }
  }

  @Autowired
  private TradingrecordRefundService tradingrecordRefundService;

  @RequestMapping(value = "course_refunds", method = {RequestMethod.GET,
      RequestMethod.POST})
  public String course_refunds(TradingrecordRefund tradingrecordRefund, HttpServletRequest request,
                               HttpServletResponse response, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      tradingrecordRefund.setMember(member);
      TradingrecordRefund item = tradingrecordRefundService.refunds(tradingrecordRefund);
      return "redirect:/member/usercourse_transactions_payments.htm";
    } else {
      return login;

    }
  }

  /**
   * 我的课程》》已付款》》确认上课
   *
   * @param request
   * @param response
   * @param curpage
   * @param pagesize
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_transactions_mylessionlist", method = RequestMethod.GET)
  public String mylessionlist(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "100") int pagesize,
      int tradingrecordid, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = courseLessonStudentService.update(member.getId(),
//          tradingrecordid, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("type", 3);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/usercourse/transactions_mylessionlist";
    } else {
      return login;

    }
  }

  @RequestMapping(value = "usercourse_transactions_finshed", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String finshed(Model model, long id, HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      CourseLessonStudent course = courseLessonStudentService.finshed2(
//          member.getId(), id);
//      if (course != null && course.getState() == 1) {
//        model.addAttribute("msg", "ok");
//      }
      return "common/text";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "usercourse_precomment", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String precomment(Model model, int lession, int courseid,
                           HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      CourseComment course = commentService.findCourse(courseid, lession);
      model.addAttribute("lessionid", lession);
      model.addAttribute("courseid", courseid);
      if (course != null) {
        model.addAttribute("item", course);
        return "member/usercourse/updatecomment";
      } else {
        return "member/usercourse/precomment";

      }
    } else {
      return login;

    }

  }

  @RequestMapping(value = "usercourse_savecomment", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String savecomment(Model model, CourseComment comment,
                            HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      comment.setMember(member);
      comment.setUps(0);
      comment.setPubdate(new Timestamp(System.currentTimeMillis()));
      CourseComment course = commentService.comment(comment);
      if (course != null) {
        model.addAttribute("msg", "ok");
      } else {
        model.addAttribute("msg", "nook");
      }
      return "common/text";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "usercourse_updatecomment", method = {
      RequestMethod.GET, RequestMethod.POST})
  public String updatecomment(Model model, CourseComment comment,
                              HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      CourseComment course = commentService.update(comment);
      if (course != null) {
        model.addAttribute("msg", "ok");
      } else {
        model.addAttribute("msg", "nook");
      }
      return "common/text";
    } else {
      return login;

    }

  }

  /**
   * 回收站  课程
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_recyclers", method = RequestMethod.GET)
  public String recyclers(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      int statetype = 1;
//      Pagination page = courseSubscribeService.pageForRecyclers(member.getId(),
//          statetype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/usercourse/recyclers";
    } else {
      return login;

    }

  }


  /**
   * 回收站  课程预约
   *
   * @param sorttype
   * @param curpage
   * @param pagesize
   * @param request
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_recyclers_coursesubscribe", method = RequestMethod.GET)
  public String recyclers_coursesubscribe(
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      int statetype = 2;
//      Pagination page = courseSubscribeService.pageForRecyclers(member.getId(),
//          statetype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/usercourse/recyclers_coursesubscribe";
    } else {
      return login;

    }

  }

  /**
   * 我的预约
   *
   * @param sorttype
   * @param curpage
   * @param pagesize
   * @param request
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_mycoursesubscribelist", method = RequestMethod.GET)
  public String mycoursesubscribelist(
      @RequestParam(value = "sorttype", required = true, defaultValue = "1") int sorttype,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      int statetype = 1;
//      Pagination page = courseSubscribeService.pageByUser(member.getId(),
//          statetype, curpage, pagesize);
//      if (page != null) {
//        model.addAttribute("page", page);
//        List<?> datas = page.getList();
//        model.addAttribute("list", datas);
//        model.addAttribute("sorttype", sorttype);
//        model.addAttribute("curpage", curpage);
//        model.addAttribute("pagesize", pagesize);
//      }
      return "member/usercourse/mycoursesubscribelist";
    } else {
      return login;

    }

  }

  /**
   * 我的预约 删除
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_deletecoursesubscribe", method = RequestMethod.GET)
  public String deletecoursesubscribe(
      long id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      courseSubscribeService.delete(id);
      return "redirect:/member/usercourse_mycoursesubscribelist.htm";

    } else {
      return login;

    }

  }

  /**
   * 我的预约 还原
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_removecoursesubscribe", method = RequestMethod.GET)
  public String removecoursesubscribe(
      long id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      courseSubscribeService.remove(id);
      return "redirect:/member/usercourse_recyclers_coursesubscribe.htm";

    } else {
      return login;

    }

  }

  @RequestMapping(value = "usercourse_removecourse", method = RequestMethod.GET)
  public String usercourse_removecourse(
      long id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      courseSubscribeService.remove(id);
      return "redirect:/member/usercourse_recyclers.htm";

    } else {
      return login;

    }

  }


  /**
   * 我的预约 还原
   *
   * @param curpage
   * @param pagesize
   * @param request
   * @param model
   * @return
   */
  @RequestMapping(value = "usercourse_backcoursesubscribe", method = RequestMethod.GET)
  public String backcoursesubscribe(
      long id,
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, Model model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      courseSubscribeService.back(id);
      return "redirect:/member/usercourse_recyclers.htm";

    } else {
      return login;

    }

  }
}
