package com.haoxuer.school.actions.member;

import java.sql.Timestamp;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.haoxuer.school.actions.front.ImageAction;
import com.haoxuer.school.data.entity.Attachment;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.MemberAuthentication;
import com.haoxuer.school.data.entity.Schoolteacher;
import com.haoxuer.school.data.service.AttachmentService;
import com.haoxuer.school.data.service.MemberAuthenticationService;
import com.haoxuer.school.data.service.SchoolteacherService;

@Controller
@RequestMapping(value = "member")
public class UserinfosAction extends ImageAction {

  @Autowired
  AttachmentService attachmentService;

  @RequestMapping(value = "imgadd", method = RequestMethod.POST)
  public String imgadd(
      @RequestParam(value = "imgFile", required = false) MultipartFile file,
      int type, HttpServletRequest request, ModelMap model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    String fileurl = upfile(file, request, model, "/attached/");
    if (member != null) {
      Attachment attachment = new Attachment();
      attachment.setFileurl(fileurl);
      attachment.setCatalog(type);
      String origName = attachment.getFileurl();
      if (origName != null) {
        String ext = FilenameUtils.getExtension(origName).toLowerCase(
            Locale.ENGLISH);
        attachment.setFiletype(ext);
      }
      attachment.setAdddate(new Timestamp(System.currentTimeMillis()));
      attachment.setMember(member);
      Attachment back = attachmentService.save(attachment);

      Upitem itemss = new Upitem();
      itemss.setError(0);
      itemss.setUrl(fileurl);
      Gson gson = new Gson();
      model.addAttribute("msg", gson.toJson(itemss));
      return "common/text";
    } else {
      return login;

    }

  }

  @RequestMapping("/userimages")
  public String userimages(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "type", required = true, defaultValue = "1") int type,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      return "/member/userinfos/userimages";
    } else {
      return login;

    }

  }

  @Autowired
  MemberAuthenticationService memberAuthenticationService;

  @RequestMapping("/authenticationinfo")
  public String authenticationinfo(HttpServletRequest request, ModelMap model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      return "/member/userinfos/authenticationinfo";
    } else {
      return login;

    }

  }

  @RequestMapping("/authentication")
  public String authentication(HttpServletRequest request, ModelMap model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      MemberAuthentication authentication = memberAuthenticationService
//          .findByUserId(member.getId());
//      model.addAttribute("item", authentication);
      return "/member/userinfos/authentication";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "/authenticationupdate", method = RequestMethod.POST)
  public String authenticationupdate(MemberAuthentication authentication,
                                     HttpServletRequest request, ModelMap model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      memberAuthenticationService.update(authentication);
      model.addAttribute("msg", "更新成功");

      return "/common/text";
    } else {
      return login;

    }

  }

  @RequestMapping("/attachment_list")
  public String attachment_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "type", required = true, defaultValue = "1") int type,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination pagination = attachmentService.pageByUserId(
//          member.getId(), type, curpage, pagesize);
//      model.addAttribute("curpage", pagination.getPageNo());
//      model.addAttribute("list", pagination.getList());
      return "/member/userinfos/attachment_list";
    } else {
      return login;

    }

  }

  @RequestMapping("/attachment_school_list")
  public String attachment_school_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "type", required = true, defaultValue = "1") int type,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    // 荣誉证书4 //教学环境5
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination pagination = attachmentService.pageByUserId(
//          member.getId(), 4, curpage, pagesize);
//      model.addAttribute("curpage", pagination.getPageNo());
//      model.addAttribute("list", pagination.getList());
//
//      Pagination pagination1 = attachmentService.pageByUserId(
//          member.getId(), 5, curpage, pagesize);
//      model.addAttribute("list1", pagination1.getList());
      return "/member/userinfos/attachment_school_list";
    } else {
      return login;

    }

  }

  @RequestMapping("/attachment_teacher_list")
  public String attachment_teacher_list(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "type", required = true, defaultValue = "1") int type,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    // 荣誉证书2 //授课风采3
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination pagination = attachmentService.pageByUserId(
//          member.getId(), 2, curpage, pagesize);
//      model.addAttribute("curpage", pagination.getPageNo());
//      model.addAttribute("list", pagination.getList());
//
//      Pagination pagination1 = attachmentService.pageByUserId(
//          member.getId(), 3, curpage, pagesize);
//      model.addAttribute("list1", pagination1.getList());
      return "/member/userinfos/attachment_teacher_list";
    } else {
      return login;

    }

  }

  @RequestMapping("/attachment_delete")
  public String attachment_delete(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "type", required = true, defaultValue = "1") int type,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      long id, HttpServletRequest request, ModelMap model) {
    // 荣誉证书2 //授课风采3
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      attachmentService.deleteById(id);
      model.addAttribute("msg", "删除成功");
      return "/common/text";
    } else {
      return login;

    }

  }

  @Autowired
  SchoolteacherService schoolteacherService;

  @RequestMapping("/my_schoolteacherlist")
  public String my_schoolteacherlist(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "type", required = true, defaultValue = "1") int type,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    // 荣誉证书4 //教学环境5
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Pagination page = schoolteacherService.pageForTeacher(
          member.getId(), 1, 10);
      if (page.getList() != null && page.getList().size() > 0) {
        model.addAttribute("list", page.getList());
      }
      model.addAttribute("page", page);
      return "/member/userinfos/my_schoolteacherlist";
    } else {
      return login;

    }

  }

  @RequestMapping("/my_schoolteacherlistinfo")
  public String my_schoolteacherlistinfo(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "type", required = true, defaultValue = "1") int type,
      @RequestParam(value = "pagesize", required = true, defaultValue = "20") int pagesize,
      HttpServletRequest request, ModelMap model) {
    // 荣誉证书4 //教学环境5
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      return "/member/userinfos/my_schoolteacherlistinfo";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "/addschoolteacher", method = RequestMethod.POST)
  public String addschoolteacher(String menpai,
                                 HttpServletRequest request, ModelMap model) {
    // 荣誉证书4 //教学环境5
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Schoolteacher page = schoolteacherService.addTeacher(menpai, member);

      return "redirect:/member/my_schoolteacherlist.htm";
    } else {
      return login;

    }

  }

  @RequestMapping(value = "/delteschoolteacher", method = RequestMethod.GET)
  public String delteschoolteacher(int id, HttpServletRequest request,
                                   ModelMap model) {
    // 荣誉证书4 //教学环境5
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      Schoolteacher page = schoolteacherService.deleteById(id);

      return "redirect:/member/my_schoolteacherlist.htm";
    } else {
      return login;

    }

  }

}
