package com.haoxuer.school.actions.member;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.haoxuer.discover.data.core.Pagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.haoxuer.school.actions.front.BaseAction;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.school.data.service.ViewPointTypesService;
import com.haoxuer.school.data.service.ViewpointService;
import com.haoxuer.school.web.utils.WebErrors;

@Controller
@RequestMapping(value = "member")
public class ViewpointAction extends BaseAction {
  private static final Logger log = LoggerFactory
      .getLogger(ViewpointAction.class);

  @RequestMapping("viewpoints_view_list")
  public String index(
      @RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
      @RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
      HttpServletRequest request, ModelMap model) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
//      Pagination page = manager.pageByUserId(member.getId(), curpage,
//          pagesize);
//      model.addAttribute("page", page);
//      model.addAttribute("list", page.getList());
      model.addAttribute("curpage", curpage);
      model.addAttribute("pagesize", pagesize);
      return "member/viewpoints/list";
    } else {
      return login;
    }

  }

  @RequestMapping("viewpoints_view_add")
  public String add(ModelMap model, HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      return "member/viewpoints/add";
    } else {
      return login;

    }
  }

  @RequestMapping("viewpoints_view_update")
  public String view_update(Integer id, ModelMap model,
                            HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      model.addAttribute("item", manager.findById(id));
      return "member/viewpoints/update";
    } else {
      return login;

    }
  }

  @RequestMapping("viewpoints_model_update")
  public String model_update(Viewpoint bean, Integer pageNo,
                             HttpServletRequest request, ModelMap model) {
//    WebErrors errors = validateUpdate(bean.getId(), request);
//    if (errors.hasErrors()) {
//      return errors.showErrorPage(model);
//    }
    bean = manager.update(bean);
    log.info("update Viewpoint id={}.", bean.getId());
    return "member/viewpoints/pubcourseok";
  }


  @RequestMapping("viewpoints_model_delete")
  public String model_delete(Integer id, ModelMap model,
                             HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      manager.deleteById(id);
      return "redirect:/member/viewpoints_view_list.htm";
    } else {
      return login;

    }
  }

  @RequestMapping("/viewpoint/fav")
  public String fav(ModelMap model, HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      return "viewpoint/fav";
    } else {
      return login;

    }
  }

  @RequestMapping("/viewpoint/v_edit")
  public String edit(Integer id, Integer pageNo, HttpServletRequest request,
                     ModelMap model) {
    WebErrors errors = validateEdit(id, request);
    if (errors.hasErrors()) {
      return errors.showErrorPage(model);
    }
    model.addAttribute("viewpoint", manager.findById(id));
    model.addAttribute("pageNo", pageNo);
    return "user/viewpoint/edit";
  }

  @RequestMapping(value = "viewpoints_model_add", method = RequestMethod.POST)
  public String save(Viewpoint bean, HttpServletRequest request,
                     ModelMap model, Integer catalogs) {
    WebErrors errors = validateSave(bean, request);
    if (errors.hasErrors()) {
      return errors.showErrorPage(model);
    }
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {
      bean.setMember(member);
      bean.setPubdate(new Timestamp(System.currentTimeMillis()));
      bean.setUps(0);
      bean = manager.save(bean);
      return "member/viewpoints/pubcourseok";
      // model.addAttribute("item", bean);
      // return "redirect:/member/viewpoints_pubcourseok.htm";
    } else {
      return login;

    }
  }

  @RequestMapping("viewpoints_pubcourseok")
  public String pubcourseok(ModelMap model, HttpServletRequest request) {
    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");
    if (member != null) {

      return "member/viewpoints/pubcourseok";
    } else {
      return login;

    }
  }

  @RequestMapping("/viewpoint/o_update")
  public String update(Viewpoint bean, Integer pageNo,
                       HttpServletRequest request, ModelMap model) {
//    WebErrors errors = validateUpdate(bean.getId(), request);
//    if (errors.hasErrors()) {
//      return errors.showErrorPage(model);
//    }
    bean = manager.update(bean);
    log.info("update Viewpoint id={}.", bean.getId());
    return "";
  }

  @RequestMapping("/viewpoint/o_delete")
  public String delete(Integer[] ids, Integer pageNo,
                       HttpServletRequest request, ModelMap model) {
    WebErrors errors = validateDelete(ids, request);
    if (errors.hasErrors()) {
      return errors.showErrorPage(model);
    }
    Viewpoint[] beans = manager.deleteByIds(ids);
    for (Viewpoint bean : beans) {
      log.info("delete Viewpoint id={}", bean.getId());
    }
    return "";
  }

  private WebErrors validateSave(Viewpoint bean, HttpServletRequest request) {
    WebErrors errors = WebErrors.create(request);
    return errors;
  }

  private WebErrors validateEdit(Integer id, HttpServletRequest request) {
    WebErrors errors = WebErrors.create(request);
    return errors;
  }

  private WebErrors validateUpdate(Integer id, HttpServletRequest request) {
    WebErrors errors = WebErrors.create(request);
    return errors;
  }

  private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
    WebErrors errors = WebErrors.create(request);
    return errors;
  }

  private boolean vldExist(Integer id, Integer siteId, WebErrors errors) {
    if (errors.ifNull(id, "id")) {
      return true;
    }
    Viewpoint entity = manager.findById(id);
    return errors.ifNotExist(entity, Viewpoint.class, id);
  }

  @Autowired
  private ViewpointService manager;

  @Autowired
  private ViewPointTypesService viewPointTypesService;
}