package com.haoxuer.school.common.security;

import org.apache.commons.codec.binary.Base64;


public class Base64Coder {
  public final static String ENCODING = "UTF-8";

  public static String encode(byte[] data) throws Exception {

    // 执行编码
    byte[] b = Base64.encodeBase64(data);

    return new String(b, ENCODING);
  }

  public static String encodeSafe(String data) throws Exception {

    // 执行编码
    byte[] b = Base64.encodeBase64(data.getBytes(ENCODING), true);// 可选换行符

    return new String(b, ENCODING);
  }

  public static byte[] decode(String data) throws Exception {

    // 执行解码
    byte[] b = Base64.decodeBase64(data.getBytes(ENCODING));

    return b;
  }
}