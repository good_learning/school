package com.haoxuer.school.common.security;

import java.security.spec.KeySpec;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class Des3Util {

  static Map<String, String> saltSet = new HashMap<String, String>(); // 用于存储临随机码

  static String keydesede = "k4@#$%^&^F*O)_<>:?9684cai&4^*3J5"; // 固定密钥，共32个字节，这个密钥可以随便设定32个字节，发送消息要传送
  static byte[] key = keydesede.getBytes();// 密钥转字节型

  public static String RandomSalt(int len) {
    if (len == 0) {
      return "";
    }
    int a = (int) (Math.random() * 3);
    if (a == 0) {
      return ((int) (Math.random() * 10)) + RandomSalt(len - 1);
    } else if (a == 1) {
      return ((char) ((int) (Math.random() * 26) + 65))
          + RandomSalt(len - 1);
    } else {
      return ((char) ((int) (Math.random() * 26) + 97))
          + RandomSalt(len - 1);
    }
  }

  public static void SetSaltInSet(String key, String value) {
    if (saltSet.containsKey(key)) {
      saltSet.replace(key, value);
    } else {
      saltSet.put(key, value);
    }
  }

  public static void ClearSaltSet() {
    saltSet.clear();
  }

  public static void RemoveSalt(String key) {
    if (saltSet.containsKey(key)) {
      saltSet.remove(key);
    }
  }

  public static String GetSaltFromSet(String key) {
    return saltSet.getOrDefault(key, "");
  }

  public static String Encrypt(String content, String random) {
    try {
      byte[] key = keydesede.getBytes();// 密钥转字节型
      byte[] data = content.getBytes();
      byte[] iv = random.getBytes();

      String HexcipherText;

      // System.out.println("\n开始生成DESede密钥");
      IvParameterSpec ivps;
      ivps = new IvParameterSpec(iv, 0, iv.length);

      SecretKeyFactory kf = SecretKeyFactory.getInstance("DESede"); // 设置密钥工厂模式为DESede，可以设置为AES、DES、DESede、PBEWith等模式
      DESedeKeySpec ks = new DESedeKeySpec(key); // 新建密钥规范, 使密钥规范化
      SecretKey ky2 = kf.generateSecret(ks); // 生成密钥,最后的执行密钥是ky2

      Cipher cipher = Cipher.getInstance("DESede/OFB/PKCS5Padding");
      // 打印Cipher对象密码服务提供者信息
      // System.out.println("\n" + cipher.getProvider().getInfo());
      // 加密
      // System.out.println("\n开始加密");
      cipher.init(Cipher.ENCRYPT_MODE, ky2, ivps);// cipher对象初始化，设置为加密
      byte[] cipherText = cipher.doFinal(data);// 结束数据加密，输出密文
      // cipherTextdes = cipherText;// 把密文赋给全局的密文变量
      HexcipherText = Base64Coder.encode(cipherText);

      System.out.println("加密完成，密文为：");
      System.out.println(HexcipherText);
      // System.out.println(new String(cipherText));// 打印乱码密文
      return HexcipherText;// 返回密文,返回的是字符串型的密文

    } catch (Exception e) {
      System.out.println("加密出错");// 这里在放入android时请去掉
      return null;
    }
  }

  public static String Decrypt(String content, String random) {

    byte[] iv = random.getBytes();
    byte[] newPlainText;
    byte[] data;
    try {
      data = Base64Coder.decode(content);
      IvParameterSpec ivps;
      ivps = new IvParameterSpec(iv, 0, iv.length);
      KeySpec ks = new DESedeKeySpec(key);
      SecretKeyFactory kf = SecretKeyFactory.getInstance("DESede");
      SecretKey ky = kf.generateSecret(ks);
      Cipher cipher = Cipher.getInstance("DESede/OFB/PKCS5Padding");
      cipher.init(Cipher.DECRYPT_MODE, ky, ivps);
      // 输出原文
      newPlainText = cipher.doFinal(data);// 这里在放入android时请去掉
      System.out.println("解密完成，明文为：");// 这里在放入android时请去掉

      System.out.println(new String(newPlainText));
      return new String(newPlainText);// 返回明文
    } catch (Exception e) {
      System.out.println("解密出错");// 这里在放入android时请去掉
      return null;
    }
  }

  protected static String generateToken() {
    UUID uuid = UUID.randomUUID();
    String token = uuid.toString().toUpperCase().replaceAll("-", "");
    return token;
  }

  public static void main(String[] args) {
    String salt = RandomSalt(8);

    String content = "发撒手动阀手动阀";
    System.out.println(content);
    System.out.println(salt);
    String datas = Encrypt(content, salt);
    Decrypt(datas, salt);
  }
}
