package com.haoxuer.school.common.template;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.haoxuer.school.data.entity.Articlecomment;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class Apps {

  public static void main(String[] args) {
    // TODO Auto-generated method stub

    try {
      Articlecomment item;
      Configuration cfg = new Configuration();
      Locale locale = Locale.CHINA;
      cfg.setEncoding(locale, "utf-8");
      String dir = Apps.class.getResource("/ftl/").getFile();
      dir = "d:\\backupfile\\";
      File file = new File(dir);
      cfg.setDirectoryForTemplateLoading(file);
      Template template = cfg.getTemplate("comment.html");
      Writer out = new StringWriter();
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("item", "item");
      template.process(map, out);
      System.out.println(out.toString());
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (TemplateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  private static void dd() {
    Reader reader = new StringReader("haha ${age} ${date?date}");
    try {
      Configuration cfg = new Configuration();
      Locale locale = Locale.CHINA;
      cfg.setEncoding(locale, "utf-8");
      Template template = new Template("", reader, cfg);
      Writer out = new StringWriter();
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("age", "陈联高");
      map.put("date", new Date());
      template.process(map, out);
      System.out.println(out.toString());
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (TemplateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
