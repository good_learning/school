package com.haoxuer.school.data.apps;

import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.school.data.service.ArticletypeService;

public class ArticletypeServiceApps {
  public static void main(String[] args) {
    // TODO Auto-generated method stub
    ArticletypeService userService = ObjectFactory.get().getBean(
        ArticletypeService.class);

    String name = "经验分享";
    int id = 1;

    add(userService, "教育资讯", 4);
    add(userService, "行业专题", 4);
    add(userService, "行业人物", 4);

    //userService.deleteById(18);


  }

  private static void add(ArticletypeService userService, String name, int id) {
    Articletype type = new Articletype();
    type.setName(name);
    Articletype p = new Articletype();
    p.setId(id);
    type.setArticletype(p);

    userService.save(type);
  }
}
