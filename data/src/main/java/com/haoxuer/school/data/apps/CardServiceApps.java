package com.haoxuer.school.data.apps;

import com.haoxuer.school.data.service.CardService;

public class CardServiceApps {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    CardService userService = ObjectFactory.get().getBean(
        CardService.class);
    long time = System.currentTimeMillis();
    System.out.println(userService.buildCards(3, 100, 5000));
    time = System.currentTimeMillis() - time;
    System.out.println(time);
  }

}
