package com.haoxuer.school.data.apps;

import com.haoxuer.school.data.service.MemberVisitService;
import com.haoxuer.discover.data.core.Pagination;

public class MemberVisitServiceApp {

  public static void main(String[] args) {
    // TODO Auto-generated method stub

    MemberVisitService userService = ObjectFactory.get().getBean(
        MemberVisitService.class);
    Pagination pages = userService.findByUserId(7, 1, 12);

    System.out.println(pages.getList());
  }

}
