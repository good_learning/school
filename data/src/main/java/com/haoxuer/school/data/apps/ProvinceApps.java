package com.haoxuer.school.data.apps;

import java.io.IOException;
import java.util.List;

import com.haoxuer.school.data.entity.City;
import com.haoxuer.school.data.entity.Province;
import com.haoxuer.school.data.entity.Town;
import com.haoxuer.school.data.service.CityService;
import com.haoxuer.school.data.service.ProvinceService;
import com.haoxuer.school.data.service.TownService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ProvinceApps {
  public static final String TOWN = "http://cdn.weather.hao.360.cn/sed_api_area_query.php?grade=town&_jsonp=loadTown&code=0804";
  public static final String TOWN1 = "http://cdn.weather.hao.360.cn/sed_api_area_query.php";

  public static void main(String[] args) throws IOException {
//		ProvinceService pservice = ObjectFactory.get().getBean(
//				ProvinceService.class);
//
//		List<Province> provinces = pservice.all();
//		for (Province province : provinces) {
//			adddowndatas(province.getId());
//
//		}
//		//TOWN
//		adddowndatas(1);
//		
    Connection con = Jsoup
        .connect("http://cdn.weather.hao.360.cn/sed_api_area_query.php");
    con.data("code", "1101");
    con.data("grade", "town");
    con.data("_jsonp", "loadTown");

    String body = con.execute().body();
    System.out.println(body);
    JsonParser jsonParser = new JsonParser();
    JsonElement jsonElement = jsonParser.parse(toJson(body));
    System.out.println(jsonElement);
    JsonArray arrays = jsonElement.getAsJsonArray();

    for (JsonElement jsonElement2 : arrays) {

      JsonArray arrays2 = jsonElement2.getAsJsonArray();
      String name = arrays2.get(0).getAsString();
      int id = arrays2.get(1).getAsInt();
      System.out.println("" + name + "  id:" + id);
    }
  }

  private static void adddowndatas(int pid) {
    CityService service = ObjectFactory.get().getBean("cityServiceImpl", CityService.class);
    List<City> citys = service.findByProvince(pid);
    for (City city : citys) {
      System.out.println(city.getName());

      String body = dd(city);
      while (body == null) {
        body = dd(city);
      }

      System.out.println(body);
      JsonParser jsonParser = new JsonParser();
      JsonElement jsonElement = jsonParser.parse(toJson(body));
      System.out.println(jsonElement);
      JsonArray arrays = jsonElement.getAsJsonArray();

      for (JsonElement jsonElement2 : arrays) {

        JsonArray arrays2 = jsonElement2.getAsJsonArray();
        String name = arrays2.get(0).getAsString();
        int id = arrays2.get(1).getAsInt();
        Town town = new Town();
        town.setCity(city);
        town.setName(name);
        town.setId(id);
        try {
          TownService tservice = ObjectFactory.get().getBean("townServiceImpl", TownService.class);
          tservice.save(town);

        } catch (Exception e) {
          // TODO: handle exception
        }
        System.out.println("" + name + "  id:" + id);
      }
    }
  }

  private static String dd(City city) {
    Connection con = Jsoup
        .connect("http://cdn.weather.hao.360.cn/sed_api_area_query.php");
    con.data("code", city.getId() < 1000 ? "0" + city.getId()
        + "" : city.getId() + "");
    con.data("grade", "town");
    con.data("_jsonp", "loadTown");

    String body = null;
    try {
      body = con.execute().body();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return body;
  }

  private static void ff() {
    CityService service = ObjectFactory.get().getBean(CityService.class);

    ProvinceService pservice = ObjectFactory.get().getBean(
        ProvinceService.class);

    List<Province> provinces = pservice.all();
    for (Province province : provinces) {
      System.out.println(province.getName());

      try {
        String body = "";
        Connection con = Jsoup
            .connect("http://cdn.weather.hao.360.cn/sed_api_area_query.php");
        con.data("code", province.getId() < 10 ? "0" + province.getId()
            + "" : province.getId() + "");
        con.data("grade", "city");
        con.data("_jsonp", "loadCity");

        body = con.execute().body();
        System.out.println(toJson(body));
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(toJson(body));
        System.out.println(jsonElement);
        JsonArray arrays = jsonElement.getAsJsonArray();

        for (JsonElement jsonElement2 : arrays) {

          JsonArray arrays2 = jsonElement2.getAsJsonArray();
          String name = arrays2.get(0).getAsString();
          int id = arrays2.get(1).getAsInt();
          City city = new City();
          city.setProvince(province);
          city.setName(name);
          city.setId(id);
          service.add(city);
          System.out.println("" + name + "  id:" + id);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private static void ggg() {
    ProvinceService service = ObjectFactory.get().getBean(
        ProvinceService.class);
    try {
      String body = Jsoup
          .connect(
              "http://cdn.weather.hao.360.cn/sed_api_area_query.php?grade=province&_jsonp=loadProvince")
          .execute().body();
      System.out.println(toJson(body));
      JsonParser jsonParser = new JsonParser();
      JsonElement jsonElement = jsonParser.parse(toJson(body));
      System.out.println(jsonElement);
      JsonArray arrays = jsonElement.getAsJsonArray();

      for (JsonElement jsonElement2 : arrays) {

        JsonArray arrays2 = jsonElement2.getAsJsonArray();
        String name = arrays2.get(0).getAsString();
        int id = arrays2.get(1).getAsInt();

        Province p = new Province();
        p.setId(id);
        p.setName(name);
        service.add(p);
        System.out.println("" + name + "  id:" + id);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String toJson(String jsonStr) {

    return jsonStr.substring(jsonStr.lastIndexOf("(") + 1,
        jsonStr.lastIndexOf(")"));
  }
}
