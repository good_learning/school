package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.forum.ActivityComment;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ActivityCommentDao {
  Pagination getPage(int pageNo, int pageSize);

  ActivityComment findById(Integer id);

  ActivityComment save(ActivityComment bean);

  ActivityComment updateByUpdater(Updater<ActivityComment> updater);

  ActivityComment deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);
}