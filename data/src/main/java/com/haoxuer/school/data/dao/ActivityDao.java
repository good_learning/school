package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.forum.Activity;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ActivityDao {
  Pagination getPage(int pageNo, int pageSize);

  Activity findById(Integer id);

  Activity save(Activity bean);

  Activity updateByUpdater(Updater<Activity> updater);

  Activity deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);
}