package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.forum.ActivityMember;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ActivityMemberDao {
  Pagination getPage(int pageNo, int pageSize);

  ActivityMember findById(Integer id);

  ActivityMember save(ActivityMember bean);

  ActivityMember updateByUpdater(Updater<ActivityMember> updater);

  ActivityMember deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);
}