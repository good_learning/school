package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.forum.ActivitySchedule;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ActivityScheduleDao {
  Pagination getPage(int pageNo, int pageSize);

  ActivitySchedule findById(Integer id);

  ActivitySchedule save(ActivitySchedule bean);

  ActivitySchedule updateByUpdater(Updater<ActivitySchedule> updater);

  ActivitySchedule deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);
}