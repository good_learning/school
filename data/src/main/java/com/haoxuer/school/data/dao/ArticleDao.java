package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ArticleDao {
  Pagination getPage(int pageNo, int pageSize);

  Article findById(Integer id);

  Article save(Article bean);

  Article updateByUpdater(Updater<Article> updater);

  Article deleteById(Integer id);

  List<Article> findArticlesById(Integer id);

  List<Article> findArticlesByPid(Integer id, int size, int sorttype);

  Pagination pageByPid(Integer id, int sorttype, int size, int curpage);

  Pagination recommend(int type, int size, int curpage);

}