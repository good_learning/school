package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Articlecomment;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ArticlecommentDao {
  Pagination getPage(int pageNo, int pageSize);

  Articlecomment findById(Integer id);

  Articlecomment save(Articlecomment bean);

  Articlecomment updateByUpdater(Updater<Articlecomment> updater);

  Articlecomment deleteById(Integer id);

  Pagination pageByArticleId(int id, int sorttype, int pagesize,
                             int curpage);
}