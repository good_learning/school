package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ArticletypeDao {
  Pagination getPage(int pageNo, int pageSize);

  Articletype findById(Integer id);

  Articletype save(Articletype bean);

  Articletype updateByUpdater(Updater<Articletype> updater);

  Articletype deleteById(Integer id);

  List<Articletype> findChild(Integer pid);
}