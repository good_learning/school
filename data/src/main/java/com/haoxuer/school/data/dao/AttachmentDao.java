package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Attachment;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface AttachmentDao extends BaseDao<Attachment, Long> {
  Pagination getPage(int pageNo, int pageSize);

  Attachment findById(Long id);

  Attachment save(Attachment bean);

  Attachment updateByUpdater(Updater<Attachment> updater);

  Attachment deleteById(Long id);

  Pagination pageByUserId(int id, int curpage, int pagesize);

  Pagination pagetypeByUserId(int id, int type, int curpage, int pagesize);

}
