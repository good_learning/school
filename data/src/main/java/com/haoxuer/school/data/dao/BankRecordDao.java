package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.BankRecord;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface BankRecordDao extends BaseDao<BankRecord, Long> {
  Pagination getPage(int pageNo, int pageSize);

  BankRecord findById(Long id);

  BankRecord save(BankRecord bean);

  BankRecord updateByUpdater(Updater<BankRecord> updater);

  BankRecord deleteById(Long id);
}