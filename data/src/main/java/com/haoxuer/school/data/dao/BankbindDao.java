package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Bankbind;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface BankbindDao extends BaseDao<Bankbind, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  Bankbind findById(Integer id);

  Bankbind save(Bankbind bean);

  Bankbind updateByUpdater(Updater<Bankbind> updater);

  Bankbind deleteById(Integer id);
}