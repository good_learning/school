package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.BrowsingHistory;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface BrowsingHistoryDao {
  Pagination getPage(int pageNo, int pageSize);

  BrowsingHistory findById(Integer id);

  BrowsingHistory save(BrowsingHistory bean);

  BrowsingHistory updateByUpdater(Updater<BrowsingHistory> updater);

  BrowsingHistory deleteById(Integer id);


  BrowsingHistory saveRecord(int userid, int courseid);

  Pagination getPage(int userid, int pageNo, int pageSize);


}