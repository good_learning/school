package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Card;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CardDao extends BaseDao<Card, Long> {
  Pagination getPage(int pageNo, int pageSize);

  Card findById(Long id);

  Card findByName(String id);

  Card save(Card bean);

  Card updateByUpdater(Updater<Card> updater);

  Card deleteById(Long id);
}