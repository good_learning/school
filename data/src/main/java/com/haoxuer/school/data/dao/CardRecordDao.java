package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.CardRecord;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CardRecordDao {
  Pagination getPage(int pageNo, int pageSize);

  CardRecord findById(Integer id);

  CardRecord save(CardRecord bean);

  CardRecord updateByUpdater(Updater<CardRecord> updater);

  CardRecord deleteById(Integer id);
}