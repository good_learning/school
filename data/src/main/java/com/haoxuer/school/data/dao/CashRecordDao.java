package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.CashRecord;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CashRecordDao extends BaseDao<CashRecord, Long> {
  Pagination getPage(int pageNo, int pageSize);

  CashRecord findById(Long id);

  CashRecord save(CashRecord bean);

  CashRecord updateByUpdater(Updater<CashRecord> updater);

  CashRecord deleteById(Long id);
}