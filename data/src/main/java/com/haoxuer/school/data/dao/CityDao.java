package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.City;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDao;

@Repository
public interface CityDao extends BaseDao<City, Integer> {

}
