package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Comment;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CommentDao {
  Pagination getPage(int pageNo, int pageSize);

  Comment findById(Integer id);

  Comment save(Comment bean);

  Comment updateByUpdater(Updater<Comment> updater);

  Comment deleteById(Integer id);
}