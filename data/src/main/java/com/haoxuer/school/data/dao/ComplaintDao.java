package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.Complaint;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ComplaintDao extends BaseDao<Complaint, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  Complaint findById(Integer id);

  Complaint save(Complaint bean);

  Complaint updateByUpdater(Updater<Complaint> updater);

  Complaint deleteById(Integer id);
}