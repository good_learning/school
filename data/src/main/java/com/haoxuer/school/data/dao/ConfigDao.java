package com.haoxuer.school.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.school.data.entity.Config;

/**
* Created by imake on 2019年10月19日18:25:41.
*/
public interface ConfigDao extends BaseDao<Config,Long>{

	 Config findById(Long id);

	 Config save(Config bean);

	 Config updateByUpdater(Updater<Config> updater);

	 Config deleteById(Long id);

	Config config();

}