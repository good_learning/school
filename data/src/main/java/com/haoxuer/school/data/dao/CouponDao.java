package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.CouponInfo;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CouponDao extends BaseDao<CouponInfo, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  CouponInfo findById(Integer id);

  CouponInfo save(CouponInfo bean);

  CouponInfo updateByUpdater(Updater<CouponInfo> updater);

  CouponInfo deleteById(Integer id);
}