package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.CourseCatalog;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

import java.util.List;

/**
 * Created by imake on 2018年08月16日23:12:28.
 */
public interface CourseCatalogDao extends BaseDao<CourseCatalog, Integer> {

  CourseCatalog findById(Integer id);

  CourseCatalog save(CourseCatalog bean);

  CourseCatalog updateByUpdater(Updater<CourseCatalog> updater);

  CourseCatalog deleteById(Integer id);

  List<CourseCatalog> child(Integer id);

  List<CourseCatalog> findTop();
}