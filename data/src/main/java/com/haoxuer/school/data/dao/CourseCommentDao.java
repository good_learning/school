package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.CourseComment;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CourseCommentDao extends BaseDao<CourseComment, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  CourseComment findById(Integer id);

  CourseComment save(CourseComment bean);

  CourseComment updateByUpdater(Updater<CourseComment> updater);

  CourseComment deleteById(Integer id);

  Pagination pageByType(int courseid, int classify, int curpage,
                        int pagesize);

  Pagination pageByCommentId(int commentid, int classify, int curpage,
                             int pagesize);

}