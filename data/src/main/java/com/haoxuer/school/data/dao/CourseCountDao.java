package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.CourseCount;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CourseCountDao {
  Pagination getPage(int pageNo, int pageSize);

  CourseCount findById(Integer id);

  CourseCount save(CourseCount bean);

  CourseCount updateByUpdater(Updater<CourseCount> updater);

  CourseCount deleteById(Integer id);
}