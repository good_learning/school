package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.Course;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseDao extends BaseDao<Course, Integer> {

  Course addCourse(Course course);

  List<Course> findByUser(int uid);

  boolean deleteCourse(Course c);

  Pagination pageByType(int uid, int sourtype, int curpage, int pagesize);

  Course findById(int id);

  Course up(int id);

  Pagination pageByTransactionType(int type, int sourtype, int curpage, int pagesize);

  Pagination pageByUser(int userid, int statetype, int sourtype, int curpage, int pagesize);

  Pagination goodByUser(int userid, int curpage, int size);

}
