package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.CourseImg;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CourseImgDao {
  Pagination getPage(int pageNo, int pageSize);

  CourseImg findById(Long id);

  CourseImg save(CourseImg bean);

  CourseImg updateByUpdater(Updater<CourseImg> updater);

  CourseImg deleteById(Long id);

  Pagination pageByCourse(int courseid, int pageNo, int pageSize);

}