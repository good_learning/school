package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CourseLessonStudentDao extends BaseDao<CourseLessonStudent, Long> {
  Pagination getPage(int pageNo, int pageSize);

  CourseLessonStudent findById(Long id);

  CourseLessonStudent save(CourseLessonStudent bean);

  CourseLessonStudent updateByUpdater(Updater<CourseLessonStudent> updater);

  CourseLessonStudent deleteById(Long id);
}