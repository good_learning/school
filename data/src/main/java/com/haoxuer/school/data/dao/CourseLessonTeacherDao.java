package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.CourseLessonTeacher;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CourseLessonTeacherDao extends BaseDao<CourseLessonTeacher, Long> {
  Pagination getPage(int pageNo, int pageSize);

  CourseLessonTeacher findById(Long id);

  CourseLessonTeacher save(CourseLessonTeacher bean);

  CourseLessonTeacher updateByUpdater(Updater<CourseLessonTeacher> updater);

  CourseLessonTeacher deleteById(Long id);
}