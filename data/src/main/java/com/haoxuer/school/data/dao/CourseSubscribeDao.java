package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.CourseSubscribe;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CourseSubscribeDao extends BaseDao<CourseSubscribe, Long> {
  Pagination getPage(int pageNo, int pageSize);

  CourseSubscribe findById(Long id);

  CourseSubscribe save(CourseSubscribe bean);

  CourseSubscribe updateByUpdater(Updater<CourseSubscribe> updater);

  CourseSubscribe deleteById(Long id);


  Pagination pageByUserId(int userid, int pageNo, int pageSize);
}