package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.CourseTeacherComment;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface CourseTeacherCommentDao extends BaseDao<CourseTeacherComment, Long> {
  Pagination getPage(int pageNo, int pageSize);

  CourseTeacherComment findById(Long id);

  CourseTeacherComment save(CourseTeacherComment bean);

  CourseTeacherComment updateByUpdater(Updater<CourseTeacherComment> updater);

  CourseTeacherComment deleteById(Long id);
}