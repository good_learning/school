package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.FavArticle;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FavArtilceDao extends BaseDao<FavArticle, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  FavArticle findById(Integer id);

  FavArticle save(FavArticle bean);

  FavArticle updateByUpdater(Updater<FavArticle> updater);

  FavArticle deleteById(Integer id);

  int fav(Long userid, int articleid);
}