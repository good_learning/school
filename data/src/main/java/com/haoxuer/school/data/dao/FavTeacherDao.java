package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.FavTeacher;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FavTeacherDao extends BaseDao<FavTeacher, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  FavTeacher findById(Integer id);

  FavTeacher save(FavTeacher bean);

  FavTeacher updateByUpdater(Updater<FavTeacher> updater);

  FavTeacher deleteById(Integer id);


  int fav(Long userid, Long courseid);
}