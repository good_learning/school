package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Favcourse;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FavcourseDao {
  Pagination getPage(int pageNo, int pageSize);

  Favcourse findById(Integer id);

  Favcourse save(Favcourse bean);

  Favcourse updateByUpdater(Updater<Favcourse> updater);

  Favcourse deleteById(Integer id);

  int fav(Long userid, int courseid);


  Pagination pageByUserId(Long userid, int pageNo, int pageSize);

}