package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Favschool;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FavschoolDao {
  Pagination getPage(int pageNo, int pageSize);

  Favschool findById(Integer id);

  Favschool save(Favschool bean);

  Favschool updateByUpdater(Updater<Favschool> updater);

  Favschool deleteById(Integer id);
}