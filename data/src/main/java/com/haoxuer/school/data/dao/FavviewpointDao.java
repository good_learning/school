package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Favviewpoint;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FavviewpointDao {
  Pagination getPage(int pageNo, int pageSize);

  Favviewpoint findById(Long id);

  Favviewpoint save(Favviewpoint bean);

  Favviewpoint updateByUpdater(Updater<Favviewpoint> updater);

  Favviewpoint deleteById(Long id);

  int fav(Long userid, Long courseid);

  Pagination pageByUserId(Long userid, int pageNo, int pageSize);
}