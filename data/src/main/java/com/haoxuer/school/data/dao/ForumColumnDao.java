package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ForumColumnDao {
  Pagination getPage(int pageNo, int pageSize);

  ForumColumn findById(Integer id);

  ForumColumn save(ForumColumn bean);

  ForumColumn updateByUpdater(Updater<ForumColumn> updater);

  ForumColumn deleteById(Integer id);

  List<ForumColumn> findChild(Integer pid);

  List<ForumColumn> findByParentId(Integer parentid);

  Pagination find(Finder finder, int pageNo, int pageSize);
}