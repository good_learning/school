package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ForumGroupDao {
  /**
   * 分页查询
   *
   * @param pageNo
   * @param pageSize
   * @return
   */
  Pagination getPage(int pageNo, int pageSize);

  /**
   * 通过id来查询
   *
   * @param id
   * @return
   */
  ForumGroup findById(Integer id);

  /**
   * 通过导航来保存bean
   *
   * @param bean
   * @return
   */
  ForumGroup save(ForumGroup bean);

  /**
   * 导航修改
   *
   * @param updater
   * @return
   */
  ForumGroup updateByUpdater(Updater<ForumGroup> updater);

  /**
   * 通过主键删除
   *
   * @param id
   * @return
   */
  ForumGroup deleteById(Integer id);

  /**
   * @param finder
   * @param pageNo
   * @param pageSize
   * @return
   */
  Pagination find(Finder finder, int pageNo, int pageSize);

  List find(Finder finder);
}