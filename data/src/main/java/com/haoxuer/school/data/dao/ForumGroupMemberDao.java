package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.forum.ForumGroupMember;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ForumGroupMemberDao {
  Pagination getPage(int pageNo, int pageSize);

  ForumGroupMember findById(Integer id);

  List<ForumGroupMember> findByGroupid(Integer id);

  ForumGroupMember save(ForumGroupMember bean);

  ForumGroupMember updateByUpdater(Updater<ForumGroupMember> updater);

  ForumGroupMember deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);

  List find(Finder finder);
}