package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.forum.ForumInformAgainst;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ForumInformAgainstDao {
  Pagination getPage(int pageNo, int pageSize);

  ForumInformAgainst findById(Integer id);

  ForumInformAgainst save(ForumInformAgainst bean);

  ForumInformAgainst updateByUpdater(
      Updater<ForumInformAgainst> updater);

  ForumInformAgainst deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);
}