package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.forum.ForumMessage;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ForumMessageDao {
  Pagination getPage(int pageNo, int pageSize);

  ForumMessage findById(Integer id);

  ForumMessage save(ForumMessage bean);

  ForumMessage updateByUpdater(Updater<ForumMessage> updater);

  ForumMessage deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);
}