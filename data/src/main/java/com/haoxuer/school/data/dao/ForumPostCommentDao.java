package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.forum.ForumPostComment;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ForumPostCommentDao {
  Pagination getPage(int pageNo, int pageSize);

  ForumPostComment findById(Integer id);

  ForumPostComment save(ForumPostComment bean);

  ForumPostComment updateByUpdater(Updater<ForumPostComment> updater);

  ForumPostComment deleteById(Integer id);

  Pagination find(Finder finder, int pageNo, int pageSize);
}