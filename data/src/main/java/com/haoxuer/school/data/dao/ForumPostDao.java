package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.forum.ForumPost;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ForumPostDao {
  Pagination getPage(int pageNo, int pageSize);

  ForumPost findById(Integer id);

  ForumPost save(ForumPost bean);

  ForumPost updateByUpdater(Updater<ForumPost> updater);

  ForumPost deleteById(Integer id);

  List<ForumPost> findByColumnId(Integer columnid);

  List<ForumPost> findByColumnId(Integer columnid, int size,
                                 String orders);

  Pagination pageByColumnId(Integer columnid, String orders, int size,
                            int curpage);

  Pagination find(Finder finder, int pageNo, int pageSize);

  int getMaxSortSeq();

  // public Pagination recommend(int type,int size,int curpage);
  List<ForumPost> find(Finder finder);
}