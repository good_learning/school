package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.Friend;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FriendDao {
  Pagination getPage(int pageNo, int pageSize);

  Friend findById(Integer id);

  Friend save(Friend bean);

  int follows(int userid, int friendid);

  int save(int userid, int friendid, int type);

  int isfollows(int userid, int friendid);

  Friend updateByUpdater(Updater<Friend> updater);

  Friend deleteById(Integer id);

  Friend deleteById(Integer id, Integer friendid);

  List<Friend> findByType(Integer uid, int type);

  Pagination getByType(int userid, int type, int curpage,
                       int pagesize);
}