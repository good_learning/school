package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Friendtype;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FriendtypeDao {
  Pagination getPage(int pageNo, int pageSize);

  Friendtype findById(Integer id);

  Friendtype save(Friendtype bean);

  Friendtype updateByUpdater(Updater<Friendtype> updater);

  Friendtype deleteById(Integer id);
}