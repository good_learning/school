package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.KeyWord;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface KeyWordDao extends BaseDao<KeyWord, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  KeyWord findById(Integer id);

  KeyWord save(KeyWord bean);

  KeyWord updateByUpdater(Updater<KeyWord> updater);

  KeyWord deleteById(Integer id);
}