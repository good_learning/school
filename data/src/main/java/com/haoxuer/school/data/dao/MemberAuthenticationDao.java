package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.MemberAuthentication;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface MemberAuthenticationDao extends BaseDao<MemberAuthentication, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  MemberAuthentication findById(Integer id);

  MemberAuthentication save(MemberAuthentication bean);

  MemberAuthentication updateByUpdater(Updater<MemberAuthentication> updater);

  MemberAuthentication deleteById(Integer id);
}