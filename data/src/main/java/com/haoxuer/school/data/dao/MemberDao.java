package com.haoxuer.school.data.dao;


import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.entity.Member;

public interface MemberDao extends CriteriaDao<Member, Long> {
  
  public Member findById(Long id);
  
  public Member save(Member bean);
  
  public Member updateByUpdater(Updater<Member> updater);
  
  public Member deleteById(Long id);
}