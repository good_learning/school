package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.MemberType;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface MemberTypeDao {
  Pagination getPage(int pageNo, int pageSize);

  MemberType findById(Integer id);

  MemberType save(MemberType bean);

  MemberType updateByUpdater(Updater<MemberType> updater);

  MemberType deleteById(Integer id);

  List<MemberType> all();
}