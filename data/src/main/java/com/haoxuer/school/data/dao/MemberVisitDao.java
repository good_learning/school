package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.MemberVisit;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface MemberVisitDao extends BaseDao<MemberVisit, Long> {
  Pagination getPage(int pageNo, int pageSize);

  MemberVisit findById(Long id);

  MemberVisit save(MemberVisit bean);

  MemberVisit updateByUpdater(Updater<MemberVisit> updater);

  MemberVisit deleteById(Long id);
}