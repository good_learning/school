package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.MoneyRecord;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface MoneyRecordDao extends BaseDao<MoneyRecord, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  MoneyRecord findById(Integer id);

  MoneyRecord save(MoneyRecord bean);

  MoneyRecord updateByUpdater(Updater<MoneyRecord> updater);

  MoneyRecord deleteById(Integer id);
}