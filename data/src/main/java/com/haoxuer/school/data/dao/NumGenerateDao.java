package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.NumGenerate;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface NumGenerateDao extends BaseDao<NumGenerate, Long> {
  Pagination getPage(int pageNo, int pageSize);

  NumGenerate findById(Long id);

  NumGenerate save(NumGenerate bean);

  NumGenerate updateByUpdater(Updater<NumGenerate> updater);

  NumGenerate deleteById(Long id);


  NumGenerate findkey(String key, String type);

}