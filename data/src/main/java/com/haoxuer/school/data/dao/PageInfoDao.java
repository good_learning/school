package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.PageInfo;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface PageInfoDao extends BaseDao<PageInfo, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  PageInfo findById(Integer id);

  PageInfo save(PageInfo bean);

  PageInfo updateByUpdater(Updater<PageInfo> updater);

  PageInfo deleteById(Integer id);
}