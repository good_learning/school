package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.PhoneRecord;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface PhoneRecordDao {
  Pagination getPage(int pageNo, int pageSize);

  PhoneRecord findById(Integer id);

  PhoneRecord save(PhoneRecord bean);

  PhoneRecord updateByUpdater(Updater<PhoneRecord> updater);

  PhoneRecord deleteById(Integer id);
}