package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Province;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDao;

@Repository
public interface ProvinceDao extends BaseDao<Province, Integer> {

}
