package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Recommend;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface RecommendDao extends BaseDao<Recommend, Long> {
  Pagination getPage(int pageNo, int pageSize);

  Recommend findById(Long id);

  Recommend save(Recommend bean);

  Recommend updateByUpdater(Updater<Recommend> updater);

  Recommend deleteById(Long id);

  Pagination recommend(int type, int size, int curpage);

}