package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.RefundRecord;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface RefundRecordDao {
  Pagination getPage(int pageNo, int pageSize);

  RefundRecord findById(Integer id);

  RefundRecord save(RefundRecord bean);

  RefundRecord updateByUpdater(Updater<RefundRecord> updater);

  RefundRecord deleteById(Integer id);
}