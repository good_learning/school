package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Schoolnetwork;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface SchoolnetworkDao extends BaseDao<Schoolnetwork, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  Schoolnetwork findById(Integer id);

  Schoolnetwork save(Schoolnetwork bean);

  Schoolnetwork updateByUpdater(Updater<Schoolnetwork> updater);

  Schoolnetwork deleteById(Integer id);
}