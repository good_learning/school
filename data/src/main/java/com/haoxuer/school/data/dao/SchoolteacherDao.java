package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Schoolteacher;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface SchoolteacherDao extends BaseDao<Schoolteacher, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  Schoolteacher findById(Integer id);

  Schoolteacher save(Schoolteacher bean);

  Schoolteacher updateByUpdater(Updater<Schoolteacher> updater);

  Schoolteacher deleteById(Integer id);
}