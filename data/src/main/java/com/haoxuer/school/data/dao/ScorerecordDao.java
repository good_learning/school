package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Scorerecord;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ScorerecordDao {
  Pagination getPage(int pageNo, int pageSize);

  Scorerecord findById(Integer id);

  Scorerecord save(Scorerecord bean);

  Scorerecord updateByUpdater(Updater<Scorerecord> updater);

  Scorerecord deleteById(Integer id);

  Pagination pageByUserId(int id, int curpage, int pagesize);

  int getallscore(Long userid);
}