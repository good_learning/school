package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface SystemInfoDao extends BaseDao<SystemInfo, Long> {
  Pagination getPage(int pageNo, int pageSize);

  SystemInfo findById(Long id);

  SystemInfo save(SystemInfo bean);

  SystemInfo updateByUpdater(Updater<SystemInfo> updater);

  SystemInfo deleteById(Long id);
}