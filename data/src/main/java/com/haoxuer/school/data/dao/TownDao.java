package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Town;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface TownDao extends BaseDao<Town, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  Town findById(Integer id);

  Town save(Town bean);

  Town updateByUpdater(Updater<Town> updater);

  Town deleteById(Integer id);
}