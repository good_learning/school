package com.haoxuer.school.data.dao;

import java.util.List;

import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface TradingrecordDao extends BaseDao<Tradingrecord, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  Tradingrecord findById(Integer id);

  Tradingrecord save(Tradingrecord bean);

  Tradingrecord updateByUpdater(Updater<Tradingrecord> updater);

  Tradingrecord deleteById(Integer id);

  List<Tradingrecord> findAllByUid(Long userid);

  List<Tradingrecord> findAllByType(int type);


}