package com.haoxuer.school.data.dao;


import com.haoxuer.school.data.entity.TradingrecordRefund;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface TradingrecordRefundDao extends BaseDao<TradingrecordRefund, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  TradingrecordRefund findById(Integer id);

  TradingrecordRefund save(TradingrecordRefund bean);

  TradingrecordRefund updateByUpdater(Updater<TradingrecordRefund> updater);

  TradingrecordRefund deleteById(Integer id);
}