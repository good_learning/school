package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.ViewPointTypes;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ViewPointTypesDao {
  Pagination getPage(int pageNo, int pageSize);

  ViewPointTypes findById(Integer id);

  ViewPointTypes save(ViewPointTypes bean);

  ViewPointTypes updateByUpdater(Updater<ViewPointTypes> updater);

  ViewPointTypes deleteById(Integer id);
}