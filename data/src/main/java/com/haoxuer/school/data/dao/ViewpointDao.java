package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ViewpointDao extends BaseDao<Viewpoint, Integer> {
  Pagination getPage(int pageNo, int pageSize);

  Viewpoint findById(Integer id);

  Viewpoint save(Viewpoint bean);

  Viewpoint updateByUpdater(Updater<Viewpoint> updater);

  Viewpoint deleteById(Integer id);

  Pagination pageByUserId(int userid, int pageNo, int pageSize);

  Viewpoint up(int id);
}