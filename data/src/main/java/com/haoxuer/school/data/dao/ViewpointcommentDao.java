package com.haoxuer.school.data.dao;

import com.haoxuer.school.data.entity.Viewpointcomment;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface ViewpointcommentDao {
  Pagination getPage(int pageNo, int pageSize);

  Viewpointcomment findById(Integer id);

  Viewpointcomment save(Viewpointcomment bean);

  Viewpointcomment updateByUpdater(Updater<Viewpointcomment> updater);

  Viewpointcomment deleteById(Integer id);

  Pagination pageByPointId(int commentid, int curpage, int pagesize);
}