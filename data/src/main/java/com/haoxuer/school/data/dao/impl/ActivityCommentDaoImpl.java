package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.forum.ActivityComment;
import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.school.data.dao.ActivityCommentDao;

@Repository
public class ActivityCommentDaoImpl extends
    BaseDaoImpl<ActivityComment, Integer> implements ActivityCommentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public ActivityComment findById(Integer id) {
    ActivityComment entity = get(id);
    return entity;
  }

  public ActivityComment save(ActivityComment bean) {
    getSession().save(bean);
    return bean;
  }

  public ActivityComment deleteById(Integer id) {
    ActivityComment entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<ActivityComment> getEntityClass() {
    return ActivityComment.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}