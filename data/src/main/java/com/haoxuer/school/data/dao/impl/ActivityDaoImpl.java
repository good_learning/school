package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.forum.Activity;
import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.school.data.dao.ActivityDao;

@Repository
public class ActivityDaoImpl extends BaseDaoImpl<Activity, Integer> implements
    ActivityDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Activity findById(Integer id) {
    Activity entity = get(id);
    return entity;
  }

  public Activity save(Activity bean) {
    getSession().save(bean);
    return bean;
  }

  public Activity deleteById(Integer id) {
    Activity entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Activity> getEntityClass() {
    return Activity.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}