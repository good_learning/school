package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.forum.ActivityMember;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.ActivityMemberDao;

@Repository
public class ActivityMemberDaoImpl extends BaseDaoImpl<ActivityMember, Integer>
    implements ActivityMemberDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public ActivityMember findById(Integer id) {
    ActivityMember entity = get(id);
    return entity;
  }

  public ActivityMember save(ActivityMember bean) {
    getSession().save(bean);
    return bean;
  }

  public ActivityMember deleteById(Integer id) {
    ActivityMember entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<ActivityMember> getEntityClass() {
    return ActivityMember.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}