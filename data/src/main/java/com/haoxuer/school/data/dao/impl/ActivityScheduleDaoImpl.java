package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.forum.ActivitySchedule;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.ActivityScheduleDao;

@Repository
public class ActivityScheduleDaoImpl extends
    BaseDaoImpl<ActivitySchedule, Integer> implements ActivityScheduleDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public ActivitySchedule findById(Integer id) {
    ActivitySchedule entity = get(id);
    return entity;
  }

  public ActivitySchedule save(ActivitySchedule bean) {
    getSession().save(bean);
    return bean;
  }

  public ActivitySchedule deleteById(Integer id) {
    ActivitySchedule entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<ActivitySchedule> getEntityClass() {
    return ActivitySchedule.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}