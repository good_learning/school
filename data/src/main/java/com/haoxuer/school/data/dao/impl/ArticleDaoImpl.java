package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ArticleDao;

@Repository
public class ArticleDaoImpl extends BaseDaoImpl<Article, Integer> implements
    ArticleDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Article findById(Integer id) {
    Article entity = get(id);
    return entity;
  }

  public Article save(Article bean) {
    getSession().save(bean);
    return bean;
  }

  public Article deleteById(Integer id) {
    Article entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Article> getEntityClass() {
    return Article.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public List<Article> findArticlesById(Integer id) {
    String queryString = "from Article u where u.articletype.id = ?";
    return (List<Article>) getHibernateTemplate().find(queryString, id);
  }

  @Override
  public List<Article> findArticlesByPid(Integer id, int size, int sorttype) {
    List<Article> result = null;
    Articletype coursetype = this.getHibernateTemplate().get(
        Articletype.class, id);
    if (coursetype != null) {
      Finder finder = Finder.create("from Article c ");
      finder.append(" where c.articletype.lft>=");
      finder.append("" + coursetype.getLft());
      finder.append(" and c.articletype.rgt <=" + coursetype.getRgt());
      if (sorttype == 1) {
        finder.append("  order by c.id desc");
      } else if (sorttype == 2) {
        finder.append("  order by c.id asc");
      } else if (sorttype == 3) {
        finder.append("  order by c.views desc");
      }
      Pagination page = find(finder, 1, size);
      if (page != null) {
        result = (List<Article>) page.getList();
      }
    }
    return result;
  }

  @Override
  public Pagination pageByPid(Integer id, int sorttype, int size, int curpage) {
    Pagination result = null;
    Articletype coursetype = this.getHibernateTemplate().get(
        Articletype.class, id);
    if (coursetype != null) {
      Finder finder = Finder.create("from Article c ");
      finder.append(" where c.articletype.lft>=");
      finder.append("" + coursetype.getLft());
      finder.append(" and c.articletype.rgt <=" + coursetype.getRgt());
      if (sorttype == 1) {
        finder.append("  order by c.id desc");
      } else if (sorttype == 2) {
        finder.append("  order by c.id asc");
      } else if (sorttype == 3) {
        finder.append("  order by c.id asc");
      }
      result = find(finder, curpage, size);

    }
    return result;
  }

  @Override
  public Pagination recommend(int type, int size, int curpage) {
    Pagination result = null;
    Articletype coursetype = this.getHibernateTemplate().get(
        Articletype.class, type);
    if (coursetype != null) {
      Finder finder = Finder.create("from Article c ");
      finder.append(" where c.articletype.lft>=");
      finder.append("" + coursetype.getLft());
      finder.append(" and c.articletype.rgt <=" + coursetype.getRgt());
      finder.append(" and c.is_recommend=1  ");
      finder.append("  order by c.sortrecommend desc");
      result = find(finder, curpage, size);

    }
    return result;
  }

}