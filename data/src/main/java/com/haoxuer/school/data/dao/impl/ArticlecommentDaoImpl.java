package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.entity.Articlecomment;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ArticlecommentDao;

@Repository
public class ArticlecommentDaoImpl extends BaseDaoImpl<Articlecomment, Integer>
    implements ArticlecommentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Articlecomment findById(Integer id) {
    Articlecomment entity = get(id);
    return entity;
  }

  public Articlecomment save(Articlecomment bean) {
    getSession().save(bean);

    Article article = getHibernateTemplate().get(Article.class,
        bean.getArticle().getId());
    Integer comments = article.getComments();
    if (comments == null) {
      comments = 0;
    }
    comments++;
    article.setComments(comments);
    return bean;
  }

  public Articlecomment deleteById(Integer id) {
    Articlecomment entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Articlecomment> getEntityClass() {
    return Articlecomment.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByArticleId(int id, int sorttype, int pagesize,
                                    int curpage) {
    Pagination result = null;
    Finder finder = Finder.create("from Articlecomment c ");
    finder.append(" where c.article.id=");
    finder.append("" + id);
    if (sorttype == 1) {
      finder.append("  order by c.id desc");
    } else if (sorttype == 2) {
      finder.append("  order by c.pubdate desc");
    } else if (sorttype == 3) {
      finder.append("  order by c.id asc");
    }
    result = find(finder, curpage, pagesize);

    return result;
  }
}