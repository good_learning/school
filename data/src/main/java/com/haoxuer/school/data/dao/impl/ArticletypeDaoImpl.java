package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ArticletypeDao;

@Repository
public class ArticletypeDaoImpl extends BaseDaoImpl<Articletype, Integer>
    implements ArticletypeDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Articletype findById(Integer id) {
    Articletype entity = get(id);
    return entity;
  }

  public Articletype save(Articletype bean) {
    Articletype type = bean.getArticletype();
    Session session = getSession();
    Integer myPosition;

    if (type != null) {
      String hql = "select bean.rgt from Articletype bean where bean.id=:pid";
      myPosition = ((Number) session.createQuery(hql).setParameter(
          "pid", bean.getArticletype().getId()).uniqueResult()).intValue();
      String hql1 = "update Articletype bean set bean.rgt  = bean.rgt + 2 WHERE bean.rgt >= :myPosition";
      String hql2 = "update Articletype  bean set bean.lft = bean.lft + 2 WHERE bean.lft >= :myPosition";
      session.createQuery(hql1)
          .setParameter("myPosition", myPosition).executeUpdate();
      session.createQuery(hql2)
          .setParameter("myPosition", myPosition).executeUpdate();
    } else {

      String hql = "select max(bean.rgt) from Articletype bean";
      Number myPositionNumber = (Number) session.createQuery(hql)
          .uniqueResult();
      // 如不存在，则为0
      if (myPositionNumber == null) {
        myPosition = 1;
      } else {
        myPosition = myPositionNumber.intValue() + 1;
      }

    }
    bean.setLft(myPosition);
    bean.setRgt(myPosition + 1);
    getSession().save(bean);
    return bean;
  }

  public Articletype deleteById(Integer id) {
    Articletype entity = super.get(id);
    if (entity != null) {
      String beanName = "Articletype";
      Session session = getSession();
      String hql = "select bean.lft from "
          + beanName + " bean where bean.id=:id";
      Integer myPosition = ((Number) session.createQuery(hql)
          .setParameter("id", id).uniqueResult())
          .intValue();
      String hql1 = "update " + beanName + " bean set bean.rgt = bean.rgt - 2 WHERE bean.rgt > :myPosition";
      String hql2 = "update " + beanName + " bean set bean.lft = bean.lft - 2 WHERE bean.lft > :myPosition";
      session.createQuery(hql1).setInteger("myPosition", myPosition)
          .executeUpdate();
      session.createQuery(hql2).setInteger("myPosition", myPosition)
          .executeUpdate();
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Articletype> getEntityClass() {
    return Articletype.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public List<Articletype> findChild(Integer pid) {
    Finder finder = Finder.create("from Articletype t where t.articletype.id=" + pid);
    finder.append(" order by t.priority asc");
    return find(finder);
  }
}