package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Attachment;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.AttachmentDao;

@Repository
public class AttachmentDaoImpl extends BaseDaoImpl<Attachment, Long> implements AttachmentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Attachment findById(Long id) {
    Attachment entity = get(id);
    return entity;
  }

  public Attachment save(Attachment bean) {
    getSession().save(bean);
    return bean;
  }

  public Attachment deleteById(Long id) {
    Attachment entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Attachment> getEntityClass() {
    return Attachment.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByUserId(int id, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("from Attachment u where u.member.id = ");
    finder.append("" + id);
    finder.append("   order by  u.adddate  desc");


    result = find(finder, curpage, pagesize);
    return result;
  }

  @Override
  public Pagination pagetypeByUserId(int id, int type, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("from Attachment u where u.member.id = ");
    finder.append("" + id);
    finder.append("and u.catalog = " + type);
    finder.append("   order by  u.adddate  desc");


    result = find(finder, curpage, pagesize);
    return result;
  }


}
