package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.BankRecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.BankRecordDao;

@Repository
public class BankRecordDaoImpl extends BaseDaoImpl<BankRecord, Long> implements BankRecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public BankRecord findById(Long id) {
    BankRecord entity = get(id);
    return entity;
  }

  public BankRecord save(BankRecord bean) {
    getSession().save(bean);
    return bean;
  }

  public BankRecord deleteById(Long id) {
    BankRecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<BankRecord> getEntityClass() {
    return BankRecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}