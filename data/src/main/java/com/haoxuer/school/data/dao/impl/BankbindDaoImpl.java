package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Bankbind;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.BankbindDao;

@Repository
public class BankbindDaoImpl extends BaseDaoImpl<Bankbind, Integer> implements BankbindDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Bankbind findById(Integer id) {
    Bankbind entity = get(id);
    return entity;
  }

  public Bankbind save(Bankbind bean) {
    getSession().save(bean);
    return bean;
  }

  public Bankbind deleteById(Integer id) {
    Bankbind entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Bankbind> getEntityClass() {
    return Bankbind.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}