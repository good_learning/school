package com.haoxuer.school.data.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import com.haoxuer.school.data.entity.BrowsingHistory;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.oto.BrowsingHistoryInfo;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.BrowsingHistoryDao;

@Repository
public class BrowsingHistoryDaoImpl extends
    BaseDaoImpl<BrowsingHistory, Integer> implements BrowsingHistoryDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public BrowsingHistory findById(Integer id) {
    BrowsingHistory entity = get(id);
    return entity;
  }

  public BrowsingHistory save(BrowsingHistory bean) {
    getSession().save(bean);
    return bean;
  }

  public BrowsingHistory deleteById(Integer id) {
    BrowsingHistory entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<BrowsingHistory> getEntityClass() {
    return BrowsingHistory.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public BrowsingHistory saveRecord(int userid, int courseid) {
    BrowsingHistory bean = null;
    List<?> list = find(
        "from BrowsingHistory b where b.member.id=? and b.course.id=?",
        userid, courseid);
    if (list != null && list.size() > 0) {

      bean = (BrowsingHistory) list.get(0);
      bean.setBrowsingdate(new Timestamp(System.currentTimeMillis()));
      bean.setBrowsingsize(bean.getBrowsingsize() + 1);

    } else {
      bean = new BrowsingHistory();
      Course course = new Course();
      course.setId(courseid);
      bean.setCourse(course);
      Member member = new Member();
      //member.setId(userid);
      bean.setMember(member);
      bean.setBrowsingdate(new Timestamp(System.currentTimeMillis()));
      bean.setBrowsingsize(1);
      getSession().save(bean);

    }

    return bean;
  }

  @Override
  public Pagination getPage(int userid, int pageNo, int pageSize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("select course.content,course.coursefeature,course.imgpath,course.name,course.money,browsinghistory.browsingdate,browsinghistory.courseid from browsinghistory inner join course");
    finder.append("  on browsinghistory.courseid=course.id and browsinghistory.userid=");
    finder.append("" + userid);
    finder.append("  order by  browsinghistory.browsingdate desc ");

    result = findSql(finder, pageNo, pageSize, BrowsingHistoryInfo.class);

    return result;
  }
}