package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.Card;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CardDao;

@Repository
public class CardDaoImpl extends BaseDaoImpl<Card, Long> implements CardDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Card findById(Long id) {
    Card entity = get(id);
    return entity;
  }

  public Card save(Card bean) {
    getSession().save(bean);
    return bean;
  }

  public Card deleteById(Long id) {
    Card entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Card> getEntityClass() {
    return Card.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Card findByName(String id) {
    Card result = null;
    String queryString = "from Card u where u.cardnum = ?";
    List<?> cards = getHibernateTemplate().find(queryString, id);
    if (cards != null && cards.size() > 0) {
      result = (Card) cards.get(0);
    }
    return result;
  }
}