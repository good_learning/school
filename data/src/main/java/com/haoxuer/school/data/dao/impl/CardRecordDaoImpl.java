package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CardRecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CardRecordDao;

@Repository
public class CardRecordDaoImpl extends BaseDaoImpl<CardRecord, Integer> implements CardRecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CardRecord findById(Integer id) {
    CardRecord entity = get(id);
    return entity;
  }

  public CardRecord save(CardRecord bean) {
    getSession().save(bean);
    return bean;
  }

  public CardRecord deleteById(Integer id) {
    CardRecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CardRecord> getEntityClass() {
    return CardRecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}