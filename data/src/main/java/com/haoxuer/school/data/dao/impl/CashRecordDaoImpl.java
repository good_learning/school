package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CashRecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CashRecordDao;

@Repository
public class CashRecordDaoImpl extends BaseDaoImpl<CashRecord, Long> implements CashRecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CashRecord findById(Long id) {
    CashRecord entity = get(id);
    return entity;
  }

  public CashRecord save(CashRecord bean) {
    getSession().save(bean);
    return bean;
  }

  public CashRecord deleteById(Long id) {
    CashRecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CashRecord> getEntityClass() {
    return CashRecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}