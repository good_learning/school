package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.City;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CityDao;

@Repository
public class CityDaoImpl extends BaseDaoImpl<City, Integer> implements CityDao {

  @Override
  protected Class<City> getEntityClass() {
    return City.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}
