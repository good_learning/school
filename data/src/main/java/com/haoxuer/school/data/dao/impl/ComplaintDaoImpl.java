package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Complaint;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.ComplaintDao;

@Repository
public class ComplaintDaoImpl extends BaseDaoImpl<Complaint, Integer> implements ComplaintDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Complaint findById(Integer id) {
    Complaint entity = get(id);
    return entity;
  }

  public Complaint save(Complaint bean) {
    getSession().save(bean);
    return bean;
  }

  public Complaint deleteById(Integer id) {
    Complaint entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Complaint> getEntityClass() {
    return Complaint.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}