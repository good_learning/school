package com.haoxuer.school.data.dao.impl;

import com.haoxuer.discover.data.core.Finder;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.school.data.dao.ConfigDao;
import com.haoxuer.school.data.entity.Config;

/**
* Created by imake on 2019年10月19日18:25:41.
*/
@Repository

public class ConfigDaoImpl extends CriteriaDaoImpl<Config, Long> implements ConfigDao {

	@Override
	public Config findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Config save(Config bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Config deleteById(Long id) {
		Config entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Config config() {
		Finder finder = Finder.create();
		finder.append("from Config c");
		Config result = findOne(finder);
		if (result == null) {
			result = new Config();
			save(result);
		}
		return result;
	}

	@Override
	protected Class<Config> getEntityClass() {
		return Config.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}