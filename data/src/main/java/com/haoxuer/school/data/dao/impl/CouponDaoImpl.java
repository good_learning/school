package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CouponInfo;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CouponDao;

@Repository
public class CouponDaoImpl extends BaseDaoImpl<CouponInfo, Integer> implements CouponDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CouponInfo findById(Integer id) {
    CouponInfo entity = get(id);
    return entity;
  }

  public CouponInfo save(CouponInfo bean) {
    getSession().save(bean);
    return bean;
  }

  public CouponInfo deleteById(Integer id) {
    CouponInfo entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CouponInfo> getEntityClass() {
    return CouponInfo.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}