package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.dao.CourseCatalogDao;
import com.haoxuer.school.data.entity.CourseCatalog;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CatalogDaoImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imake on 2018年08月16日23:12:28.
 */
@Repository

public class CourseCatalogDaoImpl extends CatalogDaoImpl<CourseCatalog, Integer> implements CourseCatalogDao {

  @Override
  public CourseCatalog findById(Integer id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public CourseCatalog save(CourseCatalog bean) {

    add(bean);


    return bean;
  }

  @Override
  public CourseCatalog deleteById(Integer id) {
    CourseCatalog entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public List<CourseCatalog> child(Integer id) {
    List<Filter> filers = new ArrayList<>();
    filers.add(Filter.eq("parent.id", id));
    List<Order> orders = new ArrayList<>();
    orders.add(Order.asc("id"));
    return list(0, 2000, filers, orders);
  }

  @Override
  public List<CourseCatalog> findTop() {
    return child(1);
  }

  @Override
  protected Class<CourseCatalog> getEntityClass() {
    return CourseCatalog.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}