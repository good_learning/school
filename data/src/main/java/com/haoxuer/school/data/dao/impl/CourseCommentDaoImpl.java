package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CourseComment;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.CourseCommentDao;

@Repository
public class CourseCommentDaoImpl extends BaseDaoImpl<CourseComment, Integer>
    implements CourseCommentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CourseComment findById(Integer id) {
    CourseComment entity = get(id);
    return entity;
  }

  public CourseComment save(CourseComment bean) {
    getSession().save(bean);
    return bean;
  }

  public CourseComment deleteById(Integer id) {
    CourseComment entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CourseComment> getEntityClass() {
    return CourseComment.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByType(int courseid, int classify, int curpage,
                               int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from CourseComment c ");
    finder.append(" where c.classify = ");
    finder.append("" + classify);
    finder.append(" and c.course.id = " + courseid);
    result = find(finder, curpage, pagesize);
    return result;
  }

  @Override
  public Pagination pageByCommentId(int commentid, int classify, int curpage,
                                    int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from CourseComment c ");
    finder.append(" where c.classify = ");
    finder.append("" + classify);
    finder.append(" and c.courseComment.id = " + commentid);
    result = find(finder, curpage, pagesize);
    return result;
  }
}