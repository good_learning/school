package com.haoxuer.school.data.dao.impl;

import java.util.Calendar;
import java.util.List;

import com.haoxuer.school.data.entity.CourseCount;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.CourseCountDao;

@Repository
public class CourseCountDaoImpl extends BaseDaoImpl<CourseCount, Integer>
    implements CourseCountDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Calendar calendar = Calendar.getInstance();
    Finder finder = Finder.create("from CourseCount c where c.yearcount");
    finder.append(" = " + calendar.get(Calendar.YEAR));
    finder.append(" and c.weekcount= " + calendar.get(Calendar.WEEK_OF_YEAR));
    finder.append(" order by c.countnum desc");
    Pagination page = find(finder, pageNo, pageSize);
    return page;
  }

  public CourseCount findById(Integer id) {
    CourseCount entity = get(id);
    return entity;
  }

  public CourseCount save(CourseCount bean) {

    String queryString = "from CourseCount u where u.course.id = ? and yearcount=? and weekcount=?";
    List<CourseCount> cs = (List<CourseCount>) getHibernateTemplate().find(
        queryString, bean.getCourse().getId(), bean.getYearcount(),
        bean.getWeekcount());
    if (cs != null && cs.size() > 0) {
      CourseCount c = cs.get(0);
      Integer count = c.getCountnum();
      if (count == null) {
        count = 1;
      }
      count++;
      c.setCountnum(count);
    } else {
      bean.setCountnum(1);
      getSession().save(bean);
    }
    return bean;
  }

  public CourseCount deleteById(Integer id) {
    CourseCount entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CourseCount> getEntityClass() {
    return CourseCount.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}