package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.CourseCatalog;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.CourseDao;

@Repository
public class CourseDaoImpl extends BaseDaoImpl<Course, Integer> implements
    CourseDao {

  @Override
  public List<Course> findByUser(int uid) {
    String queryString = "from Course u where u.userid = ?";
    return (List<Course>) getHibernateTemplate().find(queryString, uid);
  }

  @Override
  public Course addCourse(Course course) {
    // TODO Auto-generated method stub
    return add(course);
  }

  @Override
  protected Class<Course> getEntityClass() {
    // TODO Auto-generated method stub
    return Course.class;
  }

  @Override
  public boolean deleteCourse(Course c) {

    if (delete(c) != null) {

    }

    return true;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByType(int uid, int sourtype, int curpage,
                               int pagesize) {
    Pagination result = new Pagination();
    CourseCatalog courseCatalog = this.getHibernateTemplate().get(
        CourseCatalog.class, uid);
    if (courseCatalog != null) {
      Finder finder = Finder.create("from Course c ");
      finder.append(" where c.courseCatalog.lft>=");
      finder.append("" + courseCatalog.getLft());
      finder.append(" and c.courseCatalog.rgt <=" + courseCatalog.getRgt());

      if (sourtype == 1) {
        finder.append("order by c.id desc");
      } else if (sourtype == 2) {
        finder.append("order by c.ups desc");
      } else if (sourtype == 3) {
        finder.append("order by c.ups desc");
      } else if (sourtype == 4) {
        finder.append("order by c.ups desc");
      } else if (sourtype == 5) {
        finder.append("order by c.usertype desc");
      } else {
        finder.append("order by c.id desc");
      }
      result = find(finder, curpage, pagesize);

    }
    return result;
  }

  @Override
  public Course findById(int id) {
    Course entity = get(id);

    return entity;
  }

  @Override
  public Course up(int id) {
    Course entity = get(id);
    Integer count = entity.getUps();
    if (count == null) {
      count = 0;
    }
    count++;
    entity.setUps(count);
    return entity;
  }

  @Override
  public Pagination pageByTransactionType(int type, int sourtype,
                                          int curpage, int pagesize) {
    Pagination result = new Pagination();

    Finder finder = Finder
        .create("select c.course,c from Tradingrecord c ");
    finder.append(" where c.state=");
    finder.append("" + type);
    finder.append(" and c.member.id =" + sourtype);
    finder.append(" order by c.tradingdate desc");
    result = find(finder, curpage, pagesize);

    return result;
  }

  @Override
  public Pagination pageByUser(int userid, int statetype, int sourtype, int curpage,
                               int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.member.id = " + userid);
    finder.append(" and c.statetype = " + statetype);
    if (sourtype == 1) {
      finder.append("order by c.id desc");
    } else if (sourtype == 2) {
      finder.append("order by c.ups desc");
    } else if (sourtype == 3) {
      finder.append("order by c.ups desc");
    } else if (sourtype == 4) {
      finder.append("order by c.ups desc");
    } else if (sourtype == 5) {
      finder.append("order by c.member.id desc");
    }
    result = find(finder, curpage, pagesize);

    return result;
  }


  @Override
  public Pagination goodByUser(int userid, int curpage, int size) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c  where c.member.id = " + userid + "  and c.isgood = 1 order by c.ups desc");
    result = find(finder, curpage, size);
    return result;
  }

}
