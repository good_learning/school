package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CourseImg;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.CourseImgDao;

@Repository
public class CourseImgDaoImpl extends BaseDaoImpl<CourseImg, Long> implements CourseImgDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CourseImg findById(Long id) {
    CourseImg entity = get(id);
    return entity;
  }

  public CourseImg save(CourseImg bean) {
    getSession().save(bean);
    return bean;
  }

  public CourseImg deleteById(Long id) {
    CourseImg entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CourseImg> getEntityClass() {
    return CourseImg.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByCourse(int courseid, int pageNo, int pageSize) {
    Pagination result = null;
    Finder finder = Finder.create("from CourseImg c ");
    finder.append(" where c.course.id =");
    finder.append("" + courseid);
    result = find(finder, pageNo, pageSize);
    return result;
  }
}