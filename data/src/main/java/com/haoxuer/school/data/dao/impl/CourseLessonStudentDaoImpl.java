package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CourseLessonStudentDao;

@Repository
public class CourseLessonStudentDaoImpl extends BaseDaoImpl<CourseLessonStudent, Long> implements CourseLessonStudentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CourseLessonStudent findById(Long id) {
    CourseLessonStudent entity = get(id);
    return entity;
  }

  public CourseLessonStudent save(CourseLessonStudent bean) {
    getSession().save(bean);
    return bean;
  }

  public CourseLessonStudent deleteById(Long id) {
    CourseLessonStudent entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CourseLessonStudent> getEntityClass() {
    return CourseLessonStudent.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}