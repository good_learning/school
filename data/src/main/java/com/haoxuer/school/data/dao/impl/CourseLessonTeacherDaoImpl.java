package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CourseLessonTeacher;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CourseLessonTeacherDao;

@Repository
public class CourseLessonTeacherDaoImpl extends BaseDaoImpl<CourseLessonTeacher, Long> implements CourseLessonTeacherDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CourseLessonTeacher findById(Long id) {
    CourseLessonTeacher entity = get(id);
    return entity;
  }

  public CourseLessonTeacher save(CourseLessonTeacher bean) {
    getSession().save(bean);
    return bean;
  }

  public CourseLessonTeacher deleteById(Long id) {
    CourseLessonTeacher entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CourseLessonTeacher> getEntityClass() {
    return CourseLessonTeacher.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}