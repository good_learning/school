package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CourseSubscribe;
import com.haoxuer.school.data.oto.SubscribeCourseInfo;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.CourseSubscribeDao;

@Repository
public class CourseSubscribeDaoImpl extends BaseDaoImpl<CourseSubscribe, Long> implements CourseSubscribeDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CourseSubscribe findById(Long id) {
    CourseSubscribe entity = get(id);
    return entity;
  }

  public CourseSubscribe save(CourseSubscribe bean) {
    getSession().save(bean);
    return bean;
  }

  public CourseSubscribe deleteById(Long id) {
    CourseSubscribe entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CourseSubscribe> getEntityClass() {
    return CourseSubscribe.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByUserId(int userid, int pageNo, int pageSize) {
    Finder finder = Finder
        .create("");
    finder.append("SELECT coursesubscribe.adddate, ");
    finder.append("member.name AS schoolname,");
    finder.append(" course.name AS coursename,");
    finder.append(" coursetype.name as typename,");
    finder.append("coursesubscribe.id,");
    finder.append(" course.id as courseid,");
    finder.append(" course.imgpath");


    finder.append(" FROM coursesubscribe,course,member,coursetype");
    finder.append(" WHERE coursesubscribe.courseid=course.id AND ");
    finder.append(" member.id=course.userid AND coursetype.id=course.coursetypeid");
    finder.append(" and coursesubscribe.userid=" + userid);

    return findSql(finder, pageNo, pageSize, SubscribeCourseInfo.class);
  }
}