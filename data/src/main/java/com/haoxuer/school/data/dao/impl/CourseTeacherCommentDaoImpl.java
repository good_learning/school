package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.CourseTeacherComment;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.CourseTeacherCommentDao;

@Repository
public class CourseTeacherCommentDaoImpl extends BaseDaoImpl<CourseTeacherComment, Long> implements CourseTeacherCommentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public CourseTeacherComment findById(Long id) {
    CourseTeacherComment entity = get(id);
    return entity;
  }

  public CourseTeacherComment save(CourseTeacherComment bean) {
    getSession().save(bean);
    return bean;
  }

  public CourseTeacherComment deleteById(Long id) {
    CourseTeacherComment entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<CourseTeacherComment> getEntityClass() {
    return CourseTeacherComment.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}