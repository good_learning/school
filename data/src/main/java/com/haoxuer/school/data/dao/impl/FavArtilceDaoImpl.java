package com.haoxuer.school.data.dao.impl;

import java.sql.Timestamp;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.entity.FavArticle;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.FavArtilceDao;

@Repository
public class FavArtilceDaoImpl extends BaseDaoImpl<FavArticle, Integer> implements FavArtilceDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public FavArticle findById(Integer id) {
    FavArticle entity = get(id);
    return entity;
  }

  public FavArticle save(FavArticle bean) {
    getSession().save(bean);
    return bean;
  }

  public FavArticle deleteById(Integer id) {
    FavArticle entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public int fav(Long userid, int courseid) {
    int result = 0;
    String queryString = "from FavArticle u where u.member.id = ";
    Finder finder = Finder.create(queryString);
    finder.append("" + userid);
    finder.append(" and u.article.id = ");
    finder.append("" + courseid);
    result = countQueryResult(finder);
    if (result < 1) {
      FavArticle favcourse = new FavArticle();
      Article course = new Article();
      course.setId(courseid);
      favcourse.setArticle(course);
      Member member = new Member();
      member.setId(userid);
      favcourse.setMember(member);
      favcourse.setFavdate(new Timestamp(System.currentTimeMillis()));
      save(favcourse);
      result = 1;
    } else {
      result = 11;
    }

    return result;
  }

  @Override
  protected Class<FavArticle> getEntityClass() {
    return FavArticle.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}