package com.haoxuer.school.data.dao.impl;

import java.sql.Timestamp;

import com.haoxuer.school.data.entity.FavTeacher;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.FavTeacherDao;

@Repository
public class FavTeacherDaoImpl extends BaseDaoImpl<FavTeacher, Integer> implements FavTeacherDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public FavTeacher findById(Integer id) {
    FavTeacher entity = get(id);
    return entity;
  }

  public FavTeacher save(FavTeacher bean) {
    getSession().save(bean);
    return bean;
  }

  public FavTeacher deleteById(Integer id) {
    FavTeacher entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<FavTeacher> getEntityClass() {
    return FavTeacher.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }


  @Override
  public int fav(Long userid, Long teacherid) {
    int result = 0;
    String queryString = "from FavTeacher u where u.member.id = ";
    Finder finder = Finder.create(queryString);
    finder.append("" + userid);
    finder.append(" and u.teacher.id = ");
    finder.append("" + teacherid);
    result = countQueryResult(finder);
    if (result < 1) {
      FavTeacher favTeacher = new FavTeacher();
      Member teacher = new Member();
      teacher.setId(teacherid);
      favTeacher.setTeacher(teacher);
      Member member = new Member();
      member.setId(userid);
      favTeacher.setMember(member);
      favTeacher.setFavdate(new Timestamp(System.currentTimeMillis()));
      save(favTeacher);
    }

    return result;
  }


}