package com.haoxuer.school.data.dao.impl;

import java.sql.Timestamp;

import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.Favcourse;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.FavcourseDao;

@Repository
public class FavcourseDaoImpl extends BaseDaoImpl<Favcourse, Integer> implements
    FavcourseDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Favcourse findById(Integer id) {
    Favcourse entity = get(id);
    return entity;
  }

  public Favcourse save(Favcourse bean) {
    getSession().save(bean);
    return bean;
  }

  public Favcourse deleteById(Integer id) {
    Favcourse entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Favcourse> getEntityClass() {
    return Favcourse.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public int fav(Long userid, int courseid) {
    int result = 0;
    String queryString = "from Favcourse u where u.member.id = ";
    Finder finder = Finder.create(queryString);
    finder.append("" + userid);
    finder.append(" and u.course.id = ");
    finder.append("" + courseid);
    result = countQueryResult(finder);
    if (result < 1) {
      Favcourse favcourse = new Favcourse();
      Course course = new Course();
      course.setId(courseid);
      favcourse.setCourse(course);
      Member member = new Member();
      member.setId(userid);
      favcourse.setMember(member);
      favcourse.setFavdate(new Timestamp(System.currentTimeMillis()));
      save(favcourse);
    }

    return result;
  }

  @Override
  public Pagination pageByUserId(Long userid, int pageNo, int pageSize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("select u.course from Favcourse u where u.member.id = ");
    finder.append("" + userid);
    finder.append("  order by u.id desc");
    result = find(finder, pageNo, pageSize);
    return result;
  }
}