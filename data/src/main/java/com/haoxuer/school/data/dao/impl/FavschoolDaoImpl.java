package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Favschool;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.FavschoolDao;

@Repository
public class FavschoolDaoImpl extends BaseDaoImpl<Favschool, Integer> implements FavschoolDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Favschool findById(Integer id) {
    Favschool entity = get(id);
    return entity;
  }

  public Favschool save(Favschool bean) {
    getSession().save(bean);
    return bean;
  }

  public Favschool deleteById(Integer id) {
    Favschool entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Favschool> getEntityClass() {
    return Favschool.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}