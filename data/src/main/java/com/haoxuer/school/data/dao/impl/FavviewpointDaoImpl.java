package com.haoxuer.school.data.dao.impl;

import java.sql.Timestamp;

import com.haoxuer.school.data.entity.Favviewpoint;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.FavviewpointDao;

@Repository
public class FavviewpointDaoImpl extends BaseDaoImpl<Favviewpoint, Long> implements FavviewpointDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Favviewpoint findById(Long id) {
    Favviewpoint entity = get(id);
    return entity;
  }

  public Favviewpoint save(Favviewpoint bean) {
    getSession().save(bean);
    return bean;
  }

  public Favviewpoint deleteById(Long id) {
    Favviewpoint entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Favviewpoint> getEntityClass() {
    return Favviewpoint.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public int fav(Long userid, Long courseid) {
    int result = 0;
    String queryString = "from Favviewpoint u where u.member.id = ";
    Finder finder = Finder.create(queryString);
    finder.append("" + userid);
    finder.append(" and u.viewpoint.id = ");
    finder.append("" + courseid);
    result = countQueryResult(finder);
    if (result < 1) {
      Favviewpoint favcourse = new Favviewpoint();
      Member member = new Member();
      member.setId(userid);
      favcourse.setMember(member);
      favcourse.setFavdate(new Timestamp(System.currentTimeMillis()));
      Viewpoint viewpoint = new Viewpoint();
      viewpoint.setId(courseid);
      favcourse.setViewpoint(viewpoint);
      save(favcourse);
      result = 2;
    }

    return result;
  }

  @Override
  public Pagination pageByUserId(Long userid, int pageNo, int pageSize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("select u.viewpoint from Favviewpoint u where u.member.id = ");
    finder.append("" + userid);
    finder.append("  order by u.id desc");
    result = find(finder, pageNo, pageSize);
    return result;
  }
}