package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ForumGroupDao;

@Repository
public class ForumGroupDaoImpl extends BaseDaoImpl<ForumGroup, Integer>
    implements ForumGroupDao {

  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public ForumGroup findById(Integer id) {
    ForumGroup entity = get(id);
    return entity;
  }

  public ForumGroup save(ForumGroup bean) {
    getSession().save(bean);
    return bean;
  }


  public ForumGroup deleteById(Integer id) {
    ForumGroup entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public ForumGroup updateByUpdater(Updater<ForumGroup> updater) {
    ForumGroup group = super.updateByUpdater(updater);
    return group;
  }

  @Override
  protected Class<ForumGroup> getEntityClass() {
    return ForumGroup.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}