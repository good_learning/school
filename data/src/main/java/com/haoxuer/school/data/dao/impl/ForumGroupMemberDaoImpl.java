package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.forum.ForumGroupMember;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ForumGroupMemberDao;

@Repository
public class ForumGroupMemberDaoImpl extends
    BaseDaoImpl<ForumGroupMember, Integer> implements ForumGroupMemberDao {

  @Override
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  @Override
  public ForumGroupMember findById(Integer id) {
    ForumGroupMember entity = get(id);
    return entity;
  }

  @Override
  public ForumGroupMember save(ForumGroupMember bean) {
    getSession().save(bean);
    return bean;
  }

  @Override
  public ForumGroupMember deleteById(Integer id) {
    ForumGroupMember entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }


  @Override
  protected Class<ForumGroupMember> getEntityClass() {
    return ForumGroupMember.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public List<ForumGroupMember> findByGroupid(Integer id) {
    Finder finder = Finder
        .create("from ForumGroupMember t where t.group=" + id);
    return find(finder);
  }
}