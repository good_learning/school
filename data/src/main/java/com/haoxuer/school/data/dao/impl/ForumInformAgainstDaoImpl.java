package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.forum.ForumInformAgainst;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.ForumInformAgainstDao;

@Repository
public class ForumInformAgainstDaoImpl extends
    BaseDaoImpl<ForumInformAgainst, Integer> implements
    ForumInformAgainstDao {

  @Override
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  @Override
  public ForumInformAgainst findById(Integer id) {
    ForumInformAgainst entity = get(id);
    return entity;
  }

  @Override
  public ForumInformAgainst save(ForumInformAgainst bean) {
    getSession().save(bean);
    return bean;
  }

  @Override
  public ForumInformAgainst deleteById(Integer id) {
    ForumInformAgainst entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }


  @Override
  protected Class<ForumInformAgainst> getEntityClass() {
    return ForumInformAgainst.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}