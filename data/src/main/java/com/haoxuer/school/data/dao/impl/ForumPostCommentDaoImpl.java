package com.haoxuer.school.data.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.haoxuer.school.data.entity.forum.ForumPost;
import com.haoxuer.school.data.entity.forum.ForumPostComment;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ForumPostCommentDao;

@Repository
public class ForumPostCommentDaoImpl extends
    BaseDaoImpl<ForumPostComment, Integer> implements ForumPostCommentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public ForumPostComment findById(Integer id) {
    ForumPostComment entity = get(id);
    return entity;
  }

  public ForumPostComment save(ForumPostComment bean) {
    getSession().save(bean);

    ForumPost post = getHibernateTemplate().get(ForumPost.class,
        bean.getForumPost().getId());
    Integer comments = post.getComments();
    if (comments == null) {
      comments = 0;
    }
    comments++;
    post.setComments(comments);
    return bean;
  }

  public ForumPostComment deleteById(Integer id) {
    ForumPostComment entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<ForumPostComment> getEntityClass() {
    return ForumPostComment.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  /**
   *
   */
  public Pagination find(Finder finder, int pageNo, int pageSize) {
    int totalCount = countQueryResult(finder);
    Pagination p = new Pagination(pageNo, pageSize, totalCount);
    if (totalCount < 1) {
      p.setList(new ArrayList());
      return p;
    }
    if (p.getPageNo() < pageNo) {

      p.setList(new ArrayList());
      return p;
    }
    Query query = getSessionFactory().getCurrentSession().createQuery(
        finder.getOrigHql());
    //finder.setParamsToQuery(query);
    query.setFirstResult(p.getFirstResult());
    query.setMaxResults(p.getPageSize());
    if (finder.isCacheable()) {
      query.setCacheable(true);
    }
    List list = query.list();
    p.setList(list);
    return p;
  }
}