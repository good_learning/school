package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.entity.forum.ForumPost;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ForumPostDao;

@Repository
public class ForumPostDaoImpl extends BaseDaoImpl<ForumPost, Integer> implements
    ForumPostDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public ForumPost findById(Integer id) {
    ForumPost entity = get(id);
    return entity;
  }

  public ForumPost save(ForumPost bean) {
    getSession().save(bean);
    return bean;
  }

  public ForumPost deleteById(Integer id) {
    ForumPost entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<ForumPost> getEntityClass() {
    return ForumPost.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public List<ForumPost> findByColumnId(Integer id) {
    String queryString = "from ForumPost u where u.forumColumn.id = ?";
    return (List<ForumPost>) getHibernateTemplate().find(queryString, id);
  }

  @Override
  public List<ForumPost> findByColumnId(Integer columnid, int size,
                                        String orders) {
    List<ForumPost> result = null;
    ForumColumn coursetype = this.getHibernateTemplate().get(
        ForumColumn.class, columnid);
    if (coursetype != null) {
      Finder finder = Finder.create("from ForumPost c ");
      finder.append(" where c.forumColumn.lft>=:lft");
      finder.append(" and c.forumColumn.rgt <=:rgt");
      finder.setParam("lft", coursetype.getLft());
      finder.setParam("rgt", coursetype.getRgt());

      if (null != orders && !orders.trim().equals("")) {
        finder.append(" order by " + orders.replaceAll("@p.", "c."));
      }
      Pagination page = find(finder, 1, size);
      if (page != null) {
        result = (List<ForumPost>) page.getList();
      }
    }
    return result;
  }

  @Override
  public Pagination pageByColumnId(Integer columnid, String orders, int size,
                                   int curpage) {
    Pagination result = null;
    ForumColumn coursetype = this.getHibernateTemplate().get(
        ForumColumn.class, columnid);
    if (coursetype != null) {
      Finder finder = Finder.create("from ForumPost c ");
      finder.append(" where c.forumColumn.lft>=:lft");
      finder.append(" and c.forumColumn.rgt <=:rgt");

      // Set Parameters
      finder.setParam("lft", coursetype.getLft());
      finder.setParam("rgt", coursetype.getRgt());

      if (null != orders && !orders.trim().equals("")) {
        finder.append(" order by " + orders.replaceAll(" p.", " c."));
      }
      result = find(finder, curpage, size);

    }
    return result;
  }

  @Override
  public int getMaxSortSeq() {
    String sql = "select max(sortSeq) from forumpost";
    Object o = this.getSession().createNativeQuery(sql).getSingleResult();
    int maxSortSeq = 0;
    if (o instanceof Number) {
      Number number = (Number) o;
      maxSortSeq = number.intValue();
    }
    return maxSortSeq;
  }

  /**
   *
   */
  // public Pagination find(Finder finder, int pageNo, int pageSize) {
  // int totalCount = countQueryResult(finder);
  // Pagination p = new Pagination(pageNo, pageSize, totalCount);
  // if (totalCount < 1) {
  // p.setList(new ArrayList());
  // return p;
  // }
  // if (p.getPageNo() < pageNo) {
  //
  // p.setList(new ArrayList());
  // return p;
  // }
  // Query query = getSessionFactory().getCurrentSession().createQuery(
  // finder.getOrigHql());
  // finder.setParamsToQuery(query);
  // query.setFirstResult(p.getFirstResult());
  // query.setMaxResults(p.getPageSize());
  // if (finder.isCacheable()) {
  // query.setCacheable(true);
  // }
  // List list = query.list();
  // p.setList(list);
  // return p;
  // }

  // @Override
  // public Pagination recommend(int type, int size, int curpage) {
  // Pagination result = null;
  // Articletype coursetype = this.getHibernateTemplate().get(
  // Articletype.class, type);
  // if (coursetype != null) {
  // Finder finder = Finder.create("from Article c ");
  // finder.append(" where c.articletype.lft>=");
  // finder.append("" + coursetype.getLft());
  // finder.append(" and c.articletype.rgt <=" + coursetype.getRgt());
  // finder.append(" and c.is_recommend=1  ");
  // finder.append("  order by c.sortrecommend desc");
  // result = find(finder, curpage, size);
  //
  // }
  // return result;
  // }

}