package com.haoxuer.school.data.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import com.haoxuer.school.data.entity.Friend;
import com.haoxuer.school.data.entity.Friendtype;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.FriendDao;

@Repository
public class FriendDaoImpl extends BaseDaoImpl<Friend, Integer> implements
    FriendDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Friend findById(Integer id) {
    Friend entity = get(id);
    return entity;
  }

  public Friend save(Friend bean) {
    getSession().save(bean);
    return bean;
  }

  public Friend deleteById(Integer id) {
    Friend entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Friend> getEntityClass() {
    return Friend.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Friend deleteById(Integer id, Integer friendid) {

    Friend data = (Friend) getHibernateTemplate().find(
        "from Friend u where u.userid = ? and u.friendid = ?", id,
        friendid).get(0);
    if (data != null) {
      getSession().delete(data);
    }
    return data;
  }

  @Override
  public List<Friend> findByType(Integer uid, int type) {

    List<Friend> data = (List<Friend>) getHibernateTemplate()
        .find("from Friend u where u.member2.id = ? and u.friendtypeBean.id = ?",
            uid, type);
    return data;

  }

  @Override
  public Pagination getByType(int userid, int type, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("select u.member1 from Friend u where u.member2.id = ");
    finder.append("" + userid);
    finder.append(" and u.friendtypeBean.id = " + type);
    result = find(finder, curpage, pagesize);
    return result;
  }

  @Override
  public int follows(int userid, int friendid) {
    int result = 0;
    Friend item = new Friend();
    Friendtype friendtypeBean = new Friendtype();
    friendtypeBean.setId(3);
    item.setFriendtypeBean(friendtypeBean);
    Member member1 = new Member();
    //member1.setId(friendid);
    item.setMember1(member1);

    Member member2 = new Member();
   // member2.setId(userid);
    item.setMember2(member2);

    item.setAdddate(new Timestamp(System.currentTimeMillis()));
    Finder finder = Finder.create();
    finder.append("from  Friend u where u.member2.id =" + userid);
    finder.append(" and u.member1.id =" + friendid);
    finder.append(" and u.friendtypeBean.id =" + 3);

    int counts = countQueryResult(finder);
    if (counts > 0) {
      result = 0;
    } else {
      save(item);
      result = 1;
      Finder finder2 = Finder.create();
      finder2.append("from  Friend u where u.member2.id =" + friendid);
      finder2.append(" and u.member1.id =" + userid);
      finder2.append(" and u.friendtypeBean.id =" + 3);
      int counts2 = countQueryResult(finder2);
      if (counts2 > 0) {
        Friend item1 = getm2(friendid, userid);
        save(item1);

        Friend item2 = getm2(userid, friendid);
        save(item2);
      }
      Member m = getHibernateTemplate().get(Member.class, friendid);
      if (m != null) {
//        Integer ups = m.getFollows();
//        if (ups == null) {
//          ups = 0;
//        }
//        ups++;
        //m.setFollows(ups);
      }

    }

    return result;
  }

  private Friend getm2(int userid, int friendid) {
    Friend item2 = new Friend();
    Friendtype friendtypeBean2 = new Friendtype();
    friendtypeBean2.setId(1);
    item2.setFriendtypeBean(friendtypeBean2);
    Member member12 = new Member();
    //member12.setId(userid);
    item2.setMember1(member12);

    Member member22 = new Member();
    //member22.setId(friendid);
    item2.setMember2(member22);

    item2.setAdddate(new Timestamp(System.currentTimeMillis()));
    return item2;
  }

  @Override
  public int isfollows(int userid, int friendid) {
    Finder finder = Finder.create();
    finder.append("from  Friend u where u.member2.id =" + userid);
    finder.append(" and u.member1.id =" + friendid);
    finder.append(" and u.friendtypeBean.id =" + 3);
    int counts = countQueryResult(finder);

    return counts;
  }

  @Override
  public int save(int userid, int friendid, int type) {
    int result = 0;
    Friend item = new Friend();
    Friendtype friendtypeBean = new Friendtype();
    friendtypeBean.setId(type);
    item.setFriendtypeBean(friendtypeBean);
    Member member1 = new Member();
    //member1.setId(friendid);
    item.setMember1(member1);

    Member member2 = new Member();
    //member2.setId(userid);
    item.setMember2(member2);

    item.setAdddate(new Timestamp(System.currentTimeMillis()));
    Finder finder = Finder.create();
    finder.append("from  Friend u where u.member2.id =" + userid);
    finder.append(" and u.member1.id =" + friendid);
    finder.append(" and u.friendtypeBean.id =" + type);

    int counts = countQueryResult(finder);
    if (counts > 0) {
      result = 0;
    } else {
      save(item);
      result = 1;
    }

    return result;

  }
}