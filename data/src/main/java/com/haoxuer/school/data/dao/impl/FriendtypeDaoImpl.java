package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Friendtype;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.FriendtypeDao;

@Repository
public class FriendtypeDaoImpl extends BaseDaoImpl<Friendtype, Integer> implements FriendtypeDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Friendtype findById(Integer id) {
    Friendtype entity = get(id);
    return entity;
  }

  public Friendtype save(Friendtype bean) {
    getSession().save(bean);
    return bean;
  }

  public Friendtype deleteById(Integer id) {
    Friendtype entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Friendtype> getEntityClass() {
    return Friendtype.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}