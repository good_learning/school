package com.haoxuer.school.data.dao.impl;

import java.util.Date;
import java.util.List;

import com.haoxuer.school.data.entity.KeyWord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.KeyWordDao;

@Repository
public class KeyWordDaoImpl extends BaseDaoImpl<KeyWord, Integer> implements KeyWordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public KeyWord findById(Integer id) {
    KeyWord entity = get(id);
    return entity;
  }

  public KeyWord save(KeyWord bean) {


    Finder finder = Finder.create("from KeyWord w where w.name =:name");
    finder.setParam("name", bean.getName());
    List<KeyWord> key = find(finder);
    if (key != null && key.size() > 0) {
      KeyWord w = key.get(0);
      int size = w.getSize();
      size++;
      w.setSize(size);
      w.setLasttime(new Date());
    } else {
      bean.setSize(1);
      bean.setLasttime(new Date());
      bean.setStartime(new Date());
      getSession().save(bean);
    }
    return bean;
  }

  public KeyWord deleteById(Integer id) {
    KeyWord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<KeyWord> getEntityClass() {
    return KeyWord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}