package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.MemberAuthentication;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.MemberAuthenticationDao;

@Repository
public class MemberAuthenticationDaoImpl extends BaseDaoImpl<MemberAuthentication, Integer> implements MemberAuthenticationDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public MemberAuthentication findById(Integer id) {
    MemberAuthentication entity = get(id);
    return entity;
  }

  public MemberAuthentication save(MemberAuthentication bean) {
    getSession().save(bean);
    return bean;
  }

  public MemberAuthentication deleteById(Integer id) {
    MemberAuthentication entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<MemberAuthentication> getEntityClass() {
    return MemberAuthentication.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}