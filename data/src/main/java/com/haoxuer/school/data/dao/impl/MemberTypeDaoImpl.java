package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.MemberType;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.MemberTypeDao;

@Repository
public class MemberTypeDaoImpl extends BaseDaoImpl<MemberType, Integer> implements MemberTypeDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public MemberType findById(Integer id) {
    MemberType entity = get(id);
    return entity;
  }

  public MemberType save(MemberType bean) {
    getSession().save(bean);
    return bean;
  }

  public MemberType deleteById(Integer id) {
    MemberType entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<MemberType> getEntityClass() {
    return MemberType.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public List<MemberType> all() {
    Finder finder = Finder.create("from MemberType m");
    return find(finder);
  }
}