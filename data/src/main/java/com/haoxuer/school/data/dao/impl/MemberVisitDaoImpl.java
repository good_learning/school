package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.MemberVisit;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.MemberVisitDao;

@Repository
public class MemberVisitDaoImpl extends BaseDaoImpl<MemberVisit, Long> implements MemberVisitDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public MemberVisit findById(Long id) {
    MemberVisit entity = get(id);
    return entity;
  }

  public MemberVisit save(MemberVisit bean) {
    getSession().save(bean);
    return bean;
  }

  public MemberVisit deleteById(Long id) {
    MemberVisit entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<MemberVisit> getEntityClass() {
    return MemberVisit.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}