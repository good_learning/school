package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.MoneyRecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.MoneyRecordDao;

@Repository
public class MoneyRecordDaoImpl extends BaseDaoImpl<MoneyRecord, Integer> implements MoneyRecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public MoneyRecord findById(Integer id) {
    MoneyRecord entity = get(id);
    return entity;
  }

  public MoneyRecord save(MoneyRecord bean) {
    getSession().save(bean);
    return bean;
  }

  public MoneyRecord deleteById(Integer id) {
    MoneyRecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<MoneyRecord> getEntityClass() {
    return MoneyRecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}