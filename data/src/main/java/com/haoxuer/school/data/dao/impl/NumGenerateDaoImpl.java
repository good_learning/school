package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.NumGenerate;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.NumGenerateDao;

@Repository
public class NumGenerateDaoImpl extends BaseDaoImpl<NumGenerate, Long>
    implements NumGenerateDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public NumGenerate findById(Long id) {
    NumGenerate entity = get(id);
    return entity;
  }

  public NumGenerate save(NumGenerate bean) {
    getSession().save(bean);
    return bean;
  }

  public NumGenerate deleteById(Long id) {
    NumGenerate entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<NumGenerate> getEntityClass() {
    return NumGenerate.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public NumGenerate findkey(String key, String type) {
    NumGenerate resutl = null;

    Finder finder = Finder.create();
    finder.append("from NumGenerate n where n.keyinfo=:keyinfo and  n.typeinfo=:typeinfo");
    finder.setParam("keyinfo", key);
    finder.setParam("typeinfo", type);
    List<NumGenerate> ns = find(finder);
    if (ns != null && ns.size() > 0) {
      resutl = ns.get(0);
    } else {
      resutl = new NumGenerate();
      resutl.setKeyinfo(key);
      resutl.setTypeinfo(type);
      resutl.setNum(0);
      save(resutl);
    }

    return resutl;
  }
}