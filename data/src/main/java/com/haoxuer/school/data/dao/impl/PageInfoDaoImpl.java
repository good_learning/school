package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.PageInfo;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.PageInfoDao;

@Repository
public class PageInfoDaoImpl extends BaseDaoImpl<PageInfo, Integer> implements PageInfoDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public PageInfo findById(Integer id) {
    PageInfo entity = get(id);
    return entity;
  }

  public PageInfo save(PageInfo bean) {
    getSession().save(bean);
    return bean;
  }

  public PageInfo deleteById(Integer id) {
    PageInfo entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<PageInfo> getEntityClass() {
    return PageInfo.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}