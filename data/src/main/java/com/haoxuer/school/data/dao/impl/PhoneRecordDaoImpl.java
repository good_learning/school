package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.PhoneRecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.PhoneRecordDao;

@Repository
public class PhoneRecordDaoImpl extends BaseDaoImpl<PhoneRecord, Integer> implements PhoneRecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public PhoneRecord findById(Integer id) {
    PhoneRecord entity = get(id);
    return entity;
  }

  public PhoneRecord save(PhoneRecord bean) {
    getSession().save(bean);
    return bean;
  }

  public PhoneRecord deleteById(Integer id) {
    PhoneRecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<PhoneRecord> getEntityClass() {
    return PhoneRecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}