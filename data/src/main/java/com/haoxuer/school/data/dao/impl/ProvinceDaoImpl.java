package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Province;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.ProvinceDao;

@Repository
public class ProvinceDaoImpl extends BaseDaoImpl<Province, Integer> implements ProvinceDao {

  @Override
  protected Class<Province> getEntityClass() {
    return Province.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}
