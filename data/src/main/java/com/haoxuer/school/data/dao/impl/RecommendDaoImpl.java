package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Recommend;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.RecommendDao;

@Repository
public class RecommendDaoImpl extends BaseDaoImpl<Recommend, Long> implements RecommendDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Recommend findById(Long id) {
    Recommend entity = get(id);
    return entity;
  }

  public Recommend save(Recommend bean) {
    getSession().save(bean);
    return bean;
  }

  public Recommend deleteById(Long id) {
    Recommend entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Recommend> getEntityClass() {
    return Recommend.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination recommend(int type, int size, int curpage) {
    Pagination result = new Pagination();

    Finder finder = Finder.create("from Recommend u where u.recommendtype ="
        + type);
    finder.append(" order by u.sortnum desc");
    result = find(finder, curpage, size);
    return result;
  }
}