package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.RefundRecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.RefundRecordDao;

@Repository
public class RefundRecordDaoImpl extends BaseDaoImpl<RefundRecord, Integer> implements RefundRecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public RefundRecord findById(Integer id) {
    RefundRecord entity = get(id);
    return entity;
  }

  public RefundRecord save(RefundRecord bean) {
    getSession().save(bean);
    return bean;
  }

  public RefundRecord deleteById(Integer id) {
    RefundRecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<RefundRecord> getEntityClass() {
    return RefundRecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}