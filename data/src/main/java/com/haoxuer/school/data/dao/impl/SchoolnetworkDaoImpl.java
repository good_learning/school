package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Schoolnetwork;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.SchoolnetworkDao;

@Repository
public class SchoolnetworkDaoImpl extends BaseDaoImpl<Schoolnetwork, Integer> implements SchoolnetworkDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Schoolnetwork findById(Integer id) {
    Schoolnetwork entity = get(id);
    return entity;
  }

  public Schoolnetwork save(Schoolnetwork bean) {
    getSession().save(bean);
    return bean;
  }

  public Schoolnetwork deleteById(Integer id) {
    Schoolnetwork entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Schoolnetwork> getEntityClass() {
    return Schoolnetwork.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}