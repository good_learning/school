package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Schoolteacher;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.SchoolteacherDao;

@Repository
public class SchoolteacherDaoImpl extends BaseDaoImpl<Schoolteacher, Integer> implements SchoolteacherDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Schoolteacher findById(Integer id) {
    Schoolteacher entity = get(id);
    return entity;
  }

  public Schoolteacher save(Schoolteacher bean) {
    getSession().save(bean);
    return bean;
  }

  public Schoolteacher deleteById(Integer id) {
    Schoolteacher entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Schoolteacher> getEntityClass() {
    return Schoolteacher.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}