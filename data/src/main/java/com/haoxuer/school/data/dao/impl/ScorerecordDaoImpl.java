package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.Scorerecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ScorerecordDao;

@Repository
public class ScorerecordDaoImpl extends BaseDaoImpl<Scorerecord, Integer>
    implements ScorerecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Scorerecord findById(Integer id) {
    Scorerecord entity = get(id);
    return entity;
  }

  public Scorerecord save(Scorerecord bean) {
    getSession().save(bean);
    return bean;
  }

  public Scorerecord deleteById(Integer id) {
    Scorerecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Scorerecord> getEntityClass() {
    return Scorerecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByUserId(int id, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("from Scorerecord u where u.member.id = ");
    finder.append("" + id);
    finder.append("   order by  u.id  desc");

    result = find(finder, curpage, pagesize);
    return result;
  }

  @Override
  public int getallscore(Long userid) {
    int result = 0;
    Finder finder = Finder
        .create("select sum(u.score) from Scorerecord u where u.member.id = "
            + userid);
    List datas = find(finder);
    if (datas != null && datas.size() > 0) {
      Number number = (Number) datas.get(0);
      if (number != null) {
        result = number.intValue();
      }
    }

    return result;
  }
}