package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.SystemInfoDao;

@Repository
public class SystemInfoDaoImpl extends BaseDaoImpl<SystemInfo, Long> implements SystemInfoDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public SystemInfo findById(Long id) {
    SystemInfo entity = get(id);
    return entity;
  }

  public SystemInfo save(SystemInfo bean) {
    getSession().save(bean);
    return bean;
  }

  public SystemInfo deleteById(Long id) {
    SystemInfo entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<SystemInfo> getEntityClass() {
    return SystemInfo.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}