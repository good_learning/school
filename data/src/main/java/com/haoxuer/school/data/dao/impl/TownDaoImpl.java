package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Town;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.TownDao;

@Repository
public class TownDaoImpl extends BaseDaoImpl<Town, Integer> implements TownDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Town findById(Integer id) {
    Town entity = get(id);
    return entity;
  }

  public Town save(Town bean) {
    getSession().save(bean);
    return bean;
  }

  public Town deleteById(Integer id) {
    Town entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Town> getEntityClass() {
    return Town.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}