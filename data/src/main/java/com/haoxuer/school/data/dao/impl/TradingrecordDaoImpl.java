package com.haoxuer.school.data.dao.impl;

import java.util.List;

import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.TradingrecordDao;

@Repository
public class TradingrecordDaoImpl extends BaseDaoImpl<Tradingrecord, Integer> implements TradingrecordDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Tradingrecord findById(Integer id) {
    Tradingrecord entity = get(id);
    return entity;
  }

  public Tradingrecord save(Tradingrecord bean) {
    getSession().save(bean);
    return bean;
  }

  public Tradingrecord deleteById(Integer id) {
    Tradingrecord entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Tradingrecord> getEntityClass() {
    return Tradingrecord.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public List<Tradingrecord> findAllByUid(Long userid) {

    String queryString = "from Tradingrecord t  where t.course.member.id= ?";
    List<Tradingrecord> result = (List<Tradingrecord>) getHibernateTemplate().find(queryString, userid);

    for (Tradingrecord tradingrecord : result) {
      tradingrecord.getCourse().getMember();
    }
    return result;

  }

  @Override
  public List<Tradingrecord> findAllByType(int type) {
    String queryString = "from Tradingrecord t  where t.course.id= ?";
    List<Tradingrecord> result = (List<Tradingrecord>) getHibernateTemplate().find(queryString, type);

    for (Tradingrecord tradingrecord : result) {
      tradingrecord.getCourse().getMember();
    }
    return result;
  }
}