package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.TradingrecordRefund;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.TradingrecordRefundDao;

@Repository
public class TradingrecordRefundDaoImpl extends BaseDaoImpl<TradingrecordRefund, Integer> implements TradingrecordRefundDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public TradingrecordRefund findById(Integer id) {
    TradingrecordRefund entity = get(id);
    return entity;
  }

  public TradingrecordRefund save(TradingrecordRefund bean) {
    getSession().save(bean);
    return bean;
  }

  public TradingrecordRefund deleteById(Integer id) {
    TradingrecordRefund entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<TradingrecordRefund> getEntityClass() {
    return TradingrecordRefund.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}