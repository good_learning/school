package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.ViewPointTypes;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.school.data.dao.ViewPointTypesDao;

@Repository
public class ViewPointTypesDaoImpl extends BaseDaoImpl<ViewPointTypes, Integer> implements ViewPointTypesDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public ViewPointTypes findById(Integer id) {
    ViewPointTypes entity = get(id);
    return entity;
  }

  public ViewPointTypes save(ViewPointTypes bean) {
    getSession().save(bean);
    return bean;
  }

  public ViewPointTypes deleteById(Integer id) {
    ViewPointTypes entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<ViewPointTypes> getEntityClass() {
    return ViewPointTypes.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}