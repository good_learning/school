package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ViewpointDao;

@Repository
public class ViewpointDaoImpl extends BaseDaoImpl<Viewpoint, Integer> implements ViewpointDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Viewpoint findById(Integer id) {
    Viewpoint entity = get(id);
    return entity;
  }

  public Viewpoint save(Viewpoint bean) {
    getSession().save(bean);
    return bean;
  }

  public Viewpoint deleteById(Integer id) {
    Viewpoint entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Viewpoint> getEntityClass() {
    return Viewpoint.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByUserId(int userid, int pageNo, int pageSize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("from Viewpoint u where u.member.id = ");
    finder.append("" + userid);
    finder.append("   order by  u.id  desc");


    result = find(finder, pageNo, pageSize);
    return result;
  }

  @Override
  public Viewpoint up(int id) {
    Viewpoint entity = get(id);
    Integer count = entity.getUps();
    if (count == null) {
      count = 0;
    }
    count++;
    entity.setUps(count);
    return entity;
  }
}