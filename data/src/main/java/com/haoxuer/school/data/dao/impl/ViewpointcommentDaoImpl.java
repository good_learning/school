package com.haoxuer.school.data.dao.impl;

import com.haoxuer.school.data.entity.Viewpointcomment;
import com.haoxuer.discover.data.core.Pagination;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ViewpointcommentDao;

@Repository
public class ViewpointcommentDaoImpl extends BaseDaoImpl<Viewpointcomment, Integer> implements ViewpointcommentDao {
  public Pagination getPage(int pageNo, int pageSize) {
    Criteria crit = createCriteria();
    Pagination page = findByCriteria(crit, pageNo, pageSize);
    return page;
  }

  public Viewpointcomment findById(Integer id) {
    Viewpointcomment entity = get(id);
    return entity;
  }

  public Viewpointcomment save(Viewpointcomment bean) {
    getSession().save(bean);
    return bean;
  }

  public Viewpointcomment deleteById(Integer id) {
    Viewpointcomment entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Viewpointcomment> getEntityClass() {
    return Viewpointcomment.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Pagination pageByPointId(int commentid, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Viewpointcomment c ");
    finder.append(" where c.viewpoint.id = ");
    finder.append("" + commentid);
    finder.append(" order by c.id desc ");
    result = find(finder, curpage, pagesize);
    return result;
  }
}