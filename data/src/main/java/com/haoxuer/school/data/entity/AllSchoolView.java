package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "allschools")
public class AllSchoolView implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  private String id;
  @ManyToOne()
  @JoinColumn(name = "userid")
  private Member member;
  private String branchaddress;
  private String branchname;

  @ManyToOne()
  @JoinColumn(name = "coursetypeid")
  private CourseCatalog courseCatalog;

  private String coursetypepath;
  private String coursename;

  /**
   * 所有浏览量
   */
  private Integer views;

  /**
   * 课程点赞次数
   */
  private Integer ups;

  public Integer getViews() {
    return views;
  }

  public void setViews(Integer views) {
    this.views = views;
  }

  public Integer getUps() {
    return ups;
  }

  public void setUps(Integer ups) {
    this.ups = ups;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public String getBranchaddress() {
    return branchaddress;
  }

  public void setBranchaddress(String branchaddress) {
    this.branchaddress = branchaddress;
  }

  public String getBranchnaem() {
    return branchname;
  }

  public void setBranchnaem(String branchname) {
    this.branchname = branchname;
  }


  public String getCoursename() {
    return coursename;
  }

  public void setCoursename(String coursename) {
    this.coursename = coursename;
  }

  public CourseCatalog getCourseCatalog() {
    return courseCatalog;
  }

  public void setCourseCatalog(CourseCatalog courseCatalog) {
    this.courseCatalog = courseCatalog;
  }

  public String getCoursetypepath() {
    return coursetypepath;
  }

  public void setCoursetypepath(String coursetypepath) {
    this.coursetypepath = coursetypepath;
  }

}
