package com.haoxuer.school.data.entity;

import org.hibernate.annotations.ColumnDefault;
import org.jsoup.Jsoup;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 有门道文章表
 */
@Entity
@Table(name = "article")
public class Article implements Serializable {
  /**
   * 去除字符串中头部和尾部所包含的空格（包括:空格(全角，半角)、制表符、换页符等）
   *
   * @param s 需要处理的字符串
   * @return 清除以后的结果
   */
  public static String trim(String s) {
    String result = "";
    if (null != s && !"".equals(s)) {
      result = s.replaceAll("^[　*| *| *|//s*]*", "").replaceAll(
          "[　*| *| *|//s*]*$", "");
    }
    return result;
  }

  private static final long serialVersionUID = 1L;
  /**
   * 文章id，数据库自动增长
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 文章评论数量
   */
  private int commentsize;

  /**
   * 文章内容
   */
  private String content;

  /*
   *
   */
  private int joinsize;
  /**
   * 文章标题
   */
  @Column(length = 50)
  private String title;
  /**
   * 文章摘要
   */
  @Column(length = 100)
  private String summary;

  @Column(length = 150)
  /**
   * 文章封面图路径
   */
  private String imgpath;

  /**
   * 文章点赞次数
   */
  private int ups;

  /**
   * 文章访问次数
   */
  @Column()
  @ColumnDefault(value = "0")
  private Integer views;

  /**
   * 文章评论次数
   */
  @ColumnDefault(value = "0")
  @Column()
  private Integer comments;
  /**
   * 是否被推荐0为未推荐1为推荐
   */
  @Column()
  @ColumnDefault("0")
  private Integer is_recommend;

  /**
   * 推荐时候的排序号
   */
  @ColumnDefault("1")
  private Integer sortrecommend;


  /**
   * 文章发布时间
   */
  private Timestamp pubdate;

  /**
   * 文章分类
   */
  @ManyToOne
  @JoinColumn(name = "articletypeid")
  private Articletype articletype;

  /**
   * 文章作者
   */
  // bi-directional many-to-one association to Member
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  // bi-directional many-to-one association to Articlecomment
  @OneToMany(mappedBy = "article")
  private List<Articlecomment> articlecomments;

  public Article() {
  }

  public Articlecomment addArticlecomment(Articlecomment articlecomment) {
    getArticlecomments().add(articlecomment);
    articlecomment.setArticle(this);

    return articlecomment;
  }

  public List<Articlecomment> getArticlecomments() {
    return this.articlecomments;
  }

  public Articletype getArticletype() {
    return this.articletype;
  }

  public Integer getComments() {
    return comments;
  }

  public int getCommentsize() {
    return this.commentsize;
  }

  public String getContent() {
    return this.content;
  }

  public int getId() {
    return this.id;
  }

  public String getImgpath() {
    return imgpath;
  }

  public Integer getIs_recommend() {
    return is_recommend;
  }

  public int getJoinsize() {
    return this.joinsize;
  }

  public String getLeftContent() {
    String result = "";

    if (summary != null && summary.length() > 5) {
      result = summary;
    } else {
      if (content != null) {
        result = content;
      }
      result = Jsoup.parse(result).text();
      result = result.replaceAll("\\s*|\t|\r|\n", "");
      result = result.replaceAll("&nbsp;", "");
      result = result.replaceAll(" ", "");
      result = trim(result);

      int maxsize = 200;
      if (result.length() > maxsize) {
        result = result.substring(0, maxsize);
      }
    }
    result = result.trim();
    return result;

  }

  public Member getMember() {
    return this.member;
  }

  public Timestamp getPubdate() {
    return pubdate;
  }

  public Integer getSortrecommend() {
    return sortrecommend;
  }

  public String getSummary() {
    return summary;
  }

  public String getTitle() {
    return this.title;
  }

  public int getUps() {
    return this.ups;
  }

  public Integer getViews() {
    return views;
  }

  public Articlecomment removeArticlecomment(Articlecomment articlecomment) {
    getArticlecomments().remove(articlecomment);
    articlecomment.setArticle(null);

    return articlecomment;
  }

  public void setArticlecomments(List<Articlecomment> articlecomments) {
    this.articlecomments = articlecomments;
  }

  public void setArticletype(Articletype articletype) {
    this.articletype = articletype;
  }

  public void setComments(Integer comments) {
    this.comments = comments;
  }

  public void setCommentsize(int commentsize) {
    this.commentsize = commentsize;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

  public void setIs_recommend(Integer is_recommend) {
    this.is_recommend = is_recommend;
  }

  public void setJoinsize(int joinsize) {
    this.joinsize = joinsize;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setPubdate(Timestamp pubdate) {
    this.pubdate = pubdate;
  }

  public void setSortrecommend(Integer sortrecommend) {
    this.sortrecommend = sortrecommend;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setUps(int ups) {
    this.ups = ups;
  }

  public void setViews(Integer views) {
    this.views = views;
  }

}