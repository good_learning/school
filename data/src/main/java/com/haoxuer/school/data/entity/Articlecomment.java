package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * 文章评论表
 */
@Entity
@Table(name = "articlecomment")
@NamedQuery(name = "Articlecomment.findAll", query = "SELECT a FROM Articlecomment a")
public class Articlecomment implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 评论类容
   */
  private String content;

  /**
   * 评论的文章
   */
  @ManyToOne
  @JoinColumn(name = "articleid")
  private Article article;
  /**
   * 对评论的回复
   */
  @ManyToOne
  @JoinColumn(name = "articlecommentid")
  private Articlecomment articlecomment;

  /**
   * 评论点赞的次数
   */
  private Integer ups;

  /**
   * 发布评论的用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;


  /**
   * 评论日期
   */
  @Column()
  private Timestamp pubdate;

  public Articlecomment() {
  }

  public Article getArticle() {
    return this.article;
  }

  public Articlecomment getArticlecomment() {
    return articlecomment;
  }

  public String getContent() {
    return this.content;
  }

  public int getId() {
    return this.id;
  }

  public Member getMember() {
    return this.member;
  }

  public Timestamp getPubdate() {
    return pubdate;
  }

  public Integer getUps() {
    return ups;
  }

  public void setArticle(Article article) {
    this.article = article;
  }

  public void setArticlecomment(Articlecomment articlecomment) {
    this.articlecomment = articlecomment;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setPubdate(Timestamp pubdate) {
    this.pubdate = pubdate;
  }

  public void setUps(Integer ups) {
    this.ups = ups;
  }

}