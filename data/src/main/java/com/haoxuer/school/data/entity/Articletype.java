package com.haoxuer.school.data.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * 文章分类表
 */
@Entity
@Table(name = "articletype")
@NamedQuery(name = "Articletype.findAll", query = "SELECT a FROM Articletype a")
public class Articletype implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 数据左节点
   */
  private int lft;

  /**
   * 分类名称
   */
  @Column(length = 20)
  private String name;

  /**
   * 数据右节点
   */
  private int rgt;

  /**
   * 分类排序号
   */
  private int priority;


  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  //bi-directional many-to-one association to Article

  @OneToMany(mappedBy = "articletype")
  private List<Article> articles;

  //bi-directional many-to-one association to Articletype
  @ManyToOne
  @JoinColumn(name = "parent_id")
  private Articletype articletype;

  //bi-directional many-to-one association to Articletype
  @OneToMany(mappedBy = "articletype")
  private List<Articletype> articletypes;

  public Articletype() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getLft() {
    return this.lft;
  }

  public void setLft(int lft) {
    this.lft = lft;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRgt() {
    return this.rgt;
  }

  public void setRgt(int rgt) {
    this.rgt = rgt;
  }

  public List<Article> getArticles() {
    return this.articles;
  }

  public void setArticles(List<Article> articles) {
    this.articles = articles;
  }

  public Article addArticle(Article article) {
    getArticles().add(article);
    article.setArticletype(this);

    return article;
  }

  public Article removeArticle(Article article) {
    getArticles().remove(article);
    article.setArticletype(null);

    return article;
  }

  public Articletype getArticletype() {
    return this.articletype;
  }

  public void setArticletype(Articletype articletype) {
    this.articletype = articletype;
  }

  public List<Articletype> getArticletypes() {
    return this.articletypes;
  }

  public void setArticletypes(List<Articletype> articletypes) {
    this.articletypes = articletypes;
  }

  public Articletype addArticletype(Articletype articletype) {
    getArticletypes().add(articletype);
    articletype.setArticletype(this);

    return articletype;
  }

  public Articletype removeArticletype(Articletype articletype) {
    getArticletypes().remove(articletype);
    articletype.setArticletype(null);

    return articletype;
  }

}