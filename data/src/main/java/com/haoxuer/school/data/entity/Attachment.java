package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * 附件表  主要用户证件照片模块
 *
 * @author 年高
 */
@Entity
@Table(name = "attachment")
public class Attachment {

  /**
   * 数据库自增id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 文件url
   */
  private String fileurl;
  /**
   * 文件后缀名;例如：jpg;png
   */
  private String filetype;
  /**
   * 文件分类 1为学生照片  2老师荣誉证书  3老师授课风采    4学校荣誉证书   5教学环境
   */
  private int catalog;
  /**
   * 文件名称
   */
  private String name;
  /**
   * 添加时间
   */
  private Timestamp adddate;


  /**
   * 上传用户
   */
  @ManyToOne
  @JoinColumn(name = "memeberid")
  private Member member;


  public Timestamp getAdddate() {
    return adddate;
  }

  public int getCatalog() {
    return catalog;
  }

  public String getFiletype() {
    return filetype;
  }

  public String getFileurl() {
    return fileurl;
  }

  public long getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public String getName() {
    return name;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setCatalog(int catalog) {
    this.catalog = catalog;
  }

  public void setFiletype(String filetype) {
    this.filetype = filetype;
  }

  public void setFileurl(String fileurl) {
    this.fileurl = fileurl;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setName(String name) {
    this.name = name;
  }
}
