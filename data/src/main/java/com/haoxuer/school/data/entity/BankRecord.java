package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 银联支付记录
 *
 * @author 年高
 */
@Entity
@Table(name = "bankrecord")
public class BankRecord implements Serializable {

  /**
   * 数据库自增id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 订单号，用户传给银联，银联通知的时候根据订单号查找充值记录
   */
  private String orderid;

  /**
   * 充值用户
   */
  @ManyToOne
  @JoinColumn(name = "memeberid")
  private Member member;

  /**
   * 充值金额 单位 元
   */
  private float moneys;
  /**
   * 失败以后需要返回给用户的钱
   */
  private float backmoney;

  /**
   * 课程订单id
   */
  @ManyToOne
  @JoinColumn(name = "tradingrecordid")
  Tradingrecord tradingrecord;

  /**
   * 交易类型 充值 或者支付课程
   */
  private String typeinfo;


  /**
   * 备注
   */
  private String demo;

  public String getDemo() {
    return demo;
  }

  public void setDemo(String demo) {
    this.demo = demo;
  }

  /**
   * 订单状态 0 为向银联发送数据 1银联返回成功数据
   */
  private int state;

  /**
   * 数据添加的时间
   */
  private Timestamp addtime;

  /**
   * 处理超时时间
   */
  private Timestamp worktime;


  public Timestamp getWorktime() {
    return worktime;
  }

  public void setWorktime(Timestamp worktime) {
    this.worktime = worktime;
  }

  /**
   * 接收到银联通知时间
   */
  private Timestamp updatetime;

  /**
   * 银联接收类型 前台通知或者后台通知
   */
  private String noticetype;

  public Timestamp getAddtime() {
    return addtime;
  }

  public float getBackmoney() {
    return backmoney;
  }

  public long getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public float getMoneys() {
    return moneys;
  }

  public String getNoticetype() {
    return noticetype;
  }

  public String getOrderid() {
    return orderid;
  }

  /**
   * 银联接收的充值金额 单位 分
   *
   * @return
   */
  public int getPayMoney() {
    int result = (int) (moneys * 100);
    return result;
  }

  public int getState() {
    return state;
  }

  public Tradingrecord getTradingrecord() {
    return tradingrecord;
  }

  public String getTypeinfo() {
    return typeinfo;
  }

  public Timestamp getUpdatetime() {
    return updatetime;
  }

  public void setAddtime(Timestamp addtime) {
    this.addtime = addtime;
  }

  public void setBackmoney(float backmoney) {
    this.backmoney = backmoney;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMoneys(float moneys) {
    this.moneys = moneys;
  }

  public void setNoticetype(String noticetype) {
    this.noticetype = noticetype;
  }

  public void setOrderid(String orderid) {
    this.orderid = orderid;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setTradingrecord(Tradingrecord tradingrecord) {
    this.tradingrecord = tradingrecord;
  }

  public void setTypeinfo(String typeinfo) {
    this.typeinfo = typeinfo;
  }

  public void setUpdatetime(Timestamp updatetime) {
    this.updatetime = updatetime;
  }
}
