package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;


/**
 * 银行卡绑定
 */
@Entity
@Table(name = "bankbind")
@NamedQuery(name = "Bankbind.findAll", query = "SELECT b FROM Bankbind b")
public class Bankbind implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 绑定时间
   */
  @Column(nullable = false)
  private Timestamp binddate;
  /**
   * 绑定银行卡卡号
   */
  @Column(length = 50)
  private String code;
  /**
   * 银行卡姓名
   */
  @Column(length = 50)
  private String name;

  /**
   * 开户行所在地
   */
  @Column(length = 100)
  private String address;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * 银行卡开户所在行
   */
  @Column(length = 50)
  private String bankname;

  public String getBankname() {
    return bankname;
  }

  public void setBankname(String bankname) {
    this.bankname = bankname;
  }

  /**
   * 联系人电话
   */
  @Column(length = 50)
  private String phonenum;


  /**
   * 身份证号
   */
  @Column(length = 50)
  private String idinfo;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhonenum() {
    return phonenum;
  }

  public void setPhonenum(String phonenum) {
    this.phonenum = phonenum;
  }

  public String getIdinfo() {
    return idinfo;
  }

  public void setIdinfo(String idinfo) {
    this.idinfo = idinfo;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  //bi-directional many-to-one association to Member
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  public Bankbind() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Timestamp getBinddate() {
    return this.binddate;
  }

  public void setBinddate(Timestamp binddate) {
    this.binddate = binddate;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Member getMember() {
    return this.member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

}