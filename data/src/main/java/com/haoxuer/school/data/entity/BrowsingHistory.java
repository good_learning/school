package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 用户额课程浏览历史
 *
 * @author 年高
 */
@Entity
@Table(name = "browsinghistory")
public class BrowsingHistory {

  /**
   * 数据库自增id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 浏览次数
   */
  private int browsingsize;

  /**
   * 浏览时间
   */
  @Column(nullable = false)
  private Timestamp browsingdate;

  /**
   * 浏览课程
   */
  @ManyToOne
  @JoinColumn(name = "courseid")
  private Course course;
  /**
   * 浏览的用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  public Timestamp getBrowsingdate() {
    return browsingdate;
  }

  public int getBrowsingsize() {
    return browsingsize;
  }

  public Course getCourse() {
    return course;
  }

  public long getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public void setBrowsingdate(Timestamp browsingdate) {
    this.browsingdate = browsingdate;
  }

  public void setBrowsingsize(int browsingsize) {
    this.browsingsize = browsingsize;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

}
