package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

/**
 * 门道网站发型的充值卡
 *
 * @author 年高
 */
@Entity
@Table(name = "card")
public class Card {

  /**
   * 数据库自增id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 充值卡号
   */
  @Column(nullable = false, unique = true, length = 50)
  private String cardnum;

  /**
   * 充值卡密码
   */
  @Column(nullable = false, length = 20)
  private String cardpass;

  /**
   * 充值卡生成的时间
   */
  private Timestamp adddate;
  /**
   * 充值卡金额
   */
  private float money;
  /**
   * 充值卡余额
   */
  @ColumnDefault(value = "0")
  private float leftmoney;

  /**
   * 充值卡有谁生成的
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;


  /**
   * 充值卡拥有用户
   */
  @ManyToOne
  @JoinColumn(name = "useuser")
  private Member user;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCardnum() {
    return cardnum;
  }

  public void setCardnum(String cardnum) {
    this.cardnum = cardnum;
  }

  public String getCardpass() {
    return cardpass;
  }

  public void setCardpass(String cardpass) {
    this.cardpass = cardpass;
  }

  public Timestamp getAdddate() {
    return adddate;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public float getMoney() {
    return money;
  }

  public void setMoney(float money) {
    this.money = money;
  }

  public float getLeftmoney() {
    return leftmoney;
  }

  public void setLeftmoney(float leftmoney) {
    this.leftmoney = leftmoney;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Member getUser() {
    return user;
  }

  public void setUser(Member user) {
    this.user = user;
  }

}
