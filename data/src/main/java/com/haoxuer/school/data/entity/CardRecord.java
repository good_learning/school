package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 充值卡使用记录
 *
 * @author 年高
 */
@Entity
@Table(name = "card_record")
public class CardRecord {

  /**
   * 数据自增id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 使用的时候的充值卡
   */
  @ManyToOne
  @JoinColumn(name = "cardid")
  private Card card;
  /**
   * 使用的用户
   */
  @ManyToOne
  @JoinColumn(name = "memberid")
  private Member member;
  /**
   * 数据产生时间
   */
  private Timestamp addtime;
  /**
   * 使用的金额
   */
  private float money;

  public Timestamp getAddtime() {
    return addtime;
  }

  public Card getCard() {
    return card;
  }

  public long getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public float getMoney() {
    return money;
  }

  public void setAddtime(Timestamp addtime) {
    this.addtime = addtime;
  }

  public void setCard(Card card) {
    this.card = card;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMoney(float money) {
    this.money = money;
  }
}
