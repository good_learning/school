package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 提现记录
 *
 * @author 年高
 */
@Entity
@Table(name = "cash_record")
public class CashRecord {

  /**
   * 数据自增id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "memeberid")
  private Member member;

  /**
   * 银行卡id
   */
  @ManyToOne
  @JoinColumn(name = "bankbindid")
  Bankbind bankbind;


  private Float money;
  /**
   * 提现状态 0为体现中，1为体现成功
   */
  private Integer state = 0;


  public Integer getState() {
    return state;
  }


  public void setState(Integer state) {
    this.state = state;
  }


  /**
   * 数据添加的时间
   */
  private Timestamp addtime;


  public long getId() {
    return id;
  }


  public void setId(long id) {
    this.id = id;
  }


  public Member getMember() {
    return member;
  }


  public void setMember(Member member) {
    this.member = member;
  }


  public Bankbind getBankbind() {
    return bankbind;
  }


  public void setBankbind(Bankbind bankbind) {
    this.bankbind = bankbind;
  }


  public Float getMoney() {
    return money;
  }


  public void setMoney(Float money) {
    this.money = money;
  }


  public Timestamp getAddtime() {
    return addtime;
  }


  public void setAddtime(Timestamp addtime) {
    this.addtime = addtime;
  }


}
