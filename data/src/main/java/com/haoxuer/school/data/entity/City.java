package com.haoxuer.school.data.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 城市表
 */
@Entity
@Table(name = "city")
@NamedQuery(name = "City.findAll", query = "SELECT c FROM City c")
public class City implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 城市名称
   */
  @Column(length = 20)
  private String name;
  /**
   * 城市所在省份
   */
  @ManyToOne
  @JoinColumn(name = "provinceid")
  private Province province;


  public City() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Province getProvince() {
    return this.province;
  }

  public void setProvince(Province province) {
    this.province = province;
  }


}