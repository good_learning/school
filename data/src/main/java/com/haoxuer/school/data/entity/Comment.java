package com.haoxuer.school.data.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * 评论表
 */
@Entity
@Table(name = "comments")
public class Comment implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 评论时间
   */
  @Column(nullable = false)
  private Timestamp commentdate;
  /**
   * 评论内容
   */
  @Column(length = 1000)
  private String content;

  /**
   * 回复用户
   */
  @ManyToOne
  @JoinColumn(name = "commentuserid")
  private Member member1;


  /**
   * 评论用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member2;

  public Comment() {
  }

  public Timestamp getCommentdate() {
    return this.commentdate;
  }

  public String getContent() {
    return this.content;
  }

  public int getId() {
    return this.id;
  }

  public Member getMember1() {
    return this.member1;
  }

  public Member getMember2() {
    return this.member2;
  }

  public void setCommentdate(Timestamp commentdate) {
    this.commentdate = commentdate;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setMember1(Member member1) {
    this.member1 = member1;
  }

  public void setMember2(Member member2) {
    this.member2 = member2;
  }

}