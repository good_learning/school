package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 投诉
 *
 * @author 年高
 */
@Entity
@Table(name = "complaint")
public class Complaint implements Serializable {

  /**
   * 数据id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private Integer id;

  /**
   * 投诉人姓名
   */
  private String name;
  @Column(length = 500)
  /**
   * 投诉内容
   */
  private String content;

  /**
   * 投诉人邮箱
   */
  private String email;

  /**
   * 投诉标题
   */
  private String title;
  /**
   * 投诉时间
   */
  private Timestamp adddate;

  public Timestamp getAdddate() {
    return adddate;
  }

  public String getContent() {
    return content;
  }

  public String getEmail() {
    return email;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getTitle() {
    return title;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
