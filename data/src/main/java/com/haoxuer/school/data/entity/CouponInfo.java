package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 优惠卷
 *
 * @author 年高
 */
@Entity
@Table(name = "coupon")
public class CouponInfo implements Serializable {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private Integer id;

  /**
   * 生成时间
   */
  private Date adddate;
  /**
   * 使用时间
   */
  private Date usedate;
  /**
   * 过期时间
   */
  private Date validdate;
  /**
   * 优惠卷金额
   */
  private float money;
  /**
   * 优惠卷生成的管理员
   */
  @JoinColumn(name = "adduserid")
  @ManyToOne()
  private Member addmember;

  /**
   * 优惠卷生成的拥有者
   */
  @JoinColumn(name = "userid")
  @ManyToOne()
  private Member usemember;
  /**
   * 优惠卷名称
   */
  private String name;
  /**
   * 优惠卷状态 0为未使用1为使用过了
   */
  private Integer state;

  public Integer getState() {
    Date now = new Date();
    if (validdate.before(now)) {
      return 2;
    } else {
      return state;
    }
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getAdddate() {
    return adddate;
  }

  public void setAdddate(Date adddate) {
    this.adddate = adddate;
  }

  public Date getUsedate() {
    return usedate;
  }

  public void setUsedate(Date usedate) {
    this.usedate = usedate;
  }

  public Date getValiddate() {
    return validdate;
  }

  public void setValiddate(Date validdate) {
    this.validdate = validdate;
  }

  public float getMoney() {
    return money;
  }

  public void setMoney(float money) {
    this.money = money;
  }

  public Member getAddmember() {
    return addmember;
  }

  public void setAddmember(Member addmember) {
    this.addmember = addmember;
  }

  public Member getUsemember() {
    return usemember;
  }

  public void setUsemember(Member usemember) {
    this.usemember = usemember;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
