package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

/**
 * 课程表
 */
@Entity
@Table(name = "bs_course")
@NamedQuery(name = "Course.findAll", query = "SELECT c FROM Course c")
public class Course implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * 课程编号
   */
  @Column(length = 100)
  private String code;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
   * 授课最小年龄
   */
  private int minage;
  /**
   * 授课最大年龄
   */
  private int maxage;
  /**
   * 折扣
   */
  private Float discount;
  /**
   * 开始时间
   */
  private Timestamp startDate;

  /**
   * 是否是特价 0为不是 1为是特价
   */
  private Integer specialstate;
  /**
   * 评论次数
   */
  private Integer commentsize;

  /**
   * 授课开始时间
   */
  private String timeofdaystart;
  /**
   * 授课结束时间
   */
  private String timeofdayend;
  /**
   * 授课开始周几
   */
  private String weekofdaystart;
  /**
   * 授课结束周几
   */
  private String weekofdayend;
  /**
   * 授课方式
   */
  private String shoukefanshi;
  /**
   * 开课时间
   */
  @Column()
  private Timestamp begintime;

  /**
   * 课程引用的课程
   */
  @JoinColumn(name = "courseid")
  @ManyToOne
  private Course course;

  /**
   * 课程描述内容
   */
  @Column()
  @Type(type = "text")
  private String content;

  /**
   * 授课模式
   */
  private String shoukemoshi;
  /**
   * 课程特点
   */
  @Column(length = 1000)
  private String coursefeature;
  /**
   * 课程到期时间
   */
  @Column()
  private Timestamp duedate;
  /**
   * 课程删除时间
   */
  private Timestamp deletedate;
  /**
   * 课程添加时间
   */
  private Timestamp adddate;
  /**
   * 课程图片路径
   */
  @Column(length = 100)
  private String imgpath;
  /**
   * 上课地址
   */
  @Column(length = 100)
  private String address;
  /**
   * 用户类型  暂时无效第一版时候的字段
   */
  @Column(length = 20)
  private String usertype;

  /**
   * 课程状态 1为正常 100为删除 1000为停止
   */
  @Column
  @ColumnDefault(value = "1")
  private Integer statetype;

  /**
   * 课程状态 0为未开始 1为开始了2为结束了
   */
  @Column
  @ColumnDefault(value = "0")
  private Integer state;

  /**
   * 课程状态 0为正常 1为停止
   */
  @Column
  @ColumnDefault(value = "0")
  private Integer stopstate;

  /**
   * 已经招生的人数
   */
  private Integer jointimes;

  /**
   * 所有浏览量
   */
  private Integer views;

  /**
   * 每周浏览量 每周一0点清零
   */
  @Column
  @ColumnDefault(value = "0")
  private Integer viewsofweek;

  /**
   * 课程价格
   */
  private float money;
  /**
   * 课程名称
   */
  @Column(length = 50)
  private String name;
  /**
   * 课程实际价格
   */
  private float realprice;
  /**
   * 课程时长  暂时无效
   */
  private int timelength;
  /**
   * 课程可以招人数量
   */
  private int times;
  /**
   * 课程确认上课课时次数
   */
  @Column
  @ColumnDefault("0")
  private Integer confirmlesson;
  /**
   * 课程课时数量
   */
  private int lesson;
  /**
   * 课程是否被推荐  0为未推荐  1为推荐
   */
  private Integer isgood;
  /**
   * 课程点赞次数
   */
  private Integer ups;


  /**
   * 课程视频地址 第一版字段  暂时无效
   */
  @Column(length = 100)
  private String videopath;

  /**
   * 课程分类
   */
  @ManyToOne
  @JoinColumn(name = "catalog_id")
  private CourseCatalog courseCatalog;

  /**
   * 课程发布者
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;


  /**
   * 课程授课时候的老师
   */
  @ManyToOne
  @JoinColumn(name = "teacherid")
  private Member teacher;


  /**
   * 课程针对的区
   */
  @ManyToOne
  @JoinColumn(name = "townid")
  private Town town;

  // bi-directional many-to-one association to Favcourse
  @OneToMany(mappedBy = "course")
  private List<Favcourse> favcourses;

  // bi-directional many-to-one association to Tradingrecord
  @OneToMany(mappedBy = "course")
  private List<Tradingrecord> tradingrecords;

  public Course() {
  }

  public Favcourse addFavcours(Favcourse favcours) {
    getFavcourses().add(favcours);
    favcours.setCourse(this);

    return favcours;
  }

  public Tradingrecord addTradingrecord(Tradingrecord tradingrecord) {
    getTradingrecords().add(tradingrecord);
    tradingrecord.setCourse(this);

    return tradingrecord;
  }

  public Timestamp getAdddate() {
    return adddate;
  }

  public String getAddress() {
    return address;
  }

  public Timestamp getBegintime() {
    return this.begintime;
  }

  public Integer getCommentsize() {
    return commentsize;
  }

  public Integer getConfirmlesson() {
    return confirmlesson;
  }

  public String getContent() {
    return this.content;
  }

  public Course getCourse() {
    return course;
  }

  public String getCoursefeature() {
    return this.coursefeature;
  }

  public String getCourseState() {
    String resutl = "招生中";
    if (duedate != null) {
      long time = System.currentTimeMillis() - duedate.getTime();
      if (time > 0) {
        resutl = "已到期";
      } else {
        if (jointimes > 0 && jointimes == times) {
          resutl = "已满额";
        } else if (statetype == 100) {
          resutl = "已停止";
        }
      }
    }
    return resutl;
  }

  public CourseCatalog getCourseCatalog() {
    return this.courseCatalog;
  }

  public Timestamp getDeletedate() {
    return deletedate;
  }

  public Float getDiscount() {
    return discount;
  }

  public Timestamp getDuedate() {
    return this.duedate;
  }

  public List<Favcourse> getFavcourses() {
    return this.favcourses;
  }

  public int getId() {
    return this.id;
  }

  public String getImgpath() {
    return this.imgpath;
  }

  public Integer getIsgood() {
    return isgood;
  }

  public int getJointimes() {
    return this.jointimes;
  }

  public int getLesson() {
    return lesson;
  }

  public int getMaxage() {
    return maxage;
  }

  public Member getMember() {
    return this.member;
  }

  public int getMinage() {
    return minage;
  }

  public float getMoney() {
    return this.money;
  }

  public String getName() {
    return this.name;
  }

  public float getRealprice() {
    return this.realprice;
  }

  public String getShoukefanshi() {
    return shoukefanshi;
  }

  public String getShoukemoshi() {
    return shoukemoshi;
  }

  public Integer getSpecialstate() {
    return specialstate;
  }

  public Timestamp getStartDate() {
    return startDate;
  }

  public Integer getState() {
    return state;
  }

  public Integer getStatetype() {
    return statetype;
  }

  public Integer getStopstate() {
    return stopstate;
  }

  public Member getTeacher() {
    return teacher;
  }

  public int getTimelength() {
    return this.timelength;
  }

  public String getTimeofdayend() {
    return timeofdayend;
  }

  public String getTimeofdaystart() {
    return timeofdaystart;
  }

  public int getTimes() {
    return this.times;
  }

  public Town getTown() {
    return town;
  }

  public List<Tradingrecord> getTradingrecords() {
    return this.tradingrecords;
  }

  public Integer getUps() {
    return ups;
  }

  public String getUsertype() {
    return usertype;
  }

  public String getVideopath() {
    return this.videopath;
  }

  public Integer getViews() {
    return views;
  }

  public Integer getViewsofweek() {
    return viewsofweek;
  }

  public String getWeekofdayend() {
    return weekofdayend;
  }

  public String getWeekofdaystart() {
    return weekofdaystart;
  }

  public Favcourse removeFavcours(Favcourse favcours) {
    getFavcourses().remove(favcours);
    favcours.setCourse(null);

    return favcours;
  }

  public Tradingrecord removeTradingrecord(Tradingrecord tradingrecord) {
    getTradingrecords().remove(tradingrecord);
    tradingrecord.setCourse(null);

    return tradingrecord;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setBegintime(Timestamp begintime) {
    this.begintime = begintime;
  }

  public void setCommentsize(Integer commentsize) {
    this.commentsize = commentsize;
  }

  public void setConfirmlesson(Integer confirmlesson) {
    this.confirmlesson = confirmlesson;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setCoursefeature(String coursefeature) {
    this.coursefeature = coursefeature;
  }

  public void setCourseCatalog(CourseCatalog courseCatalog) {
    this.courseCatalog = courseCatalog;
  }

  public void setDeletedate(Timestamp deletedate) {
    this.deletedate = deletedate;
  }

  public void setDiscount(Float discount) {
    this.discount = discount;
  }

  public void setDuedate(Timestamp duedate) {
    this.duedate = duedate;
  }

  public void setFavcourses(List<Favcourse> favcourses) {
    this.favcourses = favcourses;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

  public void setIsgood(Integer isgood) {
    this.isgood = isgood;
  }

  public void setJointimes(Integer jointimes) {
    this.jointimes = jointimes;
  }

  public void setLesson(int lesson) {
    this.lesson = lesson;
  }

  public void setMaxage(int maxage) {
    this.maxage = maxage;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMinage(int minage) {
    this.minage = minage;
  }

  public void setMoney(float money) {
    this.money = money;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setRealprice(float realprice) {
    this.realprice = realprice;
  }

  public void setShoukefanshi(String shoukefanshi) {
    this.shoukefanshi = shoukefanshi;
  }

  public void setShoukemoshi(String shoukemoshi) {
    this.shoukemoshi = shoukemoshi;
  }

  public void setSpecialstate(Integer specialstate) {
    this.specialstate = specialstate;
  }

  public void setStartDate(Timestamp startDate) {
    this.startDate = startDate;
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public void setStatetype(Integer statetype) {
    this.statetype = statetype;
  }

  public void setStopstate(Integer stopstate) {
    this.stopstate = stopstate;
  }

  public void setTeacher(Member teacher) {
    this.teacher = teacher;
  }

  public void setTimelength(int timelength) {
    this.timelength = timelength;
  }

  public void setTimeofdayend(String timeofdayend) {
    this.timeofdayend = timeofdayend;
  }

  public void setTimeofdaystart(String timeofdaystart) {
    this.timeofdaystart = timeofdaystart;
  }

  public void setTimes(int times) {
    this.times = times;
  }

  public void setTown(Town town) {
    this.town = town;
  }

  public void setTradingrecords(List<Tradingrecord> tradingrecords) {
    this.tradingrecords = tradingrecords;
  }

  public void setUps(Integer ups) {
    this.ups = ups;
  }

  public void setUsertype(String usertype) {
    this.usertype = usertype;
  }

  public void setVideopath(String videopath) {
    this.videopath = videopath;
  }

  public void setViews(Integer views) {
    this.views = views;
  }

  public void setViewsofweek(Integer viewsofweek) {
    this.viewsofweek = viewsofweek;
  }

  public void setWeekofdayend(String weekofdayend) {
    this.weekofdayend = weekofdayend;
  }

  public void setWeekofdaystart(String weekofdaystart) {
    this.weekofdaystart = weekofdaystart;
  }

}