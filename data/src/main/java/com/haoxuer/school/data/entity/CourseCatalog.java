package com.haoxuer.school.data.entity;

import com.haoxuer.discover.data.annotations.FormAnnotation;
import com.haoxuer.discover.data.entity.CatalogEntity;

import javax.persistence.*;
import java.util.List;


/**
 * 课程分类
 */
@FormAnnotation(list = "课程分类",menu = "1,41,54")
@Entity
@Table(name = "bs_course_catalog")
public class CourseCatalog extends CatalogEntity {


  @JoinColumn(name = "pid")
  @ManyToOne(fetch = FetchType.LAZY)
  private CourseCatalog parent;

  //bi-directional many-to-one association to Course
  @OneToMany(mappedBy = "courseCatalog")
  private List<Course> courses;


  @Override
  public Integer getParentId() {
    if (parent != null) {
      return parent.getId();
    }
    return null;
  }

  public CourseCatalog getParent() {
    return parent;
  }

  public void setParent(CourseCatalog parent) {
    this.parent = parent;
  }

  public List<Course> getCourses() {
    return courses;
  }

  public void setCourses(List<Course> courses) {
    this.courses = courses;
  }
}