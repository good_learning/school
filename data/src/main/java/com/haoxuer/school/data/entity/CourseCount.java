package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

/**
 * 课程访问统计
 *
 * @author 年高
 */
@Entity
@Table(name = "bs_course_count")
public class CourseCount implements Serializable {


  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "increment")
  @GenericGenerator(name = "increment", strategy = "increment")
  private long id;


  /**
   * 访问的年
   */
  private int yearcount;
  /**
   * 访问的周
   */
  private int weekcount;
  /**
   * 访问的数量
   */
  @ColumnDefault(value = "0")
  private Integer countnum;
  /**
   * 访问的课程
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "courseid")
  private Course course;


  public Integer getCountnum() {
    return countnum;
  }


  public Course getCourse() {
    return course;
  }


  public long getId() {
    return id;
  }


  public int getWeekcount() {
    return weekcount;
  }


  public int getYearcount() {
    return yearcount;
  }


  public void setCountnum(Integer countnum) {
    this.countnum = countnum;
  }


  public void setCourse(Course course) {
    this.course = course;
  }


  public void setId(long id) {
    this.id = id;
  }


  public void setWeekcount(int weekcount) {
    this.weekcount = weekcount;
  }


  public void setYearcount(int yearcount) {
    this.yearcount = yearcount;
  }


}
