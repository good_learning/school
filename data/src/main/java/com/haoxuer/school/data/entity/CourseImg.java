package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * 课程图片集
 *
 * @author 年高
 */
@Entity
@Table(name = "bs_course_img")
public class CourseImg {


  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 文件路径
   */
  private String fileurl;

  /**
   * 文件后缀名
   */
  private String filetype;

  /**
   * 文件分类
   */
  private int catalog;

  /**
   * 文件分类名称
   */
  private String name;

  /**
   * 文件添加
   */
  private Timestamp adddate;


  /**
   * 文件上传者
   */
  @ManyToOne
  @JoinColumn(name = "memeberid")
  private Member member;
  /**
   * 文件对应的课程
   */
  @ManyToOne
  @JoinColumn(name = "courseid")
  private Course course;

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public Timestamp getAdddate() {
    return adddate;
  }

  public int getCatalog() {
    return catalog;
  }

  public String getFiletype() {
    return filetype;
  }

  public String getFileurl() {
    return fileurl;
  }

  public long getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public String getName() {
    return name;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setCatalog(int catalog) {
    this.catalog = catalog;
  }

  public void setFiletype(String filetype) {
    this.filetype = filetype;
  }

  public void setFileurl(String fileurl) {
    this.fileurl = fileurl;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setName(String name) {
    this.name = name;
  }
}
