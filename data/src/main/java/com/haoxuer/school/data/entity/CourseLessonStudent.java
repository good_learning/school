package com.haoxuer.school.data.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

/**
 * 学生授课表
 *
 * @author 年高
 */
@Entity
@Table(name = "bs_course_lesson_student")
public class CourseLessonStudent {

  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;


  /**
   * 上课学生
   */
  @JoinColumn(name = "memberid")
  @ManyToOne()
  private Member member;
  /**
   * 老师提醒学生的时间 24内提醒一次
   */
  private Date reminderDate;
  /**
   * 学生上课状态信息
   */
  private String stateinfo;

  /**
   * 老师邀请学生的时间 24内提醒一次
   */
  private Date inviteDate;
  /**
   * 学生上课的课程
   */
  @JoinColumn(name = "courseid")
  @ManyToOne()
  private Course course;
  /**
   * 学生的购买记录
   */
  @JoinColumn(name = "tradingrecordid")
  @ManyToOne()
  private Tradingrecord tradingrecord;

  /**
   * 第几课时
   */
  private int period;

  /**
   * 授课状态 1未开始 2 已完成
   */
  private int state;
  /**
   * 评论状态
   */
  private int commentstate;

  /**
   * 老师是否确认课时状态0为未开始1为已完成
   */
  @ColumnDefault(value = "0")
  private int teacherstate;

  /**
   * 授课时间
   */
  private Timestamp teachingtime;

  /**
   * 学生确认时间
   */
  private Timestamp confirmtime;

  public int getCommentstate() {
    return commentstate;
  }

  public Timestamp getConfirmtime() {
    return confirmtime;
  }

  public Course getCourse() {
    return course;
  }

  public long getId() {
    return id;
  }

  public boolean getInvite() {
    boolean result = true;
    if (inviteDate != null) {
      Date now = new Date();
      if (now.before(inviteDate)) {
        result = false;
      }
    }
    return result;
  }

  public Date getInviteDate() {
    return inviteDate;
  }

  public Member getMember() {
    return member;
  }

  public int getPeriod() {
    return period;
  }

  public boolean getReminder() {
    boolean result = true;
    if (reminderDate != null) {
      Date now = new Date();
      if (now.before(reminderDate)) {
        result = false;
      }
    }
    return result;
  }

  public Date getReminderDate() {
    return reminderDate;
  }

  public int getState() {
    return state;
  }

  public String getStateinfo() {
    return stateinfo;
  }

  public int getTeacherstate() {
    return teacherstate;
  }

  public Timestamp getTeachingtime() {
    return teachingtime;
  }

  public Tradingrecord getTradingrecord() {
    return tradingrecord;
  }

  public void setCommentstate(int commentstate) {
    this.commentstate = commentstate;
  }

  public void setConfirmtime(Timestamp confirmtime) {
    this.confirmtime = confirmtime;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setInviteDate(Date inviteDate) {
    this.inviteDate = inviteDate;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setPeriod(int period) {
    this.period = period;
  }

  public void setReminderDate(Date reminderDate) {
    this.reminderDate = reminderDate;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setStateinfo(String stateinfo) {
    this.stateinfo = stateinfo;
  }

  public void setTeacherstate(int teacherstate) {
    this.teacherstate = teacherstate;
  }

  public void setTeachingtime(Timestamp teachingtime) {
    this.teachingtime = teachingtime;
  }

  public void setTradingrecord(Tradingrecord tradingrecord) {
    this.tradingrecord = tradingrecord;
  }

}
