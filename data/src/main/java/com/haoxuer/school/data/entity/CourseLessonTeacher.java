package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

/**
 * 老师对学生上课的评价
 *
 * @author 年高
 */
@Entity
@Table(name = "bs_course_lesson_teacher")
public class CourseLessonTeacher {

  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;


  /**
   * 评论老师
   */
  @JoinColumn(name = "memberid")
  @ManyToOne()
  private Member member;

  /**
   * 评论课程
   */
  @JoinColumn(name = "courseid")
  @ManyToOne()
  private Course course;
  /**
   * 第几课时
   */
  private int period;
  /**
   * 授课状态0未开始1 已完成
   */
  private int state;
  /**
   * 评论状态
   */
  private int commentstate;

  /**
   * 评论条数
   */
  @ColumnDefault(value = "0")
  private int commentsize;


  /**
   * 确认数
   */
  @ColumnDefault(value = "0")
  private int confirmsize;
  /**
   * 授课时间
   */
  private Timestamp teachingtime;

  /**
   * 老师确认时间
   */
  private Timestamp confirmtime;

  public int getCommentsize() {
    return commentsize;
  }

  public int getCommentstate() {
    return commentstate;
  }

  public int getConfirmsize() {
    return confirmsize;
  }

  public Timestamp getConfirmtime() {
    return confirmtime;
  }

  public Course getCourse() {
    return course;
  }

  public long getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public int getPeriod() {
    return period;
  }

  public int getState() {
    return state;
  }

  public Timestamp getTeachingtime() {
    return teachingtime;
  }

  public void setCommentsize(int commentsize) {
    this.commentsize = commentsize;
  }

  public void setCommentstate(int commentstate) {
    this.commentstate = commentstate;
  }

  public void setConfirmsize(int confirmsize) {
    this.confirmsize = confirmsize;
  }

  public void setConfirmtime(Timestamp confirmtime) {
    this.confirmtime = confirmtime;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setPeriod(int period) {
    this.period = period;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setTeachingtime(Timestamp teachingtime) {
    this.teachingtime = teachingtime;
  }

}
