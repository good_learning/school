package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

/**
 * 课程预定
 *
 * @author 年高
 */
@Entity
@Table(name = "bs_course_subscribe")
public class CourseSubscribe implements Serializable {

  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;
  /**
   * 编号
   */
  private String code;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
   * 1为报名 2为预约
   */
  private int state;

  /**
   * 1为正常 100为删除
   */
  private int statetype;
  /**
   * 0为未处理 1为处理
   */
  @ColumnDefault(value = "0")
  private int handlestate;
  /**
   * 留言
   */
  @Column(length = 1200)
  private String content;
  /**
   * 老师回复
   */
  @Column(length = 1200)
  private String msg;

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  /**
   * 预定人姓名
   */
  private String name;
  /**
   * 预定人身份证号码
   */
  private String idcard;
  /**
   * 预定人联系电话
   */
  private String phonenum;
  /**
   * 预定人开始时间
   */
  @Column(length = 50)
  private String mintime;
  /**
   * 预定人结束时间
   */
  @Column(length = 50)
  private String maxtime;

  public String getGodate() {
    return godate;
  }

  public void setGodate(String godate) {
    this.godate = godate;
  }

  /**
   * 预定人
   */
  @Column(length = 50)
  private String users;

  /**
   * 预定人来上课的时间
   */
  @Column(length = 50)
  private String godate;

  /**
   * 预定课程
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "courseid")
  private Course course;

  /**
   * 预定在网站的用户
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "userid")
  private Member member;

  /**
   * 立即报名的时候使用的优惠卷
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "couponid")
  private CouponInfo couponInfo;

  /**
   * 立即报名的时候购买记录
   */
  @OneToOne
  @JoinColumn(name = "tradingrecordid")
  private Tradingrecord tradingrecord;

  /**
   * 预定时间
   */
  private Timestamp adddate;

  public Timestamp getAdddate() {
    return adddate;
  }

  public String getContent() {
    return content;
  }

  public CouponInfo getCouponInfo() {
    return couponInfo;
  }

  public Course getCourse() {
    return course;
  }

  public int getHandlestate() {
    return handlestate;
  }

  public long getId() {
    return id;
  }

  public String getIdcard() {
    return idcard;
  }

  public String getMaxtime() {
    return maxtime;
  }

  public Member getMember() {
    return member;
  }

  public String getMintime() {
    return mintime;
  }

  public String getName() {
    return name;
  }

  public String getPhonenum() {
    return phonenum;
  }

  public int getState() {
    return state;
  }

  public int getStatetype() {
    return statetype;
  }

  public Tradingrecord getTradingrecord() {
    return tradingrecord;
  }

  public String getUsers() {
    return users;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setCouponInfo(CouponInfo couponInfo) {
    this.couponInfo = couponInfo;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setHandlestate(int handlestate) {
    this.handlestate = handlestate;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setIdcard(String idcard) {
    this.idcard = idcard;
  }

  public void setMaxtime(String maxtime) {
    this.maxtime = maxtime;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMintime(String mintime) {
    this.mintime = mintime;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPhonenum(String phonenum) {
    this.phonenum = phonenum;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setStatetype(int statetype) {
    this.statetype = statetype;
  }

  public void setTradingrecord(Tradingrecord tradingrecord) {
    this.tradingrecord = tradingrecord;
  }

  public void setUsers(String users) {
    this.users = users;
  }

}
