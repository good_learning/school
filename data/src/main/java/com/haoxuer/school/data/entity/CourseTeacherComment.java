package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;


/**
 * 授课老师对学生的评价
 *
 * @author 年高
 */
@Entity
@Table(name = "bs_course_teacher_comment")
public class CourseTeacherComment implements Serializable {


  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;


  @Column()
  private Timestamp pubdate;


  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;


  @ManyToOne
  @JoinColumn(name = "commentid")
  private CourseTeacherComment courseComment;

  @ManyToOne
  @JoinColumn(name = "courseid")
  private Course course;


  @ManyToOne
  @JoinColumn(name = "lessonid")
  private CourseLessonStudent courseLessonStudent;


  @ColumnDefault(value = "0")
  @Column()
  private Integer ups;


  @Column()
  private Integer classify;


  @Column(length = 1000)
  private String content;


  @ColumnDefault(value = "5")
  private int servicescore;


  @ColumnDefault(value = "5")
  private int descriptionscore;

  @ColumnDefault(value = "5")
  private int teachingscore;

  /**
   * 评价类型1为好评2为中评3为差评
   */
  @ColumnDefault(value = "1")
  private int commenttype;


  public Integer getClassify() {
    return classify;
  }


  public int getCommenttype() {
    return commenttype;
  }


  public String getContent() {
    return content;
  }


  public Course getCourse() {
    return course;
  }


  public CourseTeacherComment getCourseComment() {
    return courseComment;
  }


  public CourseLessonStudent getCourseLessonStudent() {
    return courseLessonStudent;
  }


  public int getDescriptionscore() {
    return descriptionscore;
  }


  public long getId() {
    return id;
  }


  public Member getMember() {
    return member;
  }


  public Timestamp getPubdate() {
    return pubdate;
  }


  public int getServicescore() {
    return servicescore;
  }


  public int getTeachingscore() {
    return teachingscore;
  }


  public Integer getUps() {
    return ups;
  }


  public void setClassify(Integer classify) {
    this.classify = classify;
  }

  public void setCommenttype(int commenttype) {
    this.commenttype = commenttype;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setCourseComment(CourseTeacherComment courseComment) {
    this.courseComment = courseComment;
  }

  public void setCourseLessonStudent(CourseLessonStudent courseLessonStudent) {
    this.courseLessonStudent = courseLessonStudent;
  }


  public void setDescriptionscore(int descriptionscore) {
    this.descriptionscore = descriptionscore;
  }


  public void setId(long id) {
    this.id = id;
  }


  public void setMember(Member member) {
    this.member = member;
  }


  public void setPubdate(Timestamp pubdate) {
    this.pubdate = pubdate;
  }


  public void setServicescore(int servicescore) {
    this.servicescore = servicescore;
  }


  public void setTeachingscore(int teachingscore) {
    this.teachingscore = teachingscore;
  }


  public void setUps(Integer ups) {
    this.ups = ups;
  }
}
