package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * 文章收藏表
 */
@Entity
@Table(name = "favarticle")
public class FavArticle implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 收藏时间
   */
  @Column(nullable = false)
  private Timestamp favdate;


  /**
   * 收藏文章
   */
  @ManyToOne
  @JoinColumn(name = "articleid")
  private Article article;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  public FavArticle() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Timestamp getFavdate() {
    return this.favdate;
  }

  public void setFavdate(Timestamp favdate) {
    this.favdate = favdate;
  }


  public Article getArticle() {
    return article;
  }

  public void setArticle(Article article) {
    this.article = article;
  }

  public Member getMember() {
    return this.member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

}