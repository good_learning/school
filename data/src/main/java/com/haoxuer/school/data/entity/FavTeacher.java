package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * 老师收藏表  ，用户老师收藏和学校收藏
 */
@Entity
@Table(name = "favteacher")
public class FavTeacher implements Serializable {

  /**
   * 数据库自增id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;


  /**
   * 收藏者
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  /**
   * 收藏的老师或者学校
   */
  @ManyToOne
  @JoinColumn(name = "teacherid")
  private Member teacher;


  /**
   * 收藏时间
   */
  @Column(nullable = false)
  private Timestamp favdate;


  public long getId() {
    return id;
  }


  public void setId(long id) {
    this.id = id;
  }


  public Member getMember() {
    return member;
  }


  public void setMember(Member member) {
    this.member = member;
  }


  public Member getTeacher() {
    return teacher;
  }


  public void setTeacher(Member teacher) {
    this.teacher = teacher;
  }


  public Timestamp getFavdate() {
    return favdate;
  }


  public void setFavdate(Timestamp favdate) {
    this.favdate = favdate;
  }
}
