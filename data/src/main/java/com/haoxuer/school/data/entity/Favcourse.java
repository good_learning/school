package com.haoxuer.school.data.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * 课程收藏表
 */
@Entity
@Table(name = "favcourse")
@NamedQuery(name = "Favcourse.findAll", query = "SELECT f FROM Favcourse f")
public class Favcourse implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 课程收藏表id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 收藏时间
   */
  @Column(nullable = false)
  private Timestamp favdate;

  /**
   * 收藏课程
   */
  @ManyToOne
  @JoinColumn(name = "courseid")
  private Course course;


  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  public Favcourse() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Timestamp getFavdate() {
    return this.favdate;
  }

  public void setFavdate(Timestamp favdate) {
    this.favdate = favdate;
  }

  public Course getCourse() {
    return this.course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public Member getMember() {
    return this.member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

}