package com.haoxuer.school.data.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * 收藏的学校  暂时无效
 */
@Entity
@Table(name = "favschool")
@NamedQuery(name = "Favschool.findAll", query = "SELECT f FROM Favschool f")
public class Favschool implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 收藏时间
   */
  @Column(nullable = false)
  private Timestamp favdate;

  /**
   * 收藏者
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member1;

  /**
   * 收藏的学校
   */
  @ManyToOne
  @JoinColumn(name = "schoolid")
  private Member member2;

  public Favschool() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Timestamp getFavdate() {
    return this.favdate;
  }

  public void setFavdate(Timestamp favdate) {
    this.favdate = favdate;
  }

  public Member getMember1() {
    return this.member1;
  }

  public void setMember1(Member member1) {
    this.member1 = member1;
  }

  public Member getMember2() {
    return this.member2;
  }

  public void setMember2(Member member2) {
    this.member2 = member2;
  }

}