package com.haoxuer.school.data.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 收藏观点表
 */
@Entity
@Table(name = "favviewpoint")
@NamedQuery(name = "Favviewpoint.findAll", query = "SELECT f FROM Favviewpoint f")
public class Favviewpoint implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 收藏时间
   */
  @Column(nullable = false)
  private Timestamp favdate;

  /**
   * 收藏者
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  /**
   * 收藏的观点
   */
  @ManyToOne
  @JoinColumn(name = "viewpointid")
  private Viewpoint viewpoint;

  public Favviewpoint() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Timestamp getFavdate() {
    return this.favdate;
  }

  public void setFavdate(Timestamp favdate) {
    this.favdate = favdate;
  }

  public Member getMember() {
    return this.member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Viewpoint getViewpoint() {
    return this.viewpoint;
  }

  public void setViewpoint(Viewpoint viewpoint) {
    this.viewpoint = viewpoint;
  }

}