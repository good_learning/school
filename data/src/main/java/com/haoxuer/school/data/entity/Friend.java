package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * 好友表
 */
@Entity
@Table(name = "friend")
@NamedQuery(name = "Friend.findAll", query = "SELECT f FROM Friend f")
public class Friend implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 好友分类
   */
  @ManyToOne
  @JoinColumn(name = "friendtype")
  private Friendtype friendtypeBean;

  /**
   * 好友
   */
  @ManyToOne
  @JoinColumn(name = "friendid")
  private Member member1;

  /**
   * 用户
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "userid")
  private Member member2;
  /**
   * 添加时间
   */
  private Timestamp adddate;

  public Timestamp getAdddate() {
    return adddate;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public Friend() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Friendtype getFriendtypeBean() {
    return this.friendtypeBean;
  }

  public void setFriendtypeBean(Friendtype friendtypeBean) {
    this.friendtypeBean = friendtypeBean;
  }

  public Member getMember1() {
    return this.member1;
  }

  public void setMember1(Member member1) {
    this.member1 = member1;
  }

  public Member getMember2() {
    return this.member2;
  }

  public void setMember2(Member member2) {
    this.member2 = member2;
  }

}