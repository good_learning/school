package com.haoxuer.school.data.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * 好友分类表
 */
@Entity
@Table(name = "friendtype")
@NamedQuery(name = "Friendtype.findAll", query = "SELECT f FROM Friendtype f")
public class Friendtype implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 分类名称
   */
  @Column(length = 20)
  private String name;
  /**
   * 管理员id
   */
  private int userid;

  //bi-directional many-to-one association to Friend
  @OneToMany(mappedBy = "friendtypeBean")
  private List<Friend> friends;

  public Friendtype() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getUserid() {
    return this.userid;
  }

  public void setUserid(int userid) {
    this.userid = userid;
  }

  public List<Friend> getFriends() {
    return this.friends;
  }

  public void setFriends(List<Friend> friends) {
    this.friends = friends;
  }

  public Friend addFriend(Friend friend) {
    getFriends().add(friend);
    friend.setFriendtypeBean(this);

    return friend;
  }

  public Friend removeFriend(Friend friend) {
    getFriends().remove(friend);
    friend.setFriendtypeBean(null);

    return friend;
  }

}