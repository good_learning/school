package com.haoxuer.school.data.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "keyword")
public class KeyWord {
  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "increment")
  @GenericGenerator(name = "increment", strategy = "increment")
  private int id;


  private int size;


  private String name;

  private Date startime;
  private Date lasttime;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getStartime() {
    return startime;
  }

  public void setStartime(Date startime) {
    this.startime = startime;
  }

  public Date getLasttime() {
    return lasttime;
  }

  public void setLasttime(Date lasttime) {
    this.lasttime = lasttime;
  }

}
