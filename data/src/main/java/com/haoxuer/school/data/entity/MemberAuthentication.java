package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 信息认证表
 *
 * @author 年高
 */
@Entity
@Table(name = "member_authentication")
public class MemberAuthentication implements Serializable {
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private Integer id;

  private String idimg;

  private Integer idstate;


  private String businessimg;

  private Integer businessstate;

  @JoinColumn(name = "memberid")
  @OneToOne
  Member member;


  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getIdimg() {
    return idimg;
  }

  public void setIdimg(String idimg) {
    this.idimg = idimg;
  }

  public Integer getIdstate() {
    return idstate;
  }

  public void setIdstate(Integer idstate) {
    this.idstate = idstate;
  }

  public String getBusinessimg() {
    return businessimg;
  }

  public void setBusinessimg(String businessimg) {
    this.businessimg = businessimg;
  }

  public Integer getBusinessstate() {
    return businessstate;
  }

  public void setBusinessstate(Integer businessstate) {
    this.businessstate = businessstate;
  }

}
