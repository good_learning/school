package com.haoxuer.school.data.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * 用户分类表
 */
@Entity
@Table(name = "membertype")
@NamedQuery(name = "Membertype.findAll", query = "SELECT m FROM MemberType m")
public class MemberType implements Serializable {
  private static final long serialVersionUID = 1L;


  /**
   * 数据id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 分类名称
   */
  @Column(length = 20)
  private String name;

  //bi-directional many-to-one association to Member
  @OneToMany(mappedBy = "memberType")
  private List<Member> members;

  public MemberType() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Member> getMembers() {
    return this.members;
  }

  public void setMembers(List<Member> members) {
    this.members = members;
  }

  public Member addMember(Member member) {
    getMembers().add(member);

    return member;
  }

  public Member removeMember(Member member) {
    getMembers().remove(member);
    return member;
  }

}