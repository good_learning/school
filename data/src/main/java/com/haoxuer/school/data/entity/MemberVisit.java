package com.haoxuer.school.data.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 用户访问表
 *
 * @author 年高
 */
@Entity
@Table(name = "member_visit")
public class MemberVisit {


  @Override
  public String toString() {
    return "MemberVisit [id=" + id + ", member=" + member
        + ", visitMember=" + visitMember + ", size=" + size
        + ", firstDate=" + firstDate + ", lastDate=" + lastDate + "]";
  }

  /**
   * 数据id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 用户
   */
  @JoinColumn(name = "memberid")
  @ManyToOne
  private Member member;


  /**
   * 访问用户
   */
  @JoinColumn(name = "visitmemberid")
  @ManyToOne
  private Member visitMember;
  /**
   * 访问次数
   */
  private int size;
  /**
   * 第一次访问的时间
   */
  private Date firstDate;
  /**
   * 最后一次访问时间
   */
  private Date lastDate;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Member getVisitMember() {
    return visitMember;
  }

  public void setVisitMember(Member visitMember) {
    this.visitMember = visitMember;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public Date getFirstDate() {
    return firstDate;
  }

  public void setFirstDate(Date firstDate) {
    this.firstDate = firstDate;
  }

  public Date getLastDate() {
    return lastDate;
  }

  public void setLastDate(Date lastDate) {
    this.lastDate = lastDate;
  }


}
