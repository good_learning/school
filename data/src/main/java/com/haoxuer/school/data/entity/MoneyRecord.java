package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;

/**
 * 课程付款记录
 */
@Entity
@Table(name = "moneyrecord")
public class MoneyRecord implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 金额
   */
  private float money;
  /**
   * 名称
   */
  @Column(length = 100)
  private String name;
  /**
   * 剩余的钱
   */
  private float leftmoney;

  /**
   * 订单类型 1为支付订单 2为预约订单
   */
  private int ordertype;

  /**
   * 1为代支付2为已经支付了 3为退款
   */
  private int state;
  /**
   * 交易日期
   */
  @Column(nullable = true)
  private Timestamp tradingdate;
  /**
   * 添加日期
   */
  @Column(nullable = false)
  private Timestamp adddate;

  /**
   * 对应的课程
   */
  @ManyToOne
  @JoinColumn(name = "courseid")
  private Course course;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;
  /**
   * 买课程的用户
   */
  @ManyToOne
  @JoinColumn(name = "sellerid")
  private Member seller;

  public MoneyRecord() {
  }

  public Timestamp getAdddate() {
    return adddate;
  }

  public Course getCourse() {
    return this.course;
  }

  public int getId() {
    return this.id;
  }

  public float getLeftmoney() {
    return leftmoney;
  }

  public Member getMember() {
    return this.member;
  }

  public float getMoney() {
    return this.money;
  }

  public String getName() {
    return name;
  }

  public int getOrdertype() {
    return ordertype;
  }

  public Member getSeller() {
    return seller;
  }

  public int getState() {
    return this.state;
  }

  public Timestamp getTradingdate() {
    return this.tradingdate;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setLeftmoney(float leftmoney) {
    this.leftmoney = leftmoney;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMoney(float money) {
    this.money = money;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setOrdertype(int ordertype) {
    this.ordertype = ordertype;
  }

  public void setSeller(Member seller) {
    this.seller = seller;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setTradingdate(Timestamp tradingdate) {
    this.tradingdate = tradingdate;
  }

}