package com.haoxuer.school.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 规则生成表
 */
@Entity
@Table(name = "numgenerate")
public class NumGenerate {


  @Id
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  @Column(unique = true, nullable = false)
  private long id;
  /**
   * 键
   */
  private String keyinfo;

  /**
   * 分类
   */
  private String typeinfo;

  /**
   * 当前数
   */

  private int num;


  public long getId() {
    return id;
  }

  public String getKeyinfo() {
    return keyinfo;
  }

  public int getNum() {
    return num;
  }

  public String getTypeinfo() {
    return typeinfo;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setKeyinfo(String keyinfo) {
    this.keyinfo = keyinfo;
  }

  public void setNum(int num) {
    this.num = num;
  }

  public void setTypeinfo(String typeinfo) {
    this.typeinfo = typeinfo;
  }
}
