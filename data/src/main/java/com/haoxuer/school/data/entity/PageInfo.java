package com.haoxuer.school.data.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 单独页面功能
 *
 * @author 年高
 */
@Entity
@Table(name = "pageInfo")
public class PageInfo {


  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 添加时间
   */
  private Date addDate;
  /**
   * 添加用户
   */
  @ManyToOne
  @JoinColumn(name = "memberid")
  private Member member;
  /**
   * 标题
   */
  private String title;
  /**
   * 内容
   */
  private String content;

  public Date getAddDate() {
    return addDate;
  }

  public String getContent() {
    return content;
  }

  public int getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public String getTitle() {
    return title;
  }

  public void setAddDate(Date addDate) {
    this.addDate = addDate;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
