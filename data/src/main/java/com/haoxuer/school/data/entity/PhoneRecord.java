package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * 电话记录
 */
@Entity
@Table(name = "phonerecord")
public class PhoneRecord {

  /**
   * 数据id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  @ManyToOne
  @JoinColumn(name = "callerid")
  private Member caller;

  public Member getCaller() {
    return caller;
  }

  public void setCaller(Member caller) {
    this.caller = caller;
  }

  /**
   * 打电话的人
   */
  @Column(length = 20)
  private String name;
  /**
   * 打给谁
   */
  private String totel;

  @ManyToOne
  @JoinColumn(name = "courseid")
  private Course course;

  /**
   * 打电话的时间
   */
  private Timestamp adddate;

  /**
   * 打电话的电话号码
   */
  @Column(length = 20)
  private String phonenum;

  public Timestamp getAdddate() {
    return adddate;
  }


  public Course getCourse() {
    return course;
  }


  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }


  public String getPhonenum() {
    return phonenum;
  }


  public String getTotel() {
    return totel;
  }


  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }


  public void setCourse(Course course) {
    this.course = course;
  }


  public void setId(long id) {
    this.id = id;
  }


  public void setName(String name) {
    this.name = name;
  }


  public void setPhonenum(String phonenum) {
    this.phonenum = phonenum;
  }


  public void setTotel(String totel) {
    this.totel = totel;
  }

}
