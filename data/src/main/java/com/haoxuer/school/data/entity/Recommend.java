package com.haoxuer.school.data.entity;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 推荐
 *
 * @author 年高
 */
@Entity
@Table(name = "recommend")
public class Recommend {
  /**
   * 数据id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 推荐标题
   */
  private String title;
  /**
   * 推荐内容
   */
  private String contents;
  /**
   * 推荐对于的url
   */
  private String url;
  /**
   * 推荐对于的图片路径
   */
  private String imgpath;
  /**
   * 推荐的分类
   */
  private Integer recommendtype;

  /**
   * 推荐的排序号
   */
  @ColumnDefault("0")
  private Integer sortnum;


  /**
   * 推荐的图片宽度
   */
  @ColumnDefault("140")
  private Integer imgwidth;


  /**
   * 推荐的图片高度
   */
  @ColumnDefault("55")
  private Integer imgheight;

  /**
   * 添加时间
   */
  private Timestamp adddate;
  @ManyToOne

  @JoinColumn(name = "memeberid")
  private Member member;

  public Timestamp getAdddate() {
    return adddate;
  }

  public String getContents() {
    return contents;
  }

  public long getId() {
    return id;
  }

  public Integer getImgheight() {
    return imgheight;
  }

  public String getImgpath() {
    return imgpath;
  }

  public Integer getImgwidth() {
    return imgwidth;
  }

  public Member getMember() {
    return member;
  }

  public Integer getRecommendtype() {
    return recommendtype;
  }

  public Integer getSortnum() {
    return sortnum;
  }

  public String getTitle() {
    return title;
  }

  public String getUrl() {
    return url;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setContents(String contents) {
    this.contents = contents;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setImgheight(Integer imgheight) {
    this.imgheight = imgheight;
  }

  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

  public void setImgwidth(Integer imgwidth) {
    this.imgwidth = imgwidth;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setRecommendtype(Integer recommendtype) {
    this.recommendtype = recommendtype;
  }

  public void setSortnum(Integer sortnum) {
    this.sortnum = sortnum;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setUrl(String url) {
    this.url = url;
  }


}
