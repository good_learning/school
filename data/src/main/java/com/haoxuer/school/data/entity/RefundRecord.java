package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 退款记录
 */
@Entity
@Table(name = "refundrecord")
public class RefundRecord {
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "memberid")
  private Member member;

  /**
   * 退款时间
   */
  private Timestamp addtime;


  /**
   * 对应的交易记录
   */
  @ManyToOne
  @JoinColumn(name = "tradingrecordid")
  private Tradingrecord tradingrecord;
  /**
   * 上传证明图片
   */
  private String imgpath;
  /**
   * 退款金额
   */
  private float money;

  /**
   * 退款原因
   */
  @Column(length = 200)
  private String reason;

  /**
   * 退款备注
   */
  @Column(length = 500)
  private String demo;

  public Timestamp getAddtime() {
    return addtime;
  }

  public String getDemo() {
    return demo;
  }

  public long getId() {
    return id;
  }

  public String getImgpath() {
    return imgpath;
  }

  public Member getMember() {
    return member;
  }

  public float getMoney() {
    return money;
  }

  public String getReason() {
    return reason;
  }

  public Tradingrecord getTradingrecord() {
    return tradingrecord;
  }

  public void setAddtime(Timestamp addtime) {
    this.addtime = addtime;
  }

  public void setDemo(String demo) {
    this.demo = demo;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMoney(float money) {
    this.money = money;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public void setTradingrecord(Tradingrecord tradingrecord) {
    this.tradingrecord = tradingrecord;
  }


}
