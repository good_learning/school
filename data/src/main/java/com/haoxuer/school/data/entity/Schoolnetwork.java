package com.haoxuer.school.data.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 学校分校
 */
@Entity
@Table(name = "schoolnetwork")
@NamedQuery(name = "Schoolnetwork.findAll", query = "SELECT s FROM Schoolnetwork s")
public class Schoolnetwork implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;


  /**
   * 分校地址
   */
  @Column(length = 100)
  private String address;

  /**
   * 分校纬度
   */
  private float latitude;
  /**
   * 分校经度
   */
  private float longitude;

  /**
   * 分校名称
   */
  @Column(length = 50)
  private String name;

  /**
   * 分校所在城市
   */
  @Column(length = 50)
  private String cityname;
  /**
   * 分校所在区
   */
  @Column(length = 50)
  private String townname;

  /**
   * 分校联系电话
   */
  @Column(length = 50)
  private String phonenum;

  /**
   * 分校所在区
   */
  @ManyToOne
  @JoinColumn(name = "townid")
  private Town town;
  /**
   * 分校gps信息  经纬度
   */
  @Column(length = 50)
  private String gps;

  /**
   * 学校id
   */
  @ManyToOne
  @JoinColumn(name = "schoolid")
  private Member member;

  public Schoolnetwork() {
  }

  public String getAddress() {
    return this.address;
  }

  public String getCityname() {
    return cityname;
  }

  public String getGps() {
    return gps;
  }

  public int getId() {
    return this.id;
  }

  public float getLatitude() {
    return this.latitude;
  }

  public float getLongitude() {
    return this.longitude;
  }

  public Member getMember() {
    return this.member;
  }

  public String getName() {
    return this.name;
  }

  public String getPhonenum() {
    return phonenum;
  }

  public Town getTown() {
    return town;
  }

  public String getTownname() {
    return townname;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setCityname(String cityname) {
    this.cityname = cityname;
  }

  public void setGps(String gps) {
    this.gps = gps;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setLatitude(float latitude) {
    this.latitude = latitude;
  }

  public void setLongitude(float longitude) {
    this.longitude = longitude;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPhonenum(String phonenum) {
    this.phonenum = phonenum;
  }

  public void setTown(Town town) {
    this.town = town;
  }

  public void setTownname(String townname) {
    this.townname = townname;
  }

}