package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

/**
 * 学校申请
 */
@Entity
@Table(name = "schoolteacher")
@NamedQuery(name = "Schoolteacher.findAll", query = "SELECT s FROM Schoolteacher s")
public class Schoolteacher implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 学校的学校
   */
  @ManyToOne
  @JoinColumn(name = "schoolid")
  private Member school;

  /**
   * 学校的老师
   */
  @ManyToOne
  @JoinColumn(name = "teacherid")
  private Member teacher;

  /**
   * 0为申请中 1为通过 2为拒绝
   */
  @Column
  @ColumnDefault(value = "0")
  private int state;

  /**
   * 0为普通1为名师
   */
  @Column
  @ColumnDefault(value = "0")
  private int techerstate;
  /**
   * 申请时间
   */
  private Timestamp addtime;
  /**
   * 学校确认时间
   */
  private Timestamp confirmtime;

  public Schoolteacher() {
  }

  public Timestamp getAddtime() {
    return addtime;
  }

  public Timestamp getConfirmtime() {
    return confirmtime;
  }

  public int getId() {
    return this.id;
  }

  public Member getSchool() {
    return school;
  }

  public int getState() {
    return state;
  }

  public Member getTeacher() {
    return teacher;
  }

  public int getTecherstate() {
    return techerstate;
  }

  public void setAddtime(Timestamp addtime) {
    this.addtime = addtime;
  }

  public void setConfirmtime(Timestamp confirmtime) {
    this.confirmtime = confirmtime;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setSchool(Member school) {
    this.school = school;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setTeacher(Member teacher) {
    this.teacher = teacher;
  }

  public void setTecherstate(int techerstate) {
    this.techerstate = techerstate;
  }

}