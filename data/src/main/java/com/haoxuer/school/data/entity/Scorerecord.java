package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;


/**
 * 积分记录
 */
@Entity
@Table(name = "scorerecord")
@NamedQuery(name = "Scorerecord.findAll", query = "SELECT s FROM Scorerecord s")
public class Scorerecord implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据id
   */
  @Id
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private int id;


  /**
   * 产生的积分
   */
  private float score;
  /**
   * 总积分
   */
  private float leftscore;
  /**
   * 产生积分的时间
   */
  @Column(nullable = false)
  private Timestamp transactiondate;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;
  /**
   * 备注
   */
  @Column(nullable = false, length = 50)
  private String demo;
  /**
   * 摘要
   */
  @Column(nullable = false, length = 50)
  private String summary;

  public Scorerecord() {
  }


  public String getDemo() {
    return demo;
  }

  public int getId() {
    return this.id;
  }

  public float getLeftscore() {
    return leftscore;
  }

  public Member getMember() {
    return this.member;
  }

  public float getScore() {
    return this.score;
  }

  public String getSummary() {
    return summary;
  }

  public Timestamp getTransactiondate() {
    return this.transactiondate;
  }

  public void setDemo(String demo) {
    this.demo = demo;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setLeftscore(float leftscore) {
    this.leftscore = leftscore;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setScore(float score) {
    this.score = score;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setTransactiondate(Timestamp transactiondate) {
    this.transactiondate = transactiondate;
  }

}