package com.haoxuer.school.data.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

/**
 * 系统消息表
 */
@Entity
@Table(name = "systeminfo")
public class SystemInfo {

  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;
  /**
   * 产生时间
   */
  private Timestamp adddate;
  /**
   * 消息内容
   */
  @Column(length = 1000)
  private String content;
  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "memberid")
  private Member member;
  /**
   * 状态 0为正常 100为删除
   */
  @Column
  @ColumnDefault(value = "0")
  private int state;


  /**
   * 阅读状态 0为正常 1阅读
   */
  @Column
  @ColumnDefault(value = "0")
  private int readsate;

  public Timestamp getAdddate() {
    return adddate;
  }

  public String getContent() {
    return content;
  }

  public long getId() {
    return id;
  }

  public Member getMember() {
    return member;
  }

  public int getReadsate() {
    return readsate;
  }

  public int getState() {
    return state;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setReadsate(int readsate) {
    this.readsate = readsate;
  }

  public void setState(int state) {
    this.state = state;
  }
}
