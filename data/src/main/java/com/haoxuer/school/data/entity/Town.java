package com.haoxuer.school.data.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 区表  ；例如 长安区
 */
@Entity
@Table(name = "town")
public class Town implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 名称
   */
  @Column(length = 20)
  private String name;
  /**
   * 区所在城市
   */
  @ManyToOne
  @JoinColumn(name = "cityid")
  private City city;


  public City getCity() {
    return city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  public Town() {
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }
}