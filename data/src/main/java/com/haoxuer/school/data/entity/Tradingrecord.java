package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;

/**
 * 订单类
 */
@Entity
@Table(name = "tradingrecord")
public class Tradingrecord implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  @Column(unique = true, nullable = false)
  private int id;


  /**
   * 对应的订阅
   */
  @OneToOne(mappedBy = "tradingrecord")
  CourseSubscribe courseSubscribe;
  /**
   * 是否使用网银
   */
  private Integer bankstate;

  /**
   * 用户确认了几次
   */
  @Column
  @ColumnDefault(value = "0")
  private int paytime;

  /**
   * 订单价格
   */
  private float money;
  /**
   * 剩余的钱
   */
  private float leftmoney;

  /**
   * 订单类型 1为支付订单 2为预约订单
   */
  private int ordertype;

  @OneToOne(mappedBy = "tradingrecord", fetch = FetchType.LAZY)
  private TradingrecordRefund refund;

  /**
   * 1为代支付2为已经支付了 3为退款
   */
  private int state;

  /**
   * 0为退款中1为同意退款
   */
  private int refundstate;

  /**
   * 0为正常状态 100为删除
   */
  @Column
  @ColumnDefault(value = "0")
  private Integer deletestate;

  /**
   * 交易时间
   */
  @Column(nullable = true)
  private Timestamp tradingdate;
  /**
   * 添加时间
   */
  @Column(nullable = false)
  private Timestamp adddate;

  /**
   * 所对应的课程
   */
  @ManyToOne
  @JoinColumn(name = "courseid")
  private Course course;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  public Tradingrecord() {
  }

  public Timestamp getAdddate() {
    return adddate;
  }

  public Integer getBankstate() {
    return bankstate;
  }

  public Course getCourse() {
    return this.course;
  }

  public CourseSubscribe getCourseSubscribe() {
    return courseSubscribe;
  }

  public Integer getDeletestate() {
    return deletestate;
  }

  public int getId() {
    return this.id;
  }

  public float getLeftmoney() {
    return leftmoney;
  }

  public Member getMember() {
    return this.member;
  }

  public float getMoney() {
    return this.money;
  }

  public int getOrdertype() {
    return ordertype;
  }

  public int getPaytime() {
    return paytime;
  }

  public TradingrecordRefund getRefund() {
    return refund;
  }

  public int getRefundstate() {
    return refundstate;
  }

  public int getState() {
    return this.state;
  }

  public Timestamp getTradingdate() {
    return this.tradingdate;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setBankstate(Integer bankstate) {
    this.bankstate = bankstate;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public void setCourseSubscribe(CourseSubscribe courseSubscribe) {
    this.courseSubscribe = courseSubscribe;
  }

  public void setDeletestate(Integer deletestate) {
    this.deletestate = deletestate;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setLeftmoney(float leftmoney) {
    this.leftmoney = leftmoney;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMoney(float money) {
    this.money = money;
  }

  public void setOrdertype(int ordertype) {
    this.ordertype = ordertype;
  }

  public void setPaytime(int paytime) {
    this.paytime = paytime;
  }

  public void setRefund(TradingrecordRefund refund) {
    this.refund = refund;
  }

  public void setRefundstate(int refundstate) {
    this.refundstate = refundstate;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setTradingdate(Timestamp tradingdate) {
    this.tradingdate = tradingdate;
  }

}