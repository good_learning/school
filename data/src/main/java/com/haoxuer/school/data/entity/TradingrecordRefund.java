package com.haoxuer.school.data.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;

/**
 * 订单类退款
 */
@Entity
@Table(name = "tradingrecord_refund")
public class TradingrecordRefund implements Serializable {
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 0为退款中1为同意退款
   */
  private int refundstate;
  private float money;
  /**
   * 剩余的钱
   */

  /**
   * 订单类型 1为支付订单 2为预约订单
   */
  private int ordertype;

  /**
   * 1为代支付2为已经支付了 3为退款
   */
  private int state;

  /**
   * 0为正常状态
   */
  @Column
  @ColumnDefault(value = "0")
  private Integer deletestate;

  /**
   * 成功退款的时间
   */
  @Column(nullable = true)
  private Timestamp tradingdate;
  /**
   * 提交申请的时间
   */
  @Column(nullable = false)
  private Timestamp adddate;

  /**
   * 对应的订单
   */
  @ManyToOne
  @JoinColumn(name = "tradingrecordid")
  private Tradingrecord tradingrecord;

  /**
   * 退款描述
   */
  private String content;
  /**
   * 退款原因
   */
  private String reason;
  /**
   * 退款分类
   */
  private String typeinfo;
  /**
   * 用户
   */
  @OneToOne
  @JoinColumn(name = "userid")
  private Member member;

  public TradingrecordRefund() {
  }

  public Timestamp getAdddate() {
    return adddate;
  }

  public String getContent() {
    return content;
  }

  public Integer getDeletestate() {
    return deletestate;
  }

  public int getId() {
    return this.id;
  }

  public Member getMember() {
    return this.member;
  }

  public float getMoney() {
    return this.money;
  }

  public int getOrdertype() {
    return ordertype;
  }

  public String getReason() {
    return reason;
  }

  public int getRefundstate() {
    return refundstate;
  }

  public int getState() {
    return this.state;
  }

  public Timestamp getTradingdate() {
    return this.tradingdate;
  }

  public Tradingrecord getTradingrecord() {
    return tradingrecord;
  }

  public String getTypeinfo() {
    return typeinfo;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setDeletestate(Integer deletestate) {
    this.deletestate = deletestate;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setMoney(float money) {
    this.money = money;
  }

  public void setOrdertype(int ordertype) {
    this.ordertype = ordertype;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public void setRefundstate(int refundstate) {
    this.refundstate = refundstate;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setTradingdate(Timestamp tradingdate) {
    this.tradingdate = tradingdate;
  }

  public void setTradingrecord(Tradingrecord tradingrecord) {
    this.tradingrecord = tradingrecord;
  }

  public void setTypeinfo(String typeinfo) {
    this.typeinfo = typeinfo;
  }

}