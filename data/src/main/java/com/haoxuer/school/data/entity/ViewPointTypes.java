package com.haoxuer.school.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 观点投递表
 *
 * @author 年高
 */
@Entity
@Table(name = "viewpoint_types")
public class ViewPointTypes {

  /**
   * 数据库id
   */
  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "identity")
  @GenericGenerator(name = "identity", strategy = "identity")
  private long id;
  /**
   * 对应的观点
   */
  @ManyToOne()
  @JoinColumn(name = "viewpointid")
  private Viewpoint viewpoint;
  /**
   * 投递时间的分类
   */
  private int catalog;

  public int getCatalog() {
    return catalog;
  }

  public long getId() {
    return id;
  }

  public Viewpoint getViewpoint() {
    return viewpoint;
  }

  public void setCatalog(int catalog) {
    this.catalog = catalog;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setViewpoint(Viewpoint viewpoint) {
    this.viewpoint = viewpoint;
  }
}
