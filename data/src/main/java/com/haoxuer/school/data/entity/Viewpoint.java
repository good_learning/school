package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.haoxuer.discover.data.entity.AbstractEntity;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.jsoup.Jsoup;

/**
 * 观点表
 */
@Entity
@Table(name = "viewpoint")
@NamedQuery(name = "Viewpoint.findAll", query = "SELECT v FROM Viewpoint v")
public class Viewpoint extends AbstractEntity {
  private static final long serialVersionUID = 1L;
  /**
   * 摘要
   */
  private String summary;
  /**
   * 对应的图片路径
   */
  private String imgpath;
  /**
   * 是否推荐
   */
  private Integer goodstate;

  /**
   * 后台审核后文章id
   */
  private Integer articeid;
  /**
   * 观点内容
   */
  private String content;
  /**
   * 观点分类
   */
  private Integer catalogs;
  /**
   * 发布时间
   */
  @Column(nullable = false)
  private Timestamp pubdate;
  /**
   * 标题
   */
  @Column(length = 50)
  private String title;
  /**
   * 点赞次数
   */
  private int ups;
  /**
   * 查看次数
   */
  @Column()
  @ColumnDefault(value = "0")
  private Integer views;
  /**
   * 评论次数
   */
  @ColumnDefault(value = "0")
  @Column()
  private Integer comments;

  // bi-directional many-to-one association to Favviewpoint
  @OneToMany(mappedBy = "viewpoint")
  private List<Favviewpoint> favviewpoints;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;
  // bi-directional many-to-one association to Viewpointcomment
  @OneToMany(mappedBy = "viewpoint")
  private List<Viewpointcomment> viewpointcomments;

  public Viewpoint() {
  }

  public Favviewpoint addFavviewpoint(Favviewpoint favviewpoint) {
    getFavviewpoints().add(favviewpoint);
    favviewpoint.setViewpoint(this);

    return favviewpoint;
  }

  public Viewpointcomment addViewpointcomment(
      Viewpointcomment viewpointcomment) {
    getViewpointcomments().add(viewpointcomment);
    viewpointcomment.setViewpoint(this);

    return viewpointcomment;
  }

  public Integer getArticeid() {
    return articeid;
  }

  public Integer getCatalogs() {
    return catalogs;
  }

  public Integer getComments() {
    return comments;
  }

  public String getContent() {
    return this.content;
  }

  public List<Favviewpoint> getFavviewpoints() {
    return this.favviewpoints;
  }

  public Integer getGoodstate() {
    return goodstate;
  }


  public String getImgpath() {
    return imgpath;
  }

  public String getLeftContent() {
    String result = "";
    if (content != null) {
      result = content;
    }
    result = Jsoup.parse(result).text();
    int maxsize = 100;
    if (result.length() > maxsize) {
      result = result.substring(0, maxsize);
    }
    return result;
  }

  public Member getMember() {
    return this.member;
  }

  public Timestamp getPubdate() {
    return this.pubdate;
  }

  public String getSummary() {
    return summary;
  }

  public String getTitle() {
    return this.title;
  }

  public int getUps() {
    return this.ups;
  }

  public List<Viewpointcomment> getViewpointcomments() {
    return this.viewpointcomments;
  }

  public Integer getViews() {
    return views;
  }

  public Favviewpoint removeFavviewpoint(Favviewpoint favviewpoint) {
    getFavviewpoints().remove(favviewpoint);
    favviewpoint.setViewpoint(null);

    return favviewpoint;
  }

  public Viewpointcomment removeViewpointcomment(
      Viewpointcomment viewpointcomment) {
    getViewpointcomments().remove(viewpointcomment);
    viewpointcomment.setViewpoint(null);

    return viewpointcomment;
  }

  public void setArticeid(Integer articeid) {
    this.articeid = articeid;
  }

  public void setCatalogs(Integer catalogs) {
    this.catalogs = catalogs;
  }

  public void setComments(Integer comments) {
    this.comments = comments;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setFavviewpoints(List<Favviewpoint> favviewpoints) {
    this.favviewpoints = favviewpoints;
  }

  public void setGoodstate(Integer goodstate) {
    this.goodstate = goodstate;
  }


  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setPubdate(Timestamp pubdate) {
    this.pubdate = pubdate;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setUps(int ups) {
    this.ups = ups;
  }

  public void setViewpointcomments(List<Viewpointcomment> viewpointcomments) {
    this.viewpointcomments = viewpointcomments;
  }

  public void setViews(Integer views) {
    this.views = views;
  }

}