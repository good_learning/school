package com.haoxuer.school.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * 观点评论
 */
@Entity
@Table(name = "viewpointcomment")
@NamedQuery(name = "Viewpointcomment.findAll", query = "SELECT v FROM Viewpointcomment v")
public class Viewpointcomment implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 数据库id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;
  /**
   * 评论内容
   */
  private String content;

  /**
   * 用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;

  /**
   * 回复的评论
   */
  @ManyToOne
  @JoinColumn(name = "viewpointcommentid")
  private Viewpointcomment viewpointcomment;

  /**
   * 评论的观点
   */
  @ManyToOne
  @JoinColumn(name = "viewpointid")
  private Viewpoint viewpoint;
  /**
   * 发布日期
   */
  @Column
  private Timestamp pubdate;
  /**
   * 点赞次数
   */
  private Integer ups;

  public Viewpointcomment() {
  }

  public String getContent() {
    return this.content;
  }

  public int getId() {
    return this.id;
  }

  public Member getMember() {
    return this.member;
  }

  public Timestamp getPubdate() {
    return pubdate;
  }

  public Integer getUps() {
    return ups;
  }

  public Viewpoint getViewpoint() {
    return this.viewpoint;
  }

  public Viewpointcomment getViewpointcomment() {
    return viewpointcomment;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setPubdate(Timestamp pubdate) {
    this.pubdate = pubdate;
  }

  public void setUps(Integer ups) {
    this.ups = ups;
  }

  public void setViewpoint(Viewpoint viewpoint) {
    this.viewpoint = viewpoint;
  }

  public void setViewpointcomment(Viewpointcomment viewpointcomment) {
    this.viewpointcomment = viewpointcomment;
  }

}