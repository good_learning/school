package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Town;

@Entity
@Table(name = "Activity")
@NamedQuery(name = "Activity.findAll", query = "SELECT a FROM Activity a")
public class Activity implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 活动标题
   */
  @Column(nullable = false, length = 100)
  private String subject;

  /**
   * 活动概述
   */
  @Column(length = 300)
  private String summary;

  /**
   * 活动内容
   */
  @Column(nullable = false)
  @Type(type = "text")
  private String content;

  /**
   * 活动发起人
   */
  @ManyToOne
  @JoinColumn(name = "memeberid")
  private Member member;

  /**
   * 活动时间类型（0：当天结束，1：连续，2：每周，3：自定义）
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int scheduleType = 0;

  /**
   * 活动起始日期
   */
  @Column(nullable = false)
  private Timestamp fromDate;

  /**
   * 活动结束日期
   */
  @Column(nullable = false)
  private Timestamp toDate;


  /**
   * 海报
   */
  @Column(nullable = false)
  private String imgPath;

  /**
   * 所在城市 地区
   */
  @ManyToOne
  @JoinColumn(name = "townid")
  private Town town;

  /**
   * 活动地址
   */
  @Column(length = 100)
  private String address;

  /**
   * 是否免费（0：免费；1：收费）
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int free = 0;


  /**
   * 现场费用
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private BigDecimal cost = BigDecimal.valueOf(0);

  /**
   * 活动人数限制
   */
  @Column(nullable = false)
  @ColumnDefault(value = "9999")
  private int maxSize = 9999;

  /**
   * 活动已参加人数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int memberCount = 0;

  /**
   * 评论人数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int commentCount = 0;

  /**
   * 关注（感兴趣）人数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int followCount = 0;

  /**
   * 访问人数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int visitCount = 0;

  /**
   * 活动状态（0：待审核；1：未通过审核；2：通过审核；）
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int status = 0;

  /**
   * 公交线路
   */
  @Column(length = 200)
  private String lineofbus;

  /**
   * 活动详细计划
   */
  @OneToMany(mappedBy = "activity", fetch = FetchType.LAZY)
  private List<ActivitySchedule> schedules;

  /**
   * 活动评论
   */
  @OneToMany(mappedBy = "activity", fetch = FetchType.LAZY)
  private List<ActivityComment> comments;

  /**
   * 活动参与人员
   */
  @OneToMany(mappedBy = "activity", fetch = FetchType.LAZY)
  private List<ActivityMember> members;


  public Activity() {
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public int getScheduleType() {
    return scheduleType;
  }

  public void setScheduleType(int scheduleType) {
    this.scheduleType = scheduleType;
  }

  public Timestamp getFromDate() {
    return fromDate;
  }

  public void setFromDate(Timestamp fromDate) {
    this.fromDate = fromDate;
  }

  public Timestamp getToDate() {
    return toDate;
  }

  public void setToDate(Timestamp toDate) {
    this.toDate = toDate;
  }


  public String getImgPath() {
    return imgPath;
  }

  public void setImgPath(String imgPath) {
    this.imgPath = imgPath;
  }

  public Town getTown() {
    return town;
  }

  public void setTown(Town town) {
    this.town = town;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public int getFree() {
    return free;
  }

  public void setFree(int free) {
    this.free = free;
  }

  public BigDecimal getCost() {
    return cost;
  }

  public void setCost(BigDecimal cost) {
    this.cost = cost;
  }

  public int getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(int maxSize) {
    this.maxSize = maxSize;
  }

  public int getMemberCount() {
    return memberCount;
  }

  public void setMemberCount(int memberCount) {
    this.memberCount = memberCount;
  }

  public int getCommentCount() {
    return commentCount;
  }

  public void setCommentCount(int commentCount) {
    this.commentCount = commentCount;
  }

  public int getFollowCount() {
    return followCount;
  }

  public void setFollowCount(int followCount) {
    this.followCount = followCount;
  }

  public int getVisitCount() {
    return visitCount;
  }

  public void setVisitCount(int visitCount) {
    this.visitCount = visitCount;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getLineofbus() {
    return lineofbus;
  }

  public void setLineofbus(String lineofbus) {
    this.lineofbus = lineofbus;
  }

  public List<ActivitySchedule> getSchedules() {
    return schedules;
  }

  public void setSchedules(List<ActivitySchedule> schedules) {
    this.schedules = schedules;
  }

  public List<ActivityComment> getComments() {
    return comments;
  }

  public void setComments(List<ActivityComment> comments) {
    this.comments = comments;
  }

  public List<ActivityMember> getMembers() {
    return members;
  }

  public void setMembers(List<ActivityMember> members) {
    this.members = members;
  }
}
