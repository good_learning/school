package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.haoxuer.school.data.entity.Member;

/**
 * 文章评论表
 */
@Entity
@Table(name = "ActivityComment")
@NamedQuery(name = "ActivityComment.findAll", query = "SELECT a FROM ActivityComment a")
public class ActivityComment implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 评论类容
   */
  @Column(nullable = false)
  @Type(type = "text")
  private String content;

  /**
   * 评论的文章
   */
  @ManyToOne
  @JoinColumn(name = "activityid")
  private Activity activity;
  /**
   * 对评论的回复
   */
  @ManyToOne
  @JoinColumn(name = "actcommentid")
  private ActivityComment activityComment;

  /**
   * 评论点赞的次数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private Integer ups = 0;


  /**
   * 发布评论的用户
   */
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;


  /**
   * 评论日期
   */
  @Column()
  private Timestamp pubdate;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Activity getActivity() {
    return activity;
  }

  public void setActivity(Activity activity) {
    this.activity = activity;
  }

  public ActivityComment getActivityComment() {
    return activityComment;
  }

  public void setActivityComment(ActivityComment activityComment) {
    this.activityComment = activityComment;
  }

  public Integer getUps() {
    return ups;
  }

  public void setUps(Integer ups) {
    this.ups = ups;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Timestamp getPubdate() {
    return pubdate;
  }

  public void setPubdate(Timestamp pubdate) {
    this.pubdate = pubdate;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  /**
   * 状态(0:正常;100:删除)
   */
  @ColumnDefault(value = "0")
  private int status;

  public ActivityComment() {
  }

}