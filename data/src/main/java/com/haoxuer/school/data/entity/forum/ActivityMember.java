package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.haoxuer.school.data.entity.Member;

/**
 * 活动参与者
 */
@Entity
@Table(name = "ActivityMember")
@NamedQuery(name = "ActivityMember.findAll", query = "SELECT a FROM ActivityComment a")
public class ActivityMember implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 活动
   */
  @ManyToOne()
  @JoinColumn(name = "activityid")
  private Activity activity;

  /**
   * 参与者
   */
  @ManyToOne()
  @JoinColumn(name = "memberid")
  private Member members;

  /**
   * 在活动中的角色(此角色在字典表中配置)
   */
  private int roleid;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Activity getActivity() {
    return activity;
  }

  public void setActivity(Activity activity) {
    this.activity = activity;
  }

  public Member getMembers() {
    return members;
  }

  public void setMembers(Member members) {
    this.members = members;
  }

  public int getRoleid() {
    return roleid;
  }

  public void setRoleid(int roleid) {
    this.roleid = roleid;
  }
}
