package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 活动计划（每天一条）
 *
 * @author Administrator
 */
@Entity
@Table(name = "ActivitySchedule")
@NamedQuery(name = "ActivitySchedule.findAll", query = "SELECT a FROM ActivitySchedule a")
public class ActivitySchedule implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 活动
   */
  @ManyToOne
  @JoinColumn(name = "activityid")
  private Activity activity;

  /**
   * 活动开始时间，精确到分钟即可
   */
  private Timestamp startTime;

  /**
   * 活动结束时间，精确到分钟即可
   */
  private Timestamp endTime;

  /**
   * 当天内容概述
   */
  @Column(length = 300)
  private String summary;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Activity getActivity() {
    return activity;
  }

  public void setActivity(Activity activity) {
    this.activity = activity;
  }

  public Timestamp getStartTime() {
    return startTime;
  }

  public void setStartTime(Timestamp startTime) {
    this.startTime = startTime;
  }

  public Timestamp getEndTime() {
    return endTime;
  }

  public void setEndTime(Timestamp endTime) {
    this.endTime = endTime;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

}
