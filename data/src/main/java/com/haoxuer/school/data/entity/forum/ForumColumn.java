package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

/**
 * 论坛版块
 */
@Entity
@Table(name = "ForumColumn")
@NamedQuery(name = "ForumColumn.findAll", query = "SELECT a FROM ForumColumn a")
public class ForumColumn implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  /**
   * 数据左节点
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int lft = 0;

  /**
   * 分类名称
   */
  @Column(length = 50, nullable = false)
  private String name;

  /**
   * 数据右节点
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int rgt = 0;

  /**
   * 分类排序号
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int priority = 0;

  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int levelinfo = 0;

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  //bi-directional many-to-one association to Article

  @OneToMany(mappedBy = "forumColumn")
  private List<ForumColumn> forumColumns;

  //bi-directional many-to-one association to Articletype
  @ManyToOne
  @JoinColumn(name = "parent_id")
  private ForumColumn forumColumn;

  @Column(length = 100)
  private String actionUrl;
//	//bi-directional many-to-one association to Articletype
//	@OneToMany(mappedBy="articletype")
//	private List<ForumColumn> articletypes;

  public ForumColumn() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getLft() {
    return lft;
  }

  public void setLft(int lft) {
    this.lft = lft;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRgt() {
    return rgt;
  }

  public void setRgt(int rgt) {
    this.rgt = rgt;
  }

  public List<ForumColumn> getForumColumns() {
    return forumColumns;
  }

  public void setForumColumns(List<ForumColumn> forumColumns) {
    this.forumColumns = forumColumns;
  }

  public ForumColumn getForumColumn() {
    return forumColumn;
  }

  public void setForumColumn(ForumColumn forumColumn) {
    this.forumColumn = forumColumn;
  }

  public String getActionUrl() {
    return actionUrl;
  }

  public void setActionUrl(String actionUrl) {
    this.actionUrl = actionUrl;
  }

  public int getLevelinfo() {
    return levelinfo;
  }

  public void setLevelinfo(int levelinfo) {
    this.levelinfo = levelinfo;
  }

}