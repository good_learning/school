package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;


/**
 * 论坛讨论小组
 */
@Entity
@Table(name = "ForumGroup")
@NamedQuery(name = "ForumGroup.findAll", query = "SELECT a FROM ForumGroup a")
public class ForumGroup implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  @Column(length = 50, nullable = false)
  private String name;

  @ManyToOne
  @JoinColumn(name = "forumcolumnid")
  private ForumColumn forumColumn;


  /**
   * 最大人数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "9999")
  private int maxSize = 9999;

  /**
   * 已加入人数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int memberCount = 0;

  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int sortSeq = 0;

  /**
   * 发帖数量
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int postCount = 0;


  /**
   * 创建时间
   */
  @Column(nullable = false)
  private Timestamp createDate;

  /**
   * 小组状态。0：正常；1；解散
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int status = 0;
  /**
   * 小组描述
   */
  @Type(type = "text")
  private String description;

  /**
   * 头像地址
   */
  private String imgPath;

  /**
   * 临时头像地址
   */
  private String tmpImgPath;

  /**
   * 临时描述（申请修改时使用）
   */
  @Column(length = 300)
  private String tmpDescription;

  @OneToMany(mappedBy = "group")
  private List<ForumGroupMember> groupMembers;

  /**
   * 是否公开
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int openEyed = 0;

  public int getOpenEyed() {
    return openEyed;
  }

  public void setOpenEyed(int openEyed) {
    this.openEyed = openEyed;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ForumColumn getForumColumn() {
    return forumColumn;
  }

  public void setForumColumn(ForumColumn forumColumn) {
    this.forumColumn = forumColumn;
  }

  public int getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(int maxSize) {
    this.maxSize = maxSize;
  }

  public int getSortSeq() {
    return sortSeq;
  }

  public void setSortSeq(int sortSeq) {
    this.sortSeq = sortSeq;
  }

  public int getPostCount() {
    return postCount;
  }

  public void setPostCount(int postCount) {
    this.postCount = postCount;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getMemberCount() {
    return memberCount;
  }

  public void setMemberCount(int memberCount) {
    this.memberCount = memberCount;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getImgPath() {
    return imgPath;
  }

  public void setImgPath(String imgPath) {
    this.imgPath = imgPath;
  }

  public String getTmpImgPath() {
    return tmpImgPath;
  }

  public void setTmpImgPath(String tmpImgPath) {
    this.tmpImgPath = tmpImgPath;
  }

  public String getTmpDescription() {
    return tmpDescription;
  }

  public void setTmpDescription(String tmpDescription) {
    this.tmpDescription = tmpDescription;
  }

  public List<ForumGroupMember> getGroupMembers() {
    return groupMembers;
  }

  public void setGroupMembers(List<ForumGroupMember> groupMembers) {
    this.groupMembers = groupMembers;
  }
}
