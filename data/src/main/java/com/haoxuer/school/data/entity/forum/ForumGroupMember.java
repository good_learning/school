package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.haoxuer.school.data.entity.Member;


/**
 * 论坛讨论小组
 */
@Entity
@Table(name = "ForumGroupMember")
@NamedQuery(name = "ForumGroupMember.findAll", query = "SELECT a FROM ForumGroupMember a")
public class ForumGroupMember implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  @ManyToOne
  @JoinColumn(name = "groupid")
  private ForumGroup group;

  @ManyToOne
  @JoinColumn(name = "memberid")
  private Member member;

  /**
   * 角色。0： 组长； 1：管理员； 100：组员
   */
  @Column(nullable = false)
  @ColumnDefault(value = "100")
  private int role = 100;


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public ForumGroup getGroup() {
    return group;
  }


  public void setGroup(ForumGroup group) {
    this.group = group;
  }


  public Member getMember() {
    return member;
  }


  public void setMember(Member member) {
    this.member = member;
  }


  public int getRole() {
    return role;
  }


  public void setRole(int role) {
    this.role = role;
  }

}
