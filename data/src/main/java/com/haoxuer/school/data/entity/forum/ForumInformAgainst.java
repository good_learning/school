package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 举报
 *
 * @author Administrator
 */
@Entity
@Table(name = "ForumInformAgainst")
@NamedQuery(name = "ForumInformAgainst.findAll", query = "SELECT a FROM ForumInformAgainst a")
public class ForumInformAgainst implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 数据库自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }


  public int getTargetid() {
    return targetid;
  }

  public void setTargetid(int targetid) {
    this.targetid = targetid;
  }

  public String getTargetType() {
    return targetType;
  }

  public void setTargetType(String targetType) {
    this.targetType = targetType;
  }

  /**
   * 被举报的目标id
   */
  @Column(nullable = false)
  private int targetid;

  /**
   * 被举报的目标类型（评论、话题等），此处为类型的Class名，不包含package
   */
  @Column(nullable = false, length = 50)
  private String targetType;


}
