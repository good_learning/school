package com.haoxuer.school.data.entity.forum;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "ForumMessage")
@NamedQuery(name = "ForumMessage.findAll", query = "SELECT a FROM ForumMessage a")
public class ForumMessage implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * 数据自增id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;

  @Column(nullable = false)
  @Type(type = "text")
  private String content;

  /**
   * 被举报的目标id
   */
  @Column(nullable = false)
  private int targetid;

  /**
   * 目标类型（评论、话题等），此处为类型的Class名，不包含package
   */
  @Column(nullable = false, length = 50)
  private String targetType;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getTargetid() {
    return targetid;
  }

  public void setTargetid(int targetid) {
    this.targetid = targetid;
  }

  public String getTargetType() {
    return targetType;
  }

  public void setTargetType(String targetType) {
    this.targetType = targetType;
  }

}
