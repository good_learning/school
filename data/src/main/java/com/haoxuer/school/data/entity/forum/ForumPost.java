package com.haoxuer.school.data.entity.forum;

import com.haoxuer.school.data.entity.Member;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.jsoup.Jsoup;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 有门道文章表
 */
@Entity
@Table(name = "ForumPost")
public class ForumPost implements Serializable {
  /**
   * 去除字符串中头部和尾部所包含的空格（包括:空格(全角，半角)、制表符、换页符等）
   *
   * @param s 需要处理的字符串
   * @return 清除以后的结果
   */
  public static String trim(String s) {
    String result = "";
    if (null != s && !"".equals(s)) {
      result = s.replaceAll("^[　*| *| *|//s*]*", "").replaceAll(
          "[　*| *| *|//s*]*$", "");
    }
    return result;
  }

  private static final long serialVersionUID = 1L;
  /**
   * 文章id，数据库自动增长
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private int id;


  /**
   * 文章内容
   */
  @Column(nullable = false)
  @Type(type = "text")
  private String content;

  /*
   *
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int joinsize = 0;
  /**
   * 文章标题
   */
  @Column(length = 50, nullable = false)
  private String title;
  /**
   * 文章摘要
   */
  @Column(length = 100)
  private String summary;

  /**
   * 文章封面图路径
   */
  private String imgpath;

  /**
   * 文章点赞次数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int ups = 0;

  /**
   * 文章访问次数
   */

  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private Integer views = 0;

  /**
   * 文章评论次数
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private Integer comments = 0;
  /**
   * 是否被推荐0为未推荐1为推荐
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private Integer is_recommend = 0;

  /**
   * 推荐时候的排序号
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private Integer sortrecommend = 0;

  /**
   * 创建时间
   */
  @Column(nullable = false)
  private Timestamp createDate;
  /**
   * 文章发布时间
   */
  @Column(nullable = false)
  private Timestamp pubdate;

  /**
   * 是否通过审核
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int verified = 0;

  /**
   * 所属版块（文章分类）
   */
  @ManyToOne
  @JoinColumn(name = "forumcolumnid")
  private ForumColumn forumColumn;

  /**
   * 所属小组
   */
  @ManyToOne
  @JoinColumn(name = "forumgroupid")
  private ForumGroup forumGroup;

  /**
   * 置顶排序号
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int sortSeq = 0;

  /**
   * 加精
   */
  @Column(nullable = false)
  @ColumnDefault(value = "0")
  private int competitive = 0;

  /**
   * 文章作者
   */
  // bi-directional many-to-one association to Member
  @ManyToOne
  @JoinColumn(name = "userid")
  private Member member;


  // bi-directional many-to-one association to Articlecomment
  @OneToMany(mappedBy = "forumPost", fetch = FetchType.LAZY)
  private List<ForumPostComment> forumPostComments;

  public ForumPost() {
  }

  public List<ForumPostComment> getArticlecomments() {
    return this.forumPostComments;
  }

  public Integer getComments() {
    return comments;
  }

  public String getContent() {
    return this.content;
  }

  public int getId() {
    return this.id;
  }

  public String getImgpath() {
    return imgpath;
  }

  public Integer getIs_recommend() {
    return is_recommend;
  }

  public int getJoinsize() {
    return this.joinsize;
  }

  public String getLeftContent() {
    String result = "";

    if (summary != null && summary.length() > 5) {
      result = summary;
    } else {
      if (content != null) {
        result = content;
      }
      result = Jsoup.parse(result).text();
      result = result.replaceAll("\\s*|\t|\r|\n", "");
      result = result.replaceAll("&nbsp;", "");
      result = result.replaceAll(" ", "");
      result = trim(result);

      int maxsize = 200;
      if (result.length() > maxsize) {
        result = result.substring(0, maxsize);
      }
    }
    result = result.trim();
    return result;

  }

  public Member getMember() {
    return this.member;
  }

  public Timestamp getPubdate() {
    return pubdate;
  }

  public Integer getSortrecommend() {
    return sortrecommend;
  }

  public String getSummary() {
    return summary;
  }

  public String getTitle() {
    return this.title;
  }

  public int getUps() {
    return this.ups;
  }

  public Integer getViews() {
    return views;
  }

  public void setComments(Integer comments) {
    this.comments = comments;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

  public void setIs_recommend(Integer is_recommend) {
    this.is_recommend = is_recommend;
  }

  public void setJoinsize(int joinsize) {
    this.joinsize = joinsize;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public void setPubdate(Timestamp pubdate) {
    this.pubdate = pubdate;
  }

  public void setSortrecommend(Integer sortrecommend) {
    this.sortrecommend = sortrecommend;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setUps(int ups) {
    this.ups = ups;
  }

  public void setViews(Integer views) {
    this.views = views;
  }

  public ForumColumn getForumColumn() {
    return forumColumn;
  }

  public void setForumColumn(ForumColumn forumColumn) {
    this.forumColumn = forumColumn;
  }

  public List<ForumPostComment> getFourmPostComments() {
    return this.forumPostComments;
  }

  public void setFourmPostComments(List<ForumPostComment> forumPostComments) {
    this.forumPostComments = forumPostComments;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public int getVerified() {
    return verified;
  }

  public void setVerified(int verified) {
    this.verified = verified;
  }

  public ForumGroup getForumGroup() {
    return forumGroup;
  }

  public void setForumGroup(ForumGroup forumGroup) {
    this.forumGroup = forumGroup;
  }

  public int getSortSeq() {
    return sortSeq;
  }

  public void setSortSeq(int sortSeq) {
    this.sortSeq = sortSeq;
  }

  public int getCompetitive() {
    return competitive;
  }

  public void setCompetitive(int competitive) {
    this.competitive = competitive;
  }

  public List<ForumPostComment> getForumPostComments() {
    return forumPostComments;
  }

  public void setForumPostComments(List<ForumPostComment> forumPostComments) {
    this.forumPostComments = forumPostComments;
  }

  public String getDaysAgo() {
    long millis = (new Timestamp(System.currentTimeMillis())).getTime()
        - this.pubdate.getTime();
    long days = millis / (60 * 1000 * 60 * 24);
    if (days == 0) {
      return "今天";
    } else if (days == 1) {
      return "昨天";
    } else if (days == 2) {
      return "前天";
    } else if (days >= 3 && days < 7) {
      return "三天前";
    } else if (days >= 7 && days < 14) {
      return "一周前";
    } else {
      return "两周前";
    }
  }
}