package com.haoxuer.school.data.enums;

public enum MemberType {
  student, teacher, school;

  @Override
  public String toString() {
    if (name().equals("student")) {
      return "学生";
    } else if (name().equals("teacher")) {
      return "老师";
    } else if (name().equals("school")) {
      return "学校";
    } else {
      return super.toString();
    }
  }

}
