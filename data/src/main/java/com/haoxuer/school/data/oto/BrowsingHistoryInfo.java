package com.haoxuer.school.data.oto;

import java.io.Serializable;
import java.sql.Timestamp;

public class BrowsingHistoryInfo implements Serializable {


  /**
   *
   */
  private static final long serialVersionUID = 1L;

  private Timestamp browsingdate;

  private String content;

  private String coursefeature;
  private int courseid;

  public int getCourseid() {
    return courseid;
  }


  public void setCourseid(int courseid) {
    this.courseid = courseid;
  }


  private String imgpath;
  private String name;

  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  private float money;

  public float getMoney() {
    return money;
  }


  public void setMoney(float money) {
    this.money = money;
  }


  public Timestamp getBrowsingdate() {
    return browsingdate;
  }


  public void setBrowsingdate(Timestamp browsingdate) {
    this.browsingdate = browsingdate;
  }


  public String getContent() {
    return content;
  }


  public void setContent(String content) {
    this.content = content;
  }


  public String getCoursefeature() {
    return coursefeature;
  }


  public void setCoursefeature(String coursefeature) {
    this.coursefeature = coursefeature;
  }


  public String getImgpath() {
    return imgpath;
  }


  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

}
