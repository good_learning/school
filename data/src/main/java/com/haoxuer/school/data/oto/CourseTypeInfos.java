package com.haoxuer.school.data.oto;

import java.io.Serializable;
import java.util.List;

public class CourseTypeInfos implements Serializable {


  private int level;
  private int level1;
  private int level2;
  private int level3;
  private int level4;

  public int getLevel1() {
    return level1;
  }

  public void setLevel1(int level1) {
    this.level1 = level1;
  }

  public int getLevel2() {
    return level2;
  }

  public void setLevel2(int level2) {
    this.level2 = level2;
  }

  public int getLevel3() {
    return level3;
  }

  public void setLevel3(int level3) {
    this.level3 = level3;
  }

  public int getLevel4() {
    return level4;
  }

  public void setLevel4(int level4) {
    this.level4 = level4;
  }

  private List list1;
  private List list2;
  private List list3;
  private List list4;

  public void setList(List listdata, int cur, int curid) {
    this.list1 = list1;
    if (cur == 2) {
      level1 = curid;
      list1 = listdata;
    }
    if (cur == 3) {
      level2 = curid;
      list2 = listdata;
    }
    if (cur == 4) {
      level3 = curid;
      list3 = listdata;
    }
    if (cur == 5) {
      level4 = curid;
      list4 = listdata;
    }
  }

  public void setNextList(List listdata, int cur) {
    this.list1 = list1;
    if (cur == 1) {
      list2 = listdata;
    }
    if (cur == 2) {
      list3 = listdata;
    }
    if (cur == 3) {
      list4 = listdata;
    }
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public List getList1() {
    return list1;
  }

  public void setList1(List list1) {
    this.list1 = list1;
  }

  public List getList2() {
    return list2;
  }

  public void setList2(List list2) {
    this.list2 = list2;
  }

  public List getList3() {
    return list3;
  }

  public void setList3(List list3) {
    this.list3 = list3;
  }

  public List getList4() {
    if (list4 != null && list4.size() > 0) {
      return list4;
    } else {
      return null;

    }
  }

  public void setList4(List list4) {
    this.list4 = list4;
  }

}
