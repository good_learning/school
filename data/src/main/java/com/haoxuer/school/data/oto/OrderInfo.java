package com.haoxuer.school.data.oto;

import java.io.Serializable;

import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.Tradingrecord;

public class OrderInfo implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -8719249606031890938L;
  private Course course;
  private Tradingrecord record;


  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public Tradingrecord getRecord() {
    return record;
  }

  public void setRecord(Tradingrecord record) {
    this.record = record;
  }
}
