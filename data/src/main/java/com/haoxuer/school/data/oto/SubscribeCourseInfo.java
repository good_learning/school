package com.haoxuer.school.data.oto;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

public class SubscribeCourseInfo implements Serializable {

  private String coursename;
  private String typename;
  private String schoolname;
  private String imgpath;

  public String getImgpath() {
    return imgpath;
  }

  public void setImgpath(String imgpath) {
    this.imgpath = imgpath;
  }

  private Timestamp adddate;
  private BigInteger id;
  private int courseid;

  public String getCoursename() {
    return coursename;
  }

  public void setCoursename(String coursename) {
    this.coursename = coursename;
  }

  public String getTypename() {
    return typename;
  }

  public void setTypename(String typename) {
    this.typename = typename;
  }

  public String getSchoolname() {
    return schoolname;
  }

  public void setSchoolname(String schoolname) {
    this.schoolname = schoolname;
  }

  public Timestamp getAdddate() {
    return adddate;
  }

  public void setAdddate(Timestamp adddate) {
    this.adddate = adddate;
  }

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public int getCourseid() {
    return courseid;
  }

  public void setCourseid(int courseid) {
    this.courseid = courseid;
  }
}
