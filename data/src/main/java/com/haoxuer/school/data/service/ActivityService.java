package com.haoxuer.school.data.service;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.forum.Activity;
import com.haoxuer.school.data.entity.forum.ActivityComment;
import com.haoxuer.school.data.entity.forum.ActivityMember;
import com.haoxuer.school.data.entity.forum.ActivitySchedule;
import com.haoxuer.discover.data.core.Pagination;

public interface ActivityService {
  /**
   * Methods of Activity
   **/
  Activity findActivityById(Integer id);

  Activity save(Activity bean);

  Activity update(Activity bean);

  Activity deleteActivityById(Integer id);

  Activity[] deleteActivityByIds(Integer[] ids);

  Pagination getPageOfActivities(Map<String, Object> params,
                                 List<String> orders, int curpage, int pagesize);

  Activity viewActivity(Integer id);


  /**
   * Methods of Comments
   **/
  ActivityComment save(ActivityComment bean);

  ActivityComment update(ActivityComment bean);

  ActivityComment deleteCommentById(Integer id);

  ActivityComment findCommentById(Integer id);

  Pagination getPageOfComments(Map<String, Object> params,
                               List<String> orders, int curpage, int pagesize);

  /**
   * Methods of Members
   **/
  ActivityMember save(ActivityMember bean);

  ActivityMember update(ActivityMember bean);

  ActivityMember deleteMemberById(Integer id);

  Pagination getPageOfMembers(Map<String, Object> params,
                              List<String> orders, int curpage, int pagesize);

  /**
   * Methods of Schedules
   **/
  ActivitySchedule save(ActivitySchedule bean);

  ActivitySchedule update(ActivitySchedule bean);

  ActivitySchedule deleteScheduleById(Integer id);

  Pagination getPageOfSchedules(Map<String, Object> params,
                                List<String> orders, int curpage, int pagesize);

  /**
   * findMenbers
   **/
  Member getMenber(Activity activity);

  Pagination getMyJoinedActivities(Map<String, Object> params,
                                   List<String> orders, int curpage, int pagesize);

  Pagination getMycommentedActivities(Map<String, Object> params,
                                      List<String> orders, int curpage, int pagesize);
}