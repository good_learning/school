package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.discover.data.core.Pagination;

public interface ArticleService {
  Pagination getPage(int pageNo, int pageSize);

  Article findById(Integer id);

  List<Article> findArticlesById(Integer id);

  List<Article> findArticlesByPid(Integer id, int size, int sorttype);

  Article save(Article bean);

  Article update(Article bean);

  Article deleteById(Integer id);

  Article[] deleteByIds(Integer[] ids);


  Pagination pageByPid(Integer id, int sorttype, int size, int curpage);

  Pagination recommend(int type, int size, int curpage);

  Integer up(int id);
}