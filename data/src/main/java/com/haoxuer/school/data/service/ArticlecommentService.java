package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Articlecomment;
import com.haoxuer.discover.data.core.Pagination;

public interface ArticlecommentService {
  Pagination getPage(int pageNo, int pageSize);

  Articlecomment findById(Integer id);

  Articlecomment save(Articlecomment bean);

  Articlecomment update(Articlecomment bean);

  Articlecomment deleteById(Integer id);

  Articlecomment[] deleteByIds(Integer[] ids);

  Pagination pageByArticleId(int id, int sorttype, int pagesize,
                             int curpage);

  Integer up(int id);

  void executesave(Articlecomment comment);
}