package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.discover.data.core.Pagination;

public interface ArticletypeService {
  Pagination getPage(int pageNo, int pageSize);

  Articletype findById(Integer id);

  List<Articletype> findByParentId(Integer id);

  Articletype save(Articletype bean);

  Articletype update(Articletype bean);

  Articletype deleteById(Integer id);

  Articletype[] deleteByIds(Integer[] ids);


  List<Articletype> findChild(Integer pid);

}