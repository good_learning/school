package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Attachment;
import com.haoxuer.discover.data.core.Pagination;

public interface AttachmentService {
  Pagination getPage(int pageNo, int pageSize);

  Attachment findById(Long id);

  Attachment save(Attachment bean);

  Attachment update(Attachment bean);

  Attachment deleteById(Long id);

  Attachment[] deleteByIds(Long[] ids);

  Pagination pageByUserId(int id, int curpage, int pagesize);

  Pagination pagetypeByUserId(int id, int type, int curpage, int pagesize);

  Pagination pageByUserId(int id, int type, int curpage, int pagesize);

}
