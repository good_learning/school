package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.BankRecord;
import com.haoxuer.discover.data.core.Pagination;

public interface BankRecordService {
  Pagination getPage(int pageNo, int pageSize);

  BankRecord findById(Long id);

  BankRecord save(BankRecord bean);

  BankRecord payok(String orderid, String type);

  BankRecord update(BankRecord bean);

  BankRecord deleteById(Long id);

  BankRecord[] deleteByIds(Long[] ids);

  BankRecord findByRecordId(int id);


  List<BankRecord> backdata(int size);

}