package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Bankbind;
import com.haoxuer.discover.data.core.Pagination;

public interface BankbindService {
  Pagination getPage(int pageNo, int pageSize);

  Bankbind findById(Integer id);

  Bankbind save(Bankbind bean);

  Bankbind update(Bankbind bean);

  Bankbind deleteById(Integer id);

  Bankbind[] deleteByIds(Integer[] ids);

  Pagination pagetByUser(Long id, int curpage, int pagesize);
}