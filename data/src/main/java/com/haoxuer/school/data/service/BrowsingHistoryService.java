package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.BrowsingHistory;
import com.haoxuer.discover.data.core.Pagination;

public interface BrowsingHistoryService {
  Pagination getPage(int pageNo, int pageSize);

  BrowsingHistory findById(Integer id);

  BrowsingHistory save(BrowsingHistory bean);

  BrowsingHistory update(BrowsingHistory bean);

  BrowsingHistory deleteById(Integer id);

  BrowsingHistory[] deleteByIds(Integer[] ids);


  BrowsingHistory saveRecord(int userid, int courseid);

  Pagination getPage(int userid, int pageNo, int pageSize);

}