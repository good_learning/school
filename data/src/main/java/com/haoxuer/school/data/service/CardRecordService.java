package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CardRecord;
import com.haoxuer.discover.data.core.Pagination;

public interface CardRecordService {
  Pagination getPage(int pageNo, int pageSize);

  CardRecord findById(Integer id);

  CardRecord save(CardRecord bean);

  CardRecord update(CardRecord bean);

  CardRecord deleteById(Integer id);

  CardRecord[] deleteByIds(Integer[] ids);
}