package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Card;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.discover.data.core.Pagination;

public interface CardService {
  Pagination getPage(int pageNo, int pageSize);

  Card findById(Long id);

  Card save(Card bean);

  Card update(Card bean);

  Card deleteById(Long id);

  Card[] deleteByIds(Long[] ids);

  String chongzhi(String cardnum, String cardpass, float money, Member member);

  int buildCards(int userid, int cars, float money);

  Pagination page(int curpage, int pagesize);
}