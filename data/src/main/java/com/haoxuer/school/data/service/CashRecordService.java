package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CashRecord;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.discover.data.core.Pagination;

public interface CashRecordService {
  Pagination getPage(int pageNo, int pageSize);

  CashRecord findById(Long id);

  CashRecord save(CashRecord bean);

  CashRecord update(CashRecord bean);

  CashRecord deleteById(Long id);

  String tixian(Integer bankid,
                Float money,
                String password, Member member);

  String tixian(Integer bankid, Float money, Member member);

  CashRecord[] deleteByIds(Long[] ids);

  Pagination pagetByUser(Long id, int curpage, int pagesize);

  Pagination page(int curpage, int pagesize);
}