package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.City;

public interface CityService {

  City add(City city);


  List<City> findByProvince(int provinceid);

}
