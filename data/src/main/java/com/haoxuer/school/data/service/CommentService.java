package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Comment;
import com.haoxuer.discover.data.core.Pagination;

public interface CommentService {
  Pagination getPage(int pageNo, int pageSize);

  Comment findById(Integer id);

  Comment save(Comment bean);

  Comment update(Comment bean);

  Comment deleteById(Integer id);

  Comment[] deleteByIds(Integer[] ids);
}