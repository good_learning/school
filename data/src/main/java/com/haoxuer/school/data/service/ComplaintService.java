package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Complaint;
import com.haoxuer.discover.data.core.Pagination;

public interface ComplaintService {
  Pagination getPage(int pageNo, int pageSize);

  Complaint findById(Integer id);

  Complaint save(Complaint bean);

  Complaint update(Complaint bean);

  Complaint deleteById(Integer id);

  Complaint[] deleteByIds(Integer[] ids);
}