package com.haoxuer.school.data.service;

import java.util.Date;

import com.haoxuer.school.data.entity.CouponInfo;
import com.haoxuer.discover.data.core.Pagination;

public interface CouponService {
  Pagination getPage(int pageNo, int pageSize);

  CouponInfo findById(Integer id);

  CouponInfo save(CouponInfo bean);

  CouponInfo update(CouponInfo bean);

  CouponInfo deleteById(Integer id);

  CouponInfo[] deleteByIds(Integer[] ids);

  int buildCards(int userid, int cars, float money, String name, Date vdate);

  Pagination pageByUser(int id, int curpage, int pagesize);

  Pagination pageByUserNotUser(int id, int curpage, int pagesize);

}