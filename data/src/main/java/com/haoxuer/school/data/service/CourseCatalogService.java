package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CourseCatalog;
import com.haoxuer.school.data.oto.CourseTypeInfos;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2018年08月16日23:12:28.
 */
public interface CourseCatalogService {

  CourseCatalog findById(Integer id);

  CourseCatalog save(CourseCatalog bean);

  CourseCatalog update(CourseCatalog bean);

  CourseCatalog deleteById(Integer id);

  CourseCatalog[] deleteByIds(Integer[] ids);

  Page<CourseCatalog> page(Pageable pageable);

  Page<CourseCatalog> page(Pageable pageable, Object search);

  List<CourseCatalog> findByTops(Integer pid);


  List<CourseCatalog> child(Integer id, Integer max);

  List<CourseCatalog> list(int first, Integer size, List<Filter> filters, List<Order> orders);

  CourseTypeInfos find(int id);
}