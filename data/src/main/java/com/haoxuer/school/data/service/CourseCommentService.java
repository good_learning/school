package com.haoxuer.school.data.service;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.CourseComment;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseCommentService {
  Pagination getPage(int pageNo, int pageSize);

  CourseComment findById(Integer id);

  CourseComment save(CourseComment bean);

  CourseComment comment(CourseComment bean);

  CourseComment update(CourseComment bean);

  CourseComment deleteById(Integer id);

  CourseComment[] deleteByIds(Integer[] ids);


  CourseComment findCourse(int courseid, int lession);

  Pagination pageByType(int courseid, int classify, int curpage, int pagesize);

  Pagination pageByCommentId(int commentid, int classify, int curpage,
                             int pagesize);

  Pagination pageForCourseByType(int id, int type, int curpage,
                                 int pagesize);

  Pagination pageForMemberByType(int userid, int type, int curpage,
                                 int pagesize);

  List<CourseComment> findByParams(Map<String, Object> params,
                                   List<String> orders);
}