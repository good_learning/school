package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CourseCount;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseCountService {
  Pagination getPage(int pageNo, int pageSize);

  CourseCount findById(Integer id);

  CourseCount save(CourseCount bean);

  CourseCount update(CourseCount bean);

  CourseCount deleteById(Integer id);

  CourseCount[] deleteByIds(Integer[] ids);
}