package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CourseImg;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseImgService {
  Pagination getPage(int pageNo, int pageSize);

  CourseImg findById(Long id);

  CourseImg save(CourseImg bean);

  CourseImg update(CourseImg bean);

  CourseImg deleteById(Long id);

  CourseImg[] deleteByIds(Long[] ids);

  Pagination pageByCourse(int courseid, int pageNo, int pageSize);

}