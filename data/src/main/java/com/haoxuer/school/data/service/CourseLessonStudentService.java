package com.haoxuer.school.data.service;

import java.util.Map;

import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseLessonStudentService {
  Pagination getPage(int pageNo, int pageSize);

  CourseLessonStudent findById(Long id);

  CourseLessonStudent save(CourseLessonStudent bean);

  CourseLessonStudent update(CourseLessonStudent bean);

  CourseLessonStudent deleteById(Long id);

  CourseLessonStudent[] deleteByIds(Long[] ids);

  Pagination update(int studentid, int tradingrecordid, int pageNo, int pageSize);

  Pagination findByPeriod(int period, int curseid, int pageNo, int pageSize);

  CourseLessonStudent finshed(int userid, Long id);

  CourseLessonStudent finshed2(int userid, Long id);

  CourseLessonStudent reminder(Long id);

  CourseLessonStudent invitecomments(long id);

  CourseLessonStudent updateInfo(long id, String info);

  Pagination getPagerData(String select, Map<String, Object> params, int pageNo, int pageSize);
}