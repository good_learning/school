package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CourseLessonTeacher;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseLessonTeacherService {
  Pagination getPage(int pageNo, int pageSize);

  CourseLessonTeacher findById(Long id);

  CourseLessonTeacher save(CourseLessonTeacher bean);

  CourseLessonTeacher update(CourseLessonTeacher bean);

  CourseLessonTeacher deleteById(Long id);

  CourseLessonTeacher[] deleteByIds(Long[] ids);

  CourseLessonTeacher finshed(int userid, Long id);

  Pagination findBySchoolId(int schoolid, int curseid, int pageNo, int pageSize);

}