package com.haoxuer.school.data.service;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.Course;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseService {


  Course add(Course Course);

  Course update(Course Course);

  Course update(Course Course, String[] imgurls);

  boolean deleteCourse(Course c);

  List<Course> findbyuser(int uid);

  Pagination pageByType(int uid, int sourtype, int curpage, int pagesize);

  Pagination pageByCount(int typeid, int curpage, int pagesize);

  Course findById(int id);

  Course view(int id);

  List<Course> goodByUser(int id, int size);

  Pagination good(int typeid, int curpage, int size);

  Pagination similar(int courseid, int curpage, int size);

  Pagination pagegood(int curpage, int size);

  Pagination search(String keyword, int curpage, int size);

  Course up(int id);


  Pagination pageByTransactionType(int type, int sourtype, int curpage, int pagesize);

  Pagination pageByUser(int userid, int statetype, int sourtype, int curpage,
                        int pagesize);

  Pagination teaching(int userid, int statetype, int curpage,
                      int pagesize);

  Pagination teachings(int userid, int state, int curpage,
                       int pagesize);

  void add(Course course, String[] imgurls);

  Course setTeacher(int courseid, int teacherid);


  Course updateState(int courseid, int state);

  Course updateState(int courseid, int userid, int state);

  Pagination search(int id, int townid, int typeid, int coursestate,
                    int sorttype, String keyword, int curpage, int pagesize);

  Pagination showmyspecialcourselist(int userid, int statetype, int sorttype,
                                     int curpage, int pagesize);

  Course recommenduser(Integer id);

  Course recommenddeluser(Integer id);

  Pagination teachingsForStar(int id, int i, int curpage, int pagesize);

  Course startCourse(int courseid);

  Pagination starCourses(int id, int i, int curpage, int pagesize);

  Pagination teachingsForTeacher(int id, int i, int curpage,
                                 int pagesize);

  Course startCourseForTeacher(int courseid);

  Pagination search(Map<String, Object> params, String[] orders,
                    String keyword, int curpage, int pagesize);

}
