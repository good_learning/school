package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CourseSubscribe;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseSubscribeService {
  Pagination getPage(int pageNo, int pageSize);

  CourseSubscribe findById(Long id);

  CourseSubscribe save(CourseSubscribe bean);

  CourseSubscribe order(CourseSubscribe bean);

  CourseSubscribe update(CourseSubscribe bean);

  CourseSubscribe deleteById(Long id);

  CourseSubscribe[] deleteByIds(Long[] ids);

  Pagination pageByUserId(int userid, int pageNo, int pageSize);

  Pagination pageByUserForTeacher(int userid, int type, int pageNo, int pageSize);

  Pagination pageByUserForTeacherGood(int userid, int type, int pageNo, int pageSize);

  Pagination pageByUserForRecyclers(int userid, int type, int pageNo, int pageSize);

  Pagination pageByUser(int id, int statetype, int curpage,
                        int pagesize);

  CourseSubscribe delete(long id);

  CourseSubscribe back(long id);

  CourseSubscribe remove(long id);

  CourseSubscribe handle(long id, String msg);

  Pagination pageForRecyclers(int id, int state, int curpage,
                              int pagesize);

}