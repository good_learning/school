package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.CourseTeacherComment;
import com.haoxuer.discover.data.core.Pagination;

public interface CourseTeacherCommentService {
  Pagination getPage(int pageNo, int pageSize);

  CourseTeacherComment findById(Long id);

  CourseTeacherComment save(CourseTeacherComment bean);

  CourseTeacherComment update(CourseTeacherComment bean);

  CourseTeacherComment deleteById(Long id);

  CourseTeacherComment[] deleteByIds(Long[] ids);

  CourseTeacherComment findCourse(int courseid, int lession);

  CourseTeacherComment comment(CourseTeacherComment bean);
}