package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.FavArticle;
import com.haoxuer.discover.data.core.Pagination;

public interface FavArtilceService {
  Pagination getPage(int pageNo, int pageSize);

  FavArticle findById(Integer id);

  FavArticle save(FavArticle bean);

  FavArticle update(FavArticle bean);

  FavArticle deleteById(Integer id);

  FavArticle[] deleteByIds(Integer[] ids);

  int fav(Long userid, int articleid);

  Pagination pageByUserId(Long id, int curpage, int pagesize);
}