package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.FavTeacher;
import com.haoxuer.discover.data.core.Pagination;

public interface FavTeacherService {
  Pagination getPage(int pageNo, int pageSize);

  FavTeacher findById(Integer id);

  FavTeacher save(FavTeacher bean);

  FavTeacher update(FavTeacher bean);

  FavTeacher deleteById(Integer id);

  FavTeacher[] deleteByIds(Integer[] ids);

  int fav(Long userid, int theacherid);

  Pagination pageByUser(Long userid, int type, int pageNo, int pageSize);

}