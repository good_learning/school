package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Favcourse;
import com.haoxuer.discover.data.core.Pagination;

public interface FavcourseService {
  Pagination getPage(int pageNo, int pageSize);

  Favcourse findById(Integer id);

  Favcourse save(Favcourse bean);

  Favcourse update(Favcourse bean);

  Favcourse deleteById(Integer id);

  Favcourse[] deleteByIds(Integer[] ids);

  int fav(Long userid, int courseid);

  Pagination pageByUserId(Long userid, int pageNo, int pageSize);
}