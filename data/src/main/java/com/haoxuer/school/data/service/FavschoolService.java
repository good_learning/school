package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Favschool;
import com.haoxuer.discover.data.core.Pagination;

public interface FavschoolService {
  Pagination getPage(int pageNo, int pageSize);

  Favschool findById(Integer id);

  Favschool save(Favschool bean);

  Favschool update(Favschool bean);

  Favschool deleteById(Integer id);

  Favschool[] deleteByIds(Integer[] ids);
}