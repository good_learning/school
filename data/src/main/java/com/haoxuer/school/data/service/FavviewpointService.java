package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Favviewpoint;
import com.haoxuer.discover.data.core.Pagination;

public interface FavviewpointService {
  Pagination getPage(int pageNo, int pageSize);

  Favviewpoint findById(Long id);

  Favviewpoint save(Favviewpoint bean);

  Favviewpoint update(Favviewpoint bean);

  Favviewpoint deleteById(Long id);

  Favviewpoint[] deleteByIds(Long[] ids);


  int fav(Long userid, Long courseid);

  Pagination pageByUserId(Long userid, int pageNo, int pageSize);
}