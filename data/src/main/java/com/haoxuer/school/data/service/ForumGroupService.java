package com.haoxuer.school.data.service;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.entity.forum.ForumGroupMember;
import com.haoxuer.discover.data.core.Pagination;


public interface ForumGroupService {

  ForumGroup findById(Integer id);

  ForumGroup update(ForumGroup entity);

  ForumGroupMember updateRole(ForumGroupMember entity);

  ForumGroup delete(Integer id);

  ForumGroup[] deleteByIds(Integer[] id);

  ForumGroup save(ForumGroup bean);

  ForumGroupMember save(ForumGroupMember bean);

  /**
   * 移除该小组成员
   *
   * @param groupid
   * @param memberid
   * @return
   */
  ForumGroupMember deleteGroupMember(Integer groupid, Integer memberid);

  /**
   * 向小组内添加成员
   *
   * @param bean
   * @return
   */
  boolean joinMember(ForumGroupMember bean);

  /**
   * 根据角色查找成员
   *
   * @param groupid
   * @return
   */
  List<ForumGroupMember> findByrole(Integer groupid, Integer role);

  /**
   * 根据parentId查询出小组类型
   *
   * @param parentid
   * @return
   */
  List<ForumColumn> findforumcolumnByParentid(Integer parentid);

  /**
   * 查询小组的成员
   *
   * @param group
   * @return
   */
  List<ForumGroupMember> findGroupMember(ForumGroup group);

  /**
   * 查询用户加入的小组
   *
   * @param memberid
   * @param roletype own：创建的小组 ；join：加入的小组
   * @return
   */
  Pagination findGroupByRoletype(Integer memberid, String roletype, List<String> orders,
                                 int curpage, int pagesize);


  Pagination getPage(Map<String, Object> params, List<String> orders,
                     int curpage, int pagesize);

  /**
   * 查询推荐的小组
   *
   * @param userid
   * @param curpage
   * @param pagesize
   * @return
   */
  Pagination getRecommendPage(Long userid, int curpage, int pagesize);

  Pagination getGroupPage(Map<String, Object> params, List<String> orders,
                          int curpage, int pagesize);

  /**
   * 查找组员
   *
   * @author 谷玉伟
   */
  List<ForumGroupMember> findGroupMember(Map<String, Object> params, List<String> orders);

  /**
   * 显示小组信息
   */
  List<ForumGroupMember> groupDtailsDIY(Map<String, Object> mapparams);

  /**
   * 根据当前用户的id和当前组的id查找role
   *
   * @param mapparams
   * @return
   */
  List<ForumGroupMember> findRoleByGMid(Map<String, Object> mapparams);

  /**
   * 是否是改组成员
   *
   * @param userid
   * @param groupid
   * @return
   */
  boolean isMemberOf(Long userid, int groupid);

  Pagination getGroupMemberPage(Map<String, Object> params,
                                List<String> orders, int curpage, int pagesize);

}