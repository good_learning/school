package com.haoxuer.school.data.service;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.school.data.entity.forum.ForumPost;
import com.haoxuer.school.data.entity.forum.ForumPostComment;
import com.haoxuer.discover.data.core.Pagination;

public interface ForumPostService {

  Pagination getPage(Map<String, Object> params, String orders,
                     int pageNo, int pageSize);

  ForumPost findById(Integer id);

  ForumPost save(ForumPost bean);

  ForumPost update(ForumPost bean);

  ForumPost delete(Integer id);

  ForumPost[] delete(Integer[] ids);

  Pagination getForumPostPage(Map<String, Object> params,
                              List<String> orders, int pageNo, int pageSize);

  Pagination getCommentPage(Map<String, Object> params,
                            List<String> orders, int pageNo, int pageSize);

  ForumPost viewForumPost(Integer id);

  ForumPost upForumPost(Integer id);

  ForumColumn saveForumColumn(ForumColumn bean);

  ForumGroup saveForumGroup(ForumGroup bean);

  ForumPostComment saveComment(ForumPostComment bean);

  List<ForumColumn> getForumColumnChild(Integer columnid);

  ForumColumn findForumColumnById(Integer columnid);

  void setTop(int id);

  Pagination getMyParticipation(Map<String, Object> params,
                                List<String> orders, int pageNo, int pageSize);
}