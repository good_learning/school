package com.haoxuer.school.data.service;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.discover.data.core.Pagination;


public interface ForumService {

  ForumColumn findColumnById(Integer id);

  ForumColumn update(ForumColumn entity);

  ForumColumn deleteColumn(Integer id);

  ForumColumn[] deleteColumnByIds(Integer[] id);

  Pagination getPageOfColumns(Map<String, Object> params,
                              List<String> orders, int curpage, int pagesize);
}