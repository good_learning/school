package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.Friend;
import com.haoxuer.discover.data.core.Pagination;

public interface FriendService {
  Pagination getPage(int pageNo, int pageSize);

  Friend findById(Integer id);

  Friend save(Friend bean);

  Friend update(Friend bean);

  Friend deleteById(Integer id);

  Friend deleteById(Integer id, Integer friendid);

  Friend[] deleteByIds(Integer[] ids);

  List<Friend> findByType(Integer uid, int id);

  Pagination pageByType(int userid, int type, int curpage,
                        int pagesize);


  int follows(int userid, int friendid);

  int isfollows(int userid, int friendid);

  int save(int userid, int friendid, int type);
}