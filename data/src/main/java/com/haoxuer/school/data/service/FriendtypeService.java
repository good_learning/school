package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Friendtype;
import com.haoxuer.discover.data.core.Pagination;

public interface FriendtypeService {
  Pagination getPage(int pageNo, int pageSize);

  Friendtype findById(Integer id);

  Friendtype save(Friendtype bean);

  Friendtype update(Friendtype bean);

  Friendtype deleteById(Integer id);

  Friendtype[] deleteByIds(Integer[] ids);
}