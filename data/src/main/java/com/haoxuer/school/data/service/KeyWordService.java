package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.KeyWord;
import com.haoxuer.discover.data.core.Pagination;

public interface KeyWordService {
  Pagination getPage(int pageNo, int pageSize);

  KeyWord findById(Integer id);

  KeyWord save(KeyWord bean);

  KeyWord update(KeyWord bean);

  KeyWord deleteById(Integer id);

  KeyWord[] deleteByIds(Integer[] ids);
}