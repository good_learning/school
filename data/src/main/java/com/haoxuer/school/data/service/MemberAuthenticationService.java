package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.MemberAuthentication;
import com.haoxuer.discover.data.core.Pagination;

public interface MemberAuthenticationService {
  Pagination getPage(int pageNo, int pageSize);

  MemberAuthentication findById(Integer id);

  MemberAuthentication findByUserId(Integer id);

  MemberAuthentication save(MemberAuthentication bean);

  MemberAuthentication update(MemberAuthentication bean);

  MemberAuthentication deleteById(Integer id);

  MemberAuthentication[] deleteByIds(Integer[] ids);
}