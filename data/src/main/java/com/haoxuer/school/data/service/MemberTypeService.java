package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.MemberType;
import com.haoxuer.discover.data.core.Pagination;

public interface MemberTypeService {
  Pagination getPage(int pageNo, int pageSize);

  MemberType findById(Integer id);

  MemberType save(MemberType bean);

  MemberType update(MemberType bean);

  MemberType deleteById(Integer id);

  MemberType[] deleteByIds(Integer[] ids);

  List<MemberType> all();
}