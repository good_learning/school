package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.MemberVisit;
import com.haoxuer.discover.data.core.Pagination;

public interface MemberVisitService {
  Pagination getPage(int pageNo, int pageSize);

  MemberVisit findById(Long id);

  MemberVisit save(MemberVisit bean);

  MemberVisit update(MemberVisit bean);

  MemberVisit deleteById(Long id);

  MemberVisit[] deleteByIds(Long[] ids);

  MemberVisit visit(int muserid, int visituserid);


  Pagination findByUserId(int userid, int pageNo, int pageSize);

}