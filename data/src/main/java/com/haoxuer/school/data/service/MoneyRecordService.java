package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.MoneyRecord;
import com.haoxuer.discover.data.core.Pagination;

public interface MoneyRecordService {
  Pagination getPage(int pageNo, int pageSize);

  MoneyRecord findById(Integer id);

  MoneyRecord save(MoneyRecord bean);

  MoneyRecord update(MoneyRecord bean);

  MoneyRecord deleteById(Integer id);

  MoneyRecord[] deleteByIds(Integer[] ids);

  MoneyRecord delete(Integer id);

  Pagination pageByUser(int id, int curpage, int pagesize);
}