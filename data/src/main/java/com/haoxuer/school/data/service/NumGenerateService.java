package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.NumGenerate;
import com.haoxuer.discover.data.core.Pagination;

public interface NumGenerateService {
  Pagination getPage(int pageNo, int pageSize);

  NumGenerate findById(Long id);

  NumGenerate save(NumGenerate bean);

  NumGenerate update(NumGenerate bean);

  NumGenerate deleteById(Long id);

  NumGenerate[] deleteByIds(Long[] ids);
}