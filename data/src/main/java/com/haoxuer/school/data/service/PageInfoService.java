package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.PageInfo;
import com.haoxuer.discover.data.core.Pagination;

public interface PageInfoService {
  Pagination getPage(int pageNo, int pageSize);

  PageInfo findById(Integer id);

  PageInfo save(PageInfo bean);

  PageInfo update(PageInfo bean);

  PageInfo deleteById(Integer id);

  PageInfo[] deleteByIds(Integer[] ids);
}