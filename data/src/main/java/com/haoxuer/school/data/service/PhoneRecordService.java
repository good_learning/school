package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.PhoneRecord;
import com.haoxuer.discover.data.core.Pagination;

public interface PhoneRecordService {
  Pagination getPage(int pageNo, int pageSize);

  PhoneRecord findById(Integer id);

  PhoneRecord save(PhoneRecord bean);

  PhoneRecord update(PhoneRecord bean);

  PhoneRecord deleteById(Integer id);

  PhoneRecord[] deleteByIds(Integer[] ids);
}