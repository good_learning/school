package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.Province;

public interface ProvinceService {

  Province add(Province city);


  List<Province> all();

}
