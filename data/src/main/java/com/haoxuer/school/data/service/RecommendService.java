package com.haoxuer.school.data.service;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.entity.Recommend;

public interface RecommendService {

  Pagination getPage(int pageNo, int pageSize);

  Recommend findById(Long id);

  Recommend save(Recommend bean);

  Recommend update(Recommend bean);

  Recommend deleteById(Long id);

  Recommend[] deleteByIds(Long[] ids);

  Pagination recommend(int type, int size, int curpage);

  Pagination page(int type, int pageNo, int pageSize);

  Pagination page(int pageNo, int pageSize);

}