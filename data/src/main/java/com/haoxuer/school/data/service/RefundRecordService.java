package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.RefundRecord;
import com.haoxuer.discover.data.core.Pagination;

public interface RefundRecordService {
  Pagination getPage(int pageNo, int pageSize);

  RefundRecord findById(Integer id);

  RefundRecord save(RefundRecord bean);

  RefundRecord update(RefundRecord bean);

  RefundRecord deleteById(Integer id);

  RefundRecord[] deleteByIds(Integer[] ids);
}