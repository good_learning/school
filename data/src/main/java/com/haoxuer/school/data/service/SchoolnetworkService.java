package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Schoolnetwork;
import com.haoxuer.discover.data.core.Pagination;

public interface SchoolnetworkService {
  Pagination getPage(int pageNo, int pageSize);

  Schoolnetwork findById(Integer id);

  Schoolnetwork save(Schoolnetwork bean);

  Schoolnetwork update(Schoolnetwork bean);

  Schoolnetwork deleteById(Integer id);

  Schoolnetwork[] deleteByIds(Integer[] ids);


  Pagination getPage(int schoolid, int pageNo, int pageSize);


}