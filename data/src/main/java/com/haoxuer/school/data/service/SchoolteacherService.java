package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Schoolteacher;
import com.haoxuer.discover.data.core.Pagination;

public interface SchoolteacherService {
  Pagination getPage(int pageNo, int pageSize);

  Schoolteacher findById(Integer id);

  Schoolteacher save(Schoolteacher bean);

  Schoolteacher update(Schoolteacher bean);

  Schoolteacher deleteById(Integer id);

  Schoolteacher[] deleteByIds(Integer[] ids);


  Pagination findSchoolTeacher(Long schoolid, int pageNo, int pageSize);

  Pagination findByType(Long schoolid, int type, int pageNo, int pageSize);

  Pagination findBySchoolAll(Long schoolid, int pageNo, int pageSize);

  Schoolteacher reject(Integer id);

  Schoolteacher cancel(Integer id);

  Schoolteacher agree(Integer id);

  Schoolteacher agreegood(Integer id);

  Schoolteacher cancelgood(Integer id);

  Pagination pageForTeacher(Long teacherid, int curpage, int pagesize);

  Schoolteacher addTeacher(Schoolteacher schoolteacher);

  Schoolteacher addTeacher(String menpai, Member member);

  Pagination findSchoolTeacherList(Long schoolid, int pageNo, int pageSize);


}