package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Scorerecord;
import com.haoxuer.discover.data.core.Pagination;

public interface ScorerecordService {
  Pagination getPage(int pageNo, int pageSize);

  Scorerecord findById(Integer id);

  Scorerecord save(Scorerecord bean);

  Scorerecord update(Scorerecord bean);

  Scorerecord deleteById(Integer id);

  Scorerecord[] deleteByIds(Integer[] ids);

  Pagination pageByUserId(int id, int curpage, int pagesize);

  int getallscore(Long userid);
}