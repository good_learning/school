package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.discover.data.core.Pagination;

public interface SystemInfoService {
  Pagination getPage(int pageNo, int pageSize);

  SystemInfo findById(Long id);

  SystemInfo recovery(Long id);

  SystemInfo read(Long id);


  SystemInfo save(SystemInfo bean);

  SystemInfo update(SystemInfo bean);

  SystemInfo deleteById(Long id);

  SystemInfo[] deleteByIds(Long[] ids);

  Pagination pageByUser(int userid, int state, int pageNo, int pageSize);

}