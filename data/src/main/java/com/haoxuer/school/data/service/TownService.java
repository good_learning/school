package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.Town;
import com.haoxuer.discover.data.core.Pagination;

public interface TownService {
  Pagination getPage(int pageNo, int pageSize);

  Town findById(Integer id);

  Town save(Town bean);

  Town update(Town bean);

  Town deleteById(Integer id);

  Town[] deleteByIds(Integer[] ids);

  List<Town> findByCity(int id);
}