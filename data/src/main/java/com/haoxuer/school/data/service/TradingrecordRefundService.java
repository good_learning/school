package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.TradingrecordRefund;
import com.haoxuer.discover.data.core.Pagination;

public interface TradingrecordRefundService {
  Pagination getPage(int pageNo, int pageSize);

  TradingrecordRefund findById(Integer id);

  TradingrecordRefund save(TradingrecordRefund bean);

  TradingrecordRefund update(TradingrecordRefund bean);

  TradingrecordRefund deleteById(Integer id);

  TradingrecordRefund[] deleteByIds(Integer[] ids);

  TradingrecordRefund refunds(TradingrecordRefund tradingrecordRefund);

  TradingrecordRefund findByTradId(int id);

  TradingrecordRefund refund(int id);

  Pagination pageByForTeacher(int id, int curpage, int pagesize);
}