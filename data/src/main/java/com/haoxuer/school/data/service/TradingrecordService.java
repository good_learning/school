package com.haoxuer.school.data.service;

import java.util.List;

import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.discover.data.core.Pagination;

public interface TradingrecordService {
  Pagination getPage(int pageNo, int pageSize);

  Tradingrecord findById(Integer id);

  Tradingrecord save(Tradingrecord bean);

  Tradingrecord update(Tradingrecord bean);

  Tradingrecord deleteById(Integer id);

  Tradingrecord delete(Integer id);

  Tradingrecord[] deleteByIds(Integer[] ids);

  List<Tradingrecord> findByUid(Long uid);

  List<Tradingrecord> findByType(int type);

  int payusecard(int id, String card, Member member);

  Pagination pageByUser(int userid, int statetype, int pageNo, int pageSize);

  Pagination pageByUserGood(int userid, int statetype, int pageNo, int pageSize);

  Pagination pageByBuyUser(int userid, int statetype, int pageNo,
                           int pageSize);

  Pagination pageByBuyUser2(int userid, int statetype, int pageNo,
                            int pageSize);

  Pagination pageByBuyUserInfo(int userid, int statetype, int pageNo,
                               int pageSize);

  /**
   * 通过密码支付 不走网银
   *
   * @param id
   * @param password
   * @param member
   * @return
   */
  String payusepassword(int id, String password, Member member);

  Tradingrecord refunds(int id);
}