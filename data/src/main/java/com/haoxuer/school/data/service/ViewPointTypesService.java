package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.ViewPointTypes;
import com.haoxuer.discover.data.core.Pagination;

public interface ViewPointTypesService {
  Pagination getPage(int pageNo, int pageSize);

  ViewPointTypes findById(Integer id);

  ViewPointTypes save(ViewPointTypes bean);

  ViewPointTypes update(ViewPointTypes bean);

  ViewPointTypes deleteById(Integer id);

  ViewPointTypes[] deleteByIds(Integer[] ids);
}