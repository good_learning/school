package com.haoxuer.school.data.service;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.discover.data.core.Pagination;

public interface ViewpointService {
  Pagination getPage(int pageNo, int pageSize);

  Viewpoint findById(Integer id);

  Viewpoint view(Integer id);

  Viewpoint save(Viewpoint bean);

  Viewpoint update(Viewpoint bean);

  Viewpoint deleteById(Integer id);

  Viewpoint[] deleteByIds(Integer[] ids);


  Pagination pageByUserId(int userid, int pageNo, int pageSize);

  Pagination pageByGood(int pageNo, int pageSize);

  Viewpoint up(int id);

  void review(Viewpoint viewpoint);

  Pagination pageData(Map<String, Object> params,
                      List<String> orders, int pageNo, int pageSize);
}