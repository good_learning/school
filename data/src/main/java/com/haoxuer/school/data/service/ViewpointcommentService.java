package com.haoxuer.school.data.service;

import com.haoxuer.school.data.entity.Viewpointcomment;
import com.haoxuer.discover.data.core.Pagination;

public interface ViewpointcommentService {
  Pagination getPage(int pageNo, int pageSize);

  Viewpointcomment findById(Integer id);

  Viewpointcomment save(Viewpointcomment bean);

  Viewpointcomment update(Viewpointcomment bean);

  Viewpointcomment deleteById(Integer id);

  Viewpointcomment[] deleteByIds(Integer[] ids);

  Pagination pageByPointId(int commentid, int curpage, int pagesize);

  Pagination pageByViewPointId(int id, int sorttype, int pagesize,
                               int curpage);

  Integer up(int id);
}