package com.haoxuer.school.data.service.impl;

import java.util.List;
import java.util.Map;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.forum.Activity;
import com.haoxuer.school.data.entity.forum.ActivityComment;
import com.haoxuer.school.data.entity.forum.ActivityMember;
import com.haoxuer.school.data.entity.forum.ActivitySchedule;
import com.haoxuer.school.data.service.ActivityService;

@Service
@Transactional
public class ActivityServiceImpl extends ForumBaseServiceImpl implements
    ActivityService {

  @Autowired
  private ActivityDao activityDao;

  @Autowired
  private ActivityCommentDao commentDao;

  @Autowired
  private ActivityMemberDao actMemberDao;

  @Autowired
  private ActivityScheduleDao scheduleDao;

  @Autowired
  private MemberDao memberInfoDao;

  /**
   * 获取Activity实例
   */
  @Override
  public Activity findActivityById(Integer id) {
    Activity entity = activityDao.findById(id);
    return entity;
  }

  /**
   * 获取ActivityComment实例
   */
  @Override
  public ActivityComment findCommentById(Integer id) {
    ActivityComment comment = commentDao.findById(id);
    return comment;
  }

  /**
   * 创建Activity实例
   */
  @Transactional
  public Activity save(Activity bean) {

    activityDao.save(bean);
    return bean;
  }

  /**
   * 创建ActivityComment实例
   */
  @Transactional
  public ActivityComment save(ActivityComment bean) {
    commentDao.save(bean);
    return bean;
  }

  /**
   * 创建ActivitySchedule实例
   */
  @Transactional
  public ActivitySchedule save(ActivitySchedule bean) {
    scheduleDao.save(bean);
    return bean;
  }

  /**
   * 创建ActivityMember实例
   */
  @Transactional
  public ActivityMember save(ActivityMember bean) {
    actMemberDao.save(bean);
    return bean;
  }

  /**
   * 修改Activity
   */
  @Transactional
  public Activity update(Activity bean) {
    Updater<Activity> updater = new Updater<Activity>(bean);
    bean = activityDao.updateByUpdater(updater);
    return bean;
  }

  /**
   * 修改ActivityMember
   */
  @Transactional
  public ActivityMember update(ActivityMember bean) {
    Updater<ActivityMember> updater = new Updater<ActivityMember>(bean);
    bean = actMemberDao.updateByUpdater(updater);
    return bean;
  }

  /**
   * 修改ActivitySchedule
   */
  @Transactional
  public ActivitySchedule update(ActivitySchedule bean) {
    Updater<ActivitySchedule> updater = new Updater<ActivitySchedule>(bean);
    bean = scheduleDao.updateByUpdater(updater);
    return bean;
  }

  /**
   * 修改ActivityComment
   */
  @Transactional
  public ActivityComment update(ActivityComment bean) {
    Updater<ActivityComment> updater = new Updater<ActivityComment>(bean);
    bean = commentDao.updateByUpdater(updater);
    return bean;
  }

  /**
   * 删除Activity
   */
  @Transactional
  public Activity deleteActivityById(Integer id) {
    Activity bean = activityDao.deleteById(id);
    return bean;
  }

  /**
   * 删除ActivityMember
   */
  @Transactional
  public ActivityMember deleteMemberById(Integer id) {
    ActivityMember bean = actMemberDao.deleteById(id);
    return bean;
  }

  /**
   * 删除ActivitySchedule
   */
  @Transactional
  public ActivitySchedule deleteScheduleById(Integer id) {
    ActivitySchedule bean = scheduleDao.deleteById(id);
    return bean;
  }

  /**
   * 删除ActivityComment
   */
  @Transactional
  public ActivityComment deleteCommentById(Integer id) {
    ActivityComment bean = commentDao.deleteById(id);
    return bean;
  }

  /**
   * 删除Activity列表
   */
  @Transactional
  public Activity[] deleteActivityByIds(Integer[] ids) {
    Activity[] beans = new Activity[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteActivityById(ids[i]);
    }
    return beans;
  }

  /**
   * Activity分页查询
   */
  @Override
  public Pagination getPageOfActivities(Map<String, Object> params,
                                        List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder.create("from Activity c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = activityDao.find(finder, curpage, pagesize);
    return page;
  }

  /**
   * ActivityComment分页查询
   */
  @Override
  public Pagination getPageOfComments(Map<String, Object> params,
                                      List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder.create("from ActivityComment c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = actMemberDao.find(finder, curpage, pagesize);
    return page;
  }

  /**
   * ActivityMember分页查询
   */
  @Override
  public Pagination getPageOfMembers(Map<String, Object> params,
                                     List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder.create("from ActivityMember c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = actMemberDao.find(finder, curpage, pagesize);
    return page;
  }

  /**
   * ActivitySchedules分页查询
   */
  @Override
  public Pagination getPageOfSchedules(Map<String, Object> params,
                                       List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder.create("from ActivitySchedule c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = scheduleDao.find(finder, curpage, pagesize);
    return page;
  }


  @Override
  public Pagination getMyJoinedActivities(Map<String, Object> params,
                                          List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder
        .create("select c.activity from ActivityMember c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = scheduleDao.find(finder, curpage, pagesize);
    return page;
  }

  @Override
  public Pagination getMycommentedActivities(Map<String, Object> params,
                                             List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder
        .create("select c.activity from ActivityComment c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = scheduleDao.find(finder, curpage, pagesize);
    return page;
  }

  /**
   * 查看活动详情（按活动id）
   */
  @Transactional
  public Activity viewActivity(Integer id) {
    Activity bean = this.findActivityById(id);
    int views = bean.getVisitCount() + 1;
    bean.setVisitCount(views);
    this.update(bean);
    return bean;
  }

  @Override
  public Member getMenber(Activity activity) {
    return null;
  }

}