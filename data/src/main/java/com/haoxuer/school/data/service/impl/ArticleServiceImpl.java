package com.haoxuer.school.data.service.impl;

import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ArticleDao;
import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.service.ArticleService;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional
  public Article findById(Integer id) {
    Article entity = dao.findById(id);
    // Integer views = entity.getViews();
    // if (views == null) {
    // views = 1;
    // }
    // views++;
    // entity.setViews(views);
    return entity;
  }

  @Transactional
  public Article save(Article bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Article update(Article bean) {
    Updater<Article> updater = new Updater<Article>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Article deleteById(Integer id) {
    Article bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Article[] deleteByIds(Integer[] ids) {
    Article[] beans = new Article[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ArticleDao dao;

  @Autowired
  public void setDao(ArticleDao dao) {
    this.dao = dao;
  }

  @Override
  public List<Article> findArticlesById(Integer id) {
    List<Article> beans = dao.findArticlesById(id);
    return beans;
  }

  @Override
  public List<Article> findArticlesByPid(Integer id, int size, int sorttype) {
    // TODO Auto-generated method stub
    return dao.findArticlesByPid(id, size, sorttype);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByPid(Integer id, int sorttype, int size, int curpage) {
    // TODO Auto-generated method stub
    return dao.pageByPid(id, sorttype, size, curpage);
  }

  @Transactional
  @Override
  public Integer up(int id) {
    Article article = dao.findById(id);
    Integer ups = 0;
    if (article != null) {
      ups = article.getUps();
      if (ups == null) {
        ups = 1;
      }
      ups++;
      article.setUps(ups);
    }
    return ups;

  }

  @Transactional(readOnly = true)
  @Override
  public Pagination recommend(int type, int size, int curpage) {
    // TODO Auto-generated method stub
    return dao.recommend(type, size, curpage);
  }
}