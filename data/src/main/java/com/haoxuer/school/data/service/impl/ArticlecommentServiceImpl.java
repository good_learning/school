package com.haoxuer.school.data.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ArticlecommentDao;
import com.haoxuer.school.data.entity.Articlecomment;
import com.haoxuer.school.data.service.ArticlecommentService;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
@Transactional
public class ArticlecommentServiceImpl implements ArticlecommentService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Articlecomment findById(Integer id) {
    Articlecomment entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Articlecomment save(Articlecomment bean) {
    dao.save(bean);
    if (bean.getArticlecomment() != null) {
      try {
        Configuration cfg = new Configuration();
        Locale locale = Locale.CHINA;
        cfg.setEncoding(locale, "utf-8");
        File file = getfile();
        cfg.setDirectoryForTemplateLoading(file);
        cfg.setTagSyntax(Configuration.SQUARE_BRACKET_TAG_SYNTAX);
        Template template = cfg.getTemplate("comment.html");
        Writer out = new StringWriter();
        Map<String, Object> map = new HashMap<String, Object>();
        Articlecomment iteminfo = dao.findById(bean.getId());
        map.put("item", iteminfo);
        map.put("siteurl", "http://www.91mydoor.com/");

        template.process(map, out);
        Articlecomment item = dao.findById(bean.getArticlecomment().getId());
        String comment = out.toString() + item.getContent();
        bean.setContent(comment);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (TemplateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return bean;
  }

  private File getfile() {
    String dir = "D:\\Program Files (x86)\\Apache Software Foundation\\Tomcat 8.0\\webapps\\ROOT\\WEB-INF\\classes\\ftl\\";
    File file = new File(dir);
    if (!file.exists()) {
      try {
        dir = getClass().getClassLoader().getResource("/ftl/").getFile();
        file = new File(dir);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }
    if (!file.exists()) {
      dir = "d:\\files\\";
    }
    return file;
  }

  @Transactional
  public Articlecomment update(Articlecomment bean) {
    Updater<Articlecomment> updater = new Updater<Articlecomment>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Articlecomment deleteById(Integer id) {
    Articlecomment bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Articlecomment[] deleteByIds(Integer[] ids) {
    Articlecomment[] beans = new Articlecomment[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ArticlecommentDao dao;

  @Autowired
  public void setDao(ArticlecommentDao dao) {
    this.dao = dao;
  }

  @Override
  public Pagination pageByArticleId(int id, int sorttype, int pagesize,
                                    int curpage) {
    // TODO Auto-generated method stub
    return dao.pageByArticleId(id, sorttype, pagesize, curpage);
  }

  @Transactional
  @Override
  public Integer up(int id) {
    Articlecomment a = dao.findById(id);
    Integer ups = a.getUps();
    if (ups == null) {
      ups = 0;
    }
    ups++;
    a.setUps(ups);
    return ups;
  }

  @Override
  public void executesave(Articlecomment comment) {
    dao.save(comment);
  }
}