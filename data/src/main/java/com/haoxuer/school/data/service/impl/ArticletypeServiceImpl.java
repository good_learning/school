package com.haoxuer.school.data.service.impl;

import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ArticletypeDao;
import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.school.data.service.ArticletypeService;

@Service
@Transactional
public class ArticletypeServiceImpl implements ArticletypeService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Articletype findById(Integer id) {
    Articletype entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Articletype save(Articletype bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Articletype update(Articletype bean) {
    Updater<Articletype> updater = new Updater<Articletype>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Articletype deleteById(Integer id) {
    Articletype bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Articletype[] deleteByIds(Integer[] ids) {
    Articletype[] beans = new Articletype[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ArticletypeDao dao;

  @Autowired
  public void setDao(ArticletypeDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public List<Articletype> findChild(Integer pid) {
    // TODO Auto-generated method stub
    return dao.findChild(pid);
  }

  @Override
  public List<Articletype> findByParentId(Integer id) {
    List<Articletype> entitys = dao.findChild(id);
    return entitys;
  }
}