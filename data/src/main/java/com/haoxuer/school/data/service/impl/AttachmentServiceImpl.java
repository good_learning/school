package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.AttachmentDao;
import com.haoxuer.school.data.entity.Attachment;
import com.haoxuer.school.data.service.AttachmentService;

@Service
@Transactional
public class AttachmentServiceImpl implements AttachmentService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Attachment findById(Long id) {
    Attachment entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Attachment save(Attachment bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Attachment update(Attachment bean) {
    Updater<Attachment> updater = new Updater<Attachment>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Attachment deleteById(Long id) {
    Attachment bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Attachment[] deleteByIds(Long[] ids) {
    Attachment[] beans = new Attachment[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private AttachmentDao dao;

  @Autowired
  public void setDao(AttachmentDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserId(int id, int curpage, int pagesize) {
    // TODO Auto-generated method stub
    return dao.pageByUserId(id, curpage, pagesize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pagetypeByUserId(int id, int type, int curpage, int pagesize) {
    // TODO Auto-generated method stub
    return dao.pagetypeByUserId(id, type, curpage, pagesize);
  }


  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserId(int id, int type, int curpage, int pagesize) {
    Finder finder = Finder
        .create("from Attachment u where u.member.id = ");
    finder.append("" + id);
    finder.append("  and u.catalog =" + type);
    finder.append("   order by  u.adddate  desc");
    return dao.find(finder, curpage, pagesize);
  }

}
