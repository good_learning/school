package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.BankRecordDao;
import com.haoxuer.school.data.dao.CourseDao;
import com.haoxuer.school.data.dao.CourseLessonStudentDao;
import com.haoxuer.school.data.dao.MoneyRecordDao;
import com.haoxuer.school.data.dao.ScorerecordDao;
import com.haoxuer.school.data.dao.SystemInfoDao;
import com.haoxuer.school.data.dao.TradingrecordDao;
import com.haoxuer.school.data.entity.BankRecord;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.MoneyRecord;
import com.haoxuer.school.data.entity.Scorerecord;
import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.service.BankRecordService;

@Service
@Transactional
public class BankRecordServiceImpl implements BankRecordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public BankRecord findById(Long id) {
    BankRecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public BankRecord save(BankRecord bean) {
    dao.save(bean);
    SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmm");
    String now = format.format(new Date());
    String orderid = now + "00" + bean.getId();
    bean.setOrderid(orderid);
    bean.setAddtime(new Timestamp(System.currentTimeMillis()));
    bean.setState(0);
    return bean;
  }

  @Transactional
  public BankRecord update(BankRecord bean) {
    Updater<BankRecord> updater = new Updater<BankRecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public BankRecord deleteById(Long id) {
    BankRecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public BankRecord[] deleteByIds(Long[] ids) {
    BankRecord[] beans = new BankRecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private BankRecordDao dao;

  @Autowired
  public void setDao(BankRecordDao dao) {
    this.dao = dao;
  }

  @Autowired
  private ScorerecordDao scorerecordDao;

  @Autowired
  CourseDao courseDao;
  @Autowired
  MoneyRecordDao moneyRecordDao;

  @Autowired
  CourseLessonStudentDao courseLessonStudentDao;

  @Autowired
  SystemInfoDao systemInfoDao;

  @Transactional
  @Override
  public BankRecord payok(String orderid, String noticetype) {
    Finder finderx = Finder.create();
    finderx.append("from   BankRecord r where r.orderid  = '" + orderid
        + "'");
    List<BankRecord> rs = dao.find(finderx);
    if (rs != null && rs.size() > 0) {
      BankRecord br = rs.get(0);
      if (br.getState() == 1) {
        return br;
      }
      br.setUpdatetime(new Timestamp(System.currentTimeMillis()));
      br.setState(1);
      br.setNoticetype(noticetype);
      String type = br.getTypeinfo();
      Member member = br.getMember();
      float sm = member.getMoneys() + br.getMoneys();
      member.setMoneys(sm);

      MoneyRecord moneyRecord = new MoneyRecord();
      moneyRecord.setAdddate(new Timestamp(System.currentTimeMillis()));
      moneyRecord
          .setTradingdate(new Timestamp(System.currentTimeMillis()));
      moneyRecord.setMoney(br.getMoneys());
      moneyRecord.setMember(member);
      moneyRecord.setName("网银充值了:" + br.getMoneys() + "元");
      moneyRecord.setState(1);

      SystemInfo systemInfo = new SystemInfo();
      systemInfo.setAdddate(new Timestamp(System.currentTimeMillis()));
      systemInfo.setMember(member);
      systemInfo.setReadsate(0);
      systemInfo.setState(0);
      systemInfo.setContent("网银充值了:" + br.getMoneys() + "元");
      systemInfoDao.add(systemInfo);

      if ("course".equals(type)) {

        Tradingrecord tradingrecord = br.getTradingrecord();
        Scorerecord record = new Scorerecord();
        record.setMember(member);
        record.setScore(tradingrecord.getMoney());
        record.setDemo("余额支付:" + "购买了"
            + tradingrecord.getCourse().getName());
        record.setSummary("余额支付:" + "购买了"
            + tradingrecord.getCourse().getName());
        record.setTransactiondate(new Timestamp(System
            .currentTimeMillis()));
        int all = (int) (tradingrecord.getMoney() + scorerecordDao
            .getallscore(member.getId()));
        record.setLeftscore(all);
        scorerecordDao.save(record);

        MoneyRecord bean = new MoneyRecord();
        bean.setAdddate(new Timestamp(System.currentTimeMillis()));
        bean.setTradingdate(new Timestamp(System.currentTimeMillis()));
        bean.setMoney(-tradingrecord.getMoney());
        bean.setMember(member);
        bean.setSeller(tradingrecord.getCourse().getMember());
        bean.setName("购买了" + tradingrecord.getCourse().getName());
        bean.setState(1);
        bean.setCourse(tradingrecord.getCourse());
        // bean.set
        moneyRecordDao.save(bean);

        scorerecordDao.getallscore(member.getId());

        Finder finder = Finder.create();
        finder.append("from CourseLessonStudent c");
        finder.append(" where  c.course.id= "
            + tradingrecord.getCourse().getId());
        finder.append(" and  c.tradingrecord.id= "
            + tradingrecord.getId());
        int size = dao.countQueryResult(finder);
        if (size == 0) {
          Course course = courseDao.findById(tradingrecord
              .getCourse().getId());
          int jsize = course.getJointimes();
          jsize++;
          course.setJointimes(jsize);
          int lessions = course.getLesson();
          List<CourseLessonStudent> lists = new ArrayList<CourseLessonStudent>();
          for (int i = 1; i <= lessions; i++) {
            CourseLessonStudent item = new CourseLessonStudent();
            item.setPeriod(i);
            item.setCourse(course);
            item.setMember(member);
            item.setTradingrecord(tradingrecord);
            courseLessonStudentDao.save(item);
            lists.add(item);
          }
        }
        tradingrecord.setState(2);
      }
    }
    return null;
  }

  @Autowired
  TradingrecordDao tradingrecordDao;

  @Transactional
  @Override
  public BankRecord findByRecordId(int id) {
    BankRecord result = null;
    Finder finderx = Finder.create();
    finderx.append("from   BankRecord r where r.tradingrecord.id  = '" + id
        + "'");
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, -30);
    Date timeout = calendar.getTime();
    SimpleDateFormat formatt = new SimpleDateFormat("yyyy-MM-dd hh:mm");

    finderx.append(" and  r.addtime   < '" + formatt.format(timeout) + "'");
    List<BankRecord> rs = dao.find(finderx);
    if (rs != null && rs.size() > 0) {
      result = rs.get(0);
    } else {

      Tradingrecord record = tradingrecordDao.findById(id);
      Member merber = record.getMember();
      float m = record.getCourse().getRealprice() - merber.getMoneys();
      if (m > 0) {
        result = new BankRecord();
        result.setTypeinfo("course");
        result.setTradingrecord(record);
        result.setBackmoney(merber.getMoneys());
        result.setMoneys(m);
        result.setMember(merber);
        Course cours = record.getCourse();
        record.setBankstate(1);
        int t = cours.getJointimes() + 1;
        cours.setJointimes(t);
        merber.setMoneys(0f);
      }
      dao.save(result);
      SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmm");
      String now = format.format(new Date());
      String orderid = now + "00" + result.getId();
      result.setOrderid(orderid);
      result.setAddtime(new Timestamp(System.currentTimeMillis()));
      result.setState(0);

    }
    return result;
  }

  @Transactional
  @Override
  public List<BankRecord> backdata(int size) {
    Finder finderx = Finder.create();
    finderx.append("from   BankRecord r where r.backmoney >0 and r.member.id >0 ");
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, -30);
    Date timeout = calendar.getTime();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");

    finderx.append(" and  r.addtime   < '" + format.format(timeout) + "'");
    finderx.append(" and r.typeinfo ='course' ");

    Pagination page = tradingrecordDao.find(finderx, 1, size);
    List<BankRecord> rs = (List<BankRecord>) page.getList();
    if (rs != null) {
      for (BankRecord bankRecord : rs) {
        float m = bankRecord.getBackmoney();
        Member member = bankRecord.getMember();
        member.setMoneys(member.getMoneys() + m);
        bankRecord.setBackmoney(0);
        bankRecord.setDemo("已经进行了超时处理");
        bankRecord.setWorktime(new Timestamp(System.currentTimeMillis()));
        bankRecord.setUpdatetime(new Timestamp(System.currentTimeMillis()));
        Course course = bankRecord.getTradingrecord().getCourse();
        int times = course.getJointimes();
        times--;
        course.setJointimes(times);
      }
    }
    return rs;
  }

}