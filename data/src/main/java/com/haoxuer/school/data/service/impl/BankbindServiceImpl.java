package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.BankbindDao;
import com.haoxuer.school.data.entity.Bankbind;
import com.haoxuer.school.data.service.BankbindService;

@Service
@Transactional
public class BankbindServiceImpl implements BankbindService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Bankbind findById(Integer id) {
    Bankbind entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Bankbind save(Bankbind bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Bankbind update(Bankbind bean) {
    Updater<Bankbind> updater = new Updater<Bankbind>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Bankbind deleteById(Integer id) {
    Bankbind bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Bankbind[] deleteByIds(Integer[] ids) {
    Bankbind[] beans = new Bankbind[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private BankbindDao dao;

  @Autowired
  public void setDao(BankbindDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pagetByUser(Long id, int curpage, int pagesize) {
    Finder finder = Finder.create("from Bankbind b where b.member.id =" + id);
    finder.append("order by b.id desc");
    return dao.find(finder, curpage, pagesize);
  }
}