package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.BrowsingHistoryDao;
import com.haoxuer.school.data.entity.BrowsingHistory;
import com.haoxuer.school.data.service.BrowsingHistoryService;

@Service
@Transactional
public class BrowsingHistoryServiceImpl implements BrowsingHistoryService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public BrowsingHistory findById(Integer id) {
    BrowsingHistory entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public BrowsingHistory save(BrowsingHistory bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public BrowsingHistory update(BrowsingHistory bean) {
    Updater<BrowsingHistory> updater = new Updater<BrowsingHistory>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public BrowsingHistory deleteById(Integer id) {
    BrowsingHistory bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public BrowsingHistory[] deleteByIds(Integer[] ids) {
    BrowsingHistory[] beans = new BrowsingHistory[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private BrowsingHistoryDao dao;

  @Autowired
  public void setDao(BrowsingHistoryDao dao) {
    this.dao = dao;
  }

  @Transactional
  @Override
  public BrowsingHistory saveRecord(int userid, int courseid) {
    // TODO Auto-generated method stub
    return dao.saveRecord(userid, courseid);
  }


  @Transactional(readOnly = true)
  @Override
  public Pagination getPage(int userid, int pageNo, int pageSize) {
    // TODO Auto-generated method stub
    return dao.getPage(userid, pageNo, pageSize);
  }
}