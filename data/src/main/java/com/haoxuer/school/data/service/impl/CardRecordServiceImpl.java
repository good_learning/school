package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CardRecordDao;
import com.haoxuer.school.data.entity.CardRecord;
import com.haoxuer.school.data.service.CardRecordService;

@Service
@Transactional
public class CardRecordServiceImpl implements CardRecordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CardRecord findById(Integer id) {
    CardRecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CardRecord save(CardRecord bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CardRecord update(CardRecord bean) {
    Updater<CardRecord> updater = new Updater<CardRecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CardRecord deleteById(Integer id) {
    CardRecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CardRecord[] deleteByIds(Integer[] ids) {
    CardRecord[] beans = new CardRecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CardRecordDao dao;

  @Autowired
  public void setDao(CardRecordDao dao) {
    this.dao = dao;
  }
}