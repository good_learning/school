package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.dao.MemberDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.by.utils.Utils;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CardDao;
import com.haoxuer.school.data.dao.CardRecordDao;
import com.haoxuer.school.data.dao.SystemInfoDao;
import com.haoxuer.school.data.entity.Card;
import com.haoxuer.school.data.entity.CardRecord;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.school.data.service.CardService;

@Service
@Transactional
public class CardServiceImpl implements CardService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Card findById(Long id) {
    Card entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Card save(Card bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Card update(Card bean) {
    Updater<Card> updater = new Updater<Card>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Card deleteById(Long id) {
    Card bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Card[] deleteByIds(Long[] ids) {
    Card[] beans = new Card[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CardDao dao;

  @Autowired
  public void setDao(CardDao dao) {
    this.dao = dao;
  }

  @Transactional
  @Override
  public int buildCards(int userid, int cars, float money) {

    int i = 0;
    while (i < cars) {
      try {
        Card card = new Card();
        Member member = new Member();
        //member.setId(userid);
        card.setMember(member);
        card.setMoney(money);
        card.setAdddate(new Timestamp(System.currentTimeMillis()));
        card.setCardpass(Utils.getpassword(6));
        card.setCardnum(Utils.getCard(32, userid + ""));
        card.setLeftmoney(money);
        i++;
        dao.save(card);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }

    return i;
  }

  @Autowired
  CardRecordDao cardRecordDao;
  @Autowired
  MemberDao memberInfoDao;

  @Autowired
  SystemInfoDao systemInfoDao;

  @Transactional
  @Override
  public String chongzhi(String cardnum, String cardpass, float money,
                         Member member) {
    String resulet = "";
    Card card = dao.findByName(cardnum);
    if (card != null) {
      String pass = card.getCardpass();
      if (pass.equals(cardpass)) {
        if (card.getLeftmoney() > money) {
          member = memberInfoDao.findById(member.getId());
          float left = card.getLeftmoney() - money;
          float m = member.getMoneys();
          m = m + money;
          card.setLeftmoney(left);
          member.setMoneys(m);

          CardRecord record = new CardRecord();
          record.setAddtime(new Timestamp(System.currentTimeMillis()));
          record.setMoney(money);
          record.setCard(card);
          record.setMember(member);
          cardRecordDao.save(record);
          resulet = "ok";
          // 老师通知
          SystemInfo teacherinfo = new SystemInfo();
          teacherinfo.setAdddate(new Timestamp(System
              .currentTimeMillis()));
          teacherinfo.setState(0);
          teacherinfo.setReadsate(0);
          teacherinfo.setMember(member);
          String content = getteacherinfo(member, record);
          teacherinfo.setContent(content);
          systemInfoDao.save(teacherinfo);
        } else {
          resulet = "充值卡余额不足";

        }
      } else {
        resulet = "充值卡密码不正确";
      }

    } else {
      resulet = "充值卡不存在";
    }
    return resulet;
  }

  private String getteacherinfo(Member member, CardRecord tradingrecord) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("尊敬的<" + member.getName()
        + ">");
    buffer.append("(门牌号:"
        + member.getId()
        + ")你好,");
    buffer.append("你通过充值卡充值了:" + tradingrecord.getMoney() + "元");
    String content = buffer.toString();
    return content;
  }

  @Override
  public Pagination page(int curpage, int pagesize) {
    Finder finder = Finder.create("from Card c order by c.id desc");
    // TODO Auto-generated method stub
    return dao.find(finder, curpage, pagesize);
  }
}