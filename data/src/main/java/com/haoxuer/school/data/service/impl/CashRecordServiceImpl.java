package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;

import com.haoxuer.school.common.security.Md5PwdEncoder;
import com.haoxuer.school.common.security.PwdEncoder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.dao.MemberDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.BankbindDao;
import com.haoxuer.school.data.dao.CashRecordDao;
import com.haoxuer.school.data.dao.SystemInfoDao;
import com.haoxuer.school.data.entity.Bankbind;
import com.haoxuer.school.data.entity.CashRecord;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.school.data.service.CashRecordService;

@Service
@Transactional
public class CashRecordServiceImpl implements CashRecordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CashRecord findById(Long id) {
    CashRecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CashRecord save(CashRecord bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CashRecord update(CashRecord bean) {
    Updater<CashRecord> updater = new Updater<CashRecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CashRecord deleteById(Long id) {
    CashRecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CashRecord[] deleteByIds(Long[] ids) {
    CashRecord[] beans = new CashRecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CashRecordDao dao;

  @Autowired
  public void setDao(CashRecordDao dao) {
    this.dao = dao;
  }

  PwdEncoder pwdEncoder;
  private static final String salt = "googlebaidu";

  @Autowired
  BankbindDao bankbindDao;

  @Autowired
  SystemInfoDao systemInfoDao;

  @Autowired
  MemberDao memberInfoDao;

  @Transactional
  @Override
  public String tixian(Integer bankid, Float money, String password,
                       Member member) {
    String result = "ok";

    if (member != null) {
      member = memberInfoDao.findById(member.getId());
      if (bankid != null) {
        if (money != null && money > 0) {
          Bankbind ban = bankbindDao.findById(bankid);
          if (member.getId() == ban.getMember().getId()) {
            pwdEncoder = new Md5PwdEncoder();
            String dbpassword = "";
            if (dbpassword == null) {
              result = "请到我的账户设置交易密码";
            } else {
              if (money <= member.getMoneys()) {
                if (pwdEncoder.isPasswordValid(dbpassword,
                    password, salt)) {
                  CashRecord record = new CashRecord();
                  record.setMember(member);
                  record.setAddtime(new Timestamp(System
                      .currentTimeMillis()));
                  record.setMoney(money);
                  record.setBankbind(ban);
                  dao.save(record);
                  float m = member.getMoneys();
                  float leftm = m - money;
                  member.setMoneys(leftm);
                  SystemInfo info = new SystemInfo();
                  info.setAdddate(new Timestamp(System.currentTimeMillis()));
                  info.setMember(member);
                  info.setContent("尊敬的" + member.getName() + ",你提现了" + money);
                  info.setReadsate(0);
                  info.setState(0);
                  systemInfoDao.save(info);
                  result = "ok";
                } else {
                  result = "密码不正确";
                }
              } else {
                result = "余额不足";
              }
            }
          } else {
            result = "非法操作";
          }
        } else {
          result = "输入的金额不正确";
        }
      } else {
        result = "请选择提现银行卡";
      }
    } else {
      result = "没有登陆";
    }

    return result;
  }

  @Transactional
  @Override
  public String tixian(Integer bankid, Float money, Member member) {
    String result = "ok";

    if (member != null) {
      member = memberInfoDao.findById(member.getId());
      if (bankid != null) {
        if (money != null && money > 0) {
          Bankbind ban = bankbindDao.findById(bankid);
          if (member.getId() == ban.getMember().getId()) {
            pwdEncoder = new Md5PwdEncoder();
            String dbpassword = "";
            if (dbpassword == null) {
              result = "请到我的账户设置交易密码";
            } else {
              if (money <= member.getMoneys()) {
                CashRecord record = new CashRecord();
                record.setMember(member);
                record.setAddtime(new Timestamp(System
                    .currentTimeMillis()));
                record.setMoney(money);
                record.setBankbind(ban);
                dao.save(record);
                float m = member.getMoneys();
                float leftm = m - money;
                member.setMoneys(leftm);
                SystemInfo info = new SystemInfo();
                info.setAdddate(new Timestamp(System
                    .currentTimeMillis()));
                info.setMember(member);
                info.setContent("尊敬的" + member.getName()
                    + ",你提现了" + money);
                info.setReadsate(0);
                info.setState(0);
                systemInfoDao.save(info);
                result = "ok";
              } else {
                result = "余额不足";
              }
            }
          } else {
            result = "非法操作";
          }
        } else {
          result = "输入的金额不正确";
        }
      } else {
        result = "请选择提现银行卡";
      }
    } else {
      result = "没有登陆";
    }

    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pagetByUser(Long id, int curpage, int pagesize) {
    Finder finder = Finder.create("from CashRecord b where b.member.id ="
        + id);
    finder.append("order by b.id desc");
    return dao.find(finder, curpage, pagesize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination page(int curpage, int pagesize) {
    Finder finder = Finder.create("from CashRecord b ");
    finder.append("order by b.id desc");
    return dao.find(finder, curpage, pagesize);
  }
}