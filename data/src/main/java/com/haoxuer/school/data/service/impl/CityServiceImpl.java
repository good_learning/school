package com.haoxuer.school.data.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.CityDao;
import com.haoxuer.school.data.entity.City;
import com.haoxuer.school.data.service.CityService;

@Service
public class CityServiceImpl implements CityService {

  @Resource
  private CityDao dao;

  @Transactional
  @Override
  public City add(City city) {
    return dao.add(city);
  }

  @Transactional(readOnly = true)
  @Override
  public List<City> findByProvince(int provinceid) {
    Finder finder = Finder.create("from City c");
    finder.append(" where c.province.id=" + provinceid);
    return dao.find(finder);
  }
}
