package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ComplaintDao;
import com.haoxuer.school.data.entity.Complaint;
import com.haoxuer.school.data.service.ComplaintService;

@Service
@Transactional
public class ComplaintServiceImpl implements ComplaintService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Complaint findById(Integer id) {
    Complaint entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Complaint save(Complaint bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Complaint update(Complaint bean) {
    Updater<Complaint> updater = new Updater<Complaint>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Complaint deleteById(Integer id) {
    Complaint bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Complaint[] deleteByIds(Integer[] ids) {
    Complaint[] beans = new Complaint[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ComplaintDao dao;

  @Autowired
  public void setDao(ComplaintDao dao) {
    this.dao = dao;
  }
}