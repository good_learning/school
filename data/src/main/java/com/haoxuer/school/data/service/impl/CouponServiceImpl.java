package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;
import java.util.Date;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CouponDao;
import com.haoxuer.school.data.entity.CouponInfo;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.service.CouponService;

@Service
@Transactional
public class CouponServiceImpl implements CouponService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CouponInfo findById(Integer id) {
    CouponInfo entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CouponInfo save(CouponInfo bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CouponInfo update(CouponInfo bean) {
    Updater<CouponInfo> updater = new Updater<CouponInfo>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CouponInfo deleteById(Integer id) {
    CouponInfo bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CouponInfo[] deleteByIds(Integer[] ids) {
    CouponInfo[] beans = new CouponInfo[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CouponDao dao;

  @Autowired
  public void setDao(CouponDao dao) {
    this.dao = dao;
  }

  @Transactional
  @Override
  public int buildCards(int userid, int cars, float money, String name, Date vdate) {
    int i = 0;
    while (i < cars) {
      try {
        CouponInfo coupon = new CouponInfo();
        Member member = new Member();
        //member.setId(userid);
        coupon.setAddmember(member);
        coupon.setName(name);
        coupon.setMoney(money);
        coupon.setValiddate(vdate);
        coupon.setAdddate(new Timestamp(System.currentTimeMillis()));
        coupon.setState(0);
        i++;
        dao.save(coupon);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }

    return i;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUser(int id, int curpage, int pagesize) {
    Finder finder = Finder.create();
    finder.append("from CouponInfo c");
    finder.append(" where c.usemember.id =" + id);
    finder.append("  order by c.validdate desc");
    // TODO Auto-generated method stub
    return dao.find(finder, curpage, pagesize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserNotUser(int id, int curpage, int pagesize) {
    Finder finder = Finder.create();
    finder.append("from CouponInfo c");
    finder.append(" where c.usemember.id =" + id);
    finder.append(" and c.state=0");
    finder.append("  order by c.validdate desc");
    return dao.find(finder, curpage, pagesize);
  }
}