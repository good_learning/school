package com.haoxuer.school.data.service.impl;

import com.haoxuer.school.data.dao.CourseCatalogDao;
import com.haoxuer.school.data.entity.CourseCatalog;
import com.haoxuer.school.data.oto.CourseTypeInfos;
import com.haoxuer.school.data.service.CourseCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
 * Created by imake on 2018年08月16日23:12:28.
 */


@Scope("prototype")
@Service
@Transactional
public class CourseCatalogServiceImpl implements CourseCatalogService {

  private CourseCatalogDao dao;


  @Override
  @Transactional(readOnly = true)
  public CourseCatalog findById(Integer id) {
    return dao.findById(id);
  }

  @Override
  public List<CourseCatalog> findByTops(Integer pid) {
    LinkedList<CourseCatalog> result = new LinkedList<CourseCatalog>();
    CourseCatalog catalog = dao.findById(pid);
    if (catalog != null) {
      while (catalog != null && catalog.getParent() != null) {
        result.addFirst(catalog);
        catalog = dao.findById(catalog.getParentId());
      }
      result.addFirst(catalog);
    }
    return result;
  }


  @Override
  public List<CourseCatalog> child(Integer id, Integer max) {
    List<Order> orders = new ArrayList<Order>();
    orders.add(Order.asc("code"));
    List<Filter> list = new ArrayList<Filter>();
    list.add(Filter.eq("parent.id", id));
    return dao.list(0, max, list, orders);
  }

  @Override
  @Transactional
  public CourseCatalog save(CourseCatalog bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public CourseCatalog update(CourseCatalog bean) {
    Updater<CourseCatalog> updater = new Updater<CourseCatalog>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public CourseCatalog deleteById(Integer id) {
    CourseCatalog bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public CourseCatalog[] deleteByIds(Integer[] ids) {
    CourseCatalog[] beans = new CourseCatalog[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(CourseCatalogDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<CourseCatalog> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<CourseCatalog> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<CourseCatalog> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }

  @Override
  public CourseTypeInfos find(int id) {
    CourseTypeInfos result = new CourseTypeInfos();
    CourseCatalog cur = dao.findById(id);
    if (cur != null) {
      Integer curlevel = cur.getLevelInfo();
      if (curlevel != null && curlevel > 0) {
        List<CourseCatalog> next = dao.child(id);
        result.setNextList(next, curlevel);
        while (cur != null) {
          curlevel = cur.getLevelInfo();
          if (cur.getParent() != null) {
            List<CourseCatalog> c = dao.child(cur.getParent().getId());
            result.setList(c, curlevel, cur.getId());
            cur = dao.findById(cur.getParent().getId());
          } else {
            break;
          }
        }

      } else {
        List<CourseCatalog> c = dao.findTop();
        result.setList1(c);
      }
    } else {
      List<CourseCatalog> c = dao.findTop();
      result.setList1(c);
    }
    return result;
  }
}