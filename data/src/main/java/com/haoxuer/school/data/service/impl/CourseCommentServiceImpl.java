package com.haoxuer.school.data.service.impl;

import java.util.List;
import java.util.Map;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CourseCommentDao;
import com.haoxuer.school.data.dao.CourseDao;
import com.haoxuer.school.data.dao.CourseLessonStudentDao;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.CourseComment;
import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.school.data.service.CourseCommentService;

@Service
@Transactional
public class CourseCommentServiceImpl implements CourseCommentService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CourseComment findById(Integer id) {
    CourseComment entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CourseComment save(CourseComment bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CourseComment update(CourseComment bean) {
    Updater<CourseComment> updater = new Updater<CourseComment>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CourseComment deleteById(Integer id) {
    CourseComment bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CourseComment[] deleteByIds(Integer[] ids) {
    CourseComment[] beans = new CourseComment[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CourseCommentDao dao;
  @Autowired
  private CourseLessonStudentDao courseLessonStudentDao;

  @Autowired
  public void setDao(CourseCommentDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByType(int courseid, int classify, int curpage,
                               int pagesize) {
    // TODO Auto-generated method stub
    return dao.pageByType(courseid, classify, curpage, pagesize);
  }

  @Override
  public Pagination pageByCommentId(int commentid, int classify, int curpage,
                                    int pagesize) {
    // TODO Auto-generated method stub
    return dao.pageByCommentId(commentid, classify, curpage, pagesize);
  }

  @Transactional(readOnly = true)
  @Override
  public CourseComment findCourse(int courseid, int lession) {
    CourseComment result = null;
    Finder finder = Finder.create();
    finder.append("from  CourseComment c ");
    finder.append(" where c.course.id =" + courseid);
    finder.append(" and c.courseLessonStudent.id =" + lession);

    List<CourseComment> cs = dao.find(finder);
    if (cs != null && cs.size() > 0) {
      result = cs.get(0);
    }
    return result;
  }

  @Autowired
  CourseDao courseDao;

  @Transactional
  @Override
  public CourseComment comment(CourseComment bean) {
    dao.save(bean);
    CourseLessonStudent s = courseLessonStudentDao.findById(bean
        .getCourseLessonStudent().getId());
    if (s != null) {
      s.setCommentstate(1);

    }
    return bean;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageForCourseByType(int id, int type, int curpage,
                                        int pagesize) {
    Course c = courseDao.findById(id);
    int cid = 0;
    if (c.getCourse() != null) {
      cid = c.getCourse().getId();
    } else {
      cid = c.getId();
    }
    Finder finder = Finder.create();
    finder.append(" from CourseComment c ");
    finder.append(" where c.course.id= " + cid);
    if (type > 0) {
      finder.append(" and c.commenttype= " + type);
    }
    finder.append(" order by c.id desc ");
    return dao.find(finder, curpage, pagesize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageForMemberByType(int userid, int type, int curpage,
                                        int pagesize) {
    Finder finder = Finder.create();
    finder.append(" from CourseComment c ");
    finder.append(" where c.course.member.id= " + userid);
    if (type > 0) {
      finder.append(" and c.commenttype= " + type);
    }
    finder.append(" order by c.id desc ");
    return dao.find(finder, curpage, pagesize);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CourseComment> findByParams(Map<String, Object> params,
                                          List<String> orders) {
    Finder finder = Finder.create("from CourseComment c ");

    return dao.find(finder);
  }
}