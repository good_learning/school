package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CourseCountDao;
import com.haoxuer.school.data.entity.CourseCount;
import com.haoxuer.school.data.service.CourseCountService;

@Service
@Transactional
public class CourseCountServiceImpl implements CourseCountService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CourseCount findById(Integer id) {
    CourseCount entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CourseCount save(CourseCount bean) {

    dao.save(bean);
    return bean;
  }

  @Transactional
  public CourseCount update(CourseCount bean) {
    Updater<CourseCount> updater = new Updater<CourseCount>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CourseCount deleteById(Integer id) {
    CourseCount bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CourseCount[] deleteByIds(Integer[] ids) {
    CourseCount[] beans = new CourseCount[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CourseCountDao dao;

  @Autowired
  public void setDao(CourseCountDao dao) {
    this.dao = dao;
  }
}