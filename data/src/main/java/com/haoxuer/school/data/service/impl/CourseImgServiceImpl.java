package com.haoxuer.school.data.service.impl;

import com.haoxuer.school.data.dao.CourseImgDao;
import com.haoxuer.school.data.entity.CourseImg;
import com.haoxuer.school.data.service.CourseImgService;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CourseImgServiceImpl implements CourseImgService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CourseImg findById(Long id) {
    CourseImg entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CourseImg save(CourseImg bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CourseImg update(CourseImg bean) {
    Updater<CourseImg> updater = new Updater<CourseImg>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CourseImg deleteById(Long id) {
    CourseImg bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CourseImg[] deleteByIds(Long[] ids) {
    CourseImg[] beans = new CourseImg[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CourseImgDao dao;

  @Autowired
  public void setDao(CourseImgDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByCourse(int courseid, int pageNo, int pageSize) {
    // TODO Auto-generated method stub
    return dao.pageByCourse(courseid, pageNo, pageSize);
  }
}