package com.haoxuer.school.data.service.impl;

import com.haoxuer.school.data.dao.*;
import com.haoxuer.school.data.entity.*;
import com.haoxuer.school.data.service.CourseLessonStudentService;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CourseLessonStudentServiceImpl implements
    CourseLessonStudentService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CourseLessonStudent findById(Long id) {
    CourseLessonStudent entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CourseLessonStudent save(CourseLessonStudent bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CourseLessonStudent update(CourseLessonStudent bean) {
    Updater<CourseLessonStudent> updater = new Updater<CourseLessonStudent>(
        bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CourseLessonStudent deleteById(Long id) {
    CourseLessonStudent bean = dao.deleteById(id);
    return bean;
  }

  @Autowired
  private CourseDao courseDao;

  @Autowired
  TradingrecordDao tradingrecordDao;

  @Transactional
  public CourseLessonStudent[] deleteByIds(Long[] ids) {
    CourseLessonStudent[] beans = new CourseLessonStudent[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CourseLessonStudentDao dao;

  @Autowired
  public void setDao(CourseLessonStudentDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination findByPeriod(int period, int curseid, int pageNo,
                                 int pageSize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create();
    finder.append("from CourseLessonStudent c");
    finder.append(" where  c.course.id= " + curseid);
    finder.append(" and  c.period= " + period);
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional
  @Override
  public Pagination update(int studentid, int tradingrecordid, int pageNo,
                           int pageSize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create();
    finder.append("from CourseLessonStudent c");
    finder.append(" where  c.tradingrecord.id= " + tradingrecordid);
    finder.append(" and  c.member.id= " + studentid);
    int size = dao.countQueryResult(finder);
    if (size == 0) {

      int curseid = tradingrecordDao.findById(tradingrecordid)
          .getCourse().getId();
      Course course = courseDao.findById(curseid);
      int lessions = course.getLesson();
      List<CourseLessonStudent> lists = new ArrayList<CourseLessonStudent>();
      for (int i = 1; i <= lessions; i++) {
        CourseLessonStudent item = new CourseLessonStudent();
        item.setPeriod(i);
        item.setCourse(course);
        Member member = new Member();
        //member.setId(studentid);
        item.setMember(member);
        dao.save(item);
        lists.add(item);
      }
      result.setList(lists);
      return result;

    } else {
      return dao.find(finder, pageNo, pageSize);

    }
  }

  @Autowired
  MoneyRecordDao moneyRecordDao;

  @Transactional
  @Override
  public CourseLessonStudent finshed(int userid, Long id) {
    CourseLessonStudent entity = dao.findById(id);
    if (userid == entity.getMember().getId() && entity.getState() == 0) {
      entity.setConfirmtime(new Timestamp(System.currentTimeMillis()));
      entity.setState(1);
      /**
       * 支付
       */
      Tradingrecord record = entity.getTradingrecord();
      if (record != null) {
        if (entity.getPeriod() == 1) {
          pay(entity, record);
        }
        payok(entity, record);
      }
    }
    return entity;
  }

  @Autowired
  SchoolteacherDao schoolteacherDao;

  private void pay(CourseLessonStudent entity, Tradingrecord record) {
    /**
     * 支付算法
     */
    float money = record.getMoney();
    Member teacher = record.getCourse().getMember();
    if (teacher.getId() == 2) {

      Finder finderschool = Finder.create();
      finderschool.append("from Schoolteacher s");
      finderschool.append(" where s.teacher.id =" + teacher.getId());
      finderschool.append(" and s.state=1");
      /**
       * 要是这个老师是某个学校的，把钱给学校，不是学校的给自己
       */
      List<Schoolteacher> ss = schoolteacherDao.find(finderschool);
      if (ss != null && ss.size() > 0) {
        Schoolteacher sss = ss.get(0);
        Member school = sss.getSchool();
        float schoolmoney = school.getMoneys();
        schoolmoney = schoolmoney + money;
        school.setMoneys(schoolmoney);

        /**
         * 付款交易记录
         */
        MoneyRecord moneyRecord = new MoneyRecord();
        moneyRecord.setMoney(money);
        String s = record.getMember().getName() + "确认了课程";
        s = s + "(" + record.getCourse().getName() + ")";
        moneyRecord.setName(s);
        moneyRecord.setCourse(record.getCourse());
        moneyRecord.setState(1);
        moneyRecord
            .setAdddate(new Timestamp(System.currentTimeMillis()));
        moneyRecord.setTradingdate(new Timestamp(System
            .currentTimeMillis()));
        moneyRecord.setSeller(record.getMember());
        moneyRecord.setMember(school);
        moneyRecordDao.save(moneyRecord);
      } else {
        float teachermoney = teacher.getMoneys();
        teachermoney = teachermoney + money;
        teacher.setMoneys(teachermoney);

        /**
         * 付款交易记录
         */
        MoneyRecord moneyRecord = new MoneyRecord();
        moneyRecord.setMoney(money);
        String s = record.getMember().getName() + "确认了课程";
        s = s + "(" + record.getCourse().getName() + ")";
        moneyRecord.setName(s);
        moneyRecord.setCourse(record.getCourse());
        moneyRecord.setState(1);
        moneyRecord
            .setAdddate(new Timestamp(System.currentTimeMillis()));
        moneyRecord.setTradingdate(new Timestamp(System
            .currentTimeMillis()));
        moneyRecord.setSeller(record.getMember());
        moneyRecord.setMember(teacher);
        moneyRecordDao.save(moneyRecord);
      }

    } else if (teacher.getId() == 3) {
      float teachermoney = teacher.getMoneys();
      teachermoney = teachermoney + money;
      teacher.setMoneys(teachermoney);

      /**
       * 付款交易记录
       */
      MoneyRecord moneyRecord = new MoneyRecord();
      moneyRecord.setMoney(money);
      String s = record.getMember().getName() + "确认了课程";
      s = s + "(" + record.getCourse().getName() + ")";
      moneyRecord.setName(s);
      moneyRecord.setCourse(record.getCourse());
      moneyRecord.setState(1);
      moneyRecord.setAdddate(new Timestamp(System.currentTimeMillis()));
      moneyRecord
          .setTradingdate(new Timestamp(System.currentTimeMillis()));
      moneyRecord.setSeller(record.getMember());
      moneyRecord.setMember(teacher);
      moneyRecordDao.save(moneyRecord);
    } else {

    }
  }

  private void payok(CourseLessonStudent entity, Tradingrecord record) {
    /**
     * 支付算法
     */
    int les = entity.getCourse().getLesson() - record.getPaytime();
    float money = record.getLeftmoney() / les;

    int paytime = record.getPaytime();
    paytime++;
    record.setPaytime(paytime);
    float lmoney = record.getLeftmoney() - money;
    record.setLeftmoney(lmoney);
  }

  public CourseLessonStudent finshedback(int userid, Long id) {
    CourseLessonStudent entity = dao.findById(id);
    if (userid == entity.getMember().getId() && entity.getState() == 0) {
      entity.setConfirmtime(new Timestamp(System.currentTimeMillis()));
      entity.setState(1);

      /**
       * 支付
       */
      Tradingrecord record = entity.getTradingrecord();
      if (record != null) {

        pay(entity, record);
      }
    }
    return entity;
  }

  @Autowired
  CourseLessonTeacherDao courseLessonTeacherDao;

  @Transactional
  @Override
  public CourseLessonStudent finshed2(int userid, Long id) {
    CourseLessonStudent entity = dao.findById(id);
    if (userid == entity.getMember().getId() && entity.getState() == 0) {
      entity.setConfirmtime(new Timestamp(System.currentTimeMillis()));
      entity.setState(1);

      Tradingrecord record = entity.getTradingrecord();
      if (record != null) {
        int les = entity.getCourse().getLesson() - record.getPaytime();
        if (les > 0) {
          float money = record.getLeftmoney() / les;

          int paytime = record.getPaytime();
          paytime++;
          record.setPaytime(paytime);
          float lmoney = record.getLeftmoney() - money;
          record.setLeftmoney(lmoney);

          if (entity.getPeriod() == 1) {
            pay(entity, record);
          }

          // 老师通知
          SystemInfo teacherinfo = new SystemInfo();
          teacherinfo.setAdddate(new Timestamp(System
              .currentTimeMillis()));
          teacherinfo.setState(0);
          teacherinfo.setReadsate(0);
          teacherinfo.setMember(record.getCourse().getMember());
          String content = getteacherinfo(entity.getPeriod(), record);
          teacherinfo.setContent(content);
          systemInfoDao.save(teacherinfo);
        }

      }
      Finder f = Finder.create();
      f.append("from CourseLessonTeacher c");
      f.append(" where c.course.id = " + entity.getCourse().getId());
      f.append(" and c.period = " + entity.getPeriod());

      // 老师确认课程人数加1
      // List list = courseLessonTeacherDao.find(f);
      // if (list != null && list.size() > 0) {
      // CourseLessonTeacher c = (CourseLessonTeacher) list.get(0);
      // int csize = c.getConfirmsize();
      // csize++;
      // c.setConfirmsize(csize);
      // }
    }
    return entity;
  }

  private String getteacherinfo(int p, Tradingrecord tradingrecord) {
    // 尊敬的李娟老师（门牌号：T1102546445）您好，小明（门牌号：G1102546445）已完成了您已完成授课的中考一对一（课程编号：T1102546445）课程第10个课时的上课确认

    StringBuffer buffer = new StringBuffer();
    buffer.append("尊敬的<" + tradingrecord.getCourse().getMember().getName()
        + ">");
    buffer.append("(门牌号:"
        + tradingrecord.getCourse().getMember().getId()
        + ")你好,");
    buffer.append("<" + tradingrecord.getMember().getName() + ">");
    buffer.append("(门牌号:" + tradingrecord.getMember().getId() + ")");
    buffer.append("已完成了您已完成授课的<"
        + tradingrecord.getCourse().getMember().getName() + ">");
    buffer.append("(课程编号:" + tradingrecord.getCourse().getId() + ")");
    buffer.append("课程第");
    buffer.append(p);

    buffer.append("个课时的上课确认");
    String content = buffer.toString();
    return content;
  }

  @Autowired
  SystemInfoDao systemInfoDao;

  @Transactional
  @Override
  public CourseLessonStudent reminder(Long id) {
    CourseLessonStudent student = dao.findById(id);
    if (student != null) {
      Calendar c = Calendar.getInstance();
      c.add(Calendar.DAY_OF_YEAR, 1);
      student.setReminderDate(c.getTime());
      SystemInfo t = new SystemInfo();
      t.setAdddate(new Timestamp(System.currentTimeMillis()));
      t.setMember(student.getMember());
      t.setReadsate(0);
      t.setState(0);
      String content = "课程(" + student.getCourse().getName();
      content = content + ")已上课，请确认下";
      t.setContent(content);
      systemInfoDao.add(t);
    }
    return student;
  }

  @Transactional
  @Override
  public CourseLessonStudent invitecomments(long id) {
    CourseLessonStudent student = dao.findById(id);
    if (student != null) {
      Calendar c = Calendar.getInstance();
      c.add(Calendar.DAY_OF_YEAR, 1);
      student.setInviteDate(c.getTime());

      SystemInfo t = new SystemInfo();
      t.setAdddate(new Timestamp(System.currentTimeMillis()));
      t.setMember(student.getMember());
      t.setReadsate(0);
      t.setState(0);
      String content = "课程(" + student.getCourse().getName();
      content = content + ")需要你的评价";
      t.setContent(content);
      systemInfoDao.add(t);
    }
    return student;
  }

  @Transactional
  @Override
  public CourseLessonStudent updateInfo(long id, String info) {
    CourseLessonStudent student = dao.findById(id);
    if (student != null) {
      student.setStateinfo(info);
    }
    return student;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination getPagerData(String select, Map<String, Object> params,
                                 int pageNo,
                                 int pageSize) {

    Finder finder = Finder.create();
    // if (select != null && !select.equals("")) {
    // finder.append("from courselesson c");
    // }
    finder.append("from courselesson c");

    if (params != null && params.size() > 0) {
      finder.append(" where ");
      int i = 0;
      String pname = "";
      for (String key : params.keySet()) {
        pname = "p" + String.valueOf(i);
        finder.append("c." + key + " = :" + pname);
        finder.setParam(pname, params.get(key));
        i++;
      }
    }
    return dao.find(finder, pageNo, pageSize);
  }

  public int getCount(Map<String, Object> params, int pageNo, int pageSize) {
    Finder finder = Finder.create();
    finder.append("from CourseLessonStudent c ");
    if (params != null && params.size() > 0) {
      finder.append(" where ");
      int i = 0;
      String pname = "";
      for (String key : params.keySet()) {
        pname = "p" + String.valueOf(i);
        finder.append("c." + key + " = :" + pname);
        finder.setParam(pname, params.get(key));
        i++;
      }
    }
    return dao.countQueryResult(finder);
  }
}