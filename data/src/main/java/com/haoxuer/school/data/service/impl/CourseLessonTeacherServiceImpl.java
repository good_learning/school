package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.haoxuer.school.data.dao.CourseLessonStudentDao;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CourseDao;
import com.haoxuer.school.data.dao.CourseLessonTeacherDao;
import com.haoxuer.school.data.dao.SystemInfoDao;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.CourseLessonStudent;
import com.haoxuer.school.data.entity.CourseLessonTeacher;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.service.CourseLessonTeacherService;

@Service
@Transactional
public class CourseLessonTeacherServiceImpl implements
    CourseLessonTeacherService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CourseLessonTeacher findById(Long id) {
    CourseLessonTeacher entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CourseLessonTeacher save(CourseLessonTeacher bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CourseLessonTeacher update(CourseLessonTeacher bean) {
    Updater<CourseLessonTeacher> updater = new Updater<CourseLessonTeacher>(
        bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CourseLessonTeacher deleteById(Long id) {
    CourseLessonTeacher bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CourseLessonTeacher[] deleteByIds(Long[] ids) {
    CourseLessonTeacher[] beans = new CourseLessonTeacher[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CourseLessonTeacherDao dao;

  @Autowired
  private CourseDao courseDao;

  @Autowired
  public void setDao(CourseLessonTeacherDao dao) {
    this.dao = dao;
  }

  @Transactional
  @Override
  public Pagination findBySchoolId(int schoolid, int curseid, int pageNo,
                                   int pageSize) {
    Course course = courseDao.findById(curseid);

    Pagination result = new Pagination();
    Finder finder = Finder.create();
    finder.append("from CourseLessonTeacher c");
    finder.append(" where  c.course.id= " + curseid);
    finder.append(" and  c.member.id= " + course.getTeacher().getId());
    int size = dao.countQueryResult(finder);
    if (size == 0) {
      int lessions = course.getLesson();
      List<CourseLessonTeacher> lists = new ArrayList<CourseLessonTeacher>();
      for (int i = 1; i <= lessions; i++) {
        CourseLessonTeacher item = new CourseLessonTeacher();
        item.setPeriod(i);
        item.setCourse(course);
        Member member = new Member();
        member.setId(course.getTeacher().getId());
        item.setMember(member);
        dao.save(item);
        lists.add(item);
      }
      result.setList(lists);
      return result;

    } else {
      return dao.find(finder, pageNo, pageSize);

    }
    // TODO Auto-generated method stub
  }

  @Autowired
  SystemInfoDao systemInfoDao;

  @Autowired
  CourseLessonStudentDao studentDao;

  @Transactional
  @Override
  public CourseLessonTeacher finshed(int userid, Long id) {
    CourseLessonTeacher entity = dao.findById(id);
    if (userid == entity.getMember().getId()) {
      entity.setState(1);
      int courseid = entity.getCourse().getId();
      int period = entity.getPeriod();
      Finder finder = Finder.create("from CourseLessonStudent c ");
      finder.append(" where c.course.id =" + courseid);
      finder.append(" and c.period=" + period);
      List<CourseLessonStudent> sts = studentDao.find(finder);
      if (sts != null && sts.size() > 0) {
        for (CourseLessonStudent courseLessonStudent : sts) {
          courseLessonStudent.setTeacherstate(1);
          courseLessonStudent.setTeachingtime(new Timestamp(System
              .currentTimeMillis()));


          //老师通知
          SystemInfo teacherinfo = new SystemInfo();
          teacherinfo.setAdddate(new Timestamp(System.currentTimeMillis()));
          teacherinfo.setState(0);
          teacherinfo.setReadsate(0);
          teacherinfo.setMember(courseLessonStudent.getMember());
          String content = getteacherinfo(entity.getPeriod(), courseLessonStudent.getTradingrecord());
          teacherinfo.setContent(content);
          systemInfoDao.save(teacherinfo);
        }
      }

      entity.setConfirmtime(new Timestamp(System.currentTimeMillis()));

      Course course = entity.getCourse();
      if (course != null) {
        Integer lesson = course.getConfirmlesson();
        if (lesson == null) {
          lesson = 0;
        }
        lesson++;
        if (lesson == course.getLesson()) {
          course.setState(2);
        }
        course.setConfirmlesson(lesson);
      }
    }
    return entity;
  }

  private String getteacherinfo(int p, Tradingrecord tradingrecord) {
    //尊敬的小明（门牌号：G1102546445）您好，您正在进行的学大教育（门牌号：S4564564565）（或李娟老师（门牌号：T1102546445））的中考一对一（课程编号：T1102546445）课程第10个课时被授课老师已完成授课并邀请您进行课时确认，请及时进行处理

    StringBuffer buffer = new StringBuffer();
    buffer.append("尊敬的<" + tradingrecord.getMember().getName() + ">");
    buffer.append("(门牌号:" + tradingrecord.getMember().getId() + ")你好,");
    buffer.append("您正在进行的<" + tradingrecord.getCourse().getMember().getName() + ">");
    buffer.append("(门牌号:" + tradingrecord.getCourse().getMember().getId() + ")");
    buffer.append("的<" + tradingrecord.getCourse().getMember().getName() + ">");
    buffer.append("(课程编号:" + tradingrecord.getCourse().getId() + ")");
    buffer.append("课程第");
    buffer.append(p);
    buffer.append("个课时被授课老师已完成授课并邀请您进行课时确认，请及时进行处理");
    String content = buffer.toString();
    return content;
  }
}