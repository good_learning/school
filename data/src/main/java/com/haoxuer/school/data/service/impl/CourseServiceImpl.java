package com.haoxuer.school.data.service.impl;

import com.haoxuer.school.data.dao.*;
import com.haoxuer.school.data.entity.*;
import com.haoxuer.school.data.service.CourseService;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CourseServiceImpl implements CourseService {

  @Autowired
  private CourseDao courseDao;

  @Autowired
  private KeyWordDao keyWordDao;

  @Autowired
  NumGenerateDao numGenerateDao;

  public static String v(int num, int length) {

    String resutlt = "" + num;
    int left = length - resutlt.length();
    if (left < 0) {
      left = 0;
    }
    for (int i = 0; i < left; i++) {
      resutlt = "0" + resutlt;
    }
    return resutlt;

  }

  @Transactional
  @Override
  public Course add(Course Course) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmm");
    String key = dateFormat.format(new Date());
    NumGenerate n = numGenerateDao.findkey(key, "course");
    int num = n.getNum();
    num++;
    n.setNum(num);
    String code = key + "1" + v(num, 4);
    Course.setCode(code);

    return courseDao.addCourse(Course);
  }

  public CourseDao getCourseDao() {
    return courseDao;
  }

  public void setCourseDao(CourseDao courseDao) {
    this.courseDao = courseDao;
  }

  @Transactional
  @Override
  public boolean deleteCourse(Course c) {
    return courseDao.deleteCourse(c);
  }

  @Override
  public List<Course> findbyuser(int uid) {
    // TODO Auto-generated method stub
    return courseDao.findByUser(uid);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByType(int uid, int sourtype, int curpage,
                               int pagesize) {
    Pagination result = courseDao.pageByType(uid, sourtype, curpage,
        pagesize);

    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Course findById(int id) {
    // TODO Auto-generated method stub
    return courseDao.findById(id);
  }

  @Transactional
  @Override
  public Course up(int id) {
    // TODO Auto-generated method stub
    return courseDao.up(id);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByTransactionType(int type, int sourtype,
                                          int curpage, int pagesize) {
    // TODO Auto-generated method stub
    return courseDao.pageByTransactionType(type, sourtype, curpage,
        pagesize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUser(int userid, int statetype, int sourtype,
                               int curpage, int pagesize) {
    // TODO Auto-generated method stub
    return courseDao.pageByUser(userid, statetype, sourtype, curpage,
        pagesize);
  }

  @Transactional
  @Override
  public void add(Course course, String[] imgurls) {
    course.setViews(1);
    course.setUps(1);
    course = courseDao.addCourse(course);
    if (imgurls != null) {
      for (String string : imgurls) {
        CourseImg img = new CourseImg();
        img.setFileurl(string);
        img.setFiletype(FilenameUtils.getExtension(string).toLowerCase(
            Locale.ENGLISH));
        img.setAdddate(new Timestamp(System.currentTimeMillis()));
        img.setMember(course.getMember());
        img.setCatalog(1);
        img.setCourse(course);
        img.setName(course.getName());
        courseImgDao.save(img);
      }
    }
  }

  @Autowired
  CourseImgDao courseImgDao;

  @Autowired
  private CourseCatalogDao courseCatalogDao;


  @Transactional(readOnly = true)
  @Override
  public List<Course> goodByUser(int id, int size) {
    List<Course> cs = (List<Course>) courseDao.goodByUser(id, 1, size)
        .getList();
    return cs;
  }

  @Transactional
  @Override
  public Course update(Course Course) {

    return courseDao.update(Course);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination good(int typeid, int curpage, int size) {
    Pagination result = new Pagination();
    CourseCatalog courseCatalog = this.courseCatalogDao.findById(typeid);
    if (courseCatalog != null) {
      Finder finder = Finder.create("from Course c ");
      finder.append(" where c.courseCatalog.lft>=");
      finder.append("" + courseCatalog.getLft());
      finder.append(" and c.courseCatalog.rgt <=" + courseCatalog.getRgt());
      finder.append(" and c.isgood = 1 order by c.ups desc ");
      result = courseDao.find(finder, curpage, size);
    }
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination teaching(int userid, int statetype, int curpage,
                             int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.member.id = " + userid);
    finder.append(" and c.statetype = " + statetype);

    result = courseDao.find(finder, curpage, pagesize);

    return result;
  }

  @Transactional
  @Override
  public Course setTeacher(int courseid, int teacherid) {
    Course course = courseDao.findById(courseid);
    if (course != null) {
      Member teacher = new Member();
      //teacher.setId(teacherid);
      course.setTeacher(teacher);
    }
    return course;
  }

  @Transactional
  @Override
  public Course updateState(int courseid, int state) {
    Course course = courseDao.findById(courseid);
    if (course != null) {
      course.setState(state);
    }
    return course;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination teachings(int userid, int state, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.member.id = " + userid);
    finder.append(" and c.state = " + state);
    finder.append(" order by c.id  desc ");
    // finder.append(" and c.statetype = 1");
    result = courseDao.find(finder, curpage, pagesize);

    return result;
  }

  @Transactional
  @Override
  public Course updateState(int courseid, int userid, int state) {
    Course course = courseDao.findById(courseid);
    if (course != null) {
      course.setState(state);
      Member teacher = new Member();
     // teacher.setId(userid);
      course.setTeacher(teacher);
    }
    return course;
  }

  public String getMoshi(int s) {
    String result = "";
    if (s == 1) {
      result = "一对一";
    } else if (s == 2) {
      result = "班组";
    } else {
      result = "";
    }
    return result;
  }

  public static String TransactSQLInjection(String str) {
    return str.replaceAll(".*([';]+|(--)+).*", " ");

  }

  public String filtRiskChar(String str) // 过滤非法字符
  {
    String s = "";

    s = str.replace("'", " ");
    s = s.replace(";", " ");
    s = s.replace("1=1", " ");
    s = s.replace("|", " ");
    s = s.replace("<", " ");
    s = s.replace(">", " ");

    return s;
  }

  @Transactional()
  @Override
  public Pagination search(int id, int townid, int typeid, int coursestate,
                           int sorttype, String keyword, int curpage, int pagesize) {

    KeyWord bean = new KeyWord();
    bean.setName(keyword);
    keyWordDao.save(bean);

    keyword = TransactSQLInjection(keyword);
    keyword = filtRiskChar(keyword);

    Pagination result = new Pagination();
    //CourseCatalog courseCatalog = coursetypeDao.findById(id);
//		if (courseCatalog != null) {
//			Finder finder = Finder.create("from Course c ");
//			finder.append(" where c.courseCatalog.lft>=");
//			finder.append("" + courseCatalog.getLft());
//			finder.append(" and c.courseCatalog.rgt <=" + courseCatalog.getRgt());
//			if (townid > 0) {
//				finder.append(" and c.member.town.id =" + townid);
//			}
//			if (coursestate > 0) {
//				finder.append(" and c.shoukemoshi = '" + getMoshi(coursestate)
//						+ "'");
//			}
//			if (typeid > 0) {
//				if (typeid == 1) {
//					finder.append(" and c.member.membertype.id =3 ");
//				} else {
//					finder.append(" and c.member.membertype.id =2 ");
//				}
//			}
//			if (keyword != null && keyword.trim().length() > 0) {
//				finder.append(" and c.name like '%" + keyword.trim() + "%'");
//			}
//			finder.append(" and c.statetype =1 ");
//
//			if (sorttype == 1) {
//				finder.append(" order by c.views desc");
//			} else if (sorttype == 2) {
//				finder.append(" order by c.realprice asc");
//			} else {
//				finder.append(" order by c.id desc");
//			}
//			result = courseDao.find(finder, curpage, pagesize);
//
//		}
    return result;
  }

  @Transactional
  @Override
  public Course view(int id) {
    Course entity = courseDao.findById(id);
    Integer num = entity.getViews();
    Integer wnum = entity.getViewsofweek();

    if (num == null) {
      num = 1;
    }
    if (wnum == null) {
      wnum = 1;
    }
    num++;
    wnum++;
    entity.setViews(num);
    entity.setViewsofweek(wnum);
    return entity;

  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByCount(int typeid, int curpage, int pagesize) {
    Pagination result = new Pagination();
//		CourseCatalog courseCatalog = this.coursetypeDao.findById(typeid);
//		if (courseCatalog != null) {
//			Finder finder = Finder.create("from Course c ");
//			finder.append(" where c.courseCatalog.lft>=");
//			finder.append("" + courseCatalog.getLft());
//			finder.append(" and c.courseCatalog.rgt <=" + courseCatalog.getRgt());
//			finder.append("  order by c.viewsofweek desc ");
//			result = courseDao.find(finder, curpage, pagesize);
//		}
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination showmyspecialcourselist(int userid, int statetype,
                                            int sorttype, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.member.id = " + userid);
    finder.append(" and c.statetype = " + statetype);
    finder.append(" and c.specialstate =1 ");

    if (sorttype == 1) {
      finder.append("order by c.id desc");
    } else if (sorttype == 2) {
      finder.append("order by c.ups desc");
    } else if (sorttype == 3) {
      finder.append("order by c.ups desc");
    } else if (sorttype == 4) {
      finder.append("order by c.ups desc");
    } else if (sorttype == 5) {
      finder.append("order by c.member.id desc");
    }
    result = courseDao.find(finder, curpage, pagesize);

    return result;
  }

  @Transactional
  @Override
  public Course update(Course course, String[] imgurls) {
    Updater<Course> updater = new Updater<Course>(course);
    updater.include("duedate").include("begintime");
    //courseDao.updateByUpdater(updater);
    Pagination page = courseImgDao.pageByCourse(course.getId(), 1, 100);
    List<?> imgs = page.getList();
    if (imgs != null) {
      for (Object object : imgs) {
        if (object instanceof CourseImg) {
          CourseImg item = (CourseImg) object;
          courseImgDao.deleteById(item.getId());
        }

      }
    }
    if (imgurls != null) {
      for (String string : imgurls) {
        CourseImg img = new CourseImg();
        img.setFileurl(string);
        img.setFiletype(FilenameUtils.getExtension(string).toLowerCase(
            Locale.ENGLISH));
        img.setAdddate(new Timestamp(System.currentTimeMillis()));
        img.setMember(course.getMember());
        img.setCatalog(1);
        img.setCourse(course);
        img.setName(course.getName());
        courseImgDao.save(img);
      }
    }
    return course;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pagegood(int curpage, int size) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.isgood = 1 order by c.id desc ");
    result = courseDao.find(finder, curpage, size);
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination search(String keyword, int curpage, int size) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.isgood != 1  or c.isgood is null");
    finder.append(" and c.name like  '%" + keyword + "%'");
    finder.append("  order by c.id desc ");
    result = courseDao.find(finder, curpage, size);
    return result;
  }

  @Transactional
  @Override
  public Course recommenduser(Integer id) {
    Course c = courseDao.findById(id);
    c.setIsgood(1);
    return c;
  }

  @Transactional
  @Override
  public Course recommenddeluser(Integer id) {
    Course c = courseDao.findById(id);
    c.setIsgood(0);
    return c;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination similar(int courseid, int curpage, int size) {
    Pagination result = new Pagination();
    Course c = courseDao.findById(courseid);
    CourseCatalog courseCatalog = c.getCourseCatalog();
    if (courseCatalog != null) {
      Finder finder = Finder.create("from Course c ");
      finder.append(" where c.courseCatalog.lft>=");
      finder.append("" + courseCatalog.getLft());
      finder.append(" and c.courseCatalog.rgt <=" + courseCatalog.getRgt());
      finder.append("  order by c.ups desc ");
      result = courseDao.find(finder, curpage, size);
    }
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination teachingsForStar(int userid, int state, int curpage,
                                     int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    // 招满人了
    finder.append(" where ( c.member.id = " + userid);
    finder.append(" and c.state = " + state);
    finder.append(" and c.statetype = 1");
    finder.append(" and c.jointimes =c.times");
    finder.append(" and c.jointimes>0)");
    // 招人以后到期了
    finder.append(" or (");
    finder.append("   c.member.id = " + userid);
    finder.append(" and c.state = " + state);
    // finder.append(" and c.statetype = 1");
    finder.append(" and c.jointimes >0");
    Date now = new Date();
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    finder.append(" and c.duedate < '" + format.format(now) + "'");
    finder.append(")");
    // 停止以后的课程
    finder.append(" or (");
    finder.append("   c.member.id = " + userid);
    finder.append(" and c.state = " + state);
    finder.append(" and c.stopstate=1 )");
    // (c.stopstate=1 and c.statetype = 1 and c.jointimes>0 and c.duedate <
    // CURRENT_DATE())
    // finder.append(" or (c.duedate < CURRENT_DATE())");
    finder.append(" order by  c.duedate desc");
    result = courseDao.find(finder, curpage, pagesize);

    return result;
  }

  @Transactional
  @Override
  public Course startCourse(int courseid) {
    Course course = courseDao.findById(courseid);
    if (course != null) {
      course.setState(1);
      course.setStartDate(new Timestamp(System.currentTimeMillis()));
    }
    return course;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination starCourses(int userid, int state, int curpage,
                                int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.member.id = " + userid);
    finder.append(" and c.state = " + state);
    finder.append(" and c.statetype = 1");
    finder.append(" order by  c.startDate desc");

    result = courseDao.find(finder, curpage, pagesize);

    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination teachingsForTeacher(int id, int i, int curpage,
                                        int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder.create("from Course c ");
    finder.append(" where c.teacher.id = " + id);
    finder.append(" and c.state =" + i);
    // finder.append(" and c.jointimes >0 ");
    result = courseDao.find(finder, curpage, pagesize);

    return result;
  }

  @Transactional
  @Override
  public Course startCourseForTeacher(int courseid) {
    Course course = courseDao.findById(courseid);
    if (course != null) {
      course.setTeacher(course.getMember());
      course.setState(1);
      course.setStopstate(1);
      course.setStartDate(new Timestamp(System.currentTimeMillis()));
    }
    return course;
  }

  @Transactional()
  @Override
  public Pagination search(Map<String, Object> params, String[] orders,
                           String keyword,
                           int curpage, int pagesize) {

    KeyWord bean = new KeyWord();
    bean.setName(keyword);
    keyWordDao.save(bean);

    keyword = TransactSQLInjection(keyword);
    keyword = filtRiskChar(keyword);


    Pagination result = new Pagination();

    Finder finder = Finder.create("from Course c ");
    if ((null != params && params.size() > 0)
        || (keyword != null && keyword.trim().length() > 0)) {
      finder.append("where c.statetype = 1");
      if (null != params) {
        // 填充查询条件
        this.buildWhere(finder, params, true);
        this.buildOrder(finder, orders);
      }
    }

    result = courseDao.find(finder, curpage, pagesize);

    return result;
  }

  public void buildWhere(Finder finder, Map<String, Object> params,
                         boolean hasCriteria) {
    if (null != params && !params.isEmpty()) {
      String p = "", operator = "";
      Pattern patern = Pattern.compile("\\w*(=|like|\\<|\\<=|\\>|>=)",
          Pattern.CASE_INSENSITIVE);
      Matcher m = null;
      int i = 0;
      for (String key : params.keySet()) {
        m = patern.matcher(key);
        while (m.find()) {
          operator = m.group();
          break;
        }
        p = ":" + key.replace(operator, "").trim().replace(".", "");
        if (hasCriteria == false) {
          finder.append(" c." + key + p);
          hasCriteria = true;
        } else {
          finder.append(" and c." + key + p);
        }

        if (operator.toLowerCase().equals("like")) {
          finder.setParam(p.substring(1), "%" + params.get(key) + "%");
        } else {
          finder.setParam(p.substring(1), params.get(key));
        }
      }
    }
  }

  public void buildOrder(Finder finder, String[] orders) {
    // 设置排序字段
    if (null != orders && orders.length > 0) {
      finder.append(" order by c." + orders[0]);
      for (int i = 1; i < orders.length; i++) {
        finder.append(" and c." + orders[i]);
      }
    }
  }
}
