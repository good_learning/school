package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CouponDao;
import com.haoxuer.school.data.dao.CourseDao;
import com.haoxuer.school.data.dao.CourseSubscribeDao;
import com.haoxuer.school.data.dao.KeyWordDao;
import com.haoxuer.school.data.dao.NumGenerateDao;
import com.haoxuer.school.data.dao.TradingrecordDao;
import com.haoxuer.school.data.entity.CouponInfo;
import com.haoxuer.school.data.entity.Course;
import com.haoxuer.school.data.entity.CourseSubscribe;
import com.haoxuer.school.data.entity.NumGenerate;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.service.CourseSubscribeService;

@Service
@Transactional
public class CourseSubscribeServiceImpl implements CourseSubscribeService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CourseSubscribe findById(Long id) {
    CourseSubscribe entity = dao.findById(id);
    return entity;
  }

  @Autowired
  private KeyWordDao keyWordDao;

  @Autowired
  NumGenerateDao numGenerateDao;

  public static String v(int num, int length) {

    String resutlt = "" + num;
    int left = length - resutlt.length();
    if (left < 0) {
      left = 0;
    }
    for (int i = 0; i < left; i++) {
      resutlt = "0" + resutlt;
    }
    return resutlt;

  }


  @Transactional
  public CourseSubscribe save(CourseSubscribe bean) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmm");
    String key = dateFormat.format(new Date());
    NumGenerate n = numGenerateDao.findkey(key, "CourseSubscribe" + bean.getState());
    int num = n.getNum();
    num++;
    n.setNum(num);
    String x = "";
    if (bean.getState() == 1) {
      x = "2";
    } else {
      x = "3";
    }
    String code = key + x + v(num, 4);
    bean.setCode(code);
    dao.save(bean);
    return bean;
  }

  @Transactional
  public CourseSubscribe update(CourseSubscribe bean) {
    Updater<CourseSubscribe> updater = new Updater<CourseSubscribe>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CourseSubscribe deleteById(Long id) {
    CourseSubscribe bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CourseSubscribe[] deleteByIds(Long[] ids) {
    CourseSubscribe[] beans = new CourseSubscribe[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CourseSubscribeDao dao;
  @Autowired
  private TradingrecordDao tradingrecordDao;

  @Autowired
  CourseDao courseDao;

  @Autowired
  public void setDao(CourseSubscribeDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserId(int userid, int pageNo, int pageSize) {
    // TODO Auto-generated method stub
    return dao.pageByUserId(userid, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserForTeacher(int userid, int type, int pageNo,
                                         int pageSize) {
    Finder finder = Finder.create("");
    finder.append("select ");
    finder.append("c.id,");
    finder.append("c.course.name, ");
    finder.append("c.course.realprice,");
    finder.append("c.adddate,");
    finder.append("c.member.name ");

    finder.append(" from CourseSubscribe c");
    finder.append(" where c.course.member.id= " + userid);
    finder.append(" and c.state= " + type);

    return dao.find(finder, pageNo, pageSize);
  }

  @Autowired
  CouponDao couponDao;

  @Transactional
  @Override
  public CourseSubscribe order(CourseSubscribe bean) {
    /**
     * 立即报名下订单
     */
    Tradingrecord tradingrecord = new Tradingrecord();
    tradingrecord.setPaytime(0);
    Course course = courseDao.findById(bean.getCourse().getId());
    if (bean.getState() == 1) {
      tradingrecord.setCourse(course);
      tradingrecord.setOrdertype(1);
      tradingrecord.setState(1);
      tradingrecord.setAdddate(new Timestamp(System.currentTimeMillis()));
      if (bean.getCouponInfo() != null) {
        CouponInfo coupon = couponDao.findById(bean.getCouponInfo()
            .getId());
        if (coupon.getState() == 0) {

          float money = course.getRealprice() - coupon.getMoney();
          if (money < 0) {
            money = 0;
          }
          coupon.setUsedate(new Timestamp(System.currentTimeMillis()));
          coupon.setState(1);
          tradingrecord.setMoney(money);
          tradingrecord.setLeftmoney(money);

        } else {
          tradingrecord.setMoney(course.getRealprice());
          tradingrecord.setLeftmoney(course.getRealprice());

        }
      } else {
        tradingrecord.setMoney(course.getRealprice());
        tradingrecord.setLeftmoney(course.getRealprice());

      }
      tradingrecord.setMember(bean.getMember());
      tradingrecord.setRefundstate(0);
      tradingrecord.setDeletestate(0);
      tradingrecordDao.save(tradingrecord);
      bean.setTradingrecord(tradingrecord);
    }
    bean.setStatetype(1);
    dao.save(bean);
    bean = dao.findById(bean.getId());
    bean.setCourse(course);
    bean.getMember();
    return bean;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUser(int id, int statetype, int curpage,
                               int pagesize) {
    Finder finder = Finder.create("");
    finder.append("from  CourseSubscribe s");
    finder.append(" where s.member.id = " + id);
    finder.append(" and s.state = 2");
    finder.append(" and s.statetype = " + statetype);
    finder.append("  order by s.id desc ");

    return dao.find(finder, curpage, pagesize);

  }

  @Transactional
  @Override
  public CourseSubscribe delete(long id) {
    CourseSubscribe c = dao.findById(id);
    if (c != null) {
      c.setStatetype(100);
    }
    return c;
  }

  @Transactional
  @Override
  public CourseSubscribe back(long id) {
    CourseSubscribe c = dao.findById(id);
    if (c != null) {
      c.setStatetype(1);
    }
    return c;
  }

  @Transactional
  @Override
  public CourseSubscribe remove(long id) {
    CourseSubscribe c = dao.findById(id);
    if (c != null) {
      c.setStatetype(1000);
    }
    return c;
  }

  @Transactional
  @Override
  public CourseSubscribe handle(long id, String msg) {
    CourseSubscribe c = dao.findById(id);
    if (c != null) {
      c.setHandlestate(1);
      c.setMsg(msg);
    }
    return c;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserForTeacherGood(int userid, int type,
                                             int pageNo, int pageSize) {
    Finder finder = Finder.create("");
    finder.append(" from CourseSubscribe c");
    finder.append(" where c.course.member.id= " + userid);
    finder.append(" and c.statetype=1 ");
    finder.append(" and c.state= " + type);

    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserForRecyclers(int userid, int type, int pageNo,
                                           int pageSize) {
    Finder finder = Finder.create("");
    finder.append(" from CourseSubscribe c");
    finder.append(" where c.course.member.id= " + userid);
    finder.append(" and c.statetype=100");
    finder.append(" and c.state= " + type);

    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageForRecyclers(int id, int state, int curpage,
                                     int pagesize) {
    Finder finder = Finder.create("");
    finder.append("from  CourseSubscribe s");
    finder.append(" where s.member.id = " + id);
    finder.append(" and s.state = " + state);
    finder.append(" and s.statetype =100 ");
    finder.append("  order by s.id desc ");

    return dao.find(finder, curpage, pagesize);
  }
}