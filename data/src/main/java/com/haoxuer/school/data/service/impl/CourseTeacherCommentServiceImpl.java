package com.haoxuer.school.data.service.impl;

import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.CourseLessonTeacherDao;
import com.haoxuer.school.data.dao.CourseTeacherCommentDao;
import com.haoxuer.school.data.entity.CourseTeacherComment;
import com.haoxuer.school.data.service.CourseTeacherCommentService;

@Service
@Transactional
public class CourseTeacherCommentServiceImpl implements
    CourseTeacherCommentService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public CourseTeacherComment findById(Long id) {
    CourseTeacherComment entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public CourseTeacherComment save(CourseTeacherComment bean) {
    dao.save(bean);
    return bean;
  }

  @Autowired
  CourseLessonTeacherDao courseLessonTeacherDao;

  @Transactional
  @Override
  public CourseTeacherComment comment(CourseTeacherComment bean) {
    dao.save(bean);
//		CourseLessonTeacher s = courseLessonTeacherDao.findById(bean.getCourseLessonTeacher().getId());
//		if (s != null) {
//			s.setCommentstate(1);
//		}
    return bean;
  }

  @Transactional
  public CourseTeacherComment update(CourseTeacherComment bean) {
    Updater<CourseTeacherComment> updater = new Updater<CourseTeacherComment>(
        bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public CourseTeacherComment deleteById(Long id) {
    CourseTeacherComment bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public CourseTeacherComment[] deleteByIds(Long[] ids) {
    CourseTeacherComment[] beans = new CourseTeacherComment[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private CourseTeacherCommentDao dao;

  @Autowired
  public void setDao(CourseTeacherCommentDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public CourseTeacherComment findCourse(int courseid, int lession) {
    CourseTeacherComment result = null;
    Finder finder = Finder.create();
    finder.append("from  CourseTeacherComment c ");
    finder.append(" where c.course.id =" + courseid);
    finder.append(" and c.courseLessonStudent.id =" + lession);

    List<CourseTeacherComment> cs = dao.find(finder);
    if (cs != null && cs.size() > 0) {
      result = cs.get(0);
    }
    return result;
  }

}