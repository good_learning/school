package com.haoxuer.school.data.service.impl;

import com.haoxuer.school.data.dao.FavArtilceDao;
import com.haoxuer.school.data.entity.FavArticle;
import com.haoxuer.school.data.service.FavArtilceService;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FavArtilceServiceImpl implements FavArtilceService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public FavArticle findById(Integer id) {
    FavArticle entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public FavArticle save(FavArticle bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public FavArticle update(FavArticle bean) {
    Updater<FavArticle> updater = new Updater<FavArticle>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public FavArticle deleteById(Integer id) {
    FavArticle bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public FavArticle[] deleteByIds(Integer[] ids) {
    FavArticle[] beans = new FavArticle[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private FavArtilceDao dao;

  @Autowired
  public void setDao(FavArtilceDao dao) {
    this.dao = dao;
  }

  @Transactional
  @Override
  public int fav(Long userid, int articleid) {
    // TODO Auto-generated method stub
    return dao.fav(userid, articleid);
  }


  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserId(Long id, int curpage, int pagesize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("select u.article from FavArticle u where u.member.id = ");
    finder.append("" + id);
    finder.append("  order by u.id desc");
    result = dao.find(finder, curpage, pagesize);
    return result;
  }
}