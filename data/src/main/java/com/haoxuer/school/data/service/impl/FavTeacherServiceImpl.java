package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.FavTeacherDao;
import com.haoxuer.school.data.entity.FavTeacher;
import com.haoxuer.school.data.service.FavTeacherService;

@Service
@Transactional
public class FavTeacherServiceImpl implements FavTeacherService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public FavTeacher findById(Integer id) {
    FavTeacher entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public FavTeacher save(FavTeacher bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public FavTeacher update(FavTeacher bean) {
    Updater<FavTeacher> updater = new Updater<FavTeacher>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public FavTeacher deleteById(Integer id) {
    FavTeacher bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public FavTeacher[] deleteByIds(Integer[] ids) {
    FavTeacher[] beans = new FavTeacher[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private FavTeacherDao dao;

  @Autowired
  public void setDao(FavTeacherDao dao) {
    this.dao = dao;
  }

  @Override
  public int fav(Long userid, int courseid) {
    // TODO Auto-generated method stub
    //return dao.fav(userid, courseid);
    return 1;
  }


  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUser(Long userid, int type, int pageNo, int pageSize) {
    String queryString = "from FavTeacher u where";
    Finder finder = Finder.create(queryString);
    finder.append("  u.teacher.membertype.id = ");
    finder.append("" + type);

    finder.append(" and u.member.id = ");
    finder.append("" + userid);
    finder.append("  order by u.id desc ");
    return dao.find(finder, pageNo, pageSize);
  }


}