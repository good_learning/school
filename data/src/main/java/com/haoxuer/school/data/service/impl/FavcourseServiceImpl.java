package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.FavcourseDao;
import com.haoxuer.school.data.entity.Favcourse;
import com.haoxuer.school.data.service.FavcourseService;

@Service
@Transactional
public class FavcourseServiceImpl implements FavcourseService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Favcourse findById(Integer id) {
    Favcourse entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Favcourse save(Favcourse bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Favcourse update(Favcourse bean) {
    Updater<Favcourse> updater = new Updater<Favcourse>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Favcourse deleteById(Integer id) {
    Favcourse bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Favcourse[] deleteByIds(Integer[] ids) {
    Favcourse[] beans = new Favcourse[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private FavcourseDao dao;

  @Autowired
  public void setDao(FavcourseDao dao) {
    this.dao = dao;
  }

  @Override
  public int fav(Long userid, int courseid) {
    // TODO Auto-generated method stub
    return dao.fav(userid, courseid);
  }

  @Override
  public Pagination pageByUserId(Long userid, int pageNo, int pageSize) {
    // TODO Auto-generated method stub
    return dao.pageByUserId(userid, pageNo, pageSize);
  }
}