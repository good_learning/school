package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.FavschoolDao;
import com.haoxuer.school.data.entity.Favschool;
import com.haoxuer.school.data.service.FavschoolService;

@Service
@Transactional
public class FavschoolServiceImpl implements FavschoolService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Favschool findById(Integer id) {
    Favschool entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Favschool save(Favschool bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Favschool update(Favschool bean) {
    Updater<Favschool> updater = new Updater<Favschool>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Favschool deleteById(Integer id) {
    Favschool bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Favschool[] deleteByIds(Integer[] ids) {
    Favschool[] beans = new Favschool[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private FavschoolDao dao;

  @Autowired
  public void setDao(FavschoolDao dao) {
    this.dao = dao;
  }
}