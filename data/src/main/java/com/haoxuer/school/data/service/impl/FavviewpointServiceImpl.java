package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.FavviewpointDao;
import com.haoxuer.school.data.entity.Favviewpoint;
import com.haoxuer.school.data.service.FavviewpointService;

@Service
@Transactional
public class FavviewpointServiceImpl implements FavviewpointService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Favviewpoint findById(Long id) {
    Favviewpoint entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Favviewpoint save(Favviewpoint bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Favviewpoint update(Favviewpoint bean) {
    Updater<Favviewpoint> updater = new Updater<Favviewpoint>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Favviewpoint deleteById(Long id) {
    Favviewpoint bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Favviewpoint[] deleteByIds(Long[] ids) {
    Favviewpoint[] beans = new Favviewpoint[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private FavviewpointDao dao;

  @Autowired
  public void setDao(FavviewpointDao dao) {
    this.dao = dao;
  }

  @Override
  public int fav(Long userid, Long courseid) {
    // TODO Auto-generated method stub
    return dao.fav(userid, courseid);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserId(Long userid, int pageNo, int pageSize) {
    // TODO Auto-generated method stub
    return dao.pageByUserId(userid, pageNo, pageSize);
  }
}