package com.haoxuer.school.data.service.impl;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.haoxuer.discover.data.core.Finder;

public class ForumBaseServiceImpl {

  public void buildWhere(Finder finder, Map<String, Object> params) {
    if (null != params && !params.isEmpty()) {
      String sql = finder.toString().toLowerCase().trim();

      // 是否包含Where关键字
      boolean hasWhere = sql.endsWith("where");
      boolean hasQuery = false;
      if (!hasWhere) {
        // 若不包含Where关键字，则追加
        finder.append(" where ");
      } else {
        if (sql.matches(" where\\s*\\w+")) {
          // 如果已经包含查询条件，则设置hasQuery为true
          hasQuery = true;
        }
      }

      /**
       * 已下代码获取查询条件
       */
      String p = "", operator = "";
      Pattern patern = Pattern.compile("\\w*(\\>=|\\<=|=|like|\\<|\\>)",
          Pattern.CASE_INSENSITIVE);
      Matcher m = null;

      for (String key : params.keySet()) {
        m = patern.matcher(key);
        while (m.find()) {
          operator = m.group().toLowerCase(); // 获取查询操作符
          break;
        }
        p = ":" + key.replace(operator, "").trim().replace(".", ""); // 生成参数名

        // 设置查询条件
        if (!hasQuery) {
          finder.append(" c." + key + p);
          hasQuery = true;
        } else {
          finder.append(" and c." + key + p);
        }

        // 设置查询参数
        if (operator.equals("like")) {
          finder.setParam(p.substring(1), "%" + params.get(key) + "%");
        } else {
          finder.setParam(p.substring(1), params.get(key));
        }
      }
    }
  }

  public void buildOrder(Finder finder, List<String> orders) {
    // 设置排序字段
    if (null != orders && orders.size() > 0) {
      finder.append(" order by c." + orders.get(0));
      for (int i = 1; i < orders.size(); i++) {
        finder.append(" and c." + orders.get(i));
      }
    }
  }
}
