package com.haoxuer.school.data.service.impl;

import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.dao.*;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.school.data.entity.forum.ForumGroupMember;
import com.haoxuer.school.data.service.ForumGroupService;

@Service
@Transactional
public class ForumGroupServiceImpl extends ForumBaseServiceImpl implements
    ForumGroupService {
  /**
   * 小组成员
   */
  private final int ROLE_MEMBER = 100;
  /**
   * 小组组长
   */
  private final int ROLE_LEADER = 0;
  /**
   * 小组管理员
   */
  private final int ROLE_MANAGER = 1;
  /**
   * 创建的小组
   */
  private final String ROLE_TYPE_OWN = "own";
  /**
   * 加入的小组
   */
  private final String ROLE_TYPE_JOIN = "join";

  @Autowired
  private ForumColumnDao columnDao;
  @Autowired
  private ForumGroupDao groupDao;
  @Autowired
  private ForumGroupMemberDao fgroupMemberDao;
  @Autowired
  private MemberDao memberInfoDao;


  @Transactional
  public ForumGroup save(ForumGroup bean) {
    groupDao.save(bean);
    return bean;
  }

  @Transactional
  public ForumGroupMember save(ForumGroupMember bean) {
    fgroupMemberDao.save(bean);
    return bean;
  }

  @Transactional
  public ForumGroup update(ForumGroup bean) {
    Updater<ForumGroup> updater = new Updater<ForumGroup>(bean);
    bean = groupDao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public ForumGroup delete(Integer id) {
    ForumGroup bean = groupDao.deleteById(id);
    return bean;
  }

  @Transactional
  public ForumGroup[] deleteByIds(Integer[] ids) {
    ForumGroup[] beans = new ForumGroup[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = delete(ids[i]);
    }
    return beans;
  }


  @Override
  public ForumGroup findById(Integer id) {
    return groupDao.findById(id);
  }

  @Override
  public Pagination getPage(Map<String, Object> params, List<String> orders,
                            int curpage, int pagesize) {
    Finder finder = Finder.create("from ForumGroup c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = groupDao.find(finder, curpage, pagesize);
    return page;
  }

  @Override
  public List<ForumColumn> findforumcolumnByParentid(Integer parentid) {
    return columnDao.findByParentId(parentid);
  }

  @Override
  public List<ForumGroupMember> findGroupMember(ForumGroup group) {
    return fgroupMemberDao.findByGroupid(group.getId());
  }

  /**
   * 加入小组
   */
  @Override
  public boolean joinMember(ForumGroupMember bean) {
    Finder finder = Finder.create("from ForumGroupMember c ");
    finder.append("where c.member = '" + bean.getMember().getId() + "'");
    finder.append(" and c.group = '" + bean.getGroup().getId() + "'");
    List<ForumGroupMember> list = fgroupMemberDao.find(finder);
    if (null != list && list.size() > 0) {
      return false;
    } else {
      update(bean.getGroup());
      ForumGroupMember upmber = save(bean);
      return null != upmber && upmber.getId() > 0;
    }
  }

  @Override
  public List<ForumGroupMember> findByrole(Integer groupid, Integer role) {
    Finder finder = Finder.create("from ForumGroupMember c ");
    finder.append("where c.group = '" + groupid + "'");
    finder.append(" and c.role = '" + role + "'");
    List<ForumGroupMember> list = fgroupMemberDao.find(finder);
    return list;
  }

  @Transactional
  public ForumGroupMember updateRole(ForumGroupMember entity) {
    Updater<ForumGroupMember> updater = new Updater<ForumGroupMember>(entity);
    updater.include("role");
    updater.exclude("member");
    updater.exclude("group");
    updater.exclude("id");
    entity = fgroupMemberDao.updateByUpdater(updater);
    return entity;
  }

  @Override
  public ForumGroupMember deleteGroupMember(Integer groupid,
                                            Integer memberid) {
    Finder finder = Finder.create("from ForumGroupMember c ");
    finder.append("where c.member = '" + memberid + "'");
    finder.append(" and c.group = '" + groupid + "'");
    List<ForumGroupMember> list = fgroupMemberDao.find(finder);
    if (null != list && list.size() > 0) {
      ForumGroupMember gmember = list.get(0);
      ForumGroupMember deletemember = fgroupMemberDao.deleteById(gmember.getId());
      return deletemember;
    }
    return null;
  }

  @Override
  public Pagination getGroupMemberPage(Map<String, Object> params,
                                       List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder.create("from ForumGroupMember c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = fgroupMemberDao.find(finder, curpage, pagesize);
    return page;

  }

  @Override
  public Pagination findGroupByRoletype(Integer memberid, String roletype, List<String> orders,
                                        int curpage, int pagesize) {
    Finder finder = Finder
        .create("select c.group from ForumGroupMember c ");
    finder.append("where c.member.id = :memberid");// '"+memberid+"'");
    finder.setParam("memberid", memberid);
    if (ROLE_TYPE_OWN.equalsIgnoreCase(roletype)) {
      finder.append(" and c.role = '" + ROLE_LEADER + "'");
    } else if (ROLE_TYPE_JOIN.equalsIgnoreCase(roletype)) {
      finder.append(" and (c.role = '" + ROLE_MEMBER + "' or c.role = '" + ROLE_MANAGER + "' )");
    }
    super.buildOrder(finder, orders);
    Pagination page = fgroupMemberDao.find(finder, curpage, pagesize);
    return page;
  }

  /**
   * 查找组员信息
   *
   * @author 谷玉伟
   */
  @Override
  public List<ForumGroupMember> findGroupMember(Map<String, Object> params,
                                                List<String> orders) {
    Finder finder = Finder.create("from ForumGroupMember c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    @SuppressWarnings("unchecked")
    List<ForumGroupMember> list = fgroupMemberDao.find(finder);
    return list;
  }

  /**
   * 显示小组的详情
   *
   * @author guyuwei
   */
  public List<ForumGroupMember> groupDtailsDIY(Map<String, Object> mapparams) {
    Finder finder = Finder.create("from ForumGroupMember c");
    super.buildWhere(finder, mapparams);
    @SuppressWarnings("unchecked")
    List<ForumGroupMember> list = fgroupMemberDao.find(finder);
    return list;
  }

  /**
   * 根据当前组的id和当前用户的id查找角色
   */
  @Override
  public List<ForumGroupMember> findRoleByGMid(Map<String, Object> mapparams) {
    Finder finder = Finder.create("from ForumGroupMember c");
    super.buildWhere(finder, mapparams);
    List<ForumGroupMember> list = fgroupMemberDao.find(finder);
    return list;
  }

  /**
   * 是否是改组成员
   *
   * @param userid
   * @param groupid
   * @return
   */
  public boolean isMemberOf(Long userid, int groupid) {
    Finder finder = Finder
        .create("from ForumGroupMember c where c.member.id= :userid and c.group.id = :groupid");
    finder.setParam("userid", userid);
    finder.setParam("groupid", groupid);
    List<ForumGroupMember> list = fgroupMemberDao.find(finder);
    return list.size() > 0;
  }

  @Override
  public Pagination getRecommendPage(Long userid, int curpage, int pagesize) {

    Pagination page;
    Finder finder;
    if (userid == 0) {
      finder = Finder.create("from ForumGroup c order by postCount desc");
      page = groupDao.find(finder, curpage, pagesize);
    } else {
      finder = Finder
          .create("from ForumGroup c where c.id not in (select t.group.id from c.groupMembers t where t.member.id = :userid)");
      // finder =
      // Finder.create("Select c.group from ForumGroupMember c ");
      // finder.append("where c.member.id != :userid group by c.group.id order by c.group.postCount desc");
      finder.setParam("userid", userid);
      page = fgroupMemberDao.find(finder, curpage, pagesize);
    }


    return page;
  }

  @Override
  public Pagination getGroupPage(Map<String, Object> params,
                                 List<String> orders, int curpage, int pagesize) {
    Finder finder = Finder.create("from ForumGroup c");

    return groupDao.find(finder, curpage, pagesize);

  }
}