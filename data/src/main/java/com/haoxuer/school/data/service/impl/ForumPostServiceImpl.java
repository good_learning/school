package com.haoxuer.school.data.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ForumColumnDao;
import com.haoxuer.school.data.dao.ForumGroupDao;
import com.haoxuer.school.data.dao.ForumPostCommentDao;
import com.haoxuer.school.data.dao.ForumPostDao;
import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.entity.forum.ForumGroup;
import com.haoxuer.school.data.entity.forum.ForumPost;
import com.haoxuer.school.data.entity.forum.ForumPostComment;
import com.haoxuer.school.data.service.ForumPostService;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
@Transactional
public class ForumPostServiceImpl extends ForumBaseServiceImpl implements ForumPostService {

  @Autowired
  private ForumColumnDao columnDao;
  //
  @Autowired
  private ForumGroupDao groupDao;

  @Autowired
  private ForumPostCommentDao commentDao;

  @Autowired
  private ForumPostDao postDao;

  @Transactional
  public ForumPost save(ForumPost bean) {
    postDao.save(bean);
    return bean;
  }

  @Transactional
  public ForumPost update(ForumPost bean) {
    Updater<ForumPost> updater = new Updater<ForumPost>(bean);
    bean = postDao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public ForumPost delete(Integer id) {
    ForumPost bean = postDao.deleteById(id);
    return bean;
  }

  @Transactional
  public ForumPost[] delete(Integer[] ids) {
    ForumPost[] beans = new ForumPost[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = delete(ids[i]);
    }
    return beans;
  }

  @Override
  public Pagination getForumPostPage(Map<String, Object> params,
                                     List<String> orders, int pageNo, int pageSize) {

    Finder finder = Finder.create("from ForumPost c ");
    // 填充查询条件
    this.buildWhere(finder, params);
    this.buildOrder(finder, orders);

    Pagination result = postDao.find(finder, pageNo, pageSize);

    return result;
  }

  @Override
  public Pagination getCommentPage(Map<String, Object> params,
                                   List<String> orders, int pageNo, int pageSize) {

    Finder finder = Finder.create("from ForumPostComment c ");
    // 填充查询条件
    this.buildWhere(finder, params);
    this.buildOrder(finder, orders);

    return commentDao.find(finder, pageNo, pageSize);
  }

  @Override
  public ForumPost findById(Integer id) {
    ForumPost entity = postDao.findById(id);
    return entity;
  }

  @Override
  public Pagination getPage(Map<String, Object> params, String orders,
                            int pageNo, int pageSize) {
    // TODO Auto-generated method stub
    return null;
  }

  public ForumColumn saveForumColumn(ForumColumn bean) {
    columnDao.save(bean);
    return bean;
  }

  public ForumGroup saveForumGroup(ForumGroup bean) {
    groupDao.save(bean);
    return bean;
  }

  @Transactional
  public ForumPostComment saveComment(ForumPostComment bean) {
    commentDao.save(bean);

    if (bean.getForumPostComment() != null) {
      try {
        Configuration cfg = new Configuration();
        Locale locale = Locale.CHINA;
        cfg.setEncoding(locale, "utf-8");
        File file = getfile();
        cfg.setDirectoryForTemplateLoading(file);
        cfg.setTagSyntax(Configuration.SQUARE_BRACKET_TAG_SYNTAX);
        Template template = cfg.getTemplate("comment.html");
        Writer out = new StringWriter();

        Map<String, Object> map = new HashMap<String, Object>();
        ForumPostComment iteminfo = commentDao.findById(bean.getId());

        map.put("item", iteminfo);
        map.put("siteurl", "http://www.91mydoor.com/");

        template.process(map, out);
        ForumPostComment item = commentDao.findById(bean
            .getForumPostComment().getId());
        String comment = out.toString() + item.getContent();
        bean.setContent(comment);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (TemplateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    return bean;
  }

  @Override
  public List<ForumColumn> getForumColumnChild(Integer columnid) {
    // TODO Auto-generated method stub

    return columnDao.findChild(columnid);
  }

  public ForumColumn findForumColumnById(Integer columnid) {
    return columnDao.findById(columnid);
  }


  /**
   * 查看活动详情
   */
  @Transactional
  public ForumPost viewForumPost(Integer id) {
    ForumPost bean = this.findById(id);
    int views = bean.getViews() + 1;
    bean.setViews(views);
    this.update(bean);
    return bean;
  }

  @Transactional
  public ForumPost upForumPost(Integer id) {
    ForumPost bean = this.findById(id);
    int ups = bean.getUps() + 1;
    bean.setUps(ups);
    this.update(bean);
    return bean;
  }

  @Override
  public void setTop(int id) {
    ForumPost forum = postDao.findById(id);
    int maxSort = postDao.getMaxSortSeq();
    forum.setSortSeq(maxSort + 1);
    Updater<ForumPost> updater = new Updater<ForumPost>(forum);
    postDao.updateByUpdater(updater);
  }

  @Override
  public Pagination getMyParticipation(Map<String, Object> params,
                                       List<String> orders, int pageNo, int pageSize) {

    Finder finder = Finder
        .create("select c.forumPost from ForumPostComment c ");
    // 填充查询条件
    this.buildWhere(finder, params);
    this.buildOrder(finder, orders);

    return commentDao.find(finder, pageNo, pageSize);
  }

  private File getfile() {
    String dir = "D:\\Program Files (x86)\\Apache Software Foundation\\Tomcat 8.0\\webapps\\ROOT\\WEB-INF\\classes\\ftl\\";
    File file = new File(dir);
    if (!file.exists()) {
      try {
        dir = getClass().getClassLoader().getResource("/ftl/")
            .getFile();
        file = new File(dir);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }
    if (!file.exists()) {
      dir = "d:\\files\\";
    }
    return file;
  }
}