package com.haoxuer.school.data.service.impl;

import java.util.List;
import java.util.Map;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ForumColumnDao;
import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.service.ForumService;

@Service
@Transactional
public class ForumServiceImpl extends ForumBaseServiceImpl implements
    ForumService {

  @Autowired
  private ForumColumnDao columnDao;

  @Transactional
  public ForumColumn save(ForumColumn bean) {
    columnDao.save(bean);
    return bean;
  }

  @Transactional
  public ForumColumn update(ForumColumn bean) {
    Updater<ForumColumn> updater = new Updater<ForumColumn>(bean);
    bean = columnDao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public ForumColumn deleteColumn(Integer id) {
    ForumColumn bean = columnDao.deleteById(id);
    return bean;
  }

  @Transactional
  public ForumColumn[] deleteColumnByIds(Integer[] ids) {
    ForumColumn[] beans = new ForumColumn[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteColumn(ids[i]);
    }
    return beans;
  }


  @Override
  public ForumColumn findColumnById(Integer id) {
    return columnDao.findById(id);
  }

  @Override
  public Pagination getPageOfColumns(Map<String, Object> params,
                                     List<String> orders,
                                     int curpage, int pagesize) {
    Finder finder = Finder.create(" from ForumColumn c ");
    super.buildWhere(finder, params);
    super.buildOrder(finder, orders);
    Pagination page = columnDao.find(finder, curpage, pagesize);
    return page;
  }
}