package com.haoxuer.school.data.service.impl;

import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.FriendDao;
import com.haoxuer.school.data.entity.Friend;
import com.haoxuer.school.data.service.FriendService;

@Service
@Transactional
public class FriendServiceImpl implements FriendService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Friend findById(Integer id) {
    Friend entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Friend save(Friend bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Friend update(Friend bean) {
    Updater<Friend> updater = new Updater<Friend>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Friend deleteById(Integer id) {
    Friend bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Friend[] deleteByIds(Integer[] ids) {
    Friend[] beans = new Friend[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private FriendDao dao;

  @Autowired
  public void setDao(FriendDao dao) {
    this.dao = dao;
  }


  @Override
  public Friend deleteById(Integer id, Integer friendid) {

    return this.dao.deleteById(id, friendid);
  }

  @Override
  public List<Friend> findByType(Integer uid, int type) {
    // TODO Auto-generated method stub
    return this.dao.findByType(uid, type);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByType(int userid, int type, int curpage, int pagesize) {
    // TODO Auto-generated method stub
    return dao.getByType(userid, type, curpage, pagesize);
  }

  @Transactional
  @Override
  public int follows(int userid, int friendid) {
    // TODO Auto-generated method stub
    return dao.follows(userid, friendid);
  }

  @Transactional(readOnly = true)
  @Override
  public int isfollows(int userid, int friendid) {
    // TODO Auto-generated method stub
    return dao.isfollows(userid, friendid);
  }

  @Override
  public int save(int userid, int friendid, int type) {
    // TODO Auto-generated method stub
    return dao.save(userid, friendid, type);
  }
}