package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.FriendtypeDao;
import com.haoxuer.school.data.entity.Friendtype;
import com.haoxuer.school.data.service.FriendtypeService;

@Service
@Transactional
public class FriendtypeServiceImpl implements FriendtypeService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Friendtype findById(Integer id) {
    Friendtype entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Friendtype save(Friendtype bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Friendtype update(Friendtype bean) {
    Updater<Friendtype> updater = new Updater<Friendtype>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Friendtype deleteById(Integer id) {
    Friendtype bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Friendtype[] deleteByIds(Integer[] ids) {
    Friendtype[] beans = new Friendtype[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private FriendtypeDao dao;

  @Autowired
  public void setDao(FriendtypeDao dao) {
    this.dao = dao;
  }
}