package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.KeyWordDao;
import com.haoxuer.school.data.entity.KeyWord;
import com.haoxuer.school.data.service.KeyWordService;

@Service
@Transactional
public class KeyWordServiceImpl implements KeyWordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public KeyWord findById(Integer id) {
    KeyWord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public KeyWord save(KeyWord bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public KeyWord update(KeyWord bean) {
    Updater<KeyWord> updater = new Updater<KeyWord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public KeyWord deleteById(Integer id) {
    KeyWord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public KeyWord[] deleteByIds(Integer[] ids) {
    KeyWord[] beans = new KeyWord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private KeyWordDao dao;

  @Autowired
  public void setDao(KeyWordDao dao) {
    this.dao = dao;
  }
}