package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.dao.MemberDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.MemberAuthenticationDao;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.MemberAuthentication;
import com.haoxuer.school.data.service.MemberAuthenticationService;

@Service
@Transactional
public class MemberAuthenticationServiceImpl implements
    MemberAuthenticationService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Autowired
  MemberDao memberInfoDao;

  @Transactional(readOnly = true)
  public MemberAuthentication findById(Integer id) {
    MemberAuthentication entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public MemberAuthentication save(MemberAuthentication bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public MemberAuthentication update(MemberAuthentication bean) {
    Updater<MemberAuthentication> updater = new Updater<MemberAuthentication>(
        bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public MemberAuthentication deleteById(Integer id) {
    MemberAuthentication bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public MemberAuthentication[] deleteByIds(Integer[] ids) {
    MemberAuthentication[] beans = new MemberAuthentication[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private MemberAuthenticationDao dao;

  @Autowired
  public void setDao(MemberAuthenticationDao dao) {
    this.dao = dao;
  }

  @Transactional
  @Override
  public MemberAuthentication findByUserId(Integer id) {
    return null;
  }
}