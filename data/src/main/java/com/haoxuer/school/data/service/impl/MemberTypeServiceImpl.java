package com.haoxuer.school.data.service.impl;

import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.entity.MemberType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.MemberTypeDao;
import com.haoxuer.school.data.service.MemberTypeService;

@Service
@Transactional
public class MemberTypeServiceImpl implements MemberTypeService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public MemberType findById(Integer id) {
    MemberType entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public MemberType save(MemberType bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public MemberType update(MemberType bean) {
    Updater<MemberType> updater = new Updater<MemberType>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public MemberType deleteById(Integer id) {
    MemberType bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public MemberType[] deleteByIds(Integer[] ids) {
    MemberType[] beans = new MemberType[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private MemberTypeDao dao;

  @Autowired
  public void setDao(MemberTypeDao dao) {
    this.dao = dao;
  }


  @Transactional(readOnly = true)
  @Override
  public List<MemberType> all() {
    // TODO Auto-generated method stub
    return dao.all();
  }
}