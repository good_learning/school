package com.haoxuer.school.data.service.impl;

import java.util.Date;
import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.MemberVisitDao;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.MemberVisit;
import com.haoxuer.school.data.service.MemberVisitService;

@Service
@Transactional
public class MemberVisitServiceImpl implements MemberVisitService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public MemberVisit findById(Long id) {
    MemberVisit entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public MemberVisit save(MemberVisit bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public MemberVisit update(MemberVisit bean) {
    Updater<MemberVisit> updater = new Updater<MemberVisit>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public MemberVisit deleteById(Long id) {
    MemberVisit bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public MemberVisit[] deleteByIds(Long[] ids) {
    MemberVisit[] beans = new MemberVisit[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private MemberVisitDao dao;

  @Autowired
  public void setDao(MemberVisitDao dao) {
    this.dao = dao;
  }

  @Transactional
  @Override
  public MemberVisit visit(int muserid, int visituserid) {
    MemberVisit result = null;
    Finder finder = Finder.create();
    finder.append("from MemberVisit m ");
    finder.append(" where m.member.id = " + muserid);
    finder.append(" and m.visitMember.id = " + visituserid);
    List<MemberVisit> vs = dao.find(finder);
    if (vs != null && vs.size() > 0) {
      result = vs.get(0);
      int vsize = result.getSize();
      vsize++;
      result.setSize(vsize);
      result.setLastDate(new Date());
    } else {
      result = new MemberVisit();
      result.setSize(1);
      result.setFirstDate(new Date());
      result.setLastDate(new Date());
      Member visitMember = new Member();
     // visitMember.setId(visituserid);
      result.setVisitMember(visitMember);
      Member member = new Member();
    //  member.setId(muserid);
      result.setMember(member);
      dao.save(result);
    }
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination findByUserId(int userid, int pageNo, int pageSize) {
    Pagination result = null;
    Finder finder = Finder.create("from MemberVisit m ");
    finder.append(" where m.member.id = " + userid);
    finder.append("  order by m.lastDate desc");
    result = dao.find(finder, pageNo, pageSize);
    return result;
  }
}