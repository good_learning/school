package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.MoneyRecordDao;
import com.haoxuer.school.data.entity.MoneyRecord;
import com.haoxuer.school.data.service.MoneyRecordService;

@Service
@Transactional
public class MoneyRecordServiceImpl implements MoneyRecordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public MoneyRecord findById(Integer id) {
    MoneyRecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public MoneyRecord save(MoneyRecord bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public MoneyRecord update(MoneyRecord bean) {
    Updater<MoneyRecord> updater = new Updater<MoneyRecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public MoneyRecord deleteById(Integer id) {
    MoneyRecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public MoneyRecord[] deleteByIds(Integer[] ids) {
    MoneyRecord[] beans = new MoneyRecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private MoneyRecordDao dao;

  @Autowired
  public void setDao(MoneyRecordDao dao) {
    this.dao = dao;
  }


  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUser(int id, int curpage, int pagesize) {
    Finder finder = Finder.create();
    finder.append("from MoneyRecord m");
    finder.append("  where m.member.id = " + id);
    finder.append("  and m.state = 1 ");


    finder.append(" order by m.id desc ");
    // TODO Auto-generated method stub
    return dao.find(finder, curpage, pagesize);
  }

  @Transactional
  @Override
  public MoneyRecord delete(Integer id) {
    MoneyRecord r = dao.findById(id);
    r.setState(2);
    return r;
  }
}