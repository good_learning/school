package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.NumGenerateDao;
import com.haoxuer.school.data.entity.NumGenerate;
import com.haoxuer.school.data.service.NumGenerateService;

@Service
@Transactional
public class NumGenerateServiceImpl implements NumGenerateService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public NumGenerate findById(Long id) {
    NumGenerate entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public NumGenerate save(NumGenerate bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public NumGenerate update(NumGenerate bean) {
    Updater<NumGenerate> updater = new Updater<NumGenerate>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public NumGenerate deleteById(Long id) {
    NumGenerate bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public NumGenerate[] deleteByIds(Long[] ids) {
    NumGenerate[] beans = new NumGenerate[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private NumGenerateDao dao;

  @Autowired
  public void setDao(NumGenerateDao dao) {
    this.dao = dao;
  }
}