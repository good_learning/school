package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.PageInfoDao;
import com.haoxuer.school.data.entity.PageInfo;
import com.haoxuer.school.data.service.PageInfoService;

@Service
@Transactional
public class PageInfoServiceImpl implements PageInfoService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public PageInfo findById(Integer id) {
    PageInfo entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public PageInfo save(PageInfo bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public PageInfo update(PageInfo bean) {
    Updater<PageInfo> updater = new Updater<PageInfo>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public PageInfo deleteById(Integer id) {
    PageInfo bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public PageInfo[] deleteByIds(Integer[] ids) {
    PageInfo[] beans = new PageInfo[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private PageInfoDao dao;

  @Autowired
  public void setDao(PageInfoDao dao) {
    this.dao = dao;
  }
}