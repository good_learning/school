package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.PhoneRecordDao;
import com.haoxuer.school.data.entity.PhoneRecord;
import com.haoxuer.school.data.service.PhoneRecordService;

@Service
@Transactional
public class PhoneRecordServiceImpl implements PhoneRecordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public PhoneRecord findById(Integer id) {
    PhoneRecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public PhoneRecord save(PhoneRecord bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public PhoneRecord update(PhoneRecord bean) {
    Updater<PhoneRecord> updater = new Updater<PhoneRecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public PhoneRecord deleteById(Integer id) {
    PhoneRecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public PhoneRecord[] deleteByIds(Integer[] ids) {
    PhoneRecord[] beans = new PhoneRecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private PhoneRecordDao dao;

  @Autowired
  public void setDao(PhoneRecordDao dao) {
    this.dao = dao;
  }
}