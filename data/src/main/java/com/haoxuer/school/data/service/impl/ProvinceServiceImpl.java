package com.haoxuer.school.data.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.school.data.dao.ProvinceDao;
import com.haoxuer.school.data.entity.Province;
import com.haoxuer.school.data.service.ProvinceService;

@Service
public class ProvinceServiceImpl implements ProvinceService {

  @Resource
  private ProvinceDao dao;

  @Transactional
  @Override
  public Province add(Province province) {
    return dao.add(province);
  }

  @Transactional(readOnly = true)
  @Override
  public List<Province> all() {
    Finder finder = Finder.create("from Province p");
    return dao.find(finder);
  }
}
