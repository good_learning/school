package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.RecommendDao;
import com.haoxuer.school.data.entity.Recommend;
import com.haoxuer.school.data.service.RecommendService;

@Service
@Transactional
public class RecommendServiceImpl implements RecommendService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Recommend findById(Long id) {
    Recommend entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Recommend save(Recommend bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Recommend update(Recommend bean) {
    Updater<Recommend> updater = new Updater<Recommend>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Recommend deleteById(Long id) {
    Recommend bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Recommend[] deleteByIds(Long[] ids) {
    Recommend[] beans = new Recommend[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private RecommendDao dao;

  @Autowired
  public void setDao(RecommendDao dao) {
    this.dao = dao;
  }

  @Override
  public Pagination recommend(int type, int size, int curpage) {
    // TODO Auto-generated method stub
    return dao.recommend(type, size, curpage);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination page(int type, int pageNo, int pageSize) {
    Finder finder = Finder.create("from Recommend u where u.recommendtype ="
        + type);
    finder.append(" order by u.sortnum asc");
    finder.setCacheable(true);
    return dao.find(finder, pageNo, pageSize);
  }

  @Override
  public Pagination page(int pageNo, int pageSize) {
    Finder finder = Finder.create("from Recommend u "
    );
    finder.append(" order by u.recommendtype desc,u.sortnum asc");
    finder.setCacheable(true);
    return dao.find(finder, pageNo, pageSize);
  }
}