package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.RefundRecordDao;
import com.haoxuer.school.data.dao.TradingrecordDao;
import com.haoxuer.school.data.entity.RefundRecord;
import com.haoxuer.school.data.entity.Tradingrecord;
import com.haoxuer.school.data.service.RefundRecordService;

@Service
@Transactional
public class RefundRecordServiceImpl implements RefundRecordService {

  @Autowired
  private TradingrecordDao tradingrecordDao;


  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public RefundRecord findById(Integer id) {
    RefundRecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public RefundRecord save(RefundRecord bean) {
    dao.save(bean);

    Tradingrecord t = tradingrecordDao.findById(bean.getTradingrecord().getId());
    t.setState(3);
    return bean;
  }

  @Transactional
  public RefundRecord update(RefundRecord bean) {
    Updater<RefundRecord> updater = new Updater<RefundRecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public RefundRecord deleteById(Integer id) {
    RefundRecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public RefundRecord[] deleteByIds(Integer[] ids) {
    RefundRecord[] beans = new RefundRecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private RefundRecordDao dao;

  @Autowired
  public void setDao(RefundRecordDao dao) {
    this.dao = dao;
  }
}