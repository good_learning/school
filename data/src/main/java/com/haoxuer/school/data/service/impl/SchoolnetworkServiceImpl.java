package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.SchoolnetworkDao;
import com.haoxuer.school.data.entity.Schoolnetwork;
import com.haoxuer.school.data.service.SchoolnetworkService;

@Service
@Transactional
public class SchoolnetworkServiceImpl implements SchoolnetworkService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Schoolnetwork findById(Integer id) {
    Schoolnetwork entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Schoolnetwork save(Schoolnetwork bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Schoolnetwork update(Schoolnetwork bean) {
    Updater<Schoolnetwork> updater = new Updater<Schoolnetwork>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Schoolnetwork deleteById(Integer id) {
    Schoolnetwork bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Schoolnetwork[] deleteByIds(Integer[] ids) {
    Schoolnetwork[] beans = new Schoolnetwork[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private SchoolnetworkDao dao;

  @Autowired
  public void setDao(SchoolnetworkDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination getPage(int schoolid, int pageNo, int pageSize) {
    Finder finder = Finder.create();
    finder.append("from Schoolnetwork s where s.member.id =");
    finder.append("" + schoolid);
    finder.append(" order by id desc");

    // TODO Auto-generated method stub
    return dao.find(finder, pageNo, pageSize);
  }

}