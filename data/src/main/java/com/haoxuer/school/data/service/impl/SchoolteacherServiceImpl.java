package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;
import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.school.data.dao.MemberDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.SchoolteacherDao;
import com.haoxuer.school.data.entity.Member;
import com.haoxuer.school.data.entity.Schoolteacher;
import com.haoxuer.school.data.service.SchoolteacherService;

@Service
@Transactional
public class SchoolteacherServiceImpl implements SchoolteacherService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Schoolteacher findById(Integer id) {
    Schoolteacher entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Schoolteacher save(Schoolteacher bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Schoolteacher update(Schoolteacher bean) {
    Updater<Schoolteacher> updater = new Updater<Schoolteacher>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Schoolteacher deleteById(Integer id) {
    Schoolteacher bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Schoolteacher[] deleteByIds(Integer[] ids) {
    Schoolteacher[] beans = new Schoolteacher[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private SchoolteacherDao dao;

  @Autowired
  public void setDao(SchoolteacherDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination findSchoolTeacher(Long schoolid, int pageNo, int pageSize) {
    Finder finder = Finder.create();
    finder.append("select t.teacher from Schoolteacher t ");
    finder.append(" where t.school.id=" + schoolid);
    finder.append(" and t.state = 1");
    finder.append(" and t.techerstate = 1");
    finder.append(" order by t.id desc");
    // TODO Auto-generated method stub
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination findByType(Long schoolid, int type, int pageNo,
                               int pageSize) {
    Finder finder = Finder.create();
    finder.append("from Schoolteacher t ");
    finder.append(" where t.school.id=" + schoolid);
    finder.append(" and t.state = " + type);
    finder.append(" order by t.addtime desc");
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination findBySchoolAll(Long schoolid, int pageNo, int pageSize) {
    Finder finder = Finder.create();
    finder.append("from Schoolteacher t ");
    finder.append(" where t.school.id=" + schoolid);
    finder.append(" order by t.state asc, t.addtime desc");
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional
  @Override
  public Schoolteacher reject(Integer id) {
    Schoolteacher result = null;
    result = dao.findById(id);
    result.setState(2);
    return result;
  }

  @Transactional
  @Override
  public Schoolteacher cancel(Integer id) {
    Schoolteacher result = null;
    result = dao.findById(id);
    result.setState(0);
    return result;
  }

  @Transactional
  @Override
  public Schoolteacher agree(Integer id) {
    Schoolteacher result = null;
    result = dao.findById(id);
    result.setState(1);
    return result;
  }

  @Transactional
  @Override
  public Schoolteacher agreegood(Integer id) {
    Schoolteacher result = null;
    result = dao.findById(id);
    result.setTecherstate(1);
    return result;
  }

  @Transactional
  @Override
  public Schoolteacher cancelgood(Integer id) {
    Schoolteacher result = null;
    result = dao.findById(id);
    result.setTecherstate(0);
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageForTeacher(Long teacherid, int curpage, int pagesize) {
    Finder finder = Finder.create();
    finder.append("from Schoolteacher t ");
    finder.append(" where t.teacher.id=" + teacherid);
    finder.append(" and t.state <=1 ");
    finder.append(" order by t.state asc, t.addtime desc");
    return dao.find(finder, curpage, pagesize);
  }

  @Transactional
  @Override
  public Schoolteacher addTeacher(Schoolteacher schoolteacher) {

    Finder finder = Finder.create();
    finder.append("from  Schoolteacher t  ");
    finder.append(" where t.teacher.id="
        + schoolteacher.getTeacher().getId());
    finder.append(" and t.school.id=" + schoolteacher.getSchool().getId());
    finder.append(" and t.state <= 1");
    List list = dao.find(finder);
    if (list != null && list.size() > 0) {

    } else {
      dao.save(schoolteacher);
    }
    return schoolteacher;
  }

  @Autowired
  MemberDao memberInfoDao;

  @Transactional
  @Override
  public Schoolteacher addTeacher(String menpai, Member member) {
    Schoolteacher schoolteacher = null;
    List<Member> ms = memberInfoDao.findByProperty("doornumber", menpai);
    if (ms != null && ms.size() > 0) {
      schoolteacher = new Schoolteacher();
      schoolteacher.setTeacher(member);
      schoolteacher.setSchool(ms.get(0));
      schoolteacher.setAddtime(new Timestamp(System.currentTimeMillis()));
      schoolteacher.setState(0);
    }
    if (schoolteacher != null) {
      Finder finder = Finder.create();
      finder.append("from  Schoolteacher t  ");
      finder.append(" where t.teacher.id="
          + schoolteacher.getTeacher().getId());
      finder.append(" and t.school.id="
          + schoolteacher.getSchool().getId());
      finder.append(" and t.state = 1");
      List list = dao.find(finder);
      if (list != null && list.size() > 0) {

      } else {
        dao.save(schoolteacher);
      }
    }

    return schoolteacher;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination findSchoolTeacherList(Long schoolid, int pageNo,
                                          int pageSize) {
    Finder finder = Finder.create();
    finder.append("select t.teacher from Schoolteacher t ");
    finder.append(" where t.school.id=" + schoolid);
    finder.append(" and t.state = 1");
    finder.append(" order by t.id desc");
    // TODO Auto-generated method stub
    return dao.find(finder, pageNo, pageSize);
  }
}