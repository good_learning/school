package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ScorerecordDao;
import com.haoxuer.school.data.entity.Scorerecord;
import com.haoxuer.school.data.service.ScorerecordService;

@Service
@Transactional
public class ScorerecordServiceImpl implements ScorerecordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Scorerecord findById(Integer id) {
    Scorerecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Scorerecord save(Scorerecord bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Scorerecord update(Scorerecord bean) {
    Updater<Scorerecord> updater = new Updater<Scorerecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Scorerecord deleteById(Integer id) {
    Scorerecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Scorerecord[] deleteByIds(Integer[] ids) {
    Scorerecord[] beans = new Scorerecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ScorerecordDao dao;

  @Autowired
  public void setDao(ScorerecordDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserId(int id, int curpage, int pagesize) {
    // TODO Auto-generated method stub
    return dao.pageByUserId(id, curpage, pagesize);
  }

  @Transactional(readOnly = true)
  @Override
  public int getallscore(Long userid) {
    // TODO Auto-generated method stub
    return dao.getallscore(userid);
  }
}