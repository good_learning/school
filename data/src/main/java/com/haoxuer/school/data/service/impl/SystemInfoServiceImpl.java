package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.SystemInfoDao;
import com.haoxuer.school.data.entity.SystemInfo;
import com.haoxuer.school.data.service.SystemInfoService;

@Service
@Transactional
public class SystemInfoServiceImpl implements SystemInfoService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public SystemInfo findById(Long id) {
    SystemInfo entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public SystemInfo save(SystemInfo bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public SystemInfo update(SystemInfo bean) {
    Updater<SystemInfo> updater = new Updater<SystemInfo>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public SystemInfo deleteById(Long id) {
    SystemInfo bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public SystemInfo[] deleteByIds(Long[] ids) {
    SystemInfo[] beans = new SystemInfo[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private SystemInfoDao dao;

  @Autowired
  public void setDao(SystemInfoDao dao) {
    this.dao = dao;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUser(int userid, int state, int pageNo, int pageSize) {
    Finder finder = Finder.create();
    finder.append("from SystemInfo s where s.member.id =" + userid);
    finder.append(" and s.state=" + state);
    finder.append(" order by s.id desc");
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional
  @Override
  public SystemInfo recovery(Long id) {
    SystemInfo systemInfo = dao.findById(id);
    systemInfo.setState(1);
    return systemInfo;
  }

  @Transactional
  @Override
  public SystemInfo read(Long id) {
    SystemInfo systemInfo = dao.findById(id);
    systemInfo.setReadsate(1);
    return systemInfo;
  }
}