package com.haoxuer.school.data.service.impl;

import java.util.List;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.TownDao;
import com.haoxuer.school.data.entity.Town;
import com.haoxuer.school.data.service.TownService;

@Service
@Transactional
public class TownServiceImpl implements TownService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Town findById(Integer id) {
    Town entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Town save(Town bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Town update(Town bean) {
    Updater<Town> updater = new Updater<Town>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Town deleteById(Integer id) {
    Town bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Town[] deleteByIds(Integer[] ids) {
    Town[] beans = new Town[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private TownDao dao;

  @Autowired
  public void setDao(TownDao dao) {
    this.dao = dao;
  }

  @Override
  public List<Town> findByCity(int id) {
    return dao.findByProperty("city.id", id);
  }
}