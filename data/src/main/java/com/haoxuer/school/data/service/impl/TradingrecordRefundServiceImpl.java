package com.haoxuer.school.data.service.impl;

import com.haoxuer.school.data.dao.*;
import com.haoxuer.school.data.entity.*;
import com.haoxuer.school.data.service.TradingrecordRefundService;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class TradingrecordRefundServiceImpl implements
    TradingrecordRefundService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public TradingrecordRefund findById(Integer id) {
    TradingrecordRefund entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public TradingrecordRefund save(TradingrecordRefund bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public TradingrecordRefund update(TradingrecordRefund bean) {
    Updater<TradingrecordRefund> updater = new Updater<TradingrecordRefund>(
        bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public TradingrecordRefund deleteById(Integer id) {
    TradingrecordRefund bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public TradingrecordRefund[] deleteByIds(Integer[] ids) {
    TradingrecordRefund[] beans = new TradingrecordRefund[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private TradingrecordRefundDao dao;

  @Autowired
  public void setDao(TradingrecordRefundDao dao) {
    this.dao = dao;
  }

  @Autowired
  TradingrecordDao tradingrecordDao;

  @Transactional
  @Override
  public TradingrecordRefund refunds(TradingrecordRefund tradingrecordRefund) {
    TradingrecordRefund result = null;
    Tradingrecord tradingrecord = tradingrecordDao
        .findById(tradingrecordRefund.getTradingrecord().getId());
    tradingrecord.setState(3);
    tradingrecordRefund.setState(0);
    tradingrecordRefund.setDeletestate(0);
    tradingrecordRefund.setRefundstate(0);
    tradingrecordRefund
        .setAdddate(new Timestamp(System.currentTimeMillis()));
    result = dao.save(tradingrecordRefund);
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public TradingrecordRefund findByTradId(int id) {
    Finder finder = Finder.create();
    finder.append("from TradingrecordRefund t");
    finder.append(" where t.tradingrecord.id=" + id);
    List lists = dao.find(finder);
    if (lists != null && lists.size() > 0) {
      return (TradingrecordRefund) lists.get(0);
    }
    return null;
  }

  @Autowired
  MemberDao memberInfoDao;

  @Autowired
  MoneyRecordDao moneyRecordDao;

  @Autowired
  CourseLessonStudentDao studentDao;

  @Transactional
  @Override
  public TradingrecordRefund refund(int id) {
    TradingrecordRefund result = dao.findById(id);
    if (result.getRefundstate() == 1) {
      return result;
    }
    result.setTradingdate(new Timestamp(System.currentTimeMillis()));
    Tradingrecord r = tradingrecordDao.findById(result.getTradingrecord()
        .getId());
    r.setRefundstate(1);
    result.setRefundstate(1);

    //如果退款时间在开课时间之前  人数减一
    Course course = result.getTradingrecord().getCourse();
    Timestamp btime = course.getBegintime();
    Timestamp now = new Timestamp(System.currentTimeMillis());
    if (btime != null && btime.after(now)) {
      int jsize = course.getJointimes();
      jsize--;
      course.setJointimes(jsize);
    }
    Finder finder = Finder.create("");
    finder.append("from CourseLessonStudent s ");
    finder.append(" where s.tradingrecord.id =" + r.getId());

    //删除上课纪律
    List<CourseLessonStudent> ss = studentDao.find(finder);
    if (ss != null) {
      for (CourseLessonStudent courseLessonStudent : ss) {
        studentDao.delete(courseLessonStudent);
      }
    }

    Member member = memberInfoDao.findById(result.getMember().getId());
    float money = member.getMoneys() + result.getMoney();
    member.setMoneys(money);

    MoneyRecord record = new MoneyRecord();
    record.setMoney(result.getMoney());
    record.setName("退款");
    record.setCourse(result.getTradingrecord().getCourse());
    record.setState(1);
    record.setAdddate(new Timestamp(System.currentTimeMillis()));
    record.setTradingdate(new Timestamp(System.currentTimeMillis()));
    record.setSeller(result.getTradingrecord().getCourse().getMember());
    record.setMember(result.getMember());
    moneyRecordDao.save(record);
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByForTeacher(int id, int curpage, int pagesize) {
    Finder finder = Finder.create();
    finder.append("from  TradingrecordRefund r");
    finder.append(" where r.tradingrecord.course.member.id=" + id);
    return dao.find(finder, curpage, pagesize);
  }
}