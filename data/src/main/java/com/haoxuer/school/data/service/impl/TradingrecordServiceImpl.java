package com.haoxuer.school.data.service.impl;

import com.haoxuer.school.common.security.Md5PwdEncoder;
import com.haoxuer.school.common.security.PwdEncoder;
import com.haoxuer.school.data.dao.*;
import com.haoxuer.school.data.entity.*;
import com.haoxuer.school.data.service.TradingrecordService;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TradingrecordServiceImpl implements TradingrecordService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Tradingrecord findById(Integer id) {
    Tradingrecord entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Tradingrecord save(Tradingrecord bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Tradingrecord update(Tradingrecord bean) {
    Updater<Tradingrecord> updater = new Updater<Tradingrecord>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Tradingrecord deleteById(Integer id) {
    Tradingrecord bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Tradingrecord[] deleteByIds(Integer[] ids) {
    Tradingrecord[] beans = new Tradingrecord[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private TradingrecordDao dao;

  @Autowired
  public void setDao(TradingrecordDao dao) {
    this.dao = dao;
  }

  @Override
  public List<Tradingrecord> findByUid(Long uid) {
    return dao.findAllByUid(uid);
  }

  @Transactional(readOnly = true)
  @Override
  public List<Tradingrecord> findByType(int type) {
    return dao.findAllByType(type);
  }

  @Autowired
  private CardDao cardDao;

  @Autowired
  private ScorerecordDao scorerecordDao;

  @Autowired
  private MemberDao memberInfoDao;

  @Autowired
  CardRecordDao cardRecordDao;

  @Autowired
  CourseDao courseDao;
  @Autowired
  MoneyRecordDao moneyRecordDao;

  @Autowired
  CourseLessonStudentDao courseLessonStudentDao;

  @Transactional
  @Override
  public int payusecard(int id, String card, Member member) {
    int result = 0;
    Tradingrecord tradingrecord = findById(id);
    Card card2 = cardDao.findByName(card);
    if (card2 == null) {
      result = 1;
    } else {
      if (card2.getLeftmoney() < tradingrecord.getMoney()) {
        result = 2;
      } else if (tradingrecord.getState() == 1) {
        float leftmoney = card2.getLeftmoney()
            - tradingrecord.getMoney();
        card2.setLeftmoney(leftmoney);
        tradingrecord.setState(2);

        Scorerecord record = new Scorerecord();
        record.setMember(member);
        record.setScore(tradingrecord.getMoney());
        record.setDemo("卡支付");
        record.setSummary("购买课程");
        record.setTransactiondate(new Timestamp(System
            .currentTimeMillis()));
        int all = (int) (tradingrecord.getMoney() + scorerecordDao
            .getallscore(member.getId()));
        record.setLeftscore(all);
        scorerecordDao.save(record);

        Member cur = memberInfoDao.findById(member.getId());
       // cur.setScores(all);

        scorerecordDao.getallscore(member.getId());
        CardRecord rec = new CardRecord();
        rec.setAddtime(new Timestamp(System.currentTimeMillis()));
        rec.setMember(member);
        rec.setMoney(tradingrecord.getMoney());
        rec.setCard(card2);
        cardRecordDao.save(rec);

        Finder finder = Finder.create();
        finder.append("from CourseLessonStudent c");
        finder.append(" where  c.course.id= "
            + tradingrecord.getCourse().getId());
        finder.append(" and  c.tradingrecord.id= "
            + tradingrecord.getId());
        int size = dao.countQueryResult(finder);
        if (size == 0) {
          Course course = courseDao.findById(tradingrecord
              .getCourse().getId());
          int lessions = course.getLesson();
          List<CourseLessonStudent> lists = new ArrayList<CourseLessonStudent>();
          for (int i = 1; i <= lessions; i++) {
            CourseLessonStudent item = new CourseLessonStudent();
            item.setPeriod(i);
            item.setCourse(course);
            item.setMember(member);
            item.setTradingrecord(tradingrecord);
            courseLessonStudentDao.save(item);
            lists.add(item);
          }
        }
      } else {
        result = 3;

      }
    }
    return result;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUser(int userid, int statetype, int pageNo,
                               int pageSize) {
    Finder finder = Finder.create();
    finder.append("select ");
    finder.append("c.id,");
    finder.append("c.course.imgpath, ");
    finder.append("c.course.name, ");
    finder.append("c.course.id,");
    finder.append("c.tradingdate,");
    finder.append("c.money,");
    finder.append("c.state,");
    finder.append("c.member.name, ");
    finder.append("c.refundstate");

    finder.append(" from Tradingrecord c");

    finder.append(" where c.course.member.id= " + userid);
    finder.append(" and c.state= " + statetype);
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByBuyUser(int userid, int statetype, int pageNo,
                                  int pageSize) {
    Finder finder = Finder.create();
    finder.append("select ");
    finder.append("c.id,");
    finder.append("c.course.name, ");
    finder.append("c.money,");
    finder.append("c.course.lesson, ");
    finder.append("c.tradingdate,");
    finder.append("c.course.begintime,");
    finder.append("c.state,");
    finder.append("c.course.id,");
    finder.append("c.refundstate");

    finder.append(" from Tradingrecord c");
    finder.append(" where c.member.id= " + userid);
    finder.append(" and c.state= " + statetype);
    finder.append(" and c.deletestate=0 ");

    finder.append(" order by c.id desc");
    return dao.find(finder, pageNo, pageSize);
  }

  private static final String salt = "googlebaidu";

  PwdEncoder pwdEncoder;

  @Autowired
  SystemInfoDao systemInfoDao;

  @Transactional
  @Override
  public String payusepassword(int id, String password, Member member) {
    String result = null;
    Member cur = memberInfoDao.findById(member.getId());
    Tradingrecord tradingrecord = findById(id);
    if (tradingrecord.getState() == 2) {

      result = "支付过了";
      return result;
    }
    int left = tradingrecord.getCourse().getTimes()
        - tradingrecord.getCourse().getJointimes();
    if (left <= 0) {
      return "该课程已经招满人了";
    }
    pwdEncoder = new Md5PwdEncoder();
    if (cur != null) {

      String dbpassword = "";
      if (dbpassword == null) {
        result = "请到我的账户设置交易密码";
      } else {
        if (pwdEncoder.isPasswordValid(dbpassword, password, salt)) {


          if (cur.getMoneys() < tradingrecord.getMoney()) {
            result = "你账号上的钱不够";
            return result;
          }
          // 登陆成功
          Scorerecord record = new Scorerecord();
          record.setMember(member);
          record.setScore(tradingrecord.getMoney());
          record.setDemo("余额支付:" + "购买了"
              + tradingrecord.getCourse().getName());
          record.setSummary("余额支付:" + "购买了"
              + tradingrecord.getCourse().getName());
          record.setTransactiondate(new Timestamp(System
              .currentTimeMillis()));
          int all = (int) (tradingrecord.getMoney() + scorerecordDao
              .getallscore(member.getId()));
          record.setLeftscore(all);
          scorerecordDao.save(record);


          //学生通知
          SystemInfo systemInfo = new SystemInfo();
          systemInfo.setAdddate(new Timestamp(System.currentTimeMillis()));
          systemInfo.setState(0);
          systemInfo.setReadsate(0);
          systemInfo.setMember(member);
          String info = getinfo(member, tradingrecord);
          systemInfo.setContent(info);
          systemInfoDao.save(systemInfo);
          //老师通知
          SystemInfo teacherinfo = new SystemInfo();
          teacherinfo.setAdddate(new Timestamp(System.currentTimeMillis()));
          teacherinfo.setState(0);
          teacherinfo.setReadsate(0);
          teacherinfo.setMember(tradingrecord.getCourse().getMember());
          String content = getteacherinfo(member, tradingrecord);
          teacherinfo.setContent(content);
          systemInfoDao.save(teacherinfo);
          //String info="尊敬的（门牌号：G1102546445）您好，您购买学大教育（门牌号：S4564564565）（或李娟老师（门牌号：T1102546445））的中考一对一（课程编号：T1102546445）课程报名和付款已成功，请及时联系课程发布机构或老师按开课时间准时上课";

          MoneyRecord bean = new MoneyRecord();
          bean.setAdddate(new Timestamp(System.currentTimeMillis()));
          bean.setTradingdate(new Timestamp(System
              .currentTimeMillis()));
          bean.setMoney(-tradingrecord.getMoney());
          bean.setMember(cur);
          bean.setSeller(tradingrecord.getCourse().getMember());
          bean.setName("购买了" + tradingrecord.getCourse().getName());
          bean.setState(1);
          bean.setCourse(tradingrecord.getCourse());
          // bean.set
          moneyRecordDao.save(bean);

          scorerecordDao.getallscore(member.getId());
          float money = cur.getMoneys() - tradingrecord.getMoney();
          cur.setMoneys(money);

          Finder finder = Finder.create();
          finder.append("from CourseLessonStudent c");
          finder.append(" where  c.course.id= "
              + tradingrecord.getCourse().getId());
          finder.append(" and  c.tradingrecord.id= "
              + tradingrecord.getId());
          int size = dao.countQueryResult(finder);
          if (size == 0) {
            Course course = courseDao.findById(tradingrecord
                .getCourse().getId());
            int jsize = course.getJointimes();
            jsize++;
            course.setJointimes(jsize);
            int lessions = course.getLesson();
            List<CourseLessonStudent> lists = new ArrayList<CourseLessonStudent>();
            for (int i = 1; i <= lessions; i++) {
              CourseLessonStudent item = new CourseLessonStudent();
              item.setPeriod(i);
              item.setCourse(course);
              item.setMember(member);
              item.setTradingrecord(tradingrecord);
              courseLessonStudentDao.save(item);
              lists.add(item);
            }
          }
          tradingrecord.setState(2);
          result = "支付成功";
        } else {
          result = "交易密码不正确";
        }
      }

    } else {
      result = "系统不存在此用户";

    }

    return result;
  }

  private String getteacherinfo(Member member, Tradingrecord tradingrecord) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("尊敬的<" + tradingrecord.getCourse().getMember().getName() + ">");
    buffer.append("(门牌号:" + tradingrecord.getCourse().getMember().getId() + ")你好,");
    buffer.append("您发布的<" + tradingrecord.getCourse().getMember().getName() + ">");
    buffer.append("(课程编号:" + tradingrecord.getCourse().getId() + ")");
    buffer.append("被");
    buffer.append("<" + member.getName() + ">");
    buffer.append("(门牌号:" + member.getId() + ")");
    buffer.append("购买并付款成功，请及时联系客户安排上课事宜");
    String content = buffer.toString();
    return content;
  }

  private String getinfo(Member member, Tradingrecord tradingrecord) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("尊敬的<" + member.getName() + ">");
    buffer.append("(门牌号:" + member.getId() + ")你好,");
    buffer.append("您购买<" + tradingrecord.getCourse().getMember().getName() + ">");
    buffer.append("(门牌号:" + tradingrecord.getCourse().getMember().getId() + ")的");
    buffer.append("" + tradingrecord.getCourse().getName());
    buffer.append("(课程编号:" + tradingrecord.getCourse().getId() + ")");
    buffer.append("课程报名和付款已成功，请及时联系课程发布机构或老师按开课时间准时上课");
    return buffer.toString();
  }

  @Transactional
  @Override
  public Tradingrecord delete(Integer id) {
    Tradingrecord tradingrecord = findById(id);
    tradingrecord.setDeletestate(1);
    CourseSubscribe c = tradingrecord.getCourseSubscribe();

    if (c != null) {
      c.setStatetype(100);
    }
    return tradingrecord;
  }

  @Transactional
  @Override
  public Tradingrecord refunds(int id) {
    Tradingrecord tradingrecord = findById(id);
    tradingrecord.setState(3);
    return tradingrecord;
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByUserGood(int userid, int statetype, int pageNo,
                                   int pageSize) {
    Finder finder = Finder.create();

    finder.append(" from CourseSubscribe c");

    finder.append(" where c.course.member.id= " + userid);
    finder.append(" and c.state= 1");
    finder.append(" and c.tradingrecord.state= " + statetype);
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByBuyUserInfo(int userid, int statetype, int pageNo,
                                      int pageSize) {
    Finder finder = Finder.create();
    finder.append(" from Tradingrecord c");
    finder.append(" where c.member.id= " + userid);
    finder.append(" and c.state= " + statetype);
    finder.append(" and c.deletestate=0 ");

    finder.append(" order by c.id desc");
    return dao.find(finder, pageNo, pageSize);
  }

  @Transactional(readOnly = true)
  @Override
  public Pagination pageByBuyUser2(int userid, int statetype, int pageNo,
                                   int pageSize) {
    Finder finder = Finder.create();
    finder.append(" from Tradingrecord c");
    finder.append(" where c.member.id= " + userid);
    finder.append(" and c.state= " + statetype);
    finder.append(" and c.deletestate=0 ");

    finder.append(" order by c.id desc");
    return dao.find(finder, pageNo, pageSize);
  }
}