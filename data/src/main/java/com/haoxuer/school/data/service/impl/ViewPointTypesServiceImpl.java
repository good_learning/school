package com.haoxuer.school.data.service.impl;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ViewPointTypesDao;
import com.haoxuer.school.data.entity.ViewPointTypes;
import com.haoxuer.school.data.service.ViewPointTypesService;

@Service
@Transactional
public class ViewPointTypesServiceImpl implements ViewPointTypesService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public ViewPointTypes findById(Integer id) {
    ViewPointTypes entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public ViewPointTypes save(ViewPointTypes bean) {
    dao.save(bean);
    return bean;
  }

  @Transactional
  public ViewPointTypes update(ViewPointTypes bean) {
    Updater<ViewPointTypes> updater = new Updater<ViewPointTypes>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public ViewPointTypes deleteById(Integer id) {
    ViewPointTypes bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public ViewPointTypes[] deleteByIds(Integer[] ids) {
    ViewPointTypes[] beans = new ViewPointTypes[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ViewPointTypesDao dao;

  @Autowired
  public void setDao(ViewPointTypesDao dao) {
    this.dao = dao;
  }
}