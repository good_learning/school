package com.haoxuer.school.data.service.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ArticleDao;
import com.haoxuer.school.data.dao.ViewpointDao;
import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.school.data.service.ViewpointService;

@Service
@Transactional
public class ViewpointServiceImpl implements ViewpointService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Viewpoint findById(Integer id) {
    Viewpoint entity = dao.findById(id);
    return entity;
  }

  @Transactional
  public Viewpoint save(Viewpoint bean) {
    bean.setGoodstate(0);
    dao.save(bean);
    return bean;
  }

  @Transactional
  public Viewpoint update(Viewpoint bean) {
    Updater<Viewpoint> updater = new Updater<Viewpoint>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Viewpoint deleteById(Integer id) {
    Viewpoint bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Viewpoint[] deleteByIds(Integer[] ids) {
    Viewpoint[] beans = new Viewpoint[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ViewpointDao dao;

  @Autowired
  public void setDao(ViewpointDao dao) {
    this.dao = dao;
  }

  @Override
  public Pagination pageByUserId(int userid, int pageNo, int pageSize) {
    return dao.pageByUserId(userid, pageNo, pageSize);
  }

  @Transactional
  @Override
  public Viewpoint up(int id) {
    return dao.up(id);
  }

  @Transactional
  @Override
  public Viewpoint view(Integer id) {
    Viewpoint entity = dao.findById(id);
    Integer views = entity.getViews();
    if (views == null) {
      views = 0;
    }
    views++;
    entity.setViews(views);
    return entity;
  }


  @Transactional(readOnly = true)
  @Override
  public Pagination pageByGood(int pageNo, int pageSize) {
    Pagination result = new Pagination();
    Finder finder = Finder
        .create("from Viewpoint u where u.goodstate = ");
    finder.append("" + 0);
    finder.append(" and u.catalogs >0");

    finder.append("   order by  u.id  desc");
    result = dao.find(finder, pageNo, pageSize);
    return result;
  }

  @Autowired
  ArticleDao articleDao;


  @Transactional
  @Override
  public void review(Viewpoint viewpoint) {
    update(viewpoint);
    viewpoint = dao.findById(1);
    if (viewpoint.getGoodstate() == 1 && viewpoint.getArticeid() == null) {
      Article article = new Article();
      article.setMember(viewpoint.getMember());
      article.setTitle(viewpoint.getTitle());
      article.setContent(viewpoint.getContent());
      article.setSummary(viewpoint.getSummary());
      Articletype articletype = new Articletype();
      articletype.setId(viewpoint.getCatalogs());
      article.setArticletype(articletype);
      article.setImgpath(viewpoint.getImgpath());
      article.setPubdate(new Timestamp(System.currentTimeMillis()));
      article.setUps(0);
      article.setComments(0);
      article.setCommentsize(0);
      article.setIs_recommend(0);
      article.setViews(0);
      articleDao.save(article);
      viewpoint.setArticeid(article.getId());
    }

  }

  @Override
  public Pagination pageData(Map<String, Object> params,
                             List<String> orders, int pageNo, int pageSize) {

    Finder finder = Finder.create("from Viewpoint c");
    return dao.find(finder, pageNo, pageSize);
  }
}