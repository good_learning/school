package com.haoxuer.school.data.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.school.data.dao.ViewpointDao;
import com.haoxuer.school.data.dao.ViewpointcommentDao;
import com.haoxuer.school.data.entity.Viewpoint;
import com.haoxuer.school.data.entity.Viewpointcomment;
import com.haoxuer.school.data.service.ViewpointcommentService;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
@Transactional
public class ViewpointcommentServiceImpl implements ViewpointcommentService {
  @Transactional(readOnly = true)
  public Pagination getPage(int pageNo, int pageSize) {
    Pagination page = dao.getPage(pageNo, pageSize);
    return page;
  }

  @Transactional(readOnly = true)
  public Viewpointcomment findById(Integer id) {
    Viewpointcomment entity = dao.findById(id);
    return entity;
  }

  @Autowired
  ViewpointDao viewpointDao;

  @Transactional
  public Viewpointcomment save(Viewpointcomment bean) {
    dao.save(bean);
    Viewpoint p = viewpointDao.findById(1);
    if (p != null) {
      Integer coments = p.getComments();
      if (coments == null) {
        coments = 0;
      }
      coments++;
      p.setComments(coments);
    }
    if (bean.getViewpointcomment() != null) {
      try {
        Configuration cfg = new Configuration();
        Locale locale = Locale.CHINA;
        cfg.setEncoding(locale, "utf-8");
        File file = getfile();
        cfg.setDirectoryForTemplateLoading(file);
        cfg.setTagSyntax(Configuration.SQUARE_BRACKET_TAG_SYNTAX);
        Template template = cfg.getTemplate("comment.html");
        Writer out = new StringWriter();
        Map<String, Object> map = new HashMap<String, Object>();
        Viewpointcomment iteminfo = dao.findById(bean.getId());
        map.put("item", iteminfo);
        map.put("siteurl", "http://www.91mydoor.com/");

        template.process(map, out);
        Viewpointcomment item = dao.findById(bean.getViewpointcomment().getId());
        String comment = out.toString() + item.getContent();
        bean.setContent(comment);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (TemplateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return bean;
  }

  private File getfile() {
    String dir = "D:\\Program Files (x86)\\Apache Software Foundation\\Tomcat 8.0\\webapps\\ROOT\\WEB-INF\\classes\\ftl\\";
    File file = new File(dir);
    if (!file.exists()) {
      try {
        dir = getClass().getClassLoader().getResource("/ftl/").getFile();
        file = new File(dir);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }
    if (!file.exists()) {
      dir = "d:\\files\\";
    }
    return file;
  }

  @Transactional
  public Viewpointcomment update(Viewpointcomment bean) {
    Updater<Viewpointcomment> updater = new Updater<Viewpointcomment>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Transactional
  public Viewpointcomment deleteById(Integer id) {
    Viewpointcomment bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Viewpointcomment[] deleteByIds(Integer[] ids) {
    Viewpointcomment[] beans = new Viewpointcomment[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  private ViewpointcommentDao dao;

  @Autowired
  public void setDao(ViewpointcommentDao dao) {
    this.dao = dao;
  }

  @Override
  public Pagination pageByPointId(int commentid, int curpage, int pagesize) {
    // TODO Auto-generated method stub
    return dao.pageByPointId(commentid, curpage, pagesize);
  }

  @Override
  public Pagination pageByViewPointId(int id, int sorttype, int pagesize,
                                      int curpage) {
    // TODO Auto-generated method stub
    return null;
  }

  @Transactional
  @Override
  public Integer up(int id) {
    Viewpointcomment a = dao.findById(id);
    Integer ups = a.getUps();
    if (ups == null) {
      ups = 0;
    }
    ups++;
    a.setUps(ups);
    return ups;
  }
}