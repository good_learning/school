DELIMITER $$

DROP FUNCTION IF EXISTS `mendao`.`getParentList`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `getParentList`(rootId INT) RETURNS varchar(1000) CHARSET utf8
    BEGIN
	DECLARE parentStr VARCHAR(1000); 	
	DECLARE parentId INT;
	DECLARE curName VARCHAR(1000);
	SET parentId = rootId;
	SET parentStr = '';
	
	WHILE parentId > 1 DO
		SELECT c.parent_id, c.name INTO parentId, curName from courseCatalog c  WHERE id = parentId;
		IF (parentId IS NOT NULL) THEN
			SET parentStr = concat(parentStr,',',curName);
		END IF;
	END WHILE;
	RETURN parentStr;
    END$$

DELIMITER ;