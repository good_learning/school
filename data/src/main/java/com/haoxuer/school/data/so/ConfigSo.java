package com.haoxuer.school.data.so;

import java.io.Serializable;

/**
* Created by imake on 2019年10月19日18:25:41.
*/
public class ConfigSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
