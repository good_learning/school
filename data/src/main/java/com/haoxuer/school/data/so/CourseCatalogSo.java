package com.haoxuer.school.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年11月20日12:49:25.
*/
@Data
public class CourseCatalogSo implements Serializable {



    private String sortField;

    private String sortMethod;

}
