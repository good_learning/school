package com.haoxuer.school.web.freemaker;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.Article;
import com.haoxuer.school.data.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class ArticleDirective implements TemplateDirectiveModel {

  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {
    // TODO Auto-generated method stub


    Integer pid = DirectiveUtils.getInt("pid", params);
    Integer size = DirectiveUtils.getInt("size", params);
    Integer sorttype = DirectiveUtils.getInt("sorttype", params);

    List<Article> rankList = articleService.findArticlesByPid(pid, size, sorttype);

    env.setVariable("list",
        ObjectWrapper.DEFAULT_WRAPPER.wrap(rankList));
    if (body != null) {
      body.render(env.getOut());
    }

  }

  @Autowired
  ArticleService articleService;
}
