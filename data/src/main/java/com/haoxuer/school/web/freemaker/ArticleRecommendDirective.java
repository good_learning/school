package com.haoxuer.school.web.freemaker;

import java.io.IOException;
import java.util.Map;

import com.haoxuer.school.data.service.ArticleService;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class ArticleRecommendDirective implements TemplateDirectiveModel {
  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {
    // TODO Auto-generated method stub


    Integer type = DirectiveUtils.getInt("type", params);
    Integer size = DirectiveUtils.getInt("size", params);

    Pagination rankList = articleService.recommend(type, size, 1);

    env.setVariable("recommendslist",
        ObjectWrapper.DEFAULT_WRAPPER.wrap(rankList.getList()));
    if (body != null) {
      body.render(env.getOut());
    }
  }

  @Autowired
  ArticleService articleService;
}
