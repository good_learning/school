package com.haoxuer.school.web.freemaker;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.Articletype;
import com.haoxuer.school.data.service.ArticletypeService;
import org.springframework.beans.factory.annotation.Autowired;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class ArticleTypeDirective implements TemplateDirectiveModel {

  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {

    Integer pid = DirectiveUtils.getInt("pid", params);
    List<Articletype> rankList = articletypeService.findChild(pid);

    env.setVariable("articletype",
        ObjectWrapper.DEFAULT_WRAPPER.wrap(rankList));
    if (body != null) {
      body.render(env.getOut());
    }
  }

  @Autowired
  ArticletypeService articletypeService;
}
