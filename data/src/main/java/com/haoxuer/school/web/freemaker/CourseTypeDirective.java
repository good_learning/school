package com.haoxuer.school.web.freemaker;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.haoxuer.school.data.entity.CourseCatalog;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class CourseTypeDirective implements TemplateDirectiveModel {

  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {


    //其实完全可以不用它，params是个Map，自己通过key取值就可以了，做一下空值判断
    Integer pid = DirectiveUtils.getInt("pid", params);
    List<CourseCatalog> rankList = null;

    env.setVariable("coursetype", ObjectWrapper.DEFAULT_WRAPPER.wrap(rankList));
    if (body != null) {
      body.render(env.getOut());
    }
  }


}
