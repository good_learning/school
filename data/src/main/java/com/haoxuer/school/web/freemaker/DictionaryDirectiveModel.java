package com.haoxuer.school.web.freemaker;

import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.haoxuer.discover.config.data.entity.Dictionary;
import com.haoxuer.discover.config.data.service.DictionaryService;
import com.haoxuer.discover.data.page.Filter;
import org.springframework.beans.factory.annotation.Autowired;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class DictionaryDirectiveModel implements TemplateDirectiveModel {

  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {
    // TODO Auto-generated method stub
    // TODO Auto-generated method stub
    String pid = DirectiveUtils.getString("type", params);

    List<Filter> filters=new ArrayList<>();
    List<Dictionary> ds = dictionaryService.list(0,100,filters,null);
    Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(
        params);
    paramWrap.put("dictionarys", DEFAULT_WRAPPER.wrap(ds));
    Map<String, TemplateModel> origMap = DirectiveUtils
        .addParamsToVariable(env, paramWrap);
    if (body != null) {
      body.render(env.getOut());
    }
    DirectiveUtils.removeParamsFromVariable(env, paramWrap, origMap);

  }

  @Autowired
  private DictionaryService dictionaryService;
}
