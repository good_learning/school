package com.haoxuer.school.web.freemaker;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * HTML文本提取并截断
 * <p>
 * 需要拦截器com.jeecms.common.web.ProcessTimeFilter支持
 */
public class HtmlDirective implements TemplateDirectiveModel {
  public static final String PARAM_S = "s";

  @SuppressWarnings("unchecked")
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {
    String s = DirectiveUtils.getString(PARAM_S, params);
    if (s != null) {
      Writer out = env.getOut();
      out.append(s.replace("\n", "<br/>"));
    }
  }
}
