package com.haoxuer.school.web.freemaker;

import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.haoxuer.school.data.service.CourseService;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class RecommendCourseDirective implements TemplateDirectiveModel {


  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {
    // TODO Auto-generated method stub


    Integer type = DirectiveUtils.getInt("type", params);
    Integer size = DirectiveUtils.getInt("size", params);

    Pagination rankList = courseService.good(type, 1, size);

    Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(
        params);
    paramWrap.put("list", DEFAULT_WRAPPER.wrap(rankList.getList()));
    Map<String, TemplateModel> origMap = DirectiveUtils
        .addParamsToVariable(env, paramWrap);
    if (body != null) {
      body.render(env.getOut());
    }
    DirectiveUtils.removeParamsFromVariable(env, paramWrap, origMap);

  }

  @Autowired
  CourseService courseService;
}
