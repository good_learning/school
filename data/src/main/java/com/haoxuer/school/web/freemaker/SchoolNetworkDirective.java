package com.haoxuer.school.web.freemaker;

import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.haoxuer.school.data.service.SchoolnetworkService;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class SchoolNetworkDirective implements TemplateDirectiveModel {


  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {
    // TODO Auto-generated method stub


    Integer schoolid = DirectiveUtils.getInt("schoolid", params);
    Integer size = DirectiveUtils.getInt("size", params);
    Integer pageNo = DirectiveUtils.getInt("page", params);

    Pagination rankList = schoolnetworkService.getPage(schoolid, pageNo, size);

    Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(
        params);
    paramWrap.put("list", DEFAULT_WRAPPER.wrap(rankList.getList()));
    Map<String, TemplateModel> origMap = DirectiveUtils
        .addParamsToVariable(env, paramWrap);
    if (body != null) {
      body.render(env.getOut());
    }
    DirectiveUtils.removeParamsFromVariable(env, paramWrap, origMap);

  }

  @Autowired
  SchoolnetworkService schoolnetworkService;
}
