package com.haoxuer.school.web.freemaker;

import java.io.IOException;
import java.util.Map;

import com.haoxuer.school.data.service.AttachmentService;
import com.haoxuer.discover.data.core.Pagination;
import org.springframework.beans.factory.annotation.Autowired;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class TeacherCourseImgsDirective implements TemplateDirectiveModel {

  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars,
                      TemplateDirectiveBody body) throws TemplateException, IOException {


    //其实完全可以不用它，params是个Map，自己通过key取值就可以了，做一下空值判断
    Integer courseid = DirectiveUtils.getInt("id", params);
    Integer pageSize = DirectiveUtils.getInt("size", params);
    Integer type = DirectiveUtils.getInt("type", params);

    Pagination page = attachmentService.pagetypeByUserId(
        courseid, type, 1, pageSize);


    env.setVariable("courseimgs", ObjectWrapper.DEFAULT_WRAPPER.wrap(page.getList()));
    if (body != null) {
      body.render(env.getOut());
    }
  }

  @Autowired
  AttachmentService attachmentService;
}