package com.haoxuer.school.web.interceptor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.school.data.entity.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AdminHandlerInterceptor extends HandlerInterceptorAdapter {

  private Logger logger = LoggerFactory.getLogger("ada");

  @Override
  public boolean preHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler)
      throws ServletException, IOException {
    logger.info("LogHandlerInterceptor  preHandle");

    String url = request.getRequestURI();
    if (url.indexOf("/admin/") > 0 && url.indexOf("/admin/login") < 0
        && url.indexOf("/admin/ronglian/") < 0) {
      HttpSession session = getSession(request);
      Object member = session.getAttribute("admin");
      if (member != null) {
        if (member instanceof Member) {

        } else {
          request.setAttribute("info", "你还没有登陆");
          request.getRequestDispatcher("/login.do").forward(request,
              response);
          return false;
        }
      } else {
        request.setAttribute("info", "你还没有登陆");
        request.getRequestDispatcher("/login.do").forward(request,
            response);
        return false;
      }
    }

    return true;
  }

  private String url;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  private HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      logger.info("会话不存在");
      session = request.getSession(true);
    }
    return session;
  }

  @Override
  public void postHandle(HttpServletRequest request,
                         HttpServletResponse response, Object handler,
                         ModelAndView modelAndView) {

    logger.info("LogHandlerInterceptor  postHandle");
  }
}
