package com.haoxuer.school.web.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.haoxuer.school.data.entity.forum.ForumColumn;
import com.haoxuer.school.data.service.ForumPostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ForumHandlerInterceptor extends HandlerInterceptorAdapter {

  @Autowired
  ForumPostService forumPostService;

  private Logger logger = LoggerFactory.getLogger("ada");

  @Override
  public boolean preHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler) {
    logger.info("ForumHandlerInterceptor  preHandle");


    return true;
  }

  @Override
  public void postHandle(HttpServletRequest request,
                         HttpServletResponse response, Object handler,
                         ModelAndView modelAndView) {
    logger.info("LogHandlerInterceptor  postHandle ");

    String requestType = request.getHeader("X-Requested-With");
    if (requestType != null && "XMLHttpRequest".equals(requestType.trim())) {
      return;
    }
    String url = request.getRequestURI();
    if (url.indexOf("/forum/") > 0) {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("levelinfo =", 2);
      List<ForumColumn> columns = forumPostService.getForumColumnChild(1);

      ForumColumn activity = forumPostService.findForumColumnById(7);

      if (columns.size() > 0) {
        columns.add(activity);
        modelAndView.addObject("sitecolumns", columns);
      }
    }
  }
}
