package com.haoxuer.school.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.haoxuer.school.web.utils.FrontUtils;

public class SiteHandlerInterceptor extends HandlerInterceptorAdapter {

  private Logger logger = LoggerFactory.getLogger("ada");

  @Override
  public boolean preHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler) {
    logger.info("LogHandlerInterceptor  preHandle");
    return true;
  }

  private String url;
  private String cdn;

  public String getCdn() {
    return cdn;
  }

  public void setCdn(String cdn) {
    this.cdn = cdn;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public void postHandle(HttpServletRequest request,
                         HttpServletResponse response, Object handler,
                         ModelAndView modelAndView) {
    if (modelAndView != null) {
      logger.info("LogHandlerInterceptor  postHandle "
          + modelAndView.getViewName());
      String siteurl = "siteurl";
      if (url != null) {
        siteurl = url;
      } else {
        siteurl = "http://" + request.getLocalAddr() + ":"
            + request.getLocalPort() + request.getContextPath();
      }
      logger.info("url:" + url);
      String url = request.getScheme() + "://";
      url += request.getHeader("host");
      url += request.getRequestURI();
//			  if(request.getQueryString()!=null)   
//			      url+="?"+request.getQueryString(); 
      logger.info("Referer:" + request.getHeader("Referer"));
      modelAndView.addObject("siteurl", siteurl);
      modelAndView.addObject("cururl", url);
      modelAndView.addObject("referer", request.getHeader("Referer"));
      modelAndView.addObject("cdn", cdn);
      modelAndView.addObject("sitename", "门道管理系统");
      FrontUtils.format(request, modelAndView);
    }

  }
}
