package com.haoxuer.school.web.interceptor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.school.data.entity.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class UserHandlerInterceptor extends HandlerInterceptorAdapter {

  private Logger logger = LoggerFactory.getLogger("ada");

  @Override
  public boolean preHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler)
      throws ServletException, IOException {
    logger.info("LogHandlerInterceptor  preHandle");
    String url = request.getRequestURI();
    if (url.indexOf("/member/") > 0 || url.indexOf("/user/") > 0) {
      HttpSession session = getSession(request);
      session.setAttribute("returnUrl", url);
      Member member = (Member) session.getAttribute("member");
      if (member == null) {
        // 如果是Ajax请求，立即结束
        if ("XMLHttpRequest".equals(request
            .getHeader("X-Requested-With"))) {
          response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
          return false;
        }
        request.setAttribute("info", "你还没有登陆");
        request.getRequestDispatcher("/login.htm").forward(request,
            response);
        return false;
      }
//      if (member != null && member.getState() == 0) {
//        // session.setAttribute("member", null);
//        request.setAttribute("phonenum", member.getPhonenum());
//        request.setAttribute("info", "您还没有激活账户");
//        // response.sendRedirect("/activate.htm");
//        request.getRequestDispatcher("/activate.htm").forward(request,
//            response);
//        return false;
//      }
    }
    return true;
  }

  private String url;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  private HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      logger.info("会话不存在");
      session = request.getSession(true);
    }
    return session;
  }

  @Override
  public void postHandle(HttpServletRequest request,
                         HttpServletResponse response, Object handler,
                         ModelAndView modelAndView) {

    String url = request.getRequestURI();
    if (url.indexOf("/user/") > 0) {
      HttpSession session = getSession(request);
      Object member = session.getAttribute("member");
      if (member != null) {
        if (member instanceof Member) {

        } else {
          modelAndView.setViewName("login");
          modelAndView.addObject("info", "你没有登陆");
        }
      } else {
        modelAndView.setViewName("login");
        modelAndView.addObject("info", "你没有登陆");
      }
    }
    if (url.indexOf("/member/") > 0) {
      HttpSession session = getSession(request);
      Object member = session.getAttribute("member");
      if (member != null) {
        if (member instanceof Member) {
//          if (((Member) member).getState() == 0) {
//            modelAndView.setViewName("login");
//            modelAndView.addObject("info", "你还没有激活");
//          }
        } else {
          modelAndView.setViewName("login");
          modelAndView.addObject("info", "你没有登陆");
        }
      } else {
        modelAndView.setViewName("login");
        modelAndView.addObject("info", "你没有登陆");
      }
    }

  }
}
