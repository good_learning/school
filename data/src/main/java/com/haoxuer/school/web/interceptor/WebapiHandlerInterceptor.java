package com.haoxuer.school.web.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.haoxuer.school.data.entity.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class WebapiHandlerInterceptor extends HandlerInterceptorAdapter {
  private Logger logger = LoggerFactory.getLogger("ada");

  @Override
  public boolean preHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler)
      throws Exception {
    logger.info("WebapiHandlerInterceptor  preHandle");
    String url = request.getRequestURI();

    HttpSession session = getSession(request);
    Member member = (Member) session.getAttribute("member");

    // 需要登陆权限
    if (url.indexOf("/auth/") > 0) {
      if (member == null) {
        throw new Exception("ERROR:404");
      } else if (!request.getParameter("userid").equals(
          String.valueOf(member.getId()))) {
        throw new Exception("您的账户异常");
      }
    }

    return true;
  }

  private HttpSession getSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session == null) {
      logger.info("会话不存在");
      session = request.getSession(true);
    }
    return session;
  }

  @Override
  public void postHandle(HttpServletRequest request,
                         HttpServletResponse response, Object handler,
                         ModelAndView modelAndView) {

    // 设置登录用户cookie
    HttpSession session = getSession(request);
    Cookie cookie = new Cookie("JSESSIONID", session.getId());
    response.addCookie(cookie);
  }
}
