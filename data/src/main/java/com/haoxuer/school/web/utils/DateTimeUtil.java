package com.haoxuer.school.web.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class DateTimeUtil {

  protected static SimpleDateFormat dtFormat = new SimpleDateFormat(
      "yyyy-MM-dd");
  protected static SimpleDateFormat dtFormat1 = new SimpleDateFormat(
      "yyyy-MM-dd HH:mm");
  protected static SimpleDateFormat dtFormat2 = new SimpleDateFormat("HH:mm");

  public static String format(Timestamp datetime, String format) {

    if (datetime == null)
      return "";
    SimpleDateFormat df = new SimpleDateFormat(format);
    return df.format(datetime);
  }

  public static String getDateAndTime(Timestamp datetime) {
    long time = ((new Timestamp(System.currentTimeMillis())).getTime() - datetime
        .getTime()) / (1000 * 60 * 60 * 24);

    if (time < 1) {
      return "今天 " + dtFormat2.format(datetime).substring(11);
    } else if (time == 1) {
      return "昨天 " + dtFormat2.format(datetime).substring(11);
    } else if (time == 2) {
      return "前天 " + dtFormat2.format(datetime).substring(11);
    } else {
      return dtFormat1.format(datetime);
    }
  }

  public static String getTimesAgo(Timestamp datetime) {
    if (datetime == null)
      return "";
    long millis = (new Timestamp(System.currentTimeMillis())).getTime()
        - datetime.getTime();
    long time = millis / 1000;
    long tempTime = time;
    if (tempTime < 60) {
      return "刚刚";
    }
    tempTime = time / (60 * 1000);
    if (tempTime < 60) {
      return String.format("%d分钟前", tempTime);
    }

    tempTime = time / (60 * 1000 * 60);
    if (tempTime < 60) {
      return String.format("%d小时前", tempTime);
    }

    tempTime = time / (60 * 1000 * 60 * 24);

    if (tempTime < 1) {
      return "今天";
    } else if (tempTime == 1) {
      return "昨天";
    } else if (tempTime == 2) {
      return "前天";
    } else if (tempTime >= 3 && tempTime < 7) {
      return "三天前";
    } else if (tempTime >= 7 && tempTime < 14) {
      return "一周前";
    } else if (tempTime >= 14 && tempTime < 21) {
      return "两周前";
    } else if (tempTime >= 21 && tempTime < 30) {
      return "三周前";
    }

    return "一月前";
  }
}
