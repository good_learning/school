package com.haoxuer.school.web.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

/**
 * 前台工具类
 */
public class FrontUtils {

  public static void format(HttpServletRequest request, ModelMap modelMap) {
    Enumeration<String> enumerations = request.getParameterNames();
    while (enumerations.hasMoreElements()) {
      String pname = enumerations.nextElement();
      modelMap.put(pname, request.getAttribute(pname));
    }
  }

  public static void format(HttpServletRequest request,
                            ModelAndView modelAndView) {
    Enumeration<String> enumerations = request.getParameterNames();
    while (enumerations.hasMoreElements()) {
      String pname = enumerations.nextElement();
      modelAndView.addObject(pname, request.getParameter(pname));
    }
  }

  public static Timestamp parseTimestamp(String time) {
    Timestamp resutl = null;
    try {
      SimpleDateFormat format = new SimpleDateFormat(
          "yyyy-MM-dd HH:mm:ss");
      Date date = format.parse(time);
      resutl = new Timestamp(date.getTime());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return resutl;

  }
}
