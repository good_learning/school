package com.haoxuer.school.web.utils;

import java.util.HashMap;
import java.util.Set;

public class UrlUtils {

  private HashMap<String, String> maps;
  private String url;

  private UrlUtils() {
    maps = new HashMap<String, String>();
  }

  public static UrlUtils create(String url) {
    UrlUtils result = new UrlUtils();
    result.url = url;
    return result;

  }

  public String tourl() {
    StringBuffer buffer = new StringBuffer();
    buffer.append(url);
    Set<String> headers = maps.keySet();
    if (headers != null && headers.size() > 0) {
      int i = 1;
      for (String string : headers) {

        if (headers.size() > 1) {
          if (i == 1) {
            buffer.append("?");
            buffer.append(string);
            buffer.append("=");
            buffer.append(maps.get(string));
          } else {
            buffer.append("&");
            buffer.append(string);
            buffer.append("=");
            buffer.append(maps.get(string));
          }
        } else {
          buffer.append("?");
          buffer.append(string);
          buffer.append("=");
          buffer.append(maps.get(string));
        }
        i++;
      }
    }

    return buffer.toString();

  }

  public static void main(String[] args) {
    UrlUtils con = UrlUtils
        .create("http://service.weibo.com/share/share.php");
    con.data("appkey", "1268671789");
    con.data("searchPic", "false");
    con.data("title", "门道网");
    con.data("url", "http://www.91mydoor.com/mendao/index.htm");
    System.out.println(con.tourl());

    UrlUtils con1 = UrlUtils
        .create("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey");

    con1.data("desc", "门道网");
    con1.data("site", "门道网");
    con1.data("summary", "门道网");
    con1.data("title", "门道网");
    con1.data("url", "http://www.91mydoor.com/mendao/index.htm");
    System.out.println(con1.tourl());

  }

  public void data(String headername, String headervalue) {
    // TODO Auto-generated method stub
    maps.put(headername, headervalue);
  }
}
