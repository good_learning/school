$(function(){
	$('.nav-more').hover(function(){
		$('.nav-popup').show();
	},function(){
		$('.nav-popup').hide();
	});

	//加入小组
	$('.join-team').bind('click',function(){
		$(this).html('√&nbsp;已加入');
		$(this).unbind();
	});

	//选择城市
	$('.city').on('click',function(e){
		if($('.address-city').is(':hidden')){
			e.stopPropagation();
			$('.address-city').show();
		}else{
			$('.address-city').hide();
		}
	});
	$('.address-city').delegate('li','click',function(){
		$('.city').text($(this).html());
		$('.address-city').hide();
	});
	//选择区域
	$('.area').on('click',function(e){
		if($('.address-area').is(':hidden')){
			e.stopPropagation();
			$('.address-area').show();
		}else{
			$('.address-area').hide();
		}
	});
	$('.address-area').delegate('li','click',function(){
		$('.area').text($(this).html());
		$('.address-area').hide();
	});
	$('body').on('click',function(){
		$('.address-list').hide();
	});

	//选择类型
	$('.type').delegate('a','click',function(){
		$('.type .active').removeClass('active');
		$(this).addClass('active');
	});
	//选择时间
	$('.time').delegate('a','click',function(){
		$('.time .active').removeClass('active');
		$(this).addClass('active');
	});



	if(!placeholderSupport()){   // 判断浏览器是否支持 placeholder
	    $('[placeholder]').focus(function() {
	        var input = $(this);
	        if (input.val() == input.attr('placeholder')) {
	            input.val('');
	            input.removeClass('placeholder');
	        }
	    }).blur(function() {
	        var input = $(this);
	        if (input.val() == '' || input.val() == input.attr('placeholder')) {
	            input.addClass('placeholder');
	            input.val(input.attr('placeholder'));
	        }
	    }).blur();
	};
	
	$('.report').click(function(){
		$('.report_div').show();
	});
	$('.report_div ul .close img').click(function(){
		$(".report_div").hide();
	});
	$(".open_managerlist").click(function(){
		if($(".manager_div").css("display")=="none"){
			$(".manager_div").css("display","block");
		}else{
			$(".manager_div").css("display","none");
		}
	});
	$(".message_list_tools .manager").hover(function(){
	  $(this).find(".nav-popup").show();
	},function(){
		$(this).find(".nav-popup").hide();
	});
	$(".message_list>li").find(".reply_bt").click(function(){
	console.log($(this).parents(".message_list>li").find(".message_ipt").css("display"));					
		
		$(this).parents(".message_list>li").find(".message_ipt").slideToggle("slow");					
	});
});
function placeholderSupport() {
    return 'placeholder' in document.createElement('input');
}