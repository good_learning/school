<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="jquery-2.1.1.js"></script>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	margin: 0;
}

#main {
	padding-top: 90px;
	text-align: center;
}

#fullbg {
	background-color: gray;
	left: 0;
	opacity: 0.5;
	position: absolute;
	top: 0;
	z-index: 3;
	filter: alpha(opacity = 50);
	-moz-opacity: 0.5;
	-khtml-opacity: 0.5;
}

#dialog {
	background-color: #fff;
	border: 5px solid rgba(0, 0, 0, 0.4);
	height: 400px;
	left: 50%;
	margin: -200px 0 0 -200px;
	padding: 1px;
	position: fixed !important; /* 浮动对话框 */
	position: absolute;
	top: 50%;
	width: 400px;
	z-index: 5;
	border-radius: 5px;
	display: none;
}

#dialog p {
	margin: 0 0 12px;
	height: 24px;
	line-height: 24px;
	background: #CCCCCC;
}

#dialog p.close {
	text-align: right;
	padding-right: 10px;
}

#dialog p.close a {
	color: #fff;
	text-decoration: none;
}
</style>
<script type="text/javascript">
	//显示灰色 jQuery 遮罩层
	function showBg() {
		var bh = $("body").height();
		var bw = $("body").width();
		$("#fullbg").css({
			height : bh,
			width : bw,
			display : "block"
		});
		$("#dialog").show();
	}
	//关闭灰色 jQuery 遮罩
	function closeBg() {
		$("#fullbg,#dialog").hide();
	}
</script>
</head>
<body>
	<div id="main">
		<a href="javascript:showBg();">点击这里查看效果</a>
		<div id="fullbg"></div>
		<div id="dialog">
			<p class="close">
				<a href="#" onclick="closeBg();">关闭</a>
			</p>
			<div>

				<div class="container">
					<div class="row" style="margin-top: 15px;">

						<!-- 聊天区 -->
						<div class="col-sm-8">
							<!-- 聊天内容 -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<span class="glyphicon glyphicon-earphone"></span> &nbsp;聊天内容
								</div>
								<div class="panel-body chat-body">
									<div class="msg-list-body">
										<!--<div class="clearfix msg-wrap">-->
										<!--<div class="msg-head">-->
										<!--<span class="msg-name label label-primary pull-left">-->
										<!--<span class="glyphicon glyphicon-user"></span>-->
										<!--&nbsp;Sc千寻-->
										<!--</span>-->
										<!--<span class="msg-time label label-default pull-left">-->
										<!--<span class="glyphicon glyphicon-time"></span>-->
										<!--&nbsp;21:34:15-->
										<!--</span>-->
										<!--</div>-->
										<!--<div class="msg-content">test</div>-->
										<!--</div>-->
									</div>
								</div>
							</div>

							<!-- 输入框 -->
							<div class="input-group input-group-lg">
								<span class="input-group-btn">
									<button class="btn btn-default" id="emotion-btn" type="button">
										<img src="img/emotion_smile.png"
											style="width: 24px; height: 24px;">
									</button>
								</span> <input type="text" class="form-control mousetrap" id="msg"
									placeholder="请输入聊天内容"> <span class="input-group-btn">
									<button class="btn btn-default" type="button" id="msgbnt">
										发送 <span class="glyphicon glyphicon-send"></span>
									</button>
								</span>
							</div>
						</div>




					</div>
				</div>


			</div>
		</div>
	</div>
	
	
		<script src="jquery-2.1.1.js"></script>
	<script src="assets/js/jquery.cookie.js"></script>
	<script src="assets/plugs/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
		href="assets/plugs/bootstrap/css/bootstrap.min.css">
		<script src="http://cdn.bootcss.com/socket.io/0.9.16/socket.io.min.js"></script>
	<script src="js/desktopnotify.js"></script>
	<script src="mousetrap.min.js"></script>
  
	<script type="text/javascript">
	var uname="<%=request.getParameter("name")%>";
	var toname="<%=request.getParameter("to")%>";
	function chatBodyToBottom() {
	    var chat_body = $('.chat-body');
	    var height = chat_body.prop("scrollHeight");
	    chat_body.prop('scrollTop', height);
	}
	function onClickChangeNickname() {
	    $('#login-modal').modal('show');
	}
	function getLocalHMS() {
	    var time = (new Date()).getTime();
	    var d = new Date();
	    return appendZero(d.getHours()) + ":" + appendZero(d.getMinutes()) + ":" + appendZero(d.getSeconds());
	}
	function appendZero(obj) {
	    if (obj < 10) return "0" + "" + obj;
	    else return obj;
	}
	function addServerMessage( _content) {
	    var msg_list = $(".msg-list-body");
	    msg_list.append(
	            '<div class="clearfix msg-wrap"><div class="msg-head">' +
	            '<span class="msg-name label label-danger pull-left">' +
	            '<span class="glyphicon glyphicon-info-sign"></span>&nbsp;&nbsp;系统消息</span>' +
	            '<span class="msg-time label label-default pull-left">' +
	            '<span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;' + getLocalHMS()+ '</span>' +
	            '</div><div class="msg-content">' + _content + '</div></div>'
	    );
	    chatBodyToBottom();
	}
	function onClickApplyNickname() {
		var nickname_edit = $('#nickname-edit');
		var nickname_error = $("#nickname-error");
	    var name = nickname_edit.val();
	    if ("" == name) {
		    nickname_error.text("请填写昵称。");
		    nickname_error.show();
		    nickname_edit.focus();
	        return;
	    }
	    if (name == $.cookie('chat_nickname')) {
		    nickname_error.text("你本来就叫这个。");
		    nickname_error.show();
	    }
	    uname=name;
	    socket.emit("newuser", uname);
	    $('#my-nickname').html('昵称：' + uname);
	    $('#login-modal').modal('hide');
	}
	function removeListUser(_user) {
	    $(".list-table tr").each(function () {
	        if (_user == $(this).find('td').text()) {
	            $(this).remove();
	        }
	    });
	}
	function addUserToList(_user) {
	    $(".list-table").append('<tr><td>' + _user + '</td></tr>');
	}
	function useUserList(_user_list) {
	    $(".list-table").html("");
	    for (var i = 0; i < _user_list.length; i++) {
	        addUserToList(_user_list[i]);
	    }
	    updateListCount();
	}

	function updateListCount() {
	    var list_count = $('.list-table').find('tr').length + 1;
	    $('#list-count').text("当前在线：" + list_count + "人");
	}
	// $('#my-nickname').html('昵称：' + uname);  
	$(document).ready(function() {
			 $('#my-nickname').html('昵称：' + uname);  
			// alert("gg");http://www.91mydoor.com:3000/  192.168.0.188
			socket = io.connect('http://192.168.0.188:3000/');
			var json = {};
			json.userid = "ddd";
			json.username ="ada";
			socket.emit("login", json);
			socket.emit("newuser", uname);
			socket.on("login", function(json) {
				var info = "<div>" + json.username + Math.random() + "</div>";
				$("#info").append(info);
			});
			socket.on("privatemessage", function(from,to,msg) {

				
				  var timestring=getLocalHMS();
				  var msg_list = $(".msg-list-body");
				    msg_list.append(
				            '<div class="clearfix msg-wrap"><div class="msg-head">' +
				            '<span class="msg-name label label-primary pull-left">' +
				            '<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;' + from+"给"+to+ '</span>' +
				            '<span class="msg-time label label-default pull-left">' +
				            '<span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;' +timestring+ '</span>' +
				            '</div><div class="msg-content">' +msg  + '</div></div>'
				    );
				    chatBodyToBottom();
				   
			});
			socket.on("message", function(json) {

				
				  var timestring=getLocalHMS();
				  var msg_list = $(".msg-list-body");
				    msg_list.append(
				            '<div class="clearfix msg-wrap"><div class="msg-head">' +
				            '<span class="msg-name label label-primary pull-left">' +
				            '<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;' + json.username + '</span>' +
				            '<span class="msg-time label label-default pull-left">' +
				            '<span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;' +timestring+ '</span>' +
				            '</div><div class="msg-content">' + json.content  + '</div></div>'
				    );
				    chatBodyToBottom();
				   
			});
			socket.on('need_nickname', function () {
				socket.emit('change_nickname', uname);
			});
			$("#bnt1").click(function() {

				var json = {};
				json.userid = "ddd";
				json.username = $("#username").val();

				socket.emit("login", json);
			});
			$("#msgbnt").click(function() {

				sendmsg();
			});
			socket.on('server_message', function (_message) {
			    addServerMessage( _message);
			});
		});
	function sendmsg(){
		var json = {};
		json.username = uname;
		json.content = $("#msg").val();
		 if ("" == 	json.content) {
		        return;
		 }
		//socket.emit("SendMessage", json);
		socket.emit("privatemessage",uname,toname,json.content);
		 $("#msg").val("");
	}
	
	Mousetrap.bind('enter', function() {
		//alert("gg");
		sendmsg();
	});
	Mousetrap.bind('a', function(e) {
	   // alert("gg");
	});
	</script>
</body>
</html>