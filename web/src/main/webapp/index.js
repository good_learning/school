﻿
var http = require('http');
var io = require('socket.io');
var app = http.createServer();
app.listen(3000, function () {
    console.log('listening on *:3000');
});
//在线用户 
var onlineUsers = {};
//当前在线人数 
var onlineCount = 0;

var io = io.listen(app);
var users = {};


//当前在线人数
var nickname_list = [];

function HasNickname(_nickname) {
    for (var i = 0; i < nickname_list.length; i++) {
        if (nickname_list[i] == _nickname) {
            return true;
        }
    }
    return false;
}

function RemoveNickname(_nickname) {
    for (var i = 0; i < nickname_list.length; i++) {
        if (nickname_list[i] == _nickname)
            nickname_list.splice(i, 1);
    }
}
function RemoveSocket(_nickname) {
   delete users[_nickname];
}
io.sockets.on('connection', function (socket) {
	socket.emit('user_list', nickname_list);
	socket.emit('need_nickname');
	socket.emit('server_message', '聊天系统加载成功~<br/>');
    console.log('a user connected**********************');
    //监听新用户加入     
    socket.on('login', function (obj) {

        //将新加入用户的唯一标识当作socket的名称，后面退出的时候会用到         
        socket.name = obj.userid;
        //检查在线列表，如果不在里面就加入         
        if (!onlineUsers.hasOwnProperty(obj.userid)) {
            onlineUsers[obj.userid] = obj.username;
            //在线人数+1             
            onlineCount++;
        }
        //向所有客户端广播用户加入         
        console.log(obj.username + '加入了聊天室');
        socket.broadcast.emit('login', obj);
        socket.emit('login', obj);

    });
    //监听客户发送信息     
    socket.on('SendMessage', function (obj) {
        //向所有客户端广播发布的消息
        console.log(obj.username + '说：' + obj.content);
        socket.broadcast.emit('message', obj);
        socket.emit('message', obj);
    });
    //监听客户发送信息     
    socket.on('msg', function (obj) {
        //向所有客户端广播发布的消息
        socket.broadcast.emit('msg', obj);
        socket.emit('msg', obj);
    });
    //监听客户发送回复    
    socket.on('SendReplay', function (repobj) {
        //向所有客户端广播发布的消息
        console.log(repobj.username + '说：' + repobj.content);
        socket.broadcast.emit('Replaymessage', repobj);
        socket.emit('Replaymessage', repobj);
    });

    //监听客户发送的提问    
    socket.on('SendAskMessage', function (askobj) {
        //向所有客户端广播发布的消息
        console.log(askobj.username + '说：' + askobj.content);
        socket.broadcast.emit('Askmessage', askobj);
        socket.emit('Askmessage', askobj);
    });
    io.sockets.emit('connect',{hell:'boy'});
    
    socket.on('privatemessage', function (from,to,msg) {
      console.log('I received a private message by ', from, ' say to ',to, msg);
      if(from===to){
    	  socket.emit('server_message','自己和自己聊天是不是很有意思~<br/>');
           return;
      }
      if(to in users){
         // users[to].emit('to'+to,{mess:msg});
          users[to].emit("privatemessage",from,to,msg);
          users[from].emit("privatemessage",from,to,msg);
      }else{
    	  socket.emit('server_message', to+'当前不在线,请稍候联系他~<br/>');
      }
    });
    socket.on('newuser',function(data){
       if(data in users){
    	   console.log(data + '已经加入了聊天室');
       }else{
          var nickname = data;
          users[nickname]= socket;
       }
       console.info(users);
    });
    
    socket.on('change_nickname', function (_nickname) {
        console.log(socket.id + ': change_nickname(' + _nickname + ')');
        var name_len = _nickname.replace(/[^\u0000-\u00ff]/g, "tt").length;
        if (name_len < 2 || name_len > 16) {
            return socket.emit('change_nickname_error', '请填写正确的昵称，应为4到16个字符。');
        }

        if (socket.nickname == _nickname) {
            return socket.emit('change_nickname_error', '你本来就叫这个。');
        }

        if (HasNickname(_nickname)) {
            return socket.emit('change_nickname_error', '此昵称已被人使用。');
        }
        var old_name = "";
        if (socket.nickname != "" && socket.nickname != null) {
            old_name = socket.nickname;
            RemoveNickname(old_name);
        }

        nickname_list.push(_nickname);
        socket.nickname = _nickname;

        console.log(nickname_list);

        socket.emit('change_nickname_done', old_name, _nickname);
        if (old_name == "") {
            return socket.broadcast.emit('user_join', _nickname)
        } else {
            return socket.broadcast.emit('user_change_nickname', old_name, _nickname)
        }
    });
    socket.on('disconnect', function () {
      console.log(socket.id + ': disconnect');
      if (socket.nickname != null && socket.nickname != "") {
    	  socket.broadcast.emit('user_quit', socket.nickname);
          RemoveNickname(socket.nickname);
          RemoveSocket(socket.nickname);
      }
    });
 


});
